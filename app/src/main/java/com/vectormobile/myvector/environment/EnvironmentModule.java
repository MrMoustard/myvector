package com.vectormobile.myvector.environment;

import com.google.gson.Gson;

import android.content.Context;
import android.support.annotation.NonNull;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.BuildConfig;
import com.vectormobile.myvector.di.ForApplication;
import com.vectormobile.myvector.domain.service.ServiceManager;
import com.vectormobile.myvector.network.OkHttpInterceptors;
import com.vectormobile.myvector.network.OkHttpNetworkInterceptors;
import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import java.io.File;
import java.security.cert.CertificateException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * VectorDigital.
 *
 * @author -   AMarinaG
 * @since -   29/10/16
 */
@Module
@Singleton
public class EnvironmentModule {

  static final int DISK_CACHE_SIZE = (int) 1_000_000L;
  public static final String TRENDS = "Trends";
  public static final String GMS = "gms";
  public static final String INTRANET_JSON = "intranet_json";
  public static final String INTRANET_XML = "intranet_xml";
  public static final String BOTFRAMEWORK = "bot_framework";

  private final App app;

  public EnvironmentModule(App app) {
    this.app = app;
  }

  @Provides
  public OkHttpClient provideOkHttpClient(@ForApplication Context app,
                                          @OkHttpInterceptors @NonNull List<Interceptor> interceptors,
                                          @OkHttpNetworkInterceptors @NonNull List<Interceptor> networkInterceptors) {

    final OkHttpClient.Builder okHttpBuilder;

    try {
      final TrustManager[] trustAllCerts = new TrustManager[]{
          new X509TrustManager() {
            @Override
            public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
            }

            @Override
            public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
            }

            @Override
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
              return new java.security.cert.X509Certificate[]{};
            }
          }
      };

      final SSLContext sslContext = SSLContext.getInstance("SSL");
      sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
      final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

      File cacheDir = new File(app.getCacheDir(), "http");
      Cache cache = new Cache(cacheDir, DISK_CACHE_SIZE);

      okHttpBuilder = new OkHttpClient.Builder();

      for (Interceptor interceptor : interceptors) {
        okHttpBuilder.addNetworkInterceptor(interceptor);
      }

      for (Interceptor networkInterceptor : networkInterceptors) {
        okHttpBuilder.addNetworkInterceptor(networkInterceptor);
      }

      okHttpBuilder.sslSocketFactory(sslSocketFactory);
      okHttpBuilder.hostnameVerifier(new HostnameVerifier() {
        @Override
        public boolean verify(String hostname, SSLSession session) {
          return true;
        }
      });

      okHttpBuilder.cache(cache);
      okHttpBuilder.readTimeout(30, TimeUnit.SECONDS);
      okHttpBuilder.writeTimeout(30, TimeUnit.SECONDS);
      okHttpBuilder.connectTimeout(30, TimeUnit.SECONDS);

    } catch (Exception e) {
      throw new RuntimeException(e);
    }

    return okHttpBuilder.build();
  }

  @Provides
  @Named(TRENDS)
  Retrofit provideTrendsRestAdapter(@NonNull OkHttpClient okHttpClient, @NonNull Gson gson) {
    Retrofit restAdapter = new Retrofit.Builder()
        .baseUrl(BuildConfig.TRENDS_ENDPOINT)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build();
    return restAdapter;
  }

  @Provides
  @Named(GMS)
  Retrofit provideGmsRestAdapter(@NonNull OkHttpClient okHttpClient, @NonNull Gson gson) {
    Retrofit restAdapter = new Retrofit.Builder()
        .baseUrl(BuildConfig.GMS_ENDPOINT)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(okHttpClient)
        .build();
    return restAdapter;
  }

  @Provides
  @Named(INTRANET_JSON)
  Retrofit provideIntranetJsonAdapter(@NonNull OkHttpClient okHttpClient, @NonNull Gson gson) {
    Retrofit restAdapter = new Retrofit.Builder()
        .baseUrl(BuildConfig.INTRANET_ENDPOINT)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build();
    return restAdapter;
  }
  @Provides
  @Named(BOTFRAMEWORK)
  Retrofit provideBotFrameworkAdapter(@NonNull OkHttpClient okHttpClient, @NonNull Gson gson) {
    Retrofit restAdapter = new Retrofit.Builder()
        .baseUrl(ServiceManager.BF_BOT_BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build();
    return restAdapter;
  }

  @Provides
  @Named(INTRANET_XML)
  Retrofit provideIntranetXmlAdapter(@NonNull OkHttpClient okHttpClient) {
    Retrofit restAdapter = new Retrofit.Builder()
        .baseUrl(BuildConfig.INTRANET_ENDPOINT)
        .client(okHttpClient)
        .addConverterFactory(ScalarsConverterFactory.create())
        .build();
    return restAdapter;
  }

}
