package com.vectormobile.myvector.environment;

import com.facebook.stetho.Stetho;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.BuildConfig;
import com.vectormobile.myvector.di.ForApplication;
import com.vectormobile.myvector.util.logging.CrashReportingTree;
import timber.log.Timber;

import javax.inject.Inject;

/**
 * Created by amarina on 21/09/2016.
 */

public class EnvironmentConfiguration {

  @Inject
  @ForApplication
  App app;

  @Inject
  public EnvironmentConfiguration() {
  }

  public void configure() {
//    StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build());
//    StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyLog().build());
    Stetho.initializeWithDefaults(app);
    if (BuildConfig.DEBUG) {
      Timber.plant(new Timber.DebugTree());
    } else {
      Timber.plant(new CrashReportingTree());
    }
  }
}
