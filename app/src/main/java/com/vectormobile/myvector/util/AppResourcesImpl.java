package com.vectormobile.myvector.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import timber.log.Timber;

/**
 * Created by kurt on 24/02/17.
 */

public class AppResourcesImpl implements AppResources {

  private Context context;

  public AppResourcesImpl(Context context) {
    this.context = context;
  }

  @Override
  public String getString(int resourceId) {
    return context.getString(resourceId);
  }

  @Override
  public String getQuantityString(int resourceId, int quantity) {
    return context.getResources().getQuantityString(resourceId, quantity);
  }

  @Override
  public String getMetaData(String name) {
    try {
      ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
      Bundle bundle = ai.metaData;
      return bundle.getString(name);
    } catch (PackageManager.NameNotFoundException e) {
      Timber.w("Unable to load meta-data: " + e.getMessage());
    }
    return null;
  }
}
