package com.vectormobile.myvector.util.logging;

import android.util.Log;
import timber.log.Timber;

/**
 * Created by Erik Medina on 28/10/2016.
 */

public class CrashReportingTree extends Timber.Tree {
  @Override
  protected void log(int priority, String tag, String message, Throwable t) {
    if (priority == Log.VERBOSE || priority == Log.DEBUG) {
      return;
    }
  }
}