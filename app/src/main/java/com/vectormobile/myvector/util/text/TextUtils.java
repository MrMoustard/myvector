package com.vectormobile.myvector.util.text;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.util.date.DateUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by kurt on 13/12/16.
 */

public class TextUtils {

  public static void hideKeyboard(Activity activity, EditText editText) {
    InputMethodManager imm = (InputMethodManager) activity.getSystemService(
        Context.INPUT_METHOD_SERVICE);
    imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
  }

  public static void hideKeyboard(Activity activity) {
    InputMethodManager imm = (InputMethodManager) activity.getSystemService(
        Context.INPUT_METHOD_SERVICE);
    imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
  }

  public static String capitalizeFirstLetter(String text) {
    return text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase();
  }

  public static String trimStringUntilCharacter(String text, String character) {
    return text.substring(text.indexOf(character) + 1);
  }

  public static void changeTextColorItemSpinner(Context context, Spinner spinner) {
    TextView textView = ((TextView) spinner.getSelectedView());
    if (textView != null) {
      textView.setTextColor(ContextCompat.getColor(context, R.color.black));
    }
  }

  public static String formatStringDate(String dateToFormat, String pattern) {
    try {
      DateFormat formatter = new SimpleDateFormat(DateUtils.GMS_SHORT_DATE_FORMAT, Locale.getDefault());
      Date date = formatter.parse(dateToFormat);

      formatter = new SimpleDateFormat(pattern, Locale.getDefault());
      return formatter.format(date);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static boolean compareStrings(String first, String second) {
    return first.equals(second);
  }

  public static String extractStringBetweenCharacters(String content, String character) {
    return content.substring(content.indexOf(character) + 1, content.lastIndexOf(character));
  }

  public static String extractUntilCharacter(String content, String character) {
    return content.substring(0, content.indexOf(character));
  }

  public static String extractFromCharacter(String content, String character) {
    return content.substring(content.indexOf(character) + 1);
  }

  public static boolean checkStartWithNumber(String content) {
    if (content.length() > 0) {
      return Character.isDigit(content.charAt(0));
    } else {
      return false;
    }
  }

  public static String ellipsizeText(String input, int maxLength) {
    if (input == null || input.length() < maxLength) {
      return input;
    }
    return input.substring(0, maxLength) + "...";
  }

  public class Gms {

    public static final int CALENDAR_EXPENSE_TO = 1;
    public static final int CALENDAR_EXPENSE_FROM = 0;
    public static final String GMS_PRICE_PER_KILOMETER = "0.19";
    public static final String GMS_EMPTY_SEARCH = " ";

    public static final String IMPUTATION_SAVED = "updated";
    public static final String IMPUTATION_UPDATED = "updated";
    public static final String IMPUTATION_SENT = "sent";
    public static final String IMPUTATION_REMOVED = "removed";

    public static final String IMPUTATION_STATUS_SENT = "ENVIADO";
    public static final String IMPUTATION_STATUS_SAVED = "CREADO";
    public static final String IMPUTATION_STATUS_REJECTED = "RECHAZADO";
    public static final String IMPUTATION_STATUS_ACCEPTED = "ACEPTADO";

    public static final String IMPUTATION_INDIVIDUAL = "INDIVIDUAL";
    public static final String IMPUTATION_MASSIVE = "MASIVA";

    //action
    public static final String ACTION_IMPUTATION_HOUR_CONTROLLER = "imputacionHoraController";
    public static final String ACTION_IMPUTATION_EXPENSE_CONTROLLER = "imputacionGastoController";
    public static final String ACTION_COMBOS_CONTROLLER = "combosController";

    //method
    public static final String METHOD_CREATE = "create";
    public static final String METHOD_UPDATE = "update";
    public static final String METHOD_REMOVE = "remove";
    public static final String METHOD_SEND = "send";
    public static final String METHOD_LIST = "list";
    public static final String METHOD_IMPUTABLE_CATEGORIES_1 = "categorias1Imputables";
    public static final String METHOD_IMPUTABLE_CATEGORIES_2 = "categorias2Imputables";

    public static final String IMPUTABLE_PROJECTS = "proyectosImputables";
    public static final String IMPUTABLE_WORK_ORDER = "ordenesTrabajoImputables";
    public static final String IMPUTABLE_JIRA_CODES = "codigosJira";
    public static final String IMPUTABLE_SEARCH_JIRA_CODES = "searchIssues";

    //GmsNotification
    public static final String NOTIFICATION_TYPE = "rpc";
    public static final String NOTIFICATION_ACTION_INBOX_CONTROLLER = "inboxController";
    public static final String NOTIFICATION_CHECK_CONTROLLER = "notificationCheckController";
    public static final String NOTIFICATION_METHOD_LIST_ALL_ACTIVE_AS_OPTION = "listAllActiveAsOption";
    public static final String NOTIFICATION_METHOD_CHECK = "check";
    public static final String NOTIFICATION_METHOD_LIST = "lista";
    public static final String NOTIFICATION_TYPE_STATE = "com.vectorsf.ngms.persistence.model.EstadoNotificacion";
    public static final String NOTIFICATION_GET_NEW = "getNuevasNotificaciones";
  }

  public class Intranet {

    public static final String LOGIN_ACTION = "ivp_plugin_ldap_login";
    public static final String DEFAULT_USER_PROFILE_IMAGE = "/assets/images/user.png";
    public static final String DEFAULT_AVATAR_FILE_NAME = "avatar.png";
    public static final String DEFAULT_AVATAR_CONTACTS_FILE_NAME = "user.png";
    //action
    public static final String ACTION_SAVE_USER_INFO = "ivp_ajax_save_user_info_attr";
    public static final String ACTION_NEWS_SEARCH_POST = "ivp_ajax_search_posts";
    public static final String ACTION_SAVE_IMAGE_USER = "ivp_ajax_request_info_employee_save_image_user";
    public static final String ACTION_CONTACTS_SEARCH_DIRECTORY = "ivp_plugin_ldap_search_directory";
    //attributes
    public static final String ATTRIBUTE_CELLPHONE_SAVE_USER_INFO = "cellphone";
    public static final String ATTRIBUTE_PHONE_SAVE_USER_INFO = "phone";
    //category
    public static final String NEWS_CATEGORY = "19";
    public static final String NEWS_PER_PAGE = "30";
    //Contacts
    public static final String CONTACTS_SHOW = "true";
    public static final String CONTACTS_DEPARTMENTS = "*";
    public static final String CONTACTS_CITIES = "*";
    public static final String SEARCH_FORMAT = "*";
    public static final String CONTACTS_EMAIL = "@";
    public static final String CONTACTS_WHITE_SPACE = " ";
    public static final String CONTACTS_MULTIPLE_WHITE_SPACE = " +";
    public static final String CONTACTS_COUNTRY_CODE_PLUS = "+";
    public static final String CONTACTS_EMAIL_SCHEME = "mailto";
    public static final String CONTACTS_EMAIL_INTENT_TITLE = "Send email...";
    //Parser
    public static final String ELEMENT_SEPARATOR = "*";
    //Image
    public static final String GOOGLE_PHOTOS_URL = "content://com.google.android.apps.photos.content";


  }

}
