package com.vectormobile.myvector.util.date;

import com.vectormobile.myvector.util.text.TextUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by kurt on 12/12/16.
 */

public class DateUtils {

  private static final long SIX_DAYS_IN_MILLISECONDS = 518400000L;
  private static final int MONTH_ARRAY_POSITION = 0;
  private static final int YEAR_ARRAY_POSITION = 1;

  public static final int WEEK_BOUND_STARTS = 0;
  public static final int WEEK_BOUND_ENDS = 1;

  public static final String CALENDAR_DATE_FORMAT = "EEEE, dd 'de' MMMM 'de' yyyy";
  public static final String GMS_SHORT_DATE_FORMAT = "yyyy-MM-dd";
  public static final String GMS_STATE_DATE_FORMAT = "yyyy-MM-dd'T'hh:mm:ss";
  public static final String NO_ITEMS_GMS_DATE_FORMAT = "dd/MM/yyyy";
  public static final String MONTH_DATE_FORMAT = "MMMM yyyy";

  public static List<String> getStringWeekBounds(Date date, String pattern) {
    DateFormat formatter = new SimpleDateFormat(pattern, Locale.getDefault());
    List<String> weekBounds = new ArrayList<>();
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
    weekBounds.add(formatter.format(calendar.getTime()));
    calendar.setTimeInMillis(calendar.getTimeInMillis() + SIX_DAYS_IN_MILLISECONDS);
    weekBounds.add(formatter.format(calendar.getTime()));
    return weekBounds;
  }

  public static void setToMidnight(Calendar calendar) {
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
  }

  public static String formatDateToCompactString(Date date, String pattern) {
    DateFormat formatter = new SimpleDateFormat(pattern, Locale.getDefault());
    return formatter.format(date);
  }

  public static Date parseStringToDate(String sDate, String pattern) {
    SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, Locale.getDefault());
    Date date = null;
    try {
      date = dateFormat.parse(sDate);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return date;
  }

  public static String formatDateToLargeString(Date date) {
    DateFormat formatter = new SimpleDateFormat(CALENDAR_DATE_FORMAT, Locale.getDefault());
    return formatter.format(date);
  }

  public static String formatDateToGmsLargeString(Date date) {
    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
    return formatter.format(date);
  }

  public static String formatCalendarDateToGmsShortDate(String dateString) {
    DateFormat fromFormat = new SimpleDateFormat(CALENDAR_DATE_FORMAT, Locale.getDefault());
    fromFormat.setLenient(false);
    DateFormat toFormat = new SimpleDateFormat(GMS_SHORT_DATE_FORMAT, Locale.getDefault());
    toFormat.setLenient(false);
    try {
      Date date = fromFormat.parse(dateString);
      return toFormat.format(date);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static Calendar convertStringToCalendar(String dateString, String pattern) {
    if (dateString == null || dateString.equals(""))
      return null;

    Calendar cal = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.getDefault());
    try {
      cal.setTime(sdf.parse(dateString));
      return cal;
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static String getCalendarFormattedCurrentDate() {
    Calendar calendar = Calendar.getInstance();
    return TextUtils.capitalizeFirstLetter(formatDateToLargeString(calendar.getTime()));
  }

  public static Date convertMillisecondsToDate(long milliSeconds) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(milliSeconds);
    return calendar.getTime();
  }

  private static int convertMonthStringToInt(String month) {
    Calendar cal = Calendar.getInstance();
    try {
      cal.setTime(new SimpleDateFormat("MMM", Locale.getDefault()).parse(month));
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return cal.get(Calendar.MONTH) + 1;
  }

  public static List<String> buildMonthBounds(String month) {
    // --- Enero 2017 ---
    String[] monthAndYear = month.split(" ");
    int monthInt = convertMonthStringToInt(monthAndYear[MONTH_ARRAY_POSITION]);

    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.MONTH, monthInt - 1);
    calendar.set(Calendar.YEAR, Integer.parseInt(monthAndYear[YEAR_ARRAY_POSITION]));
    int numDays = calendar.getActualMaximum(Calendar.DATE);

    List<String> monthBounds = new ArrayList<>();
    monthBounds.add(String.format(Locale.getDefault(), "%1$s-%2$s-%3$s",
        calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, "1"));
    monthBounds.add(String.format(Locale.getDefault(), "%1$s-%2$s-%3$s",
        calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, numDays));
    return monthBounds;
  }

  public static Calendar getTypedDate(String dateOneString, String dateTwoString) {
    Calendar calendar;

    calendar = convertStringToCalendar(dateOneString, CALENDAR_DATE_FORMAT);
    if (calendar == null) {
      calendar = convertStringToCalendar(dateTwoString, CALENDAR_DATE_FORMAT);
    }
    return (calendar != null) ? calendar : Calendar.getInstance();
  }

  public static String getExpensePdfDefaultStartDate() {
    return getExpensePdfDefaultStartDate(Calendar.getInstance());
  }

  public static String getExpensePdfDefaultStartDate(Calendar calendar) {
    int FIRST_DAY_OF_THE_MONTH = 1;
    int SECOND_FORTNIGHT_START_DAY = 15;

    if ((calendar.get(Calendar.DAY_OF_MONTH)) == 1) {
      calendar.set(Calendar.DAY_OF_MONTH, SECOND_FORTNIGHT_START_DAY);
      calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
    } else if (calendar.get(Calendar.DAY_OF_MONTH) > 1 && calendar.get(Calendar.DAY_OF_MONTH) <= 15) {
      calendar.set(Calendar.DAY_OF_MONTH, FIRST_DAY_OF_THE_MONTH);
    } else {
      calendar.set(Calendar.DAY_OF_MONTH, SECOND_FORTNIGHT_START_DAY);
    }
    return formatDateToCompactString(calendar.getTime(), CALENDAR_DATE_FORMAT);
  }
}
