package com.vectormobile.myvector.util.widget;

/**
 * Created by amarina on 22/03/2017.
 */

public interface CustomIncomingMessageClickListener {
  void buttonClick(String value);
}
