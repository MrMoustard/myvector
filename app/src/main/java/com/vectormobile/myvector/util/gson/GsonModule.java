package com.vectormobile.myvector.util.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ajruiz on 16/11/2016.
 */

@Module
public class GsonModule {

  @Provides
  public GsonBuilder provideDefaultGsonBuilder() {
    GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.setLenient();
    gsonBuilder.setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

    return gsonBuilder;
  }

  @Provides
  public Gson provideGson(GsonBuilder gsonBuilder) {
    return gsonBuilder.create();
  }

}
