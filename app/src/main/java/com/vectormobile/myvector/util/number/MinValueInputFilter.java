package com.vectormobile.myvector.util.number;

import android.text.InputFilter;
import android.text.Spanned;

/**
 * Created by Erik Medina on 30/01/2017.
 */

public class MinValueInputFilter implements InputFilter {

  private int min;

  public MinValueInputFilter(int min) {
    this.min = min;
  }

  public MinValueInputFilter(String min) {
    this.min = Integer.parseInt(min);
  }

  @Override
  public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
    try {
      int input = Integer.parseInt(dest.toString() + source.toString());
      if (isInRange(min, input))
        return null;
    } catch (NumberFormatException nfe) {
    }
    return "";
  }

  private boolean isInRange(int minValue, int input) {
    return input >= minValue;
  }
}
