package com.vectormobile.myvector.util.widget;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import com.stfalcon.chatkit.messages.MessagesListAdapter;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.domain.entity.bot.BFButton;
import com.vectormobile.myvector.model.bot.Message;

import java.util.List;

/**
 * Created by amarina on 22/03/2017.
 */

public class CustomIncomingMessageViewHolder extends MessagesListAdapter.IncomingMessageViewHolder<Message> {
  private LinearLayout buttonsContent;
  private LayoutInflater layoutInflater;

  public CustomIncomingMessageViewHolder(View itemView) {
    super(itemView);
    buttonsContent = (LinearLayout) itemView.findViewById(R.id.buttons_content);
    layoutInflater = LayoutInflater.from(itemView.getContext());
  }

  @Override
  public void onBind(Message message) {
    super.onBind(message);
    buttonsContent.removeAllViewsInLayout();
    if (message.getButtons() != null) {
      List<BFButton> buttons = message.getButtons();
      for (final BFButton bfButton : buttons) {
        Button button = (Button) layoutInflater.inflate(R.layout.custom_action_chat_button, buttonsContent, false);
        button.setText(bfButton.getTitle());
        button.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            ((CustomIncomingMessageClickListener) itemView.getContext()).buttonClick(bfButton.getValue());
          }
        });
        buttonsContent.addView(button);
      }
      buttonsContent.setVisibility(View.VISIBLE);
    } else {
      buttonsContent.removeAllViewsInLayout();
      buttonsContent.setVisibility(View.GONE);
    }
  }
}
