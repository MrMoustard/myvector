package com.vectormobile.myvector.util;

/**
 * Created by kurt on 24/02/17.
 */

public interface AppResources {

  String getString(int resourceId);

  String getQuantityString(int resourceId, int quantity);

  String getMetaData(String name);
}
