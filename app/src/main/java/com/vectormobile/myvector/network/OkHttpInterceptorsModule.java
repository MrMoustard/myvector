package com.vectormobile.myvector.network;

import android.support.annotation.NonNull;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.di.ForApplication;
import com.vectormobile.myvector.domain.interceptor.AddCookiesInterceptor;
import com.vectormobile.myvector.domain.interceptor.ReceivedCookiesInterceptor;
import com.vectormobile.myvector.domain.interceptor.SessionAliveInterceptor;
import com.vectormobile.myvector.storage.SessionManager;
import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.logging.HttpLoggingInterceptor;
import timber.log.Timber;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;
import javax.inject.Singleton;

/**
 * Created by amarina on 16/11/2016.
 */
@Module
@Singleton
public class OkHttpInterceptorsModule {
  @Provides
  @NonNull
  public HttpLoggingInterceptor provideHttpLoggingInterceptor() {
    HttpLoggingInterceptor logging = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
      @Override
      public void log(String message) {
        Timber.d(message);
      }
    });
    logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
    return logging;
  }

  @Provides
  @NonNull
  public ReceivedCookiesInterceptor provideReceivedCookiesInterceptor(SessionManager sessionManager) {
    return new ReceivedCookiesInterceptor(sessionManager);
  }

  @Provides
  @NonNull
  public AddCookiesInterceptor provideAddCookiesInterceptor(SessionManager sessionManager) {
    return new AddCookiesInterceptor(sessionManager);
  }

  @Provides
  @Nonnull
  public SessionAliveInterceptor provideSessionAliveInterceptor(SessionManager sessionManager, @ForApplication App application) {
    return new SessionAliveInterceptor(sessionManager, application);
  }

  @Provides
  @OkHttpInterceptors
  @NonNull
  public List<Interceptor> provideOkHttpInterceptors(@NonNull HttpLoggingInterceptor httpLoggingInterceptor) {
    List<Interceptor> _return = new ArrayList<>();
    _return.add(httpLoggingInterceptor);
    return _return;
  }

  @Provides
  @OkHttpNetworkInterceptors
  @NonNull
  public List<Interceptor> provideOkHttpNetworkInterceptors(
      @NonNull ReceivedCookiesInterceptor receivedCookiesInterceptor,
      @NonNull AddCookiesInterceptor addCookiesInterceptor,
      @NonNull SessionAliveInterceptor sessionAliveInterceptor
  ) {
    List<Interceptor> _return = new ArrayList<>();
    _return.add(new StethoInterceptor());
    _return.add(receivedCookiesInterceptor);
    _return.add(addCookiesInterceptor);
    _return.add(sessionAliveInterceptor);
    return _return;
  }

}
