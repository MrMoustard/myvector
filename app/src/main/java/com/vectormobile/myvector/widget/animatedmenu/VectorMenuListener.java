package com.vectormobile.myvector.widget.animatedmenu;

import com.vectormobile.myvector.model.VectorItemMenu;

/**
 * VectorDigital.
 *
 * @author -   AMarinaG
 * @since -   7/11/16
 */
public interface VectorMenuListener {
  void onItemMenuClick(VectorItemMenu itemMenu);
}
