package com.vectormobile.myvector.widget.animatedmenu;

import static com.vectormobile.myvector.R.id.menu_vector_label;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.model.VectorItemMenu;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by ajruiz on 10/11/2016.
 */

public class AnimatedMenuAdapter extends RecyclerView.Adapter<AnimatedMenuAdapter.AnimatedMenuViewHolder> {

  private static final int ROW_HEIGHT_NORMAL = 56;
  public static final int DURATION_ANIMATION_MILLIS = 400;
  private static int normalRowHeightInPixels;

  private Context context;
  private RelativeLayout container;
  private List<VectorItemMenu> items;
  private VectorMenuListener listener;

  private int headerRowHeightInPixels;
  private float scale;
  private int headerItem;
  private boolean scrollAvailable = true;

  public boolean isScrollAvailable() {
    return scrollAvailable;
  }

  @Inject
  public AnimatedMenuAdapter(Context context, RelativeLayout container) {
    this.context = context;
    this.container = container;
    items = new ArrayList<>();
    scale = context.getResources().getDisplayMetrics().density;
    normalRowHeightInPixels = (int) (ROW_HEIGHT_NORMAL * scale + 0.5F);
  }

  @Override
  public AnimatedMenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(context);
    View view = inflater.inflate(R.layout.item_vector_menu, parent, false);
    return new AnimatedMenuViewHolder(view);
  }

  @Override
  public void onBindViewHolder(AnimatedMenuViewHolder holder, int position) {
    final VectorItemMenu itemMenu = items.get(position % items.size());

    holder.menuVectorLabel.setText(itemMenu.getLabel());
    holder.menuVectorImage.setImageResource(itemMenu.getBgImage());
    holder.menuVectorRoot.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (listener != null) {
          listener.onItemMenuClick(itemMenu);
        }
      }
    });
    if (position == headerItem) {
      setHeightOfHeaderRow(holder, position);
    } else {
      holder.menuVectorLabel.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
      holder.viewShadow.setAlpha(0.8F);
      holder.menuVectorRoot.setLayoutParams(
          new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, normalRowHeightInPixels));
    }
  }

  private void setHeightOfHeaderRow(AnimatedMenuViewHolder holder, int position) {
    headerRowHeightInPixels = container.getHeight() - (normalRowHeightInPixels * (items.size() - 1));
    holder.menuVectorRoot.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, headerRowHeightInPixels));
    holder.menuVectorLabel.setTextSize(TypedValue.COMPLEX_UNIT_SP, 33);
    holder.viewShadow.setAlpha(0);
  }

  @Override
  public int getItemViewType(int position) {
    return super.getItemViewType(position);
  }

  public void scrollDownAnimation(final RecyclerView recyclerView) {
    scrollAvailable = false;
    final LinearLayoutManager llm = (LinearLayoutManager) recyclerView.getLayoutManager();

    final int firstItemPosition = llm.findFirstVisibleItemPosition();

    final int firstItemSize = llm.findViewByPosition(firstItemPosition).getHeight();
    int secondItemYPosition = (int) llm.findViewByPosition(firstItemPosition + 1).getY();
    final ViewGroup.LayoutParams paramsSecondItem = llm.findViewByPosition(firstItemPosition + 1).getLayoutParams();
    final ViewGroup.LayoutParams params = paramsSecondItem;


    if (areSystemAnimationsEnabled()) {
      ValueAnimator scrollDownAnimator = new ValueAnimator();
      scrollDownAnimator.setObjectValues(secondItemYPosition, secondItemYPosition - firstItemSize);
      scrollDownAnimator.setDuration(DURATION_ANIMATION_MILLIS);
      scrollDownAnimator.setInterpolator(new DecelerateInterpolator());
      scrollDownAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
        public void onAnimationUpdate(ValueAnimator animation) {
          llm.scrollToPositionWithOffset(firstItemPosition + 1, (int) animation.getAnimatedValue());
        }
      });

      ValueAnimator increaseSizeSecondItemAnimator = new ValueAnimator();
      increaseSizeSecondItemAnimator.setObjectValues(normalRowHeightInPixels, headerRowHeightInPixels);
      increaseSizeSecondItemAnimator.setDuration(DURATION_ANIMATION_MILLIS);
      increaseSizeSecondItemAnimator.setInterpolator(new DecelerateInterpolator());
      increaseSizeSecondItemAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
        public void onAnimationUpdate(ValueAnimator animation) {
          params.height = (int) animation.getAnimatedValue();
          llm.findViewByPosition(firstItemPosition + 1).setLayoutParams(params);

        }
      });

      ValueAnimator increaseSizeTextSecondItemAnimator = new ValueAnimator();
      increaseSizeTextSecondItemAnimator.setObjectValues(18, 33);
      increaseSizeTextSecondItemAnimator.setDuration(DURATION_ANIMATION_MILLIS - 150);
      increaseSizeTextSecondItemAnimator.setInterpolator(new AccelerateInterpolator());
      increaseSizeTextSecondItemAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
        public void onAnimationUpdate(ValueAnimator animation) {
          ((TextView) llm.findViewByPosition(firstItemPosition + 1).findViewById(R.id.menu_vector_label)).setTextSize((int) animation.getAnimatedValue());
        }
      });

      ValueAnimator alphaSecondItemAnimator = new ValueAnimator();
      alphaSecondItemAnimator.setObjectValues(0.8F, 0F);
      alphaSecondItemAnimator.setDuration(DURATION_ANIMATION_MILLIS);
      alphaSecondItemAnimator.setInterpolator(new DecelerateInterpolator());
      alphaSecondItemAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
        public void onAnimationUpdate(ValueAnimator animation) {
          llm.findViewByPosition(firstItemPosition + 1).findViewById(R.id.view_shadow).setAlpha((Float) animation.getAnimatedValue());
        }
      });

      AnimatorSet completeAnimator = new AnimatorSet();
      completeAnimator.play(increaseSizeSecondItemAnimator).with(scrollDownAnimator);
      completeAnimator.play(scrollDownAnimator).with(increaseSizeTextSecondItemAnimator);
      completeAnimator.play(increaseSizeTextSecondItemAnimator).with(alphaSecondItemAnimator);

      completeAnimator.addListener(new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animator) {
        }

        @Override
        public void onAnimationEnd(Animator animator) {
          scrollAvailable = true;
        }

        @Override
        public void onAnimationCancel(Animator animator) {
        }

        @Override
        public void onAnimationRepeat(Animator animator) {
        }
      });
      completeAnimator.start();
    }
  }

  public void scrollUpAnimation(final RecyclerView recyclerView) {
    final LinearLayoutManager llm = (LinearLayoutManager) recyclerView.getLayoutManager();
    scrollAvailable = false;

    final int firstItemPosition = llm.findFirstVisibleItemPosition();
    final View firstItemView = llm.findViewByPosition(firstItemPosition);
    final ViewGroup.LayoutParams paramsSecondItem = llm.findViewByPosition(firstItemPosition + 1).getLayoutParams();

    if (areSystemAnimationsEnabled()) {
      final ValueAnimator scrollAnimator = new ValueAnimator();
      scrollAnimator.setObjectValues((int) firstItemView.getY(), (int) firstItemView.getY() + paramsSecondItem.height);
      scrollAnimator.setDuration((long) (DURATION_ANIMATION_MILLIS / 2));
      scrollAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
        public void onAnimationUpdate(ValueAnimator animation) {
          llm.scrollToPositionWithOffset(firstItemPosition, (int) animation.getAnimatedValue());
        }
      });

      scrollAnimator.addListener(new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animator) {
        }

        @Override
        public void onAnimationEnd(Animator animator) {
          continueScrollUpAnimation(recyclerView);
        }

        @Override
        public void onAnimationCancel(Animator animator) {
        }

        @Override
        public void onAnimationRepeat(Animator animator) {
        }
      });
      scrollAnimator.setInterpolator(new AccelerateInterpolator());
      scrollAnimator.start();
    }
  }

  public void continueScrollUpAnimation(RecyclerView recyclerView) {
    final LinearLayoutManager llm = (LinearLayoutManager) recyclerView.getLayoutManager();
    final int firstItemPosition = llm.findFirstVisibleItemPosition();
    final ViewGroup.LayoutParams secondItemParams = llm.findViewByPosition(firstItemPosition + 1).getLayoutParams();
    final ViewGroup.LayoutParams firstItemParams = llm.findViewByPosition(firstItemPosition).getLayoutParams();

    ValueAnimator increaseSizeFirstItemAnimator = new ValueAnimator();
    increaseSizeFirstItemAnimator.setObjectValues(normalRowHeightInPixels, headerRowHeightInPixels);
    increaseSizeFirstItemAnimator.setDuration(DURATION_ANIMATION_MILLIS);
    increaseSizeFirstItemAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
      public void onAnimationUpdate(ValueAnimator animation) {
        firstItemParams.height = (int) animation.getAnimatedValue();
        llm.findViewByPosition(llm.findFirstVisibleItemPosition()).setLayoutParams(firstItemParams);
      }
    });

    ValueAnimator decreaseSizeSecondItemAnimator = new ValueAnimator();
    decreaseSizeSecondItemAnimator.setObjectValues(headerRowHeightInPixels, normalRowHeightInPixels);
    decreaseSizeSecondItemAnimator.setDuration(DURATION_ANIMATION_MILLIS);
    decreaseSizeSecondItemAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
      public void onAnimationUpdate(ValueAnimator animation) {
        secondItemParams.height = (int) animation.getAnimatedValue();
        llm.findViewByPosition(llm.findFirstVisibleItemPosition() + 1).setLayoutParams(secondItemParams);
      }
    });

    ValueAnimator increaseTextSizeFirstItemAnimator = new ValueAnimator();
    increaseTextSizeFirstItemAnimator.setObjectValues(18, 33);
    increaseTextSizeFirstItemAnimator.setDuration(DURATION_ANIMATION_MILLIS - 250);
    increaseTextSizeFirstItemAnimator.setInterpolator(new AccelerateInterpolator());
    increaseTextSizeFirstItemAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
      public void onAnimationUpdate(ValueAnimator animation) {
        ((TextView) llm.findViewByPosition(llm.findFirstVisibleItemPosition())
            .findViewById(R.id.menu_vector_label)).setTextSize((int) animation.getAnimatedValue());
      }
    });

    ValueAnimator decreaseTextSizeSecondItemAnimator = new ValueAnimator();
    decreaseTextSizeSecondItemAnimator.setObjectValues(33, 18);
    decreaseTextSizeSecondItemAnimator.setDuration(DURATION_ANIMATION_MILLIS - 250);
    decreaseTextSizeSecondItemAnimator.setInterpolator(new AccelerateInterpolator());
    decreaseTextSizeSecondItemAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
      public void onAnimationUpdate(ValueAnimator animation) {
        ((TextView) llm.findViewByPosition(llm.findFirstVisibleItemPosition() + 1)
            .findViewById(R.id.menu_vector_label)).setTextSize((int) animation.getAnimatedValue());
      }
    });

    ValueAnimator alphaFirstItemAnimator = new ValueAnimator();
    alphaFirstItemAnimator.setObjectValues(0.8F, 0F);
    alphaFirstItemAnimator.setDuration(DURATION_ANIMATION_MILLIS);
    alphaFirstItemAnimator.setInterpolator(new DecelerateInterpolator());
    alphaFirstItemAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
      public void onAnimationUpdate(ValueAnimator animation) {
        (llm.findViewByPosition(llm.findFirstVisibleItemPosition())
            .findViewById(R.id.view_shadow)).setAlpha((Float) animation.getAnimatedValue());
      }
    });

    ValueAnimator alphaSecondItemAnimator = new ValueAnimator();
    alphaSecondItemAnimator.setObjectValues(0F, 0.8F);
    alphaSecondItemAnimator.setDuration(DURATION_ANIMATION_MILLIS);
    alphaSecondItemAnimator.setInterpolator(new DecelerateInterpolator());
    alphaSecondItemAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
      public void onAnimationUpdate(ValueAnimator animation) {
        (llm.findViewByPosition(llm.findFirstVisibleItemPosition() + 1)
            .findViewById(R.id.view_shadow)).setAlpha((Float) animation.getAnimatedValue());
      }
    });

    AnimatorSet animatorSet = new AnimatorSet();
    animatorSet.play(decreaseSizeSecondItemAnimator).with(increaseSizeFirstItemAnimator);
    animatorSet.play(decreaseSizeSecondItemAnimator).with(increaseTextSizeFirstItemAnimator);
    animatorSet.play(increaseTextSizeFirstItemAnimator).with(decreaseTextSizeSecondItemAnimator);
    animatorSet.play(decreaseTextSizeSecondItemAnimator).with(alphaFirstItemAnimator);
    animatorSet.play(alphaFirstItemAnimator).with(alphaSecondItemAnimator);
    animatorSet.setInterpolator(new DecelerateInterpolator());
    animatorSet.addListener(new Animator.AnimatorListener() {
      @Override
      public void onAnimationStart(Animator animator) {
      }

      @Override
      public void onAnimationEnd(Animator animator) {
        scrollAvailable = true;
      }

      @Override
      public void onAnimationCancel(Animator animator) {
      }

      @Override
      public void onAnimationRepeat(Animator animator) {
      }
    });
    animatorSet.start();
  }

  private boolean areSystemAnimationsEnabled() {
    float duration, transition;
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
      duration = Settings.Global.getFloat(context.getContentResolver(), Settings.Global.ANIMATOR_DURATION_SCALE, 1);
      transition = Settings.Global.getFloat(context.getContentResolver(), Settings.Global.TRANSITION_ANIMATION_SCALE, 1);
    } else {
      duration = Settings.System.getFloat(context.getContentResolver(), Settings.System.ANIMATOR_DURATION_SCALE, 1);
      transition = Settings.System.getFloat(context.getContentResolver(), Settings.System.TRANSITION_ANIMATION_SCALE, 1);
    }
    return (duration != 0 && transition != 0);
  }

  public void setListener(VectorMenuListener listener) {
    this.listener = listener;
  }

  @Override
  public void onViewDetachedFromWindow(AnimatedMenuViewHolder holder) {
    super.onViewDetachedFromWindow(holder);
    holder.clearAnimation();
  }

  @Override
  public int getItemCount() {
    return Integer.MAX_VALUE;
  }

  public void setItems(List<VectorItemMenu> items) {
    this.items = items;
    notifyItemRangeChanged(0, items.size());
  }

  public void setHeaderItem(int headerItem) {
    this.headerItem = headerItem;
  }

  public class AnimatedMenuViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.menu_vector_root)
    FrameLayout menuVectorRoot;
    @BindView(menu_vector_label)
    TextView menuVectorLabel;
    @BindView(R.id.menu_vector_image)
    ImageView menuVectorImage;
    @BindView(R.id.view_shadow)
    View viewShadow;

    public AnimatedMenuViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }

    public void clearAnimation() {
      menuVectorRoot.clearAnimation();
    }
  }
}