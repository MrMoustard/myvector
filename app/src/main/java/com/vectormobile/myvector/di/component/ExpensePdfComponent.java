package com.vectormobile.myvector.di.component;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.di.module.ExpensePdfModule;
import com.vectormobile.myvector.ui.gms.expenses.pdf.ExpensePdfActivity;
import dagger.Component;

/**
 * Created by kurt on 05/12/16.
 */
@ActivityScope
@Component(dependencies = AppComponent.class, modules = ExpensePdfModule.class)
public interface ExpensePdfComponent {
  void inject(ExpensePdfActivity activity);
}
