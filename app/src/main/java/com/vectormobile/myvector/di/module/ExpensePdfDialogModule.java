package com.vectormobile.myvector.di.module;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.gms.expenses.pdf.dialog.ExpensePdfDialogActivity;
import dagger.Module;
import dagger.Provides;

/**
 * Created by kurt on 05/12/16.
 */
@ActivityScope
@Module
public class ExpensePdfDialogModule extends ActivityModule<ExpensePdfDialogActivity> {

  private ExpensePdfDialogActivity activity;

  public ExpensePdfDialogModule(ExpensePdfDialogActivity activity) {
    super(activity);
    this.activity = activity;
  }

  @Provides
  ExpensePdfDialogActivity provideExpensePdfDialogActivity() {
    return activity;
  }
}
