package com.vectormobile.myvector.di.component;

/**
 * Created by ajruiz on 24/01/2017.
 */

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.di.module.HoursListModule;
import com.vectormobile.myvector.ui.gms.hour.list.HoursListActivity;
import dagger.Component;

@ActivityScope
@Component(dependencies = AppComponent.class, modules = HoursListModule.class)
public interface HoursListComponent {
  void inject(HoursListActivity activity);
}
