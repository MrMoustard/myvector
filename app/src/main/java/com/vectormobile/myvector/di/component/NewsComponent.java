package com.vectormobile.myvector.di.component;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.di.module.NewsModule;
import com.vectormobile.myvector.ui.news.NewsActivity;
import com.vectormobile.myvector.ui.news.detail.NewsDetailFragment;
import com.vectormobile.myvector.ui.news.news.NewsFragment;
import dagger.Component;

/**
 * Created by ajruiz on 11/11/2016.
 */

@ActivityScope
@Component(dependencies = AppComponent.class, modules = NewsModule.class)
public interface NewsComponent {
  void inject(NewsActivity activity);

  void inject(NewsFragment fragment);

  void inject(NewsDetailFragment fragment);
}
