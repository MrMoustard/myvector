package com.vectormobile.myvector.di.module;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.offering.detail.presentation.PresentationActivity;
import dagger.Module;
import dagger.Provides;

/**
 * Created by ajruiz on 23/11/2016.
 */

@ActivityScope
@Module
public class PresentationModule extends ActivityModule<PresentationActivity>  {

  private PresentationActivity activity;

  public PresentationModule(PresentationActivity activity) {
    super(activity);
    this.activity = activity;
  }

  @Provides
  PresentationActivity providePresentationActivity(){
    return activity;
  }

}
