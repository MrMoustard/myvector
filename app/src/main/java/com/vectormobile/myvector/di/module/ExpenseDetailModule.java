package com.vectormobile.myvector.di.module;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.gms.expenses.detail.ExpenseDetailActivity;
import dagger.Module;
import dagger.Provides;

/**
 * Created by kurt on 05/12/16.
 */
@ActivityScope
@Module
public class ExpenseDetailModule extends ActivityModule<ExpenseDetailActivity> {

  private ExpenseDetailActivity activity;

  public ExpenseDetailModule(ExpenseDetailActivity activity) {
    super(activity);
    this.activity = activity;
  }

  @Provides
  ExpenseDetailActivity provideExpenseDetailActivity() {
    return activity;
  }
}
