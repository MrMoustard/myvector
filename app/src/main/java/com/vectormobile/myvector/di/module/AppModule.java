package com.vectormobile.myvector.di.module;

import android.content.Context;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.di.ForApplication;
import com.vectormobile.myvector.domain.interactor.botframework.GetConversationHistoryInteractor;
import com.vectormobile.myvector.domain.interactor.botframework.GetConversationHistoryInteractorImpl;
import com.vectormobile.myvector.domain.interactor.botframework.SendBFTextInteractor;
import com.vectormobile.myvector.domain.interactor.botframework.SendBFTextInteractorImpl;
import com.vectormobile.myvector.domain.interactor.botframework.StartConversationInteractor;
import com.vectormobile.myvector.domain.interactor.botframework.StartConversationInteractorImpl;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsBodyRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsBodyRequestInteractorImpl;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsDayStateRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsDayStateRequestInteractorImpl;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsExpenseRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsExpenseRequestInteractorImpl;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsHourRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsHourRequestInteractorImpl;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsJiraRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsJiraRequestInteractorImpl;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsRequestInteractorImpl;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsTimeLapseExpenseRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsTimeLapseExpenseRequestInteractorImpl;
import com.vectormobile.myvector.domain.interactor.gms.BuildJiraBrowserBodyRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.BuildJiraBrowserBodyRequestInteractorImpl;
import com.vectormobile.myvector.domain.interactor.gms.GetCurrenciesInteractor;
import com.vectormobile.myvector.domain.interactor.gms.GetCurrenciesInteractorImpl;
import com.vectormobile.myvector.domain.interactor.gms.GetExpenseCategoriesInteractor;
import com.vectormobile.myvector.domain.interactor.gms.GetExpenseCategoriesInteractorImpl;
import com.vectormobile.myvector.domain.interactor.gms.GetReportCategoryInteractor;
import com.vectormobile.myvector.domain.interactor.gms.GetReportCategoryInteractorImpl;
import com.vectormobile.myvector.domain.interactor.gms.GetWorkOrderInteractor;
import com.vectormobile.myvector.domain.interactor.gms.GetWorkOrderInteractorImpl;
import com.vectormobile.myvector.domain.interactor.gms.RetrieveGmsNotificationsInteractor;
import com.vectormobile.myvector.domain.interactor.gms.RetrieveGmsNotificationsInteractorImpl;
import com.vectormobile.myvector.domain.interactor.gms.SaveExpenseInteractor;
import com.vectormobile.myvector.domain.interactor.gms.SaveExpenseInteractorImpl;
import com.vectormobile.myvector.domain.interactor.gms.SaveExpenseWithImageInteractor;
import com.vectormobile.myvector.domain.interactor.gms.SaveExpenseWithImageInteractorImpl;
import com.vectormobile.myvector.domain.interactor.gms.SharedPdfImputationInteractor;
import com.vectormobile.myvector.domain.interactor.gms.SharedPdfImputationInteractorImpl;
import com.vectormobile.myvector.domain.interactor.intranet.ContactsListInteractor;
import com.vectormobile.myvector.domain.interactor.intranet.ContactsListInteractorImpl;
import com.vectormobile.myvector.domain.interactor.intranet.NewsInteractor;
import com.vectormobile.myvector.domain.interactor.intranet.NewsInteractorImpl;
import com.vectormobile.myvector.domain.interactor.intranet.NewsItemDetailInteractor;
import com.vectormobile.myvector.domain.interactor.intranet.NewsItemDetailInteractorImpl;
import com.vectormobile.myvector.domain.interactor.intranet.ProfileDataInteractor;
import com.vectormobile.myvector.domain.interactor.intranet.ProfileDataInteractorImpl;
import com.vectormobile.myvector.domain.interactor.intranet.ProfileSaveUserInfoInteractor;
import com.vectormobile.myvector.domain.interactor.intranet.ProfileSaveUserInfoInteractorImpl;
import com.vectormobile.myvector.domain.interactor.intranet.SendProfileImageInteractor;
import com.vectormobile.myvector.domain.interactor.intranet.SendProfileImageInteractorImpl;
import com.vectormobile.myvector.domain.interactor.login.GmsLoginInteractor;
import com.vectormobile.myvector.domain.interactor.login.GmsLoginInteractorImpl;
import com.vectormobile.myvector.domain.interactor.login.IntranetLoginInteractor;
import com.vectormobile.myvector.domain.interactor.login.IntranetLoginInteractorImpl;
import com.vectormobile.myvector.domain.interactor.login.LogoutInteractor;
import com.vectormobile.myvector.domain.interactor.login.LogoutInteractorImpl;
import com.vectormobile.myvector.domain.interactor.login.OfferingLoginInteractor;
import com.vectormobile.myvector.domain.interactor.login.OfferingLoginInteractorImpl;
import com.vectormobile.myvector.domain.interactor.offering.OfferingMenuInteractor;
import com.vectormobile.myvector.domain.interactor.offering.OfferingMenuInteractorImpl;
import com.vectormobile.myvector.domain.interactor.offering.OfferingNewsInteractor;
import com.vectormobile.myvector.domain.interactor.offering.OfferingNewsInteractorImpl;
import com.vectormobile.myvector.domain.repository.BFRepository;
import com.vectormobile.myvector.domain.repository.BFRepositoryImpl;
import com.vectormobile.myvector.domain.repository.GmsRepository;
import com.vectormobile.myvector.domain.repository.GmsRepositoryImpl;
import com.vectormobile.myvector.domain.repository.IntranetRepository;
import com.vectormobile.myvector.domain.repository.IntranetRepositoryImpl;
import com.vectormobile.myvector.domain.repository.TrendsRepository;
import com.vectormobile.myvector.domain.repository.TrendsRepositoryImpl;
import com.vectormobile.myvector.domain.service.GmsService;
import com.vectormobile.myvector.domain.service.IntranetServiceJson;
import com.vectormobile.myvector.domain.service.IntranetServiceXml;
import com.vectormobile.myvector.domain.service.RoomService;
import com.vectormobile.myvector.domain.service.TrendsService;
import com.vectormobile.myvector.environment.EnvironmentModule;
import com.vectormobile.myvector.storage.ImageManager;
import com.vectormobile.myvector.storage.PreferencesManager;
import com.vectormobile.myvector.storage.SessionManager;
import com.vectormobile.myvector.ui.rocketchat.RocketWebView;
import com.vectormobile.myvector.ui.outlook.agenda.OutlookAgendaWebView;
import com.vectormobile.myvector.ui.outlook.mail.OutlookMailWebView;
import com.vectormobile.myvector.util.AppResources;
import com.vectormobile.myvector.util.AppResourcesImpl;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

import javax.inject.Named;

/**
 * VectorDigital.
 *
 * @author -   AMarinaG
 * @since -   28/10/16
 */
@Module
public class AppModule {
  private App application;
  private AppResources appResources;
  private PreferencesManager preferencesManager;
  private SessionManager sessionManager;
  private RocketWebView rocketWebView;
  private OutlookMailWebView outlookMailWebView;
  private OutlookAgendaWebView outlookAgendaWebView;

  public AppModule(App application) {
    this.application = application;
    preferencesManager = new PreferencesManager(application);
    appResources = new AppResourcesImpl(application);
    sessionManager = new SessionManager(preferencesManager, appResources);
    rocketWebView = new RocketWebView(application);
    outlookMailWebView = new OutlookMailWebView(application);
    outlookAgendaWebView = new OutlookAgendaWebView(application);
  }

  @Provides
  @ForApplication
  App provideApp() {
    return application;
  }

  @Provides
  @ForApplication
  Context provideContext() {
    return application;
  }

  @Provides
  AppResources provideAppResource() {
    return appResources;
  }

  @Provides
  ImageManager provideImageManager() {
    return ImageManager.getInstance();
  }

  @Provides
  RocketWebView provideRocketWebView() {
    return rocketWebView;
  }

  @Provides
  OutlookMailWebView provideOutlookMailWebView() {
    return outlookMailWebView;
  }

  @Provides
  OutlookAgendaWebView provideOutlookAgendaWebView() {
    return outlookAgendaWebView;
  }

  @Provides
  TrendsRepository provideTrendsRepository(@Named(EnvironmentModule.TRENDS) Retrofit retrofit, SessionManager sessionManager) {
    TrendsService service = retrofit.create(TrendsService.class);
    return new TrendsRepositoryImpl(service, sessionManager);
  }

  @Provides
  GmsRepository provideGmsRepository(@Named(EnvironmentModule.GMS) Retrofit retrofit) {
    GmsService service = retrofit.create(GmsService.class);
    return new GmsRepositoryImpl(service);
  }

  @Provides
  IntranetRepository provideIntranetRepository(@Named(EnvironmentModule.INTRANET_JSON) Retrofit retrofitJson, @Named(EnvironmentModule.INTRANET_XML) Retrofit retrofitXml, SessionManager sessionManager) {
    IntranetServiceJson serviceJson = retrofitJson.create(IntranetServiceJson.class);
    IntranetServiceXml serviceXml = retrofitXml.create(IntranetServiceXml.class);
    return new IntranetRepositoryImpl(serviceJson, serviceXml, sessionManager);
  }

  @Provides
  BFRepository provideBfRepository(@Named(EnvironmentModule.BOTFRAMEWORK) Retrofit retrofit, AppResources appResources) {
    return new BFRepositoryImpl(retrofit.create(RoomService.class), appResources, preferencesManager);
  }

  @Provides
  RetrieveGmsNotificationsInteractor provideGmsRetrieveNotificationsInteractor(GmsRepository gmsRepository) {
    return new RetrieveGmsNotificationsInteractorImpl(gmsRepository);
  }

  @Provides
  OfferingLoginInteractor provideLoginOfferingInteractor(TrendsRepository trendsRepository) {
    return new OfferingLoginInteractorImpl(trendsRepository);
  }

  @Provides
  GmsLoginInteractor provideLoginGmsInteractor(GmsRepository gmsRepository) {
    return new GmsLoginInteractorImpl(gmsRepository);
  }

  @Provides
  IntranetLoginInteractor provideLoginIntranetInteractor(IntranetRepository intranetRepository) {
    return new IntranetLoginInteractorImpl(intranetRepository);
  }

  @Provides
  OfferingMenuInteractor provideOfferingMenuInteractor(TrendsRepository repository) {
    return new OfferingMenuInteractorImpl(repository);
  }

  @Provides
  OfferingNewsInteractor provideOfferingNewsInteractor(TrendsRepository repository) {
    return new OfferingNewsInteractorImpl(repository);
  }

  @Provides
  NewsInteractor provideNewsInteractor(IntranetRepository repository) {
    return new NewsInteractorImpl(repository);
  }

  @Provides
  NewsItemDetailInteractor provideNewsDetailInteractor(IntranetRepository repository) {
    return new NewsItemDetailInteractorImpl(repository);
  }

  @Provides
  ProfileDataInteractor providesProfileDataInteractor(IntranetRepository repository) {
    return new ProfileDataInteractorImpl(repository);
  }

  @Provides
  PreferencesManager providePreferenceManager() {
    return preferencesManager;
  }

  @Provides
  SessionManager provideSessionManager() {
    return sessionManager;
  }

  @Provides
  GetCurrenciesInteractor provideGetCurrenciesInteractor(GmsRepository gmsRepository) {
    return new GetCurrenciesInteractorImpl(gmsRepository);
  }

  @Provides
  BuildGmsBodyRequestInteractor provideGetImputableProjectsInteractor(GmsRepository gmsRepository) {
    return new BuildGmsBodyRequestInteractorImpl(gmsRepository);
  }

  @Provides
  GetWorkOrderInteractor provideGetWorkOrderInteractor(GmsRepository gmsRepository) {
    return new GetWorkOrderInteractorImpl(gmsRepository);
  }

  @Provides
  GetExpenseCategoriesInteractor provideGetExpenseCategoriesInteractor(GmsRepository gmsRepository) {
    return new GetExpenseCategoriesInteractorImpl(gmsRepository);
  }

  @Provides
  SaveExpenseInteractor provideSaveExpenseInteractor(GmsRepository gmsRepository) {
    return new SaveExpenseInteractorImpl(gmsRepository);
  }

  @Provides
  SaveExpenseWithImageInteractor provideExpenseWithImageInteractor(GmsRepository gmsRepository) {
    return new SaveExpenseWithImageInteractorImpl(gmsRepository);
  }

  @Provides
  GetReportCategoryInteractor provideGetReportCategoryInteractor(GmsRepository gmsRepository) {
    return new GetReportCategoryInteractorImpl(gmsRepository);
  }

  @Provides
  BuildGmsExpenseRequestInteractor provideBuildGmsExpenseRequestInteractor(GmsRepository gmsRepository) {
    return new BuildGmsExpenseRequestInteractorImpl(gmsRepository);
  }

  @Provides
  BuildGmsTimeLapseExpenseRequestInteractor provideBuildGmsTimeLapseExpenseRequestInteractor(GmsRepository gmsRepository) {
    return new BuildGmsTimeLapseExpenseRequestInteractorImpl(gmsRepository);
  }

  @Provides
  BuildGmsHourRequestInteractor provideBuildGmsHourRequestInteractor(GmsRepository gmsRepository) {
    return new BuildGmsHourRequestInteractorImpl(gmsRepository);
  }

  @Provides
  BuildGmsRequestInteractor provideSendExpenseInteractor(GmsRepository gmsRepository) {
    return new BuildGmsRequestInteractorImpl(gmsRepository);
  }

  @Provides
  ProfileSaveUserInfoInteractor provideProfileSaveUserInfoInteractor(IntranetRepository intranetRepository) {
    return new ProfileSaveUserInfoInteractorImpl(intranetRepository);
  }

  @Provides
  SendProfileImageInteractor provideSendProfileImageInteractor(IntranetRepository intranetRepository) {
    return new SendProfileImageInteractorImpl(intranetRepository);
  }

  @Provides
  ContactsListInteractor provideContactsListInteractor(IntranetRepository intranetRepository) {
    return new ContactsListInteractorImpl(intranetRepository);
  }

  @Provides
  LogoutInteractor provideLogoutInteractor(SessionManager sessionManager) {
    return new LogoutInteractorImpl(sessionManager);
  }

  @Provides
  BuildJiraBrowserBodyRequestInteractor provideBuildJiraBrowserBodyRequestInteractor(GmsRepository gmsRepository) {
    return new BuildJiraBrowserBodyRequestInteractorImpl(gmsRepository);
  }

  @Provides
  BuildGmsJiraRequestInteractor provideBuildGmsJiraRequestInteractor(GmsRepository gmsRepository) {
    return new BuildGmsJiraRequestInteractorImpl(gmsRepository);
  }

  @Provides
  StartConversationInteractor provideStartConversationInteractor(BFRepository repository) {
    return new StartConversationInteractorImpl(repository);
  }

  @Provides
  GetConversationHistoryInteractor provideGetConversationHistoryInteractor(BFRepository repository) {
    return new GetConversationHistoryInteractorImpl(repository);
  }

  @Provides
  SendBFTextInteractor provideSendBFTextInteractor(BFRepository repository) {
    return new SendBFTextInteractorImpl(repository);
  }


  @Provides
  SharedPdfImputationInteractor provideSharedPdfImputationInteractor(GmsRepository gmsRepository) {
    return new SharedPdfImputationInteractorImpl(gmsRepository);
  }

  @Provides
  BuildGmsDayStateRequestInteractor provideBuildGmsDayStateRequestInteractor(GmsRepository repository) {
    return new BuildGmsDayStateRequestInteractorImpl(repository);
  }
}
