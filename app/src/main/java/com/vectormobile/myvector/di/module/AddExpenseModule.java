package com.vectormobile.myvector.di.module;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.gms.expenses.add.AddExpenseActivity;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Erik Medina on 07/12/2016.
 */
@ActivityScope
@Module
public class AddExpenseModule extends ActivityModule<AddExpenseActivity> {

  private AddExpenseActivity activity;

  public AddExpenseModule(AddExpenseActivity activity) {
    super(activity);
    this.activity = activity;
  }

  @Provides
  AddExpenseActivity provideAddExpenseActivity() {
    return activity;
  }
}
