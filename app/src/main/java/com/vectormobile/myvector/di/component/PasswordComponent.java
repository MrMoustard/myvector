package com.vectormobile.myvector.di.component;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.di.module.PasswordModule;
import com.vectormobile.myvector.ui.password.PasswordActivity;
import dagger.Component;

/**
 * Created by Erik Medina on 14/12/2016.
 */
@ActivityScope
@Component(dependencies = AppComponent.class, modules= PasswordModule.class)
public interface PasswordComponent {

  void inject(PasswordActivity activity);
}
