package com.vectormobile.myvector.di.module;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.news.NewsActivity;
import com.vectormobile.myvector.ui.news.detail.NewsDetailFragment;
import com.vectormobile.myvector.ui.news.detail.NewsDetailView;
import com.vectormobile.myvector.ui.news.news.NewsFragment;
import com.vectormobile.myvector.ui.news.news.NewsView;
import dagger.Module;
import dagger.Provides;

/**
 * Created by ajruiz on 11/11/2016.
 */

@Module
@ActivityScope
public class NewsModule extends ActivityModule<NewsActivity> {
  private NewsActivity activity;

  public NewsModule(NewsActivity activity) {
    super(activity);
    this.activity = activity;
  }

  @Provides
  NewsActivity provideNewsActivity() {
    return activity;
  }

  @Provides
  NewsView provideNewsView() {
    return new NewsFragment();
  }

  @Provides
  NewsDetailView provideNewsDetailView() {
    return new NewsDetailFragment();
  }

}
