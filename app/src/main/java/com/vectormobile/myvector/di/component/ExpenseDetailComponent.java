package com.vectormobile.myvector.di.component;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.di.module.ExpenseDetailModule;
import com.vectormobile.myvector.ui.gms.expenses.detail.ExpenseDetailActivity;
import dagger.Component;

/**
 * Created by kurt on 05/12/16.
 */
@ActivityScope
@Component(dependencies = AppComponent.class, modules = ExpenseDetailModule.class)
public interface ExpenseDetailComponent {
  void inject(ExpenseDetailActivity activity);
}
