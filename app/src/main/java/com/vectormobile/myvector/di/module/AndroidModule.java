package com.vectormobile.myvector.di.module;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import com.vectormobile.myvector.di.ForApplication;
import com.vectormobile.myvector.storage.PreferencesManager;
import dagger.Module;
import dagger.Provides;

/**
 * VectorDigital.
 *
 * @author -   AMarinaG
 * @since -   29/10/16
 */
@Module
public class AndroidModule {
  @Provides
  @ForApplication
  SharedPreferences provideSharedPreferences(@ForApplication Context application) {
    return application.getSharedPreferences(application.getPackageName(), Context.MODE_PRIVATE);
  }

  @Provides
  @ForApplication
  PreferencesManager providePreferencesManager(@ForApplication Context application) {
    return new PreferencesManager(application);
  }//

  @Provides
  @ForApplication
  LayoutInflater provideLayoutInflater(@ForApplication Application application) {
    return LayoutInflater.from(application);
  }
}
