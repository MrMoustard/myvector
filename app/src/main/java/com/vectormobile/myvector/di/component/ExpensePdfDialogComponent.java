package com.vectormobile.myvector.di.component;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.di.module.ExpensePdfDialogModule;
import com.vectormobile.myvector.ui.gms.expenses.pdf.dialog.ExpensePdfDialogActivity;
import dagger.Component;

/**
 * Created by kurt on 05/12/16.
 */
@ActivityScope
@Component(dependencies = AppComponent.class, modules = ExpensePdfDialogModule.class)
public interface ExpensePdfDialogComponent {
  void inject(ExpensePdfDialogActivity activity);
}
