package com.vectormobile.myvector.di.module;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.gms.hour.add.jira.JiraBrowserActivity;
import dagger.Module;
import dagger.Provides;

/**
 * Created by ajruiz on 28/02/2017.
 */

@ActivityScope
@Module
public class JiraBrowserModule extends ActivityModule<JiraBrowserActivity> {

  private JiraBrowserActivity activity;

  public JiraBrowserModule(JiraBrowserActivity activity) {
    super(activity);
    this.activity = activity;
  }

  @Provides
  JiraBrowserActivity provideJiraBrowserActivity() {
    return activity;
  }

}
