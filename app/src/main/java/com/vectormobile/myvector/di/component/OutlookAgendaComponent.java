package com.vectormobile.myvector.di.component;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.di.module.OutlookAgendaModule;
import com.vectormobile.myvector.ui.outlook.agenda.OutlookAgendaActivity;
import dagger.Component;

/**
 * Created by amarina on 13/03/2017.
 */
@ActivityScope
@Component(dependencies = AppComponent.class, modules = OutlookAgendaModule.class)
public interface OutlookAgendaComponent {
  void inject(OutlookAgendaActivity activity);
}
