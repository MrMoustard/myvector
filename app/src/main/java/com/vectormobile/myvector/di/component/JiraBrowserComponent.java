package com.vectormobile.myvector.di.component;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.di.module.JiraBrowserModule;
import com.vectormobile.myvector.ui.gms.hour.add.jira.JiraBrowserActivity;
import dagger.Component;

/**
 * Created by ajruiz on 28/02/2017.
 */
@ActivityScope
@Component(dependencies = AppComponent.class, modules = JiraBrowserModule.class)
public interface JiraBrowserComponent {
  void inject(JiraBrowserActivity activity);
}
