package com.vectormobile.myvector.di.module;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.outlook.agenda.OutlookAgendaActivity;
import dagger.Module;
import dagger.Provides;

/**
 * Created by amarina on 13/03/2017.
 */
@ActivityScope
@Module
public class OutlookAgendaModule extends ActivityModule<OutlookAgendaActivity> {

  private OutlookAgendaActivity activity;

  public OutlookAgendaModule(OutlookAgendaActivity activity) {
    super(activity);
    this.activity = activity;
  }

  @Provides
  OutlookAgendaActivity provideActivity() {
    return activity;
  }
}
