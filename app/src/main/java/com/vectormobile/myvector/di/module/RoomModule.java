package com.vectormobile.myvector.di.module;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.bot.RoomActivity;
import dagger.Module;
import dagger.Provides;

/**
 * Created by amarina on 14/03/2017.
 */
@ActivityScope
@Module
public class RoomModule extends ActivityModule<RoomActivity> {
  private RoomActivity activity;

  public RoomModule(RoomActivity activity) {
    super(activity);
    this.activity = activity;
  }

  @Provides
  RoomActivity provideActivity() {
    return activity;
  }
}
