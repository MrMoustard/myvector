package com.vectormobile.myvector.di.module;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.browser.list.ContactsListActivity;
import dagger.Module;
import dagger.Provides;

/**
 * Created by ajruiz on 01/02/2017.
 */
@ActivityScope
@Module
public class ContactsListModule extends ActivityModule<ContactsListActivity> {
  private ContactsListActivity activity;

  public ContactsListModule(ContactsListActivity activity) {
    super(activity);
    this.activity = activity;
  }

  @Provides
  ContactsListActivity provideContactsListActivity(){
    return activity;
  }

}
