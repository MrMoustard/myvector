package com.vectormobile.myvector.di.module;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.login.LoginActivity;
import dagger.Module;
import dagger.Provides;

/**
 * VectorDigital.
 *
 * @author -   AMarinaG
 * @since -   29/10/16
 */
@Module
@ActivityScope
public class LoginModule extends ActivityModule<LoginActivity> {
  private LoginActivity activity;

  public LoginModule(LoginActivity activity) {
    super(activity);
    this.activity = activity;
  }

  @Provides
  LoginActivity provideActivity() {
    return activity;
  }
}
