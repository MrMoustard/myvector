package com.vectormobile.myvector.di.component;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.di.module.MainModule;
import com.vectormobile.myvector.ui.main.MainActivity;
import com.vectormobile.myvector.ui.menu.corporate.CorporateMenuFragment;
import com.vectormobile.myvector.ui.menu.news.NewsMenuFragment;
import com.vectormobile.myvector.ui.menu.personal.PersonalMenuFragment;
import dagger.Component;

/**
 * VectorDigital.
 *
 * @author -   AMarinaG
 * @since -   29/10/16
 */
@ActivityScope
@Component(dependencies = AppComponent.class, modules = MainModule.class)
public interface MainComponent {
  void inject(MainActivity activity);

  void inject(CorporateMenuFragment fragment);

  void inject(NewsMenuFragment fragment);

  void inject(PersonalMenuFragment fragment);
}
