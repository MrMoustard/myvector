package com.vectormobile.myvector.di.module;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.profile.ProfileActivity;
import dagger.Module;
import dagger.Provides;

/**
 * VectorDigital.
 *
 * @author -   AMarinaG
 * @since -   29/10/16
 */
@ActivityScope
@Module
public class ProfileModule extends ActivityModule<ProfileActivity> {
  private ProfileActivity activity;

  public ProfileModule(ProfileActivity activity) {
    super(activity);
    this.activity = activity;
  }

  @Provides
  ProfileActivity provideMainActivity() {
    return activity;
  }
}
