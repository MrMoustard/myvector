package com.vectormobile.myvector.di.component;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.di.module.LoginModule;
import com.vectormobile.myvector.ui.login.LoginActivity;
import dagger.Component;

/**
 * VectorDigital.
 *
 * @author -   AMarinaG
 * @since -   29/10/16
 */
@ActivityScope
@Component(dependencies = AppComponent.class, modules = LoginModule.class)
public interface LoginComponent {

  void inject(LoginActivity activity);

}
