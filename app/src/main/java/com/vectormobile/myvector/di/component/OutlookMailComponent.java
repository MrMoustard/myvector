package com.vectormobile.myvector.di.component;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.di.module.OutlookMailModule;
import com.vectormobile.myvector.ui.outlook.mail.OutlookMailActivity;
import dagger.Component;

/**
 * Created by amarina on 13/03/2017.
 */
@ActivityScope
@Component(dependencies = AppComponent.class, modules = OutlookMailModule.class)
public interface OutlookMailComponent {
  void inject(OutlookMailActivity activity);
}
