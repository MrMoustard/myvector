package com.vectormobile.myvector.di.component;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.di.module.AddHourModule;
import com.vectormobile.myvector.ui.gms.hour.add.AddHourActivity;
import dagger.Component;

/**
 * Created by Erik Medina on 13/01/2017.
 */
@ActivityScope
@Component(dependencies = AppComponent.class, modules = AddHourModule.class)
public interface AddHourComponent {
  void inject(AddHourActivity activity);
}
