package com.vectormobile.myvector.di.component;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.di.module.PresentationModule;
import com.vectormobile.myvector.ui.offering.detail.presentation.PresentationActivity;
import dagger.Component;

/**
 * Created by ajruiz on 23/11/2016.
 */

@ActivityScope
@Component(dependencies = AppComponent.class, modules = PresentationModule.class)
public interface PresentationComponent {
  void inject(PresentationActivity activity);
}
