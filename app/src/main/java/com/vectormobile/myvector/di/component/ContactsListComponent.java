package com.vectormobile.myvector.di.component;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.di.module.ContactsListModule;
import com.vectormobile.myvector.ui.browser.list.ContactsListActivity;
import dagger.Component;

/**
 * Created by ajruiz on 01/02/2017.
 */

@ActivityScope
@Component(dependencies = AppComponent.class, modules = ContactsListModule.class)
public interface ContactsListComponent {
  void inject(ContactsListActivity activity);
}
