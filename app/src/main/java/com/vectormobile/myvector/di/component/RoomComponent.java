package com.vectormobile.myvector.di.component;

import com.vectormobile.myvector.di.module.RoomModule;
import com.vectormobile.myvector.ui.bot.RoomActivity;
import dagger.Component;

/**
 * Created by amarina on 14/03/2017.
 */
@Component(dependencies = AppComponent.class, modules = RoomModule.class)
public interface RoomComponent {
  void inject(RoomActivity activity);
}
