package com.vectormobile.myvector.di.module;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.rocketchat.RocketchatActivity;
import dagger.Module;
import dagger.Provides;

/**
 * Created by amarina on 26/01/2017.
 */
@ActivityScope
@Module
public class RocketchatModule extends ActivityModule<RocketchatActivity> {
  private RocketchatActivity activity;

  public RocketchatModule(RocketchatActivity activity) {
    super(activity);
    this.activity = activity;
  }

  @Provides
  RocketchatActivity provideRocketchatActivity() {
    return activity;
  }
}
