package com.vectormobile.myvector.di.module;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.password.PasswordActivity;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Erik Medina on 14/12/2016.
 */
@Module
@ActivityScope
public class PasswordModule extends ActivityModule<PasswordActivity> {

  private PasswordActivity activity;

  public PasswordModule(PasswordActivity activity) {
    super(activity);
    this.activity = activity;
  }

  @Provides
  PasswordActivity provideActivity() {
    return activity;
  }
}
