package com.vectormobile.myvector.di.component;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.di.module.AddExpenseModule;
import com.vectormobile.myvector.ui.gms.expenses.add.AddExpenseActivity;
import dagger.Component;

/**
 * Created by Erik Medina on 07/12/2016.
 */
@ActivityScope
@Component(dependencies = AppComponent.class, modules = AddExpenseModule.class)
public interface AddExpenseComponent {
  void inject(AddExpenseActivity activity);
}
