package com.vectormobile.myvector.di.module;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.gms.hour.list.HoursListActivity;
import dagger.Module;
import dagger.Provides;

/**
 * Created by ajruiz on 24/01/2017.
 */

@ActivityScope
@Module
public class HoursListModule extends ActivityModule<HoursListActivity> {

  private HoursListActivity activity;

  public HoursListModule(HoursListActivity activity) {
    super(activity);
    this.activity = activity;
  }

  @Provides
  HoursListActivity provideHoursListActivity() {
    return activity;
  }

}
