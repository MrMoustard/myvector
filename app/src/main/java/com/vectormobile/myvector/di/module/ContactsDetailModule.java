package com.vectormobile.myvector.di.module;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.browser.detail.ContactsDetailActivity;
import dagger.Module;
import dagger.Provides;

/**
 * Created by ajruiz on 03/02/2017.
 */
@ActivityScope
@Module
public class ContactsDetailModule extends ActivityModule<ContactsDetailActivity> {

  private ContactsDetailActivity activity;

  public ContactsDetailModule(ContactsDetailActivity activity) {
    super(activity);
    this.activity = activity;
  }

  @Provides
  ContactsDetailActivity provideContactsDetailActivity() {
    return activity;
  }

}
