package com.vectormobile.myvector.di.module;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.base.BaseActivity;
import dagger.Module;
import dagger.Provides;

/**
 * VectorDigital.
 *
 * @author -   AMarinaG
 * @since -   29/10/16
 */
@Module
@ActivityScope
public abstract class ActivityModule<A extends BaseActivity> {
  protected A activity;

  public ActivityModule(A activity) {
    this.activity = activity;
  }

  @Provides
  @ActivityScope
  A activity() {
    return activity;
  }

}
