package com.vectormobile.myvector.di.module;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.gms.hour.add.AddHourActivity;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Erik Medina on 13/01/2017.
 */
@ActivityScope
@Module
public class AddHourModule extends ActivityModule<AddHourActivity> {

  private AddHourActivity activity;

  public AddHourModule(AddHourActivity activity) {
    super(activity);
    this.activity = activity;
  }

  @Provides
  AddHourActivity provideAddHourActivity() {
    return activity;
  }
}
