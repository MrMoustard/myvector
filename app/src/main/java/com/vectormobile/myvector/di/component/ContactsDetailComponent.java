package com.vectormobile.myvector.di.component;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.di.module.ContactsDetailModule;
import com.vectormobile.myvector.ui.browser.detail.ContactsDetailActivity;
import dagger.Component;

/**
 * Created by ajruiz on 03/02/2017.
 */

@ActivityScope
@Component(dependencies = AppComponent.class, modules = ContactsDetailModule.class)
public interface ContactsDetailComponent {
  void inject(ContactsDetailActivity activity);
}
