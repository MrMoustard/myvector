package com.vectormobile.myvector.di.component;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.di.module.RocketchatModule;
import com.vectormobile.myvector.ui.rocketchat.RocketchatActivity;
import dagger.Component;

/**
 * Created by amarina on 26/01/2017.
 */
@ActivityScope
@Component(dependencies = AppComponent.class, modules = RocketchatModule.class)
public interface RocketchatComponent {
  void inject(RocketchatActivity activity);
}
