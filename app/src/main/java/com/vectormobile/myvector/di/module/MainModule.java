package com.vectormobile.myvector.di.module;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.main.MainActivity;
import dagger.Module;
import dagger.Provides;

/**
 * VectorDigital.
 *
 * @author -   AMarinaG
 * @since -   29/10/16
 */
@ActivityScope
@Module
public class MainModule extends ActivityModule<MainActivity> {
  private MainActivity activity;

  public MainModule(MainActivity activity) {
    super(activity);
    this.activity = activity;
  }

  @Provides
  MainActivity provideMainActivity() {
    return activity;
  }
}
