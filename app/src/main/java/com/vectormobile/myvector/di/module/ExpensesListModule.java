package com.vectormobile.myvector.di.module;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.gms.expenses.list.ExpensesListActivity;
import dagger.Module;
import dagger.Provides;

/**
 * Created by kurt on 05/12/16.
 */
@ActivityScope
@Module
public class ExpensesListModule extends ActivityModule<ExpensesListActivity> {

  private ExpensesListActivity activity;

  public ExpensesListModule(ExpensesListActivity activity) {
    super(activity);
    this.activity = activity;
  }

  @Provides
  ExpensesListActivity provideExpensesListActivity() {
    return activity;
  }
}
