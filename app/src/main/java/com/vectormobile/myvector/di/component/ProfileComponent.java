package com.vectormobile.myvector.di.component;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.di.module.ProfileModule;
import com.vectormobile.myvector.ui.profile.ProfileActivity;
import dagger.Component;

/**
 * VectorDigital.
 *
 * @author -   AMarinaG
 * @since -   29/10/16
 */
@ActivityScope
@Component(dependencies = AppComponent.class, modules = ProfileModule.class)
public interface ProfileComponent {
  void inject(ProfileActivity activity);
}
