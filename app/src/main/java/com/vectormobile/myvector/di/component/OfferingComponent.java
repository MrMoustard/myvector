package com.vectormobile.myvector.di.component;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.di.module.OfferingModule;
import com.vectormobile.myvector.ui.offering.OfferingActivity;
import com.vectormobile.myvector.ui.offering.detail.OfferingDetailFragment;
import com.vectormobile.myvector.ui.offering.menu.OfferingMenuFragment;
import com.vectormobile.myvector.ui.offering.news.OfferingNewsFragment;
import dagger.Component;

/**
 * Created by ajruiz on 03/11/2016.
 */

@ActivityScope
@Component(dependencies = AppComponent.class, modules = OfferingModule.class)
public interface OfferingComponent {
  void inject(OfferingActivity activity);

  void inject(OfferingMenuFragment fragment);

  void inject(OfferingDetailFragment fragment);

  void inject(OfferingNewsFragment fragment);
}
