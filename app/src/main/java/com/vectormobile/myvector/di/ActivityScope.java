package com.vectormobile.myvector.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * VectorDigital.
 *
 * @author -   AMarinaG
 * @since -   29/10/16
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface ActivityScope {
}
