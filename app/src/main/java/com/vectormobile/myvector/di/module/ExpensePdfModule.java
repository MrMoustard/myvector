package com.vectormobile.myvector.di.module;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.gms.expenses.pdf.ExpensePdfActivity;
import dagger.Module;
import dagger.Provides;

/**
 * Created by kurt on 05/12/16.
 */
@ActivityScope
@Module
public class ExpensePdfModule extends ActivityModule<ExpensePdfActivity> {

  private ExpensePdfActivity activity;

  public ExpensePdfModule(ExpensePdfActivity activity) {
    super(activity);
    this.activity = activity;
  }

  @Provides
  ExpensePdfActivity provideExpensePdfActivity() {
    return activity;
  }
}
