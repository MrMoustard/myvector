package com.vectormobile.myvector.di.component;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.di.module.ExpensesListModule;
import com.vectormobile.myvector.ui.gms.expenses.list.ExpensesListActivity;
import dagger.Component;

/**
 * Created by kurt on 05/12/16.
 */
@ActivityScope
@Component(dependencies = AppComponent.class, modules = ExpensesListModule.class)
public interface ExpensesListComponent {
  void inject(ExpensesListActivity activity);
}
