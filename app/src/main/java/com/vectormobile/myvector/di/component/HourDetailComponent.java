package com.vectormobile.myvector.di.component;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.di.module.HourDetailModule;
import com.vectormobile.myvector.ui.gms.hour.detail.HourDetailActivity;
import dagger.Component;

/**
 * Created by ajruiz on 26/01/2017.
 */

@ActivityScope
@Component(dependencies = AppComponent.class, modules = HourDetailModule.class)
public interface HourDetailComponent {
  void inject(HourDetailActivity activity);
}
