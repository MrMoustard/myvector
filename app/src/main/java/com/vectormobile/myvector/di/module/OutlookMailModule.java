package com.vectormobile.myvector.di.module;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.outlook.mail.OutlookMailActivity;
import dagger.Module;
import dagger.Provides;

/**
 * Created by amarina on 13/03/2017.
 */
@ActivityScope
@Module
public class OutlookMailModule extends ActivityModule<OutlookMailActivity> {
  private OutlookMailActivity activity;

  public OutlookMailModule(OutlookMailActivity activity) {
    super(activity);
    this.activity = activity;
  }

  @Provides
  OutlookMailActivity provideActivity() {
    return activity;
  }
}
