package com.vectormobile.myvector.di.component;

import android.content.Context;
import android.content.SharedPreferences;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.di.ForApplication;
import com.vectormobile.myvector.di.module.AndroidModule;
import com.vectormobile.myvector.di.module.AppModule;
import com.vectormobile.myvector.domain.interactor.botframework.GetConversationHistoryInteractor;
import com.vectormobile.myvector.domain.interactor.botframework.SendBFTextInteractor;
import com.vectormobile.myvector.domain.interactor.botframework.StartConversationInteractor;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsBodyRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsDayStateRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsExpenseRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsHourRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsJiraRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsTimeLapseExpenseRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.BuildJiraBrowserBodyRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.GetCurrenciesInteractor;
import com.vectormobile.myvector.domain.interactor.gms.GetExpenseCategoriesInteractor;
import com.vectormobile.myvector.domain.interactor.gms.GetReportCategoryInteractor;
import com.vectormobile.myvector.domain.interactor.gms.GetWorkOrderInteractor;
import com.vectormobile.myvector.domain.interactor.gms.RetrieveGmsNotificationsInteractor;
import com.vectormobile.myvector.domain.interactor.gms.SaveExpenseInteractor;
import com.vectormobile.myvector.domain.interactor.gms.SaveExpenseWithImageInteractor;
import com.vectormobile.myvector.domain.interactor.gms.SharedPdfImputationInteractor;
import com.vectormobile.myvector.domain.interactor.intranet.ContactsListInteractor;
import com.vectormobile.myvector.domain.interactor.intranet.NewsInteractor;
import com.vectormobile.myvector.domain.interactor.intranet.NewsItemDetailInteractor;
import com.vectormobile.myvector.domain.interactor.intranet.ProfileDataInteractor;
import com.vectormobile.myvector.domain.interactor.intranet.ProfileSaveUserInfoInteractor;
import com.vectormobile.myvector.domain.interactor.intranet.SendProfileImageInteractor;
import com.vectormobile.myvector.domain.interactor.login.GmsLoginInteractor;
import com.vectormobile.myvector.domain.interactor.login.IntranetLoginInteractor;
import com.vectormobile.myvector.domain.interactor.login.LogoutInteractor;
import com.vectormobile.myvector.domain.interactor.login.OfferingLoginInteractor;
import com.vectormobile.myvector.domain.interactor.offering.OfferingMenuInteractor;
import com.vectormobile.myvector.domain.interactor.offering.OfferingNewsInteractor;
import com.vectormobile.myvector.environment.EnvironmentModule;
import com.vectormobile.myvector.network.OkHttpInterceptorsModule;
import com.vectormobile.myvector.storage.ImageManager;
import com.vectormobile.myvector.storage.PreferencesManager;
import com.vectormobile.myvector.storage.SessionManager;
import com.vectormobile.myvector.ui.rocketchat.RocketWebView;
import com.vectormobile.myvector.ui.outlook.agenda.OutlookAgendaWebView;
import com.vectormobile.myvector.ui.outlook.mail.OutlookMailWebView;
import com.vectormobile.myvector.util.AppResources;
import com.vectormobile.myvector.util.gson.GsonModule;
import dagger.Component;

/**
 * VectorDigital.
 *
 * @author -   AMarinaG
 * @since -   29/10/16
 */
@ForApplication
@Component(modules = {
    AppModule.class,
    AndroidModule.class,
    EnvironmentModule.class,
    GsonModule.class,
    OkHttpInterceptorsModule.class
})
public interface AppComponent {
  void inject(App app);

  @ForApplication
  App provideApp();

  @ForApplication
  SharedPreferences sharedPreferences();

  @ForApplication
  Context provideContext();

  AppResources provideAppResource();

  RocketWebView provideRocketWebView();

  OutlookMailWebView provideOutlookMailWebView();

  OutlookAgendaWebView provideOutlookAgendaWebView();

  SessionManager sessionManager();

  PreferencesManager preferenceManager();

  ImageManager imageManager();

  OfferingLoginInteractor provideOfferingLoginInteractor();

  GmsLoginInteractor provideGmsLoginInteractor();

  IntranetLoginInteractor provideIntranetLoginInteractor();

  OfferingMenuInteractor provideOfferingMenuInteractor();

  OfferingNewsInteractor provideOfferingNewsInteractor();

  NewsInteractor provideNewsInteractor();

  NewsItemDetailInteractor provideNewsItemDetailInteractor();

  ProfileDataInteractor provideProfileDataInteractor();

  RetrieveGmsNotificationsInteractor provideGmsRetrieveNotificationsInteractor();

  GetCurrenciesInteractor provideGetCurrenciesInteractor();

  BuildGmsBodyRequestInteractor provideGetImputableProjectsInteractor();

  GetWorkOrderInteractor provideGetWorkOrderInteractor();

  GetExpenseCategoriesInteractor provideGetExpenseCategoriesInteractor();

  SaveExpenseInteractor provideSaveExpenseInteractor();

  SaveExpenseWithImageInteractor provideSaveExpenseWithImageInteractor();

  GetReportCategoryInteractor provideGetReportCategoryInteractor();

  BuildGmsExpenseRequestInteractor provideBuildGmsExpenseRequestInteractor();

  BuildGmsTimeLapseExpenseRequestInteractor provideBuildGmsTimeLapseExpenseRequestInteractor();

  BuildGmsHourRequestInteractor provideBuildGmsHourRequestInteractor();

  BuildGmsRequestInteractor provideSendExpenseInteractor();

  ProfileSaveUserInfoInteractor provideProfileSaveUserInfoInteractor();

  SendProfileImageInteractor provideSendProfileImageInteractor();

  LogoutInteractor provideLogoutInteractor();

  ContactsListInteractor provideContactsListInteractor();

  BuildJiraBrowserBodyRequestInteractor provideBuildJiraBrowserBodyRequestInteractor();

  BuildGmsJiraRequestInteractor provideBuildGmsJiraRequestInteractor();

  SharedPdfImputationInteractor provideSharedPdfImputationInteractor();

  StartConversationInteractor provideStartConversationInteractor();

  GetConversationHistoryInteractor provideGetConversationHistoryInteractor();

  SendBFTextInteractor provideSendBfTextInteractor();

  BuildGmsDayStateRequestInteractor provideBuildGmsDayStateRequestInteractor();

  final class Initializer {
    public static AppComponent init(App app) {
      return DaggerAppComponent.builder()
          .androidModule(new AndroidModule())
          .appModule(new AppModule(app))
          .environmentModule(new EnvironmentModule(app))
          .gsonModule(new GsonModule())
          .okHttpInterceptorsModule(new OkHttpInterceptorsModule())
          .build();
    }
  }
}