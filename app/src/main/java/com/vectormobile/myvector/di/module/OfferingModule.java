package com.vectormobile.myvector.di.module;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.offering.OfferingActivity;
import com.vectormobile.myvector.ui.offering.detail.OfferingDetailFragment;
import com.vectormobile.myvector.ui.offering.detail.OfferingDetailView;
import com.vectormobile.myvector.ui.offering.menu.OfferingMenuFragment;
import com.vectormobile.myvector.ui.offering.menu.OfferingMenuView;
import com.vectormobile.myvector.ui.offering.news.OfferingNewsFragment;
import com.vectormobile.myvector.ui.offering.news.OfferingNewsView;
import dagger.Module;
import dagger.Provides;

/**
 * Created by ajruiz on 03/11/2016.
 */

@Module
@ActivityScope
public class OfferingModule extends ActivityModule<OfferingActivity> {
  private OfferingActivity activity;

  public OfferingModule(OfferingActivity activity) {
    super(activity);
    this.activity = activity;
  }

  @Provides
  OfferingActivity provideOfferingActivity() {
    return activity;
  }

  @Provides
  OfferingMenuView provideOfferingMenuView() {
    return new OfferingMenuFragment();
  }

  @Provides
  OfferingNewsView provideOfferingNewsView() {
    return new OfferingNewsFragment();
  }

  @Provides
  OfferingDetailView provideOfferingDetailView() {
    return new OfferingDetailFragment();
  }
}
