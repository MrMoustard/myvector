package com.vectormobile.myvector.di.module;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.gms.hour.detail.HourDetailActivity;
import dagger.Module;
import dagger.Provides;

/**
 * Created by ajruiz on 26/01/2017.
 */

@ActivityScope
@Module
public class HourDetailModule extends ActivityModule<HourDetailActivity> {

  private HourDetailActivity activity;

  public HourDetailModule(HourDetailActivity activity) {
    super(activity);
    this.activity = activity;
  }

  @Provides
  HourDetailActivity provideHoureDetailActivity() {
    return activity;
  }
}
