package com.vectormobile.myvector;

import android.app.Application;
import android.os.StrictMode;
import com.crashlytics.android.Crashlytics;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.vectormobile.myvector.di.component.AppComponent;
import com.vectormobile.myvector.environment.EnvironmentConfiguration;
import com.vectormobile.myvector.ui.base.BaseActivity;
import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

import javax.inject.Inject;

/**
 * Created by Erik Medina on 27/10/2016.
 */

public class App extends Application {

  @Inject
  EnvironmentConfiguration environment;

  private AppComponent appComponent;

  private BaseActivity currentActivity;
  private RefWatcher refWatcher;

  @Inject
  public App() {
  }

  @Override
  public void onCreate() {
    super.onCreate();
    setUpLeakCanary();
    Fabric.with(this, new Crashlytics());

    appComponent = createComponent();
    environment.configure();
    currentActivity = null;

//    if (BuildConfig.DEBUG) {
//      StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
//          .detectAll()
//          .penaltyLog()
//          .build());
//      StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
//          .detectAll()
//          .penaltyLog()
//          .build());
//    }
  }

  private void setUpLeakCanary() {
    if (LeakCanary.isInAnalyzerProcess(this)) {
      // This process is dedicated to LeakCanary for heap analysis.
      // You should not init your app in this process.
      return;
    }
    refWatcher = LeakCanary.install(this);
  }

  public void mustDie(Object object) {
    if (refWatcher != null)
      refWatcher.watch(object);
  }

  private AppComponent createComponent() {
    appComponent = AppComponent.Initializer.init(this);
    appComponent.inject(this);
    return appComponent;
  }

  public AppComponent getComponent() {
    if (appComponent == null) {
      createComponent();
    }
    return appComponent;
  }

  public void setAppComponent(AppComponent appComponent) {
    this.appComponent = appComponent;
  }

  public BaseActivity getCurrentActivity() {
    return currentActivity;
  }

  public void setCurrentActivity(BaseActivity currentActivity) {
    this.currentActivity = currentActivity;
  }
}
