package com.vectormobile.myvector.domain.entity.bot;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amarina on 22/03/2017.
 */

public class BFButton {
  @SerializedName("type")
  @Expose
  private String type;
  @SerializedName("title")
  @Expose
  private String title;
  @SerializedName("value")
  @Expose
  private String value;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "BFButton{" +
        "type='" + type + '\'' +
        ", title='" + title + '\'' +
        ", value='" + value + '\'' +
        '}';
  }
}
