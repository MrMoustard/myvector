package com.vectormobile.myvector.domain.entity.gms.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Erik Medina on 05/01/2017.
 */

public class GmsCurrencyRequest {

  @SerializedName("tipo")
  @Expose
  private String type;
  @SerializedName("fields")
  @Expose
  private List<String> fields = null;
  @SerializedName("page")
  @Expose
  private int page;
  @SerializedName("start")
  @Expose
  private int start;
  @SerializedName("limit")
  @Expose
  private int limit;

  public GmsCurrencyRequest() {
    type = "com.vectorsf.ngms.persistence.model.Moneda";
    List<String> fields = new ArrayList<>();
    fields.add("fractionDigits");
    fields.add("simbolo");
    this.fields = fields;
    page = 1;
    start = 0;
    limit = 25;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public List<String> getFields() {
    return fields;
  }

  public void setFields(List<String> fields) {
    this.fields = fields;
  }

  public int getPage() {
    return page;
  }

  public void setPage(int page) {
    this.page = page;
  }

  public int getStart() {
    return start;
  }

  public void setStart(int start) {
    this.start = start;
  }

  public int getLimit() {
    return limit;
  }

  public void setLimit(int limit) {
    this.limit = limit;
  }
}
