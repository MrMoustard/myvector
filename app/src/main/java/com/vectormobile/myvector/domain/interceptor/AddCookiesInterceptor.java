package com.vectormobile.myvector.domain.interceptor;

import com.vectormobile.myvector.storage.SessionManager;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 * Created by ajruiz on 29/12/2016.
 */
public class AddCookiesInterceptor implements Interceptor {
  private SessionManager sessionManager;

  public AddCookiesInterceptor(SessionManager sessionManager) {
    this.sessionManager = sessionManager;
  }

  @Override
  public Response intercept(Interceptor.Chain chain) throws IOException {
    Request request = chain.request();
    Request.Builder builder = sessionManager.addCookieIfNeeded(request);
    return chain.proceed(builder.build());
  }

}