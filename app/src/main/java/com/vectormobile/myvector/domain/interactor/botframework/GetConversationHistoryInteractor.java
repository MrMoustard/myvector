package com.vectormobile.myvector.domain.interactor.botframework;

import com.vectormobile.myvector.domain.interactor.botframework.listener.GetConversationHistoryListener;

/**
 * Created by amarina on 30/03/2017.
 */

public interface GetConversationHistoryInteractor {
  void execute(GetConversationHistoryListener getConversationHistoryListener);
}
