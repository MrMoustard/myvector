package com.vectormobile.myvector.domain.repository;

import com.vectormobile.myvector.domain.entity.bot.BFHistoryResponse;
import com.vectormobile.myvector.domain.entity.bot.BFMessageRequest;
import com.vectormobile.myvector.domain.entity.bot.BFMessageResponse;
import com.vectormobile.myvector.domain.entity.bot.BFUser;
import com.vectormobile.myvector.domain.entity.botframework.RsLoginResponse;
import com.vectormobile.myvector.domain.interactor.botframework.listener.GetConversationHistoryListener;
import com.vectormobile.myvector.domain.interactor.botframework.listener.StartConversationListener;
import com.vectormobile.myvector.domain.service.RoomService;
import com.vectormobile.myvector.storage.PreferencesManager;
import com.vectormobile.myvector.util.AppResources;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by amarina on 15/03/2017.
 */
public class BFRepositoryImpl implements BFRepository {
  private RoomService service;
  private AppResources appResources;
  private PreferencesManager preferencesManager;

  public BFRepositoryImpl(RoomService service, AppResources appResources, PreferencesManager preferencesManager) {
    this.service = service;
    this.appResources = appResources;
    this.preferencesManager = preferencesManager;
  }

  @Override
  public void startConversation(final StartConversationListener listener) {
    final String conversationId = preferencesManager.getConversacionId();
    Call<RsLoginResponse> call;
    if (conversationId != null) {
      call = service.reconectConversation("Bearer " + appResources.getMetaData(RoomService.PRIMARY_TOKEN_METADATA_NAME), conversationId);
    } else {
      call = service.startConversation("Bearer " + appResources.getMetaData(RoomService.PRIMARY_TOKEN_METADATA_NAME));
    }
    call.enqueue(new Callback<RsLoginResponse>() {
      @Override
      public void onResponse(Call<RsLoginResponse> call, Response<RsLoginResponse> response) {
        if (response.isSuccessful()) {
          preferencesManager.saveConversationId(response.body().getConversationId());
          listener.onSuccess(response.body());
        } else {
          listener.onError();
        }
      }

      @Override
      public void onFailure(Call<RsLoginResponse> call, Throwable t) {
        Timber.e(t, "Error en la petición: %s", t.getMessage());
      }
    });
  }

  @Override
  public void sendMessage(String message) {
    BFMessageRequest request = new BFMessageRequest();
    request.setText(message);
    BFUser sender = new BFUser();
    sender.setId(preferencesManager.getUsername());
    sender.setName(preferencesManager.getUsername());
    request.setFrom(sender);
    String authentication = "Bearer " + appResources.getMetaData(RoomService.PRIMARY_TOKEN_METADATA_NAME);
    String conversationId = preferencesManager.getConversacionId();
    Call<BFMessageResponse> call = service.sendMessage(authentication, conversationId, request);
    call.enqueue(new Callback<BFMessageResponse>() {
      @Override
      public void onResponse(Call<BFMessageResponse> call, Response<BFMessageResponse> response) {
        if (response.isSuccessful()) {
          Timber.d("no queremos hacer nada con esto, de momento");
        }
      }

      @Override
      public void onFailure(Call<BFMessageResponse> call, Throwable t) {
        Timber.e(t, "Error de conexión: %s", t.getMessage());
      }
    });
  }

  @Override
  public void getHistory(final GetConversationHistoryListener getConversationHistoryListener) {
    String authentication = "Bearer " + appResources.getMetaData(RoomService.PRIMARY_TOKEN_METADATA_NAME);
    String conversationId = preferencesManager.getConversacionId();
    Call<BFHistoryResponse> call = service.getHistoryByConversationId(authentication, conversationId);
    call.enqueue(new Callback<BFHistoryResponse>() {
      @Override
      public void onResponse(Call<BFHistoryResponse> call, Response<BFHistoryResponse> response) {
        Timber.d("La respuesta buena: %s", response.body());
        getConversationHistoryListener.onSuccess(response.body());
      }

      @Override
      public void onFailure(Call<BFHistoryResponse> call, Throwable t) {

      }
    });
  }
}
