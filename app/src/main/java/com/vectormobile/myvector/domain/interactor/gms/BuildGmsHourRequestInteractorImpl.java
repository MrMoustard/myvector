package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.entity.gms.request.GmsTimeLapseHourBodyRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.domain.repository.GmsRepository;

/**
 * Created by ajruiz on 24/01/2017.
 */

public class BuildGmsHourRequestInteractorImpl implements BuildGmsHourRequestInteractor {

  private GmsRepository gmsRepository;

  public BuildGmsHourRequestInteractorImpl(GmsRepository gmsRepository) {
    this.gmsRepository = gmsRepository;
  }

  @Override
  public void execute(String action, String method, GmsTimeLapseHourBodyRequest gmsHourBodyRequest, OnListRetrievedListener<GmsResponse> listener) {
    gmsRepository.buildGmsHourRequest(action, method, gmsHourBodyRequest, listener);
  }
}
