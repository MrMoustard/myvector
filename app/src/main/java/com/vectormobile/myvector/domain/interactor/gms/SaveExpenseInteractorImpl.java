package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.entity.gms.request.ExpenseRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.domain.repository.GmsRepository;

/**
 * Created by Erik Medina on 10/01/2017.
 */
public class SaveExpenseInteractorImpl implements SaveExpenseInteractor {

  private GmsRepository gmsRepository;

  public SaveExpenseInteractorImpl(GmsRepository gmsRepository) {
    this.gmsRepository = gmsRepository;
  }

  @Override
  public void execute(ExpenseRequest expenseRequest, String action, String method, OnListRetrievedListener<GmsResponse> listener) {
    gmsRepository.saveExpense(expenseRequest, action, method, listener);
  }
}
