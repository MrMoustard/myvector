package com.vectormobile.myvector.domain.entity.bot;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amarina on 14/03/2017.
 */

public class BFUser {
  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("name")
  @Expose
  private String name;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "BFUser{" +
        "id='" + id + '\'' +
        ", name='" + name + '\'' +
        '}';
  }
}
