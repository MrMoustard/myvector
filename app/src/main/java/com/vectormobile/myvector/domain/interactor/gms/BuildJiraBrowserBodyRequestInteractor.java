package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.entity.gms.request.GmsJiraBrowserRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;

/**
 * Created by ajruiz on 01/03/2017.
 */

public interface BuildJiraBrowserBodyRequestInteractor {

  void execute(String action, String method, GmsJiraBrowserRequest gmsJiraBrowserRequest,
               OnListRetrievedListener<GmsResponse> listener);

}
