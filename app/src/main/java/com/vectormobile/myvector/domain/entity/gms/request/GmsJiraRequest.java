package com.vectormobile.myvector.domain.entity.gms.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Erik Medina on 04/01/2017.
 */

public class GmsJiraRequest {

  @SerializedName("action")
  @Expose
  private String action;
  @SerializedName("method")
  @Expose
  private String method;
  @SerializedName("data")
  @Expose
  private List<String> data = null;
  @SerializedName("type")
  private String type;
  @SerializedName("tid")
  private int tid;

  public GmsJiraRequest() {
    data = new ArrayList<>();
    type = "rpc";
    action = "imputacionHoraController";
    method = "codigosJira";
  }

  public String getAction() {
    return action;
  }

  public List<String> getData() {
    return data;
  }

  public String getMethod() {
    return method;
  }

  public int getTid() {
    return tid;
  }

  public void setTid(int tid) {
    this.tid = tid;
  }

  public String getType() {
    return type;
  }

  @Override
  public String toString() {
    return "GmsJiraRequest{" +
        "action='" + action + '\'' +
        ", method='" + method + '\'' +
        ", data=" + data +
        ", type='" + type + '\'' +
        ", tid=" + tid +
        '}';
  }
}