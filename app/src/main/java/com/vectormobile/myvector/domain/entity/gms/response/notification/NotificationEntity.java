package com.vectormobile.myvector.domain.entity.gms.response.notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.internal.LinkedTreeMap;

/**
 * Created by ajruiz on 30/12/2016.
 */

public class NotificationEntity {

  @SerializedName("id")
  @Expose
  private Double id;
  @SerializedName("fechaEmision")
  @Expose
  private String dateOfIssue;
  @SerializedName("estado")
  @Expose
  private String status;
  @SerializedName("estadoText")
  @Expose
  private String statusText;
  @SerializedName("origen")
  @Expose
  private String origin;
  @SerializedName("entradilla")
  @Expose
  private String leadIn;
  @SerializedName("titulo")
  @Expose
  private String title;
  @SerializedName("carpeta")
  @Expose
  private String folder;
  @SerializedName("carpetaText")
  @Expose
  private String folderText;
  @SerializedName("envioMail")
  @Expose
  private Boolean sendMail;
  @SerializedName("envioBuzon")
  @Expose
  private Boolean sendMailBox;
  Response response = new Response();

  public NotificationEntity(LinkedTreeMap linkedTreeMap) {
    id = (Double) linkedTreeMap.get("id");
    dateOfIssue = (String) linkedTreeMap.get("fechaEmision");
    status = (String) linkedTreeMap.get("estado");
    statusText = (String) linkedTreeMap.get("estadoText");
    origin = (String) linkedTreeMap.get("origen");
    leadIn = (String) linkedTreeMap.get("entradilla");
    title = (String) linkedTreeMap.get("titulo");
    folder = (String) linkedTreeMap.get("carpeta");
    folderText = (String) linkedTreeMap.get("carpetaText");
    sendMail = (Boolean) linkedTreeMap.get("envioMail");
    sendMailBox = (Boolean) linkedTreeMap.get("envioBuzon");

    response.id = (Double) linkedTreeMap.get("id");
    response.dateOfIssue = (String) linkedTreeMap.get("fechaEmision");
    response.status = (String) linkedTreeMap.get("estado");
    response.statusText = (String) linkedTreeMap.get("estadoText");
    response.origin = (String) linkedTreeMap.get("origen");
    response.leadIn = (String) linkedTreeMap.get("entradilla");
    response.title = (String) linkedTreeMap.get("titulo");
    response.folder = (String) linkedTreeMap.get("carpeta");
    response.folderText = (String) linkedTreeMap.get("carpetaText");
    response.sendMail = (Boolean) linkedTreeMap.get("envioMail");
    response.sendMailBox = (Boolean) linkedTreeMap.get("envioBuzon");
  }

  public static class Request {
    public Double id;
    public String dateOfIssue;
    public String origin;
    public String leadIn;
    public String title;
  }

  public static class Response {
    public Double id;
    public String dateOfIssue;
    public String status;
    public String statusText;
    public String origin;
    public String leadIn;
    public String title;
    public String folder;
    public String folderText;
    public Boolean sendMail;
    public Boolean sendMailBox;
  }

  public Response getResponse() {
    return response;
  }

  public Double getId() {
    return id;
  }

  public void setId(Double id) {
    this.id = id;
  }

  public String getDateOfIssue() {
    return dateOfIssue;
  }

  public void setDateOfIssue(String dateOfIssue) {
    this.dateOfIssue = dateOfIssue;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getStatusText() {
    return statusText;
  }

  public void setStatusText(String statusText) {
    this.statusText = statusText;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public String getLeadIn() {
    return leadIn;
  }

  public void setLeadIn(String leadIn) {
    this.leadIn = leadIn;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getFolder() {
    return folder;
  }

  public void setFolder(String folder) {
    this.folder = folder;
  }

  public String getFolderText() {
    return folderText;
  }

  public void setFolderText(String folderText) {
    this.folderText = folderText;
  }

  public Boolean getSendMail() {
    return sendMail;
  }

  public void setSendMail(Boolean sendMail) {
    this.sendMail = sendMail;
  }

  public Boolean getSendMailBox() {
    return sendMailBox;
  }

  public void setSendMailBox(Boolean sendMailBox) {
    this.sendMailBox = sendMailBox;
  }

}
