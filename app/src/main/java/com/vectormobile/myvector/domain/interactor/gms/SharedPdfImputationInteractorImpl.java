package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.interactor.listener.DownloadPdfListener;
import com.vectormobile.myvector.domain.repository.GmsRepository;

import java.util.List;

/**
 * Created by amarina on 06/03/2017.
 */
public class SharedPdfImputationInteractorImpl implements SharedPdfImputationInteractor {

  private GmsRepository gmsRepository;

  public SharedPdfImputationInteractorImpl(GmsRepository gmsRepository) {
    this.gmsRepository = gmsRepository;
  }

  @Override
  public void execute(List<String> imputationsIds, DownloadPdfListener downloadPdfListener) {
    gmsRepository.downloadPdfImputation(imputationsIds, downloadPdfListener);
  }
}
