package com.vectormobile.myvector.domain.entity.gms.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.vectormobile.myvector.util.date.DateUtils;

import java.util.Calendar;

/**
 * Created by Erik Medina on 09/01/2017.
 */

public class GmsWorkOrderRequest {

  @SerializedName("imputacionId")
  @Expose
  private Object imputationId;
  @SerializedName("proyectoId")
  @Expose
  private String projectId;
  @SerializedName("fecha")
  @Expose
  private String date;
  @SerializedName("page")
  @Expose
  private int page;
  @SerializedName("start")
  @Expose
  private int start;
  @SerializedName("limit")
  @Expose
  private int limit;

  public GmsWorkOrderRequest() {
    this.imputationId = null;
    Calendar calendar = Calendar.getInstance();
    this.date = DateUtils.formatDateToGmsLargeString(calendar.getTime());
    this.page = 1;
    this.start = 0;
    this.limit = 25;
  }

  public Object getImputationId() {
    return imputationId;
  }

  public void setImputationId(Object imputationId) {
    this.imputationId = imputationId;
  }

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public int getPage() {
    return page;
  }

  public void setPage(int page) {
    this.page = page;
  }

  public int getStart() {
    return start;
  }

  public void setStart(int start) {
    this.start = start;
  }

  public int getLimit() {
    return limit;
  }

  public void setLimit(int limit) {
    this.limit = limit;
  }
}
