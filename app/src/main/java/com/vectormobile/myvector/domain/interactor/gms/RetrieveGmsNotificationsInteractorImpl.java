package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.domain.repository.GmsRepository;
import com.vectormobile.myvector.model.gms.GmsNotification;

/**
 * Created by ajruiz on 30/12/2016.
 */

public class RetrieveGmsNotificationsInteractorImpl implements RetrieveGmsNotificationsInteractor {

  private GmsRepository gmsRepository;

  public RetrieveGmsNotificationsInteractorImpl(GmsRepository gmsRepository) {
    this.gmsRepository = gmsRepository;
  }

  @Override
  public void execute(OnListRetrievedListener<GmsNotification> listener) {
    gmsRepository.retrieveGmsNotifications(listener);
  }

}
