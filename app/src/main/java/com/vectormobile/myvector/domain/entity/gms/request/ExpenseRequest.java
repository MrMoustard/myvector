package com.vectormobile.myvector.domain.entity.gms.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Erik Medina on 04/01/2017.
 */
public class ExpenseRequest {

  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("tipoAltaImputacion")
  @Expose
  private String imputationType;
  @SerializedName("fechaGasto")
  @Expose
  private String expenseDate;
  @SerializedName("fechaDesde")
  @Expose
  private String dateFrom;
  @SerializedName("fechaHasta")
  @Expose
  private String dateTo;
  @SerializedName("fecha")
  @Expose
  private String date;
  @SerializedName("idProyecto")
  @Expose
  private String projectId;
  @SerializedName("idCategoriaVenta")
  @Expose
  private String saleCategoryId;
  @SerializedName("precioUnitario")
  @Expose
  private String unitPrice;
  @SerializedName("monedaId")
  @Expose
  private String currencyId;
  @SerializedName("idOrdenTrabajo")
  @Expose
  private String workOrderId;
  @SerializedName("idCategoriaReporte")
  @Expose
  private String reportCategoryId;
  @SerializedName("unidades")
  @Expose
  private String units;
  @SerializedName("comentarioImputador")
  @Expose
  private String imputationComment;
  @SerializedName("comentarioAprobador")
  @Expose
  private String approverComment;


  public ExpenseRequest() {
    this.approverComment = "";
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getImputationType() {
    return imputationType;
  }

  public void setImputationType(String imputationType) {
    this.imputationType = imputationType;
  }

  public String getExpenseDate() {
    return expenseDate;
  }

  public void setExpenseDate(String expenseDate) {
    this.expenseDate = expenseDate;
  }

  public String getDateFrom() {
    return dateFrom;
  }

  public void setDateFrom(String dateFrom) {
    this.dateFrom = dateFrom;
  }

  public String getDateTo() {
    return dateTo;
  }

  public void setDateTo(String dateTo) {
    this.dateTo = dateTo;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public String getSaleCategoryId() {
    return saleCategoryId;
  }

  public void setSaleCategoryId(String saleCategoryId) {
    this.saleCategoryId = saleCategoryId;
  }

  public String getUnitPrice() {
    return unitPrice;
  }

  public void setUnitPrice(String unitPrice) {
    this.unitPrice = unitPrice;
  }

  public String getCurrencyId() {
    return currencyId;
  }

  public void setCurrencyId(String currencyId) {
    this.currencyId = currencyId;
  }

  public String getWorkOrderId() {
    return workOrderId;
  }

  public void setWorkOrderId(String workOrderId) {
    this.workOrderId = workOrderId;
  }

  public String getReportCategoryId() {
    return reportCategoryId;
  }

  public void setReportCategoryId(String reportCategoryId) {
    this.reportCategoryId = reportCategoryId;
  }

  public String getUnits() {
    return units;
  }

  public void setUnits(String units) {
    this.units = units;
  }

  public String getImputationComment() {
    return imputationComment;
  }

  public void setImputationComment(String imputationComment) {
    this.imputationComment = imputationComment;
  }

  public String getApproverComment() {
    return approverComment;
  }

  public void setApproverComment(String approverComment) {
    this.approverComment = approverComment;
  }

  @Override
  public String toString() {
    return "ExpenseRequest{" +
        "imputationType='" + imputationType + '\'' +
        ", expenseDate='" + expenseDate + '\'' +
        ", date='" + date + '\'' +
        ", projectId='" + projectId + '\'' +
        ", saleCategoryId='" + saleCategoryId + '\'' +
        ", unitPrice='" + unitPrice + '\'' +
        ", currencyId='" + currencyId + '\'' +
        ", workOrderId='" + workOrderId + '\'' +
        ", reportCategoryId='" + reportCategoryId + '\'' +
        ", units='" + units + '\'' +
        ", imputationComment='" + imputationComment + '\'' +
        ", approverComment='" + approverComment + '\'' +
        '}';
  }
}
