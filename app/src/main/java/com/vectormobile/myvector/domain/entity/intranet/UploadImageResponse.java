package com.vectormobile.myvector.domain.entity.intranet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amarina on 02/02/2017.
 */

public class UploadImageResponse {
  @SerializedName("type")
  @Expose
  private String type;
  @SerializedName("msg")
  @Expose
  private String msg;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public boolean isSuccess() {
    return "success".equals(type);
  }

  @Override
  public String toString() {
    return "UploadImageResponse{" +
        "type='" + type + '\'' +
        ", msg='" + msg + '\'' +
        '}';
  }
}
