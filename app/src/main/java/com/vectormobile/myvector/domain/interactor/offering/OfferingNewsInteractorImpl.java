package com.vectormobile.myvector.domain.interactor.offering;

import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.domain.repository.TrendsRepository;

/**
 * Created by ajruiz on 18/11/2016.
 */

public class OfferingNewsInteractorImpl implements OfferingNewsInteractor {

  private TrendsRepository repository;

  public OfferingNewsInteractorImpl(TrendsRepository repository) {
    this.repository = repository;
  }

  @Override
  public void execute(int id, int page, int postsPerPage, OnListRetrievedListener listener) {
    repository.getOfferingNews(id, page, postsPerPage, listener);
  }
}
