package com.vectormobile.myvector.domain.interactor.botframework;

import com.vectormobile.myvector.domain.repository.BFRepository;

/**
 * Created by amarina on 22/03/2017.
 */
public class SendBFTextInteractorImpl implements SendBFTextInteractor {
  private BFRepository bfRepository;

  public SendBFTextInteractorImpl(BFRepository bfRepository) {
    this.bfRepository = bfRepository;
  }

  @Override
  public void execute(String message) {
    bfRepository.sendMessage(message);
  }
}
