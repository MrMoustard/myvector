package com.vectormobile.myvector.domain.entity.bot.converter;

import com.vectormobile.myvector.domain.entity.bot.BFAttachment;
import com.vectormobile.myvector.domain.entity.bot.BFResponse;
import com.vectormobile.myvector.model.bot.Message;
import timber.log.Timber;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by amarina on 14/03/2017.
 */

public abstract class MessageConverter {
  public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());

  public static Message wrap(BFResponse model) {
    Message entity = new Message();
    entity.setId(model.getReplyToId());
    List<BFAttachment> attachment = model.getAttachments();
    if (attachment != null && attachment.size() > 0) {
      entity.setText(attachment.get(0).getContent().getText());
      entity.setButtons(attachment.get(0).getContent().getButtons());
    } else {
      entity.setText(model.getText());
    }
    entity.setUser(UserConverter.wrap(model.getFrom()));
    try {
      entity.setCreatedAt(sdf.parse(model.getTimestamp()));
    } catch (ParseException e) {
      entity.setCreatedAt(null);
      Timber.e(e, "Error de parseo: %s", e.getMessage());
    }
    return entity;
  }
}
