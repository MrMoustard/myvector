package com.vectormobile.myvector.domain.interactor.offering;

import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.domain.repository.TrendsRepository;

/**
 * Created by ajruiz on 15/11/2016.
 */

public class OfferingMenuInteractorImpl implements OfferingMenuInteractor {

  private TrendsRepository repository;

  public OfferingMenuInteractorImpl(TrendsRepository repository) {
    this.repository = repository;
  }

  @Override
  public void execute(OnItemRetrievedListener listener) {
    repository.buildOfferingMenu(listener);
  }
}
