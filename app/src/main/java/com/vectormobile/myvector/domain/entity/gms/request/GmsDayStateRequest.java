package com.vectormobile.myvector.domain.entity.gms.request;

import java.util.ArrayList;

/**
 * Created by kurt on 23/03/17.
 */

public class GmsDayStateRequest extends GmsRequest<String> {

  public GmsDayStateRequest(String starts, String ends) {
    action = "calendarioImputacionController";
    method = "getCalendarioUsuario";
    data = new ArrayList<>();
    type = "rpc";

    buildData(starts, ends);
  }

  private void buildData(String starts, String ends) {
    data.add(starts);
    data.add(ends);
    data.add(null);
  }
}
