package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.entity.gms.request.GmsRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.domain.repository.GmsRepository;

/**
 * Created by Erik Medina on 25/01/2017.
 */
public class BuildGmsRequestInteractorImpl implements BuildGmsRequestInteractor {

  private GmsRepository gmsRepository;

  public BuildGmsRequestInteractorImpl(GmsRepository gmsRepository) {
    this.gmsRepository = gmsRepository;
  }

  @Override
  public void execute(GmsRequest gmsRequest, OnListRetrievedListener<GmsResponse> listener) {
    gmsRepository.sendExpense(gmsRequest, listener);
  }
}
