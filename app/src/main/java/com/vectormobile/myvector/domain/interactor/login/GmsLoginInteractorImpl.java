package com.vectormobile.myvector.domain.interactor.login;

import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.domain.repository.GmsRepository;

/**
 * Created by ajruiz on 29/12/2016.
 */

public class GmsLoginInteractorImpl implements GmsLoginInteractor {

  private GmsRepository gmsRepository;

  public GmsLoginInteractorImpl(GmsRepository gmsRepository) {
    this.gmsRepository = gmsRepository;
  }

  @Override
  public void execute(String username, String password, OnItemRetrievedListener listener) {
    gmsRepository.validateLogin(username, password, listener);
  }

}
