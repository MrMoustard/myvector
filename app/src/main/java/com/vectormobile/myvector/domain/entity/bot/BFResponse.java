package com.vectormobile.myvector.domain.entity.bot;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by amarina on 14/03/2017.
 */

public class BFResponse {
  @SerializedName("type")
  @Expose
  private String type;
  @SerializedName("timestamp")
  @Expose
  private String timestamp;
  @SerializedName("serviceUrl")
  @Expose
  private String serviceUrl;
  @SerializedName("channelId")
  @Expose
  private String channelId;
  @SerializedName("from")
  @Expose
  private BFUser from;
  @SerializedName("conversation")
  @Expose
  private BFConversation conversation;
  @SerializedName("recipient")
  @Expose
  private BFUser recipient;
  @SerializedName("text")
  @Expose
  private String text;
  @SerializedName("attachments")
  @Expose
  private List<BFAttachment> attachments = null;
  @SerializedName("entities")
  @Expose
  private List<Object> entities = null;
  @SerializedName("replyToId")
  @Expose
  private String replyToId;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public String getServiceUrl() {
    return serviceUrl;
  }

  public void setServiceUrl(String serviceUrl) {
    this.serviceUrl = serviceUrl;
  }

  public String getChannelId() {
    return channelId;
  }

  public void setChannelId(String channelId) {
    this.channelId = channelId;
  }

  public BFUser getFrom() {
    return from;
  }

  public void setFrom(BFUser from) {
    this.from = from;
  }

  public BFConversation getConversation() {
    return conversation;
  }

  public void setConversation(BFConversation conversation) {
    this.conversation = conversation;
  }

  public BFUser getRecipient() {
    return recipient;
  }

  public void setRecipient(BFUser recipient) {
    this.recipient = recipient;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public List<BFAttachment> getAttachments() {
    return attachments;
  }

  public void setAttachments(List<BFAttachment> attachments) {
    this.attachments = attachments;
  }

  public List<Object> getEntities() {
    return entities;
  }

  public void setEntities(List<Object> entities) {
    this.entities = entities;
  }

  public String getReplyToId() {
    return replyToId;
  }

  public void setReplyToId(String replyToId) {
    this.replyToId = replyToId;
  }

  @Override
  public String toString() {
    return "BFResponse{" +
        "type='" + type + '\'' +
        ", timestamp='" + timestamp + '\'' +
        ", serviceUrl='" + serviceUrl + '\'' +
        ", channelId='" + channelId + '\'' +
        ", from=" + from +
        ", conversation=" + conversation +
        ", recipient=" + recipient +
        ", text='" + text + '\'' +
        ", attachments=" + attachments +
        ", entities=" + entities +
        ", replyToId='" + replyToId + '\'' +
        '}';
  }
}

