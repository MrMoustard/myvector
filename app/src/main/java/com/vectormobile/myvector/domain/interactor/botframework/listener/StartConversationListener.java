package com.vectormobile.myvector.domain.interactor.botframework.listener;

import com.vectormobile.myvector.domain.entity.botframework.RsLoginResponse;

/**
 * Created by amarina on 15/03/2017.
 */
public interface StartConversationListener {
  void onError();

  void onSuccess(RsLoginResponse body);
}
