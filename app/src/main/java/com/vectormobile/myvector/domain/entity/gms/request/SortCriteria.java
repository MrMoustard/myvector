package com.vectormobile.myvector.domain.entity.gms.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ajruiz on 30/12/2016.
 */

public class SortCriteria {

  @SerializedName("page")
  @Expose
  private Integer page;
  @SerializedName("start")
  @Expose
  private Integer start;
  @SerializedName("limit")
  @Expose
  private Integer limit;
  @SerializedName("sort")
  @Expose
  private List<Sort> sort = null;

  public Integer getPage() {
    return page;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  public Integer getStart() {
    return start;
  }

  public void setStart(Integer start) {
    this.start = start;
  }

  public Integer getLimit() {
    return limit;
  }

  public void setLimit(Integer limit) {
    this.limit = limit;
  }

  public List<Sort> getSort() {
    return sort;
  }

  public void setSort(List<Sort> sort) {
    this.sort = sort;
  }

}