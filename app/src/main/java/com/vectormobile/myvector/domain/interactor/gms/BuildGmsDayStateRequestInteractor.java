package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.entity.gms.request.GmsDayStateRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsDayStateResult;
import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;

/**
 * Created by kurt on 23/03/17.
 */

public interface BuildGmsDayStateRequestInteractor {

  void execute(GmsDayStateRequest gmsBodyRequest, OnItemRetrievedListener<GmsDayStateResult> listener);
}
