package com.vectormobile.myvector.domain.interactor.listener;

import java.util.List;

/**
 * Created by amarina on 06/03/2017.
 */

public interface DownloadPdfListener {
  void onNetworkError();

  void onSavePdfError();

  void onSuccess(List<String> expensesId);
}
