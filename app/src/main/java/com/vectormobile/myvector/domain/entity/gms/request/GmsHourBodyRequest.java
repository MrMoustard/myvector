package com.vectormobile.myvector.domain.entity.gms.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ajruiz on 25/01/2017.
 */

public class GmsHourBodyRequest {

  @SerializedName("check")
  @Expose
  public boolean check;
  @SerializedName("page")
  @Expose
  public int page;
  @SerializedName("start")
  @Expose
  public int start;
  @SerializedName("limit")
  @Expose
  public int limit;
  @SerializedName("proyecto")
  @Expose
  public Object project;
  @SerializedName("ordenTrabajo")
  @Expose
  public List<Object> workOrder = null;
  @SerializedName("fechaDesde")
  @Expose
  public String startDate;
  @SerializedName("estadoId")
  @Expose
  public List<Object> status = null;
  @SerializedName("fechaHasta")
  @Expose
  public String endDate;
  @SerializedName("group")
  @Expose
  public List<Group> group = null;
  @SerializedName("sort")
  @Expose
  public List<Sort> sort = null;

  public GmsHourBodyRequest() {
    this.page = 1;
    this.start = 0;
    this.limit = 75; //TODO Alf pendiente paginado
  }

  public boolean isCheck() {
    return check;
  }

  public void setCheck(boolean check) {
    this.check = check;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public List<Group> getGroup() {
    return group;
  }

  public void setGroup(List<Group> group) {
    this.group = group;
  }

  public int getLimit() {
    return limit;
  }

  public void setLimit(int limit) {
    this.limit = limit;
  }

  public int getPage() {
    return page;
  }

  public void setPage(int page) {
    this.page = page;
  }

  public Object getProject() {
    return project;
  }

  public void setProject(Object project) {
    this.project = project;
  }

  public List<Sort> getSort() {
    return sort;
  }

  public void setSort(List<Sort> sort) {
    this.sort = sort;
  }

  public int getStart() {
    return start;
  }

  public void setStart(int start) {
    this.start = start;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public List<Object> getStatus() {
    return status;
  }

  public void setStatus(List<Object> status) {
    this.status = status;
  }

  public List<Object> getWorkOrder() {
    return workOrder;
  }

  public void setWorkOrder(List<Object> workOrder) {
    this.workOrder = workOrder;
  }
}
