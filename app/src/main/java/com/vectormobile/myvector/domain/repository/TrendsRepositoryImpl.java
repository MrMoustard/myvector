package com.vectormobile.myvector.domain.repository;

import com.vectormobile.myvector.R;
import com.vectormobile.myvector.domain.entity.offering.AuthenticationResponse;
import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.domain.service.TrendsService;
import com.vectormobile.myvector.model.PostAbout;
import com.vectormobile.myvector.model.offering.SectionMenu;
import com.vectormobile.myvector.storage.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;

public class TrendsRepositoryImpl implements TrendsRepository {

  public static final int POSTS_PER_PAGE = 19;
  private static final String STATUS_OK = "ok";

  private SessionManager sessionManager;

  private TrendsService service;

  public TrendsRepositoryImpl(TrendsService service, SessionManager sessionManager) {
    this.sessionManager = sessionManager;
    this.service = service;
  }

  @Override
  public void validateLogin(String email, String password, final OnItemRetrievedListener listener) {
    Call<AuthenticationResponse> call = service.userLogin(email, password);
    call.enqueue(new Callback<AuthenticationResponse>() {
      @Override
      public void onResponse(Call<AuthenticationResponse> call, Response<AuthenticationResponse> response) {
        if (response.isSuccessful()) {
          if (response.body().getStatus().equals(STATUS_OK)) {
            listener.onSuccess(response.body());
          } else {
            listener.onError(R.string.login_wrong_username_or_password);
          }

        } else {
          listener.onError(R.string.login_unsuccessful_authentication);
        }
      }

      @Override
      public void onFailure(Call<AuthenticationResponse> call, Throwable t) {
        listener.onError(R.string.error_trends_login_failure);
      }
    });
  }

  @Override
  public void buildOfferingMenu(final OnItemRetrievedListener listener) {
    Call<SectionMenu> call = service.getSectionMenuList();
    call.enqueue(new Callback<SectionMenu>() {
      @Override
      public void onResponse(Call<SectionMenu> call, Response<SectionMenu> response) {
        if (response.isSuccessful()) {
          listener.onSuccess(response.body());
        } else {
          listener.onError(R.string.error_offering_menu_failure);
        }
      }

      @Override
      public void onFailure(Call<SectionMenu> call, Throwable t) {
        listener.onError(R.string.error_offering_menu_failure);
      }
    });
  }

  @Override
  public void getOfferingNews(int id, int page, int postsPerPage, final OnListRetrievedListener listener) {
    Call<List<PostAbout>> call = service.getOfferingNewsById(id, page, postsPerPage);
    call.enqueue(new Callback<List<PostAbout>>() {
      @Override
      public void onResponse(Call<List<PostAbout>> call, Response<List<PostAbout>> response) {
        listener.onSuccess(response.body());
      }

      @Override
      public void onFailure(Call<List<PostAbout>> call, Throwable t) {
        listener.onError(R.string.error_offering_news_failure);
      }
    });
  }

}
