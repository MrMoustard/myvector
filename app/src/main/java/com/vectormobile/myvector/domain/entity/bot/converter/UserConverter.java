package com.vectormobile.myvector.domain.entity.bot.converter;

import com.vectormobile.myvector.domain.entity.bot.BFUser;
import com.vectormobile.myvector.model.bot.User;

/**
 * Created by amarina on 14/03/2017.
 */
public abstract class UserConverter {

  public static User wrap(BFUser entity) {
    User model = new User();
    model.setId(entity.getId());
    model.setName(entity.getName());
    return model;
  }

  public static BFUser unwrap(User model) {
    BFUser entity = new BFUser();
    model.setId(entity.getId());
    model.setName(entity.getName());
    return entity;
  }
}
