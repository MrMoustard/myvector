package com.vectormobile.myvector.domain.interactor.login;

/**
 * Created by amarina on 02/02/2017.
 */
public interface LogoutInteractor {
  void execute();
}
