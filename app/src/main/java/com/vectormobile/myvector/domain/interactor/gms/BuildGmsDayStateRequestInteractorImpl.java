package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.entity.gms.request.GmsDayStateRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsDayStateResult;
import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.domain.repository.GmsRepository;

/**
 * Created by kurt on 23/03/17.
 */

public class BuildGmsDayStateRequestInteractorImpl implements BuildGmsDayStateRequestInteractor {

  private GmsRepository gmsRepository;

  public BuildGmsDayStateRequestInteractorImpl(GmsRepository gmsRepository) {
    this.gmsRepository = gmsRepository;
  }

  @Override
  public void execute(GmsDayStateRequest bodyRequest, OnItemRetrievedListener<GmsDayStateResult> listener) {
    gmsRepository.getMonthDaysState(bodyRequest, listener);
  }
}
