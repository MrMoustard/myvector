package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.entity.gms.request.ExpenseRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;

/**
 * Created by Erik Medina on 10/01/2017.
 */

public interface SaveExpenseInteractor {

  void execute(ExpenseRequest expenseRequest, String action, String method, OnListRetrievedListener<GmsResponse> listener);
}
