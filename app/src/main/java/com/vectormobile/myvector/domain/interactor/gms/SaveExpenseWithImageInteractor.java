package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.entity.gms.request.ExpenseRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsFindExpenseByCreateTimeRequest;
import com.vectormobile.myvector.domain.interactor.listener.MultiActionGmsListener;

/**
 * Created by Erik Medina on 10/01/2017.
 */

public interface SaveExpenseWithImageInteractor {
  void execute(ExpenseRequest expenseRequest, GmsFindExpenseByCreateTimeRequest countRequest, String expenseAction, String expenseMethod, String countAction, MultiActionGmsListener multiActionGmsListener);
}
