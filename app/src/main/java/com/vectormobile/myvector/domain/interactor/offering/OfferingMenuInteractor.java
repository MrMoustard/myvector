package com.vectormobile.myvector.domain.interactor.offering;

import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;

/**
 * Created by ajruiz on 15/11/2016.
 */

public interface OfferingMenuInteractor {

  void execute(OnItemRetrievedListener listener);

}
