package com.vectormobile.myvector.domain.interactor.login;

import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;

/**
 * Created by ajruiz on 30/12/2016.
 */

public interface IntranetLoginInteractor {

  void execute(String username, String password, OnItemRetrievedListener listener);

}
