package com.vectormobile.myvector.domain.entity.gms.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Erik Medina on 05/01/2017.
 */

public class GmsResponse {

  @SerializedName("type")
  @Expose
  public String type;
  @SerializedName("tid")
  @Expose
  public int tid;
  @SerializedName("action")
  @Expose
  public String action;
  @SerializedName("method")
  @Expose
  public String method;
  @SerializedName("result")
  @Expose
  public Result result;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public int getTid() {
    return tid;
  }

  public void setTid(int tid) {
    this.tid = tid;
  }

  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public String getMethod() {
    return method;
  }

  public void setMethod(String method) {
    this.method = method;
  }

  public Result getResult() {
    return result;
  }

  public void setResult(Result result) {
    this.result = result;
  }
}
