package com.vectormobile.myvector.domain.interactor.intranet;

import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;

/**
 * Created by ajruiz on 21/12/2016.
 */

public interface NewsItemDetailInteractor {

  void execute(String id, OnItemRetrievedListener listener);

}
