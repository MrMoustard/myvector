package com.vectormobile.myvector.domain.entity.intranet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ajruiz on 01/02/2017.
 */

public class ContactsResponse {

  @SerializedName("html_results")
  @Expose
  public String htmlResults;
  @SerializedName("count")
  @Expose
  public int count;
  @SerializedName("total")
  @Expose
  public int total;
  @SerializedName("departments")
  @Expose
  public List<String> departments = null;
  @SerializedName("cities")
  @Expose
  public List<String> cities = null;

  public List<String> getCities() {
    return cities;
  }

  public void setCities(List<String> cities) {
    this.cities = cities;
  }

  public int getCount() {
    return count;
  }

  public void setCount(int count) {
    this.count = count;
  }

  public List<String> getDepartments() {
    return departments;
  }

  public void setDepartments(List<String> departments) {
    this.departments = departments;
  }

  public String getHtmlResults() {
    return htmlResults;
  }

  public void setHtmlResults(String htmlResults) {
    this.htmlResults = htmlResults;
  }

  public int getTotal() {
    return total;
  }

  public void setTotal(int total) {
    this.total = total;
  }
}
