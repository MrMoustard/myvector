package com.vectormobile.myvector.domain.interactor.intranet;

import com.vectormobile.myvector.domain.entity.intranet.UploadImageResponse;
import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;

import java.io.File;

/**
 * Created by amarina on 01/02/2017.
 */
public interface SendProfileImageInteractor {
  void execute(File photoProfile, OnItemRetrievedListener<UploadImageResponse> onItemRetrievedListener);
}
