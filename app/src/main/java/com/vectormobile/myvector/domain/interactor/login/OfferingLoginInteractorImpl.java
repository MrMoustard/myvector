package com.vectormobile.myvector.domain.interactor.login;

import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.domain.repository.TrendsRepository;

/**
 * Created by Erik Medina on 02/11/2016.
 */
public class OfferingLoginInteractorImpl implements OfferingLoginInteractor {

  private TrendsRepository trendsRepository;

  public OfferingLoginInteractorImpl(TrendsRepository trendsRepository) {
    this.trendsRepository = trendsRepository;
  }

  @Override
  public void execute(String username, String password, final OnItemRetrievedListener listener) {
    trendsRepository.validateLogin(username, password, listener);
  }
}
