package com.vectormobile.myvector.domain.entity.gms.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Arrays;
import java.util.List;

/**
 * Created by amarina on 31/01/2017.
 */

public class GmsFindExpenseByCreateTimeRequest {
  @SerializedName("fecha")
  @Expose
  private String fecha;
  @SerializedName("limit")
  @Expose
  private int limit = 150;
  @SerializedName("page")
  @Expose
  private int page = 1;
  @SerializedName("sort")
  @Expose
  private List<Sort> sort = null;
  @SerializedName("start")
  @Expose
  private int start = 0;

  public GmsFindExpenseByCreateTimeRequest(long timeCreated) {
    this.fecha = String.valueOf(timeCreated);
    sort = Arrays.asList(new Sort("fecha", "ASC"));
  }
}
