package com.vectormobile.myvector.domain.entity.gms.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kurt on 23/03/17.
 */

public class Day {

  public static final String WORKDAY = "LABORAL";
  public static final String NOT_WORKDAY = "NOLABORAL";
  public static final String HOLIDAY = "FESTIVO";
  public static final String NOT_REPORTED = "NOIMPUTADO";

  @SerializedName("date")
  @Expose
  private Long date;
  @SerializedName("type")
  @Expose
  private String type;
  @SerializedName("message")
  @Expose
  private String message;

  public Long getDate() {
    return date;
  }

  public void setDate(Long date) {
    this.date = date;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
