package com.vectormobile.myvector.domain.interactor.intranet;

import com.vectormobile.myvector.domain.entity.intranet.UploadImageResponse;
import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.domain.repository.IntranetRepository;

import java.io.File;

/**
 * Created by amarina on 01/02/2017.
 */
public class SendProfileImageInteractorImpl implements SendProfileImageInteractor {
  private IntranetRepository repository;

  public SendProfileImageInteractorImpl(IntranetRepository repository) {
    this.repository = repository;
  }

  @Override
  public void execute(File photoProfile, OnItemRetrievedListener<UploadImageResponse> onItemRetrievedListener) {
    repository.sendImage(photoProfile, onItemRetrievedListener);
  }
}
