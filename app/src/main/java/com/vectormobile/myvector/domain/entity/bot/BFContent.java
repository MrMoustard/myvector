package com.vectormobile.myvector.domain.entity.bot;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by amarina on 22/03/2017.
 */

public class BFContent {
  @SerializedName("title")
  @Expose
  private String title;
  @SerializedName("text")
  @Expose
  private String text;
  @SerializedName("buttons")
  @Expose
  private List<BFButton> buttons = null;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public List<BFButton> getButtons() {
    return buttons;
  }

  public void setButtons(List<BFButton> buttons) {
    this.buttons = buttons;
  }

  @Override
  public String toString() {
    return "BFContent{" +
        "title='" + title + '\'' +
        ", text='" + text + '\'' +
        ", buttons=" + buttons +
        '}';
  }
}
