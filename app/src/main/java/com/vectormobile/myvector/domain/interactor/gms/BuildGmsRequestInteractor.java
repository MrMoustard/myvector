package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.entity.gms.request.GmsRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;

/**
 * Created by Erik Medina on 25/01/2017.
 */

public interface BuildGmsRequestInteractor {

  void execute(GmsRequest gmsRequest, OnListRetrievedListener<GmsResponse> listener);
}
