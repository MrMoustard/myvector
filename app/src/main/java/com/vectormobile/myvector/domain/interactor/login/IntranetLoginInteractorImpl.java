package com.vectormobile.myvector.domain.interactor.login;

import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.domain.repository.IntranetRepository;

/**
 * Created by ajruiz on 30/12/2016.
 */

public class IntranetLoginInteractorImpl implements IntranetLoginInteractor {

  private IntranetRepository intranetRepository;

  public IntranetLoginInteractorImpl(IntranetRepository intranetRepository) {
    this.intranetRepository = intranetRepository;
  }

  @Override
  public void execute(String username, String password, OnItemRetrievedListener listener) {
    intranetRepository.validateLogin(username, password, listener);
  }

}
