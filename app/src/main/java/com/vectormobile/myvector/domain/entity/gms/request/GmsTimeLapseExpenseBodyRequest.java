package com.vectormobile.myvector.domain.entity.gms.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kurt on 25/01/17.
 */

public class GmsTimeLapseExpenseBodyRequest {

  @SerializedName("proyecto")
  @Expose
  private String project;
  @SerializedName("ordenTrabajo")
  @Expose
  private List<String> workOrders = null;
  @SerializedName("fechaDesde")
  @Expose
  private String dateFrom;
  @SerializedName("estado")
  @Expose
  private List<Object> states = null;
  @SerializedName("fechaHasta")
  @Expose
  private String dateTo;
  @SerializedName("page")
  @Expose
  private int page;
  @SerializedName("start")
  @Expose
  private int start;
  @SerializedName("limit")
  @Expose
  private int limit;
  @SerializedName("group")
  @Expose
  private List<Group> groups;
  @SerializedName("sort")
  @Expose
  private List<Sort> sorts;

  public GmsTimeLapseExpenseBodyRequest(String dateFrom, String dateTo) {
    this.project = null;
    this.dateFrom = dateFrom;
    this.dateTo = dateTo;
    this.page = 1;
    this.start = 0;
    this.limit = 25;
    groups = new ArrayList<>();
    groups.add(new Group("semanaFrom", "ASC"));
    sorts = new ArrayList<>();
    sorts.add(new Sort("semanaFrom", "ASC"));
  }

  public String getProject() {
    return project;
  }

  public void setProject(String project) {
    this.project = project;
  }

  public List<String> getWorkOrders() {
    return workOrders;
  }

  public void setWorkOrders(List<String> workOrders) {
    this.workOrders = workOrders;
  }

  public String getDateFrom() {
    return dateFrom;
  }

  public void setDateFrom(String dateFrom) {
    this.dateFrom = dateFrom;
  }

  public List<Object> getStates() {
    return states;
  }

  public void setStates(List<Object> states) {
    this.states = states;
  }

  public String getDateTo() {
    return dateTo;
  }

  public void setDateTo(String dateTo) {
    this.dateTo = dateTo;
  }

  public int getPage() {
    return page;
  }

  public void setPage(int page) {
    this.page = page;
  }

  public int getStart() {
    return start;
  }

  public void setStart(int start) {
    this.start = start;
  }

  public int getLimit() {
    return limit;
  }

  public void setLimit(int limit) {
    this.limit = limit;
  }

  public List<Group> getGroup() {
    return groups;
  }

  public void setGroup(List<Group> groups) {
    this.groups = groups;
  }

  public List<Sort> getSort() {
    return sorts;
  }

  public void setSort(List<Sort> sorts) {
    this.sorts = sorts;
  }
}
