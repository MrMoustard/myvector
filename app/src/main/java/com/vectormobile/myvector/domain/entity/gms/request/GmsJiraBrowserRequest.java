package com.vectormobile.myvector.domain.entity.gms.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ajruiz on 01/03/2017.
 */

public class GmsJiraBrowserRequest {

  @SerializedName("clave")
  @Expose
  public String key;
  @SerializedName("descripcion")
  @Expose
  public String description;
  @SerializedName("idProyecto")
  @Expose
  public String projectId;
  @SerializedName("idTrabajador")
  @Expose
  public String employeeId;
  @SerializedName("page")
  @Expose
  public int page;
  @SerializedName("start")
  @Expose
  public int start;
  @SerializedName("limit")
  @Expose
  public int limit;
  @SerializedName("sort")
  @Expose
  public List<Sort> sort = null;

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getEmployeeId() {
    return employeeId;
  }

  public void setEmployeeId(String employeeId) {
    this.employeeId = employeeId;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public int getLimit() {
    return limit;
  }

  public void setLimit(int limit) {
    this.limit = limit;
  }

  public int getPage() {
    return page;
  }

  public void setPage(int page) {
    this.page = page;
  }

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public List<Sort> getSort() {
    return sort;
  }

  public void setSort(List<Sort> sort) {
    this.sort = sort;
  }

  public int getStart() {
    return start;
  }

  public void setStart(int start) {
    this.start = start;
  }
}
