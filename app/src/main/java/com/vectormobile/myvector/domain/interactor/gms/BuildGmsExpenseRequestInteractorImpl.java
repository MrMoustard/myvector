package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.entity.gms.request.GmsExpenseBodyRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.domain.repository.GmsRepository;

/**
 * Created by Erik Medina on 20/01/2017.
 */
public class BuildGmsExpenseRequestInteractorImpl implements BuildGmsExpenseRequestInteractor {

  private GmsRepository gmsRepository;

  public BuildGmsExpenseRequestInteractorImpl(GmsRepository gmsRepository) {
    this.gmsRepository = gmsRepository;
  }

  @Override
  public void execute(String action, String method, GmsExpenseBodyRequest gmsExpenseBodyRequest,
                      OnListRetrievedListener<GmsResponse> listener) {
    gmsRepository.buildGmsExpenseRequest(action, method, gmsExpenseBodyRequest, listener);
  }
}
