package com.vectormobile.myvector.domain.interactor.botframework;

import com.vectormobile.myvector.domain.interactor.botframework.listener.StartConversationListener;
import com.vectormobile.myvector.domain.repository.BFRepository;

/**
 * Created by amarina on 15/03/2017.
 */
public class StartConversationInteractorImpl implements StartConversationInteractor {
  private BFRepository bfRepository;

  public StartConversationInteractorImpl(BFRepository bfRepository) {
    this.bfRepository = bfRepository;
  }

  @Override
  public void execute(StartConversationListener listener) {
    bfRepository.startConversation(listener);
  }
}
