package com.vectormobile.myvector.domain.service;

import com.vectormobile.myvector.domain.entity.gms.request.GmsDayStateRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsJiraRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsDayStateResponse;
import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;

import java.util.List;

/**
 * Created by kurt on 30/11/16.
 */

public interface GmsService {

  @FormUrlEncoded
  @POST(ServiceManager.BASE_GMS_URL + ServiceManager.GMS_LOGIN_ENDPOINT)
  Call<ResponseBody> userLogin(@Field("j_username") String username,
                               @Field("j_password") String password,
                               @Field("simple") boolean mode);

  @POST(ServiceManager.BASE_GMS_URL + ServiceManager.GMS_ROUTER_ENDPOINT)
  Call<List<GmsResponse>> getUserNotifications(@Body GmsRequest gmsRequest);

  @POST(ServiceManager.GMS_ROUTER_ENDPOINT)
  Call<List<GmsResponse>> getGmsData(@Body GmsRequest gmsRequest);

  @POST(ServiceManager.GMS_ROUTER_ENDPOINT)
  Call<List<GmsResponse>> getGmsData(@Body GmsJiraRequest gmsRequest);

  @POST(ServiceManager.GMS_ROUTER_ENDPOINT)
  Call<List<GmsResponse>> getGmsMultiActionData(@Body List<GmsRequest> multiActionRequest);

  @POST("imputaciones/notaGasto.pdf")
  Call<ResponseBody> getImputationPdf(
      @Query("download") String filename,
      @Query("gastos") List<String> imputationIds
  );

  @POST(ServiceManager.GMS_ROUTER_ENDPOINT)
  Call<List<GmsDayStateResponse>> getMonthDaysState(@Body GmsDayStateRequest dayStateRequest);
}
