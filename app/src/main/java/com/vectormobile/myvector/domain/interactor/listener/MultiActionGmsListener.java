package com.vectormobile.myvector.domain.interactor.listener;

import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;

import java.util.List;

/**
 * Created by amarina on 31/01/2017.
 */
public interface MultiActionGmsListener {
  void onSuccess(List<GmsResponse> body);

  void onUnknowError();

  void onNetworkError();
}
