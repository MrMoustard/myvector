package com.vectormobile.myvector.domain.entity.bot;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by amarina on 21/03/2017.
 */

public class BFWebSocketResponse {
  @SerializedName("activities")
  @Expose
  public List<BFResponse> activities = null;
  @SerializedName("watermark")
  @Expose
  public String watermark;

  public List<BFResponse> getActivities() {
    return activities;
  }

  public void setActivities(List<BFResponse> activities) {
    this.activities = activities;
  }

  public String getWatermark() {
    return watermark;
  }

  public void setWatermark(String watermark) {
    this.watermark = watermark;
  }

  @Override
  public String toString() {
    return "BFWebSocketResponse{" +
        "activities=" + activities +
        ", watermark='" + watermark + '\'' +
        '}';
  }
}
