package com.vectormobile.myvector.domain.entity.gms.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ajruiz on 25/01/2017.
 */

public class Group {

  @SerializedName("property")
  @Expose
  public String property;
  @SerializedName("direction")
  @Expose
  public String direction;

  public Group(String property, String direction) {
    this.property = property;
    this.direction = direction;
  }

  public String getDirection() {
    return direction;
  }

  public void setDirection(String direction) {
    this.direction = direction;
  }

  public String getProperty() {
    return property;
  }

  public void setProperty(String property) {
    this.property = property;
  }
}
