package com.vectormobile.myvector.domain.entity.botframework;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amarina on 15/03/2017.
 */

public class RsLoginResponse {
  @SerializedName("conversationId")
  @Expose
  private String conversationId;
  @SerializedName("token")
  @Expose
  private String token;
  @SerializedName("expires_in")
  @Expose
  private int expiresIn;
  @SerializedName("streamUrl")
  @Expose
  private String streamUrl;

  public String getConversationId() {
    return conversationId;
  }

  public void setConversationId(String conversationId) {
    this.conversationId = conversationId;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public int getExpiresIn() {
    return expiresIn;
  }

  public void setExpiresIn(int expiresIn) {
    this.expiresIn = expiresIn;
  }

  public String getStreamUrl() {
    return streamUrl;
  }

  public void setStreamUrl(String streamUrl) {
    this.streamUrl = streamUrl;
  }

  @Override
  public String toString() {
    return "RsLoginResponse{" +
        "conversationId='" + conversationId + '\'' +
        ", token='" + token + '\'' +
        ", expiresIn=" + expiresIn +
        ", streamUrl='" + streamUrl + '\'' +
        '}';
  }
}
