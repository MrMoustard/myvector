package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.entity.gms.request.GmsTimeLapseExpenseBodyRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.domain.repository.GmsRepository;

/**
 * Created by Erik Medina on 20/01/2017.
 */
public class BuildGmsTimeLapseExpenseRequestInteractorImpl implements BuildGmsTimeLapseExpenseRequestInteractor {

  private GmsRepository gmsRepository;

  public BuildGmsTimeLapseExpenseRequestInteractorImpl(GmsRepository gmsRepository) {
    this.gmsRepository = gmsRepository;
  }

  @Override
  public void execute(String action, String method, GmsTimeLapseExpenseBodyRequest bodyRequest,
                      OnListRetrievedListener<GmsResponse> listener) {
    gmsRepository.buildGmsTimeLapseExpenseRequest(action, method, bodyRequest, listener);
  }
}
