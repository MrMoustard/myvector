package com.vectormobile.myvector.domain.entity.gms.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Erik Medina on 05/01/2017.
 */
public class Result {

  @SerializedName("success")
  @Expose
  private boolean success;
  @SerializedName("total")
  @Expose
  private int total;
  @SerializedName("records")
  @Expose
  private List<Record> records = null;
  @SerializedName("message")
  @Expose
  private String message;
  @SerializedName("errors")
  @Expose
  private Object errors;

  public boolean isSuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }

  public int getTotal() {
    return total;
  }

  public void setTotal(int total) {
    this.total = total;
  }

  public List<Record> getRecords() {
    return records;
  }

  public void setRecords(List<Record> records) {
    this.records = records;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Object getErrors() {
    return errors;
  }

  public void setErrors(Object errors) {
    this.errors = errors;
  }
}
