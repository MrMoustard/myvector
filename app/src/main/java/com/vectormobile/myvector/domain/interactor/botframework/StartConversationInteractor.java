package com.vectormobile.myvector.domain.interactor.botframework;

import com.vectormobile.myvector.domain.interactor.botframework.listener.StartConversationListener;

/**
 * Created by amarina on 15/03/2017.
 */
public interface StartConversationInteractor {
  void execute(StartConversationListener listener);
}
