package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.entity.gms.request.GmsTimeLapseExpenseBodyRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;

/**
 * Created by Erik Medina on 20/01/2017.
 */

public interface BuildGmsTimeLapseExpenseRequestInteractor {

  void execute(String action, String method, GmsTimeLapseExpenseBodyRequest bodyRequest,
               OnListRetrievedListener<GmsResponse> listener);
}
