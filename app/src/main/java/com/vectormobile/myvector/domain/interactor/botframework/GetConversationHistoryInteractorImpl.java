package com.vectormobile.myvector.domain.interactor.botframework;

import com.vectormobile.myvector.domain.interactor.botframework.listener.GetConversationHistoryListener;
import com.vectormobile.myvector.domain.repository.BFRepository;

/**
 * Created by amarina on 30/03/2017.
 */

public class GetConversationHistoryInteractorImpl implements GetConversationHistoryInteractor {
  private BFRepository bfRepository;

  public GetConversationHistoryInteractorImpl(BFRepository bfRepository) {
    this.bfRepository = bfRepository;
  }

  @Override
  public void execute(GetConversationHistoryListener getConversationHistoryListener) {
    bfRepository.getHistory(getConversationHistoryListener);
  }
}
