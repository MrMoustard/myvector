package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.entity.gms.request.ExpenseRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsFindExpenseByCreateTimeRequest;
import com.vectormobile.myvector.domain.interactor.listener.MultiActionGmsListener;
import com.vectormobile.myvector.domain.repository.GmsRepository;

/**
 * Created by amarina on 31/01/2017.
 */
public class SaveExpenseWithImageInteractorImpl implements SaveExpenseWithImageInteractor {
  private GmsRepository gmsRepository;

  public SaveExpenseWithImageInteractorImpl(GmsRepository gmsRepository) {
    this.gmsRepository = gmsRepository;
  }

  @Override
  public void execute(
      ExpenseRequest expenseRequest,
      GmsFindExpenseByCreateTimeRequest countRequest,
      String expenseAction,
      String expenseMethod,
      String countAction,
      MultiActionGmsListener multiActionGmsListener
  ) {
    gmsRepository.sendExpenseWithImage(
        expenseRequest,
        countRequest,
        expenseAction,
        expenseMethod,
        countAction,
        multiActionGmsListener);
  }
}
