package com.vectormobile.myvector.domain.interactor.intranet;

import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;

/**
 * Created by ajruiz on 01/02/2017.
 */

public interface ContactsListInteractor {

  void execute(int page, String search, OnItemRetrievedListener listener);

}
