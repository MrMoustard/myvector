package com.vectormobile.myvector.domain.entity.bot;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amarina on 22/03/2017.
 */
public class BFAttachment {
  @SerializedName("contentType")
  @Expose
  private String contentType;
  @SerializedName("content")
  @Expose
  private BFContent content;

  public String getContentType() {
    return contentType;
  }

  public void setContentType(String contentType) {
    this.contentType = contentType;
  }

  public BFContent getContent() {
    return content;
  }

  public void setContent(BFContent content) {
    this.content = content;
  }

  @Override
  public String toString() {
    return "BFAttachment{" +
        "contentType='" + contentType + '\'' +
        ", content=" + content +
        '}';
  }
}
