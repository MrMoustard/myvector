package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.interactor.listener.DownloadPdfListener;

import java.util.List;

/**
 * Created by amarina on 06/03/2017.
 */
public interface SharedPdfImputationInteractor {

  void execute(List<String> imputationsIds, DownloadPdfListener downloadPdfListener);
}
