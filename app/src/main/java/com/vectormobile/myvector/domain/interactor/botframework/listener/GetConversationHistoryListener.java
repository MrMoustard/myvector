package com.vectormobile.myvector.domain.interactor.botframework.listener;

import com.vectormobile.myvector.domain.entity.bot.BFHistoryResponse;

/**
 * Created by amarina on 30/03/2017.
 */

public interface GetConversationHistoryListener {
  void onSuccess(BFHistoryResponse bfHistoryResponse);
}
