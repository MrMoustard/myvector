package com.vectormobile.myvector.domain.interactor.intranet;

import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.domain.repository.IntranetRepository;

/**
 * Created by ajruiz on 21/12/2016.
 */

public class NewsItemDetailInteractorImpl implements NewsItemDetailInteractor {

  private IntranetRepository repository;

  public NewsItemDetailInteractorImpl(IntranetRepository repository) {
    this.repository = repository;
  }

  @Override
  public void execute(String id, OnItemRetrievedListener listener) {
    repository.getIntranetNewsById(id, listener);
  }
}
