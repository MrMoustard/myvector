package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.domain.repository.GmsRepository;

/**
 * Created by Erik Medina on 05/01/2017.
 */
public class GetCurrenciesInteractorImpl implements GetCurrenciesInteractor {

  private GmsRepository gmsRepository;

  public GetCurrenciesInteractorImpl(GmsRepository gmsRepository) {
    this.gmsRepository = gmsRepository;
  }

  @Override
  public void execute(OnListRetrievedListener<GmsResponse> listener) {
    gmsRepository.getCurrencies(listener);
  }
}
