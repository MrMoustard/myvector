package com.vectormobile.myvector.domain.interactor.intranet;

import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.domain.repository.IntranetRepository;

/**
 * Created by Erik Medina on 15/11/2016.
 */
public class NewsInteractorImpl implements NewsInteractor {

  private IntranetRepository repository;

  public NewsInteractorImpl(IntranetRepository repository) {
    this.repository = repository;
  }

  @Override
  public void execute(int page, OnItemRetrievedListener listener) {
    repository.getIntranetNews(page, listener);
  }
}
