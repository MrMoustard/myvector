package com.vectormobile.myvector.domain.entity.gms.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.vectormobile.myvector.util.date.DateUtils;

import java.util.Calendar;

/**
 * Created by Erik Medina on 09/01/2017.
 */

public class GmsExpenseBodyRequest {

  @SerializedName("imputacionId")
  @Expose
  private Object imputationId;
  @SerializedName("trabajadorId")
  @Expose
  private Object employeeId;
  @SerializedName("fecha")
  @Expose
  private String date;
  @SerializedName("page")
  @Expose
  private int page;
  @SerializedName("start")
  @Expose
  private int start;
  @SerializedName("limit")
  @Expose
  private int limit;

  public GmsExpenseBodyRequest() {
    this.imputationId = null;
    Calendar calendar = Calendar.getInstance();
    this.date = DateUtils.formatDateToGmsLargeString(calendar.getTime());
    this.page = 1;
    this.start = 0;
    this.limit = 75;//TODO: [Erik] Queda pendiente cómo vamos a realizar las peticiones teniendo en cuenta el paginado y el número de elementos por página
  }

  public Object getImputationId() {
    return imputationId;
  }

  public void setImputationId(Object imputationId) {
    this.imputationId = imputationId;
  }

  public Object getEmployeeId() {
    return employeeId;
  }

  public void setEmployeeId(Object employeeId) {
    this.employeeId = employeeId;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public int getPage() {
    return page;
  }

  public void setPage(int page) {
    this.page = page;
  }

  public int getStart() {
    return start;
  }

  public void setStart(int start) {
    this.start = start;
  }

  public int getLimit() {
    return limit;
  }

  public void setLimit(int limit) {
    this.limit = limit;
  }
}
