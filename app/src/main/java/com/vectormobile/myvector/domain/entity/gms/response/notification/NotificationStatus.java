package com.vectormobile.myvector.domain.entity.gms.response.notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.vectormobile.myvector.domain.entity.gms.response.Record;

import java.util.List;

/**
 * Created by ajruiz on 30/12/2016.
 */

public class NotificationStatus {

  @SerializedName("success")
  @Expose
  private Boolean success;
  @SerializedName("total")
  @Expose
  private Integer total;
  @SerializedName("records")
  @Expose
  private List<Record> records = null;

  public Boolean getSuccess() {
    return success;
  }

  public void setSuccess(Boolean success) {
    this.success = success;
  }

  public Integer getTotal() {
    return total;
  }

  public void setTotal(Integer total) {
    this.total = total;
  }

  public List<Record> getRecords() {
    return records;
  }

  public void setRecords(List<Record> records) {
    this.records = records;
  }

}
