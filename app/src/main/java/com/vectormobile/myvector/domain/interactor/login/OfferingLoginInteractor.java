package com.vectormobile.myvector.domain.interactor.login;

import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;

/**
 * Created by amarina on 21/09/2016.
 */
public interface OfferingLoginInteractor {

  void execute(String username, String password, OnItemRetrievedListener listener);

}
