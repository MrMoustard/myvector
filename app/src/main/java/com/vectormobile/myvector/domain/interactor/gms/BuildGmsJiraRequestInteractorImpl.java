package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.entity.gms.request.GmsJiraRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.domain.repository.GmsRepository;

/**
 * Created by ajruiz on 02/03/2017.
 */
public class BuildGmsJiraRequestInteractorImpl implements BuildGmsJiraRequestInteractor {

  private GmsRepository gmsRepository;

  public BuildGmsJiraRequestInteractorImpl(GmsRepository gmsRepository) {
    this.gmsRepository = gmsRepository;
  }

  @Override
  public void execute(GmsJiraRequest gmsJiraRequest, OnListRetrievedListener<GmsResponse> listener) {
    gmsRepository.getJiraCodesAssigned(gmsJiraRequest, listener);
  }
}
