package com.vectormobile.myvector.domain.interactor.botframework;

/**
 * Created by amarina on 22/03/2017.
 */

public interface SendBFTextInteractor {
  void execute(String message);
}
