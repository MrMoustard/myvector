package com.vectormobile.myvector.domain.entity.gms.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by kurt on 23/03/17.
 */

public class GmsDayStateResult {

  @SerializedName("fechaDesde")
  @Expose
  private Long dateFrom;
  @SerializedName("fechaHasta")
  @Expose
  private Long dateTo;
  @SerializedName("dias")
  @Expose
  private List<Day> days = null;

  public Long getDateFrom() {
    return dateFrom;
  }

  public void setDateFrom(Long dateFrom) {
    this.dateFrom = dateFrom;
  }

  public Long getDateTo() {
    return dateTo;
  }

  public void setDateTo(Long dateTo) {
    this.dateTo = dateTo;
  }

  public List<Day> getDays() {
    return days;
  }

  public void setDays(List<Day> days) {
    this.days = days;
  }
}
