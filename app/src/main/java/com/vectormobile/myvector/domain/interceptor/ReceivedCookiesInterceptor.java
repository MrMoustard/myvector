package com.vectormobile.myvector.domain.interceptor;

import com.vectormobile.myvector.storage.SessionManager;
import okhttp3.Interceptor;
import okhttp3.Response;

import java.io.IOException;

/**
 * Created by ajruiz on 29/12/2016.
 */

public class ReceivedCookiesInterceptor implements Interceptor {

  private static final String COOKIES_ARRAY = "Set-Cookie";
  public static final String COOKIE_SESSION_ID = "JSESSIONID";
  public static final String COOKIE_SESSION_INTRANET_ID = "wordpress_logged_in_45e5b46749cb1b863a0e3e09c544a641";
  public static final String COOKIE_SESSION_ID_WORDPRESS = "wordpress_sec_45e5b46749cb1b863a0e3e09c544a641";
  public static final String COOKIE_USER_ID = "USER_UUID";

  private SessionManager sessionManager;

  private String cookie;

  public ReceivedCookiesInterceptor(SessionManager sessionManager) {
    this.sessionManager = sessionManager;
  }

  @Override
  public Response intercept(Chain chain) throws IOException {
    Response originalResponse = chain.proceed(chain.request());
    persistSessionCookie(originalResponse);
    return originalResponse;
  }

  private void persistSessionCookie(Response response) {
    cookie = getCookieHeader(response);
    if (cookie != null) {
      getLocationHeader(response);
    }
  }

  private String getCookieHeader(Response response) {
    if (!response.headers("Set-Cookie").isEmpty()) {
      for (String cookie : response.headers().toMultimap().get(COOKIES_ARRAY)) {
        if (cookie.startsWith(COOKIE_SESSION_ID)
            || (cookie.startsWith(COOKIE_SESSION_ID_WORDPRESS)
            && cookie.contains("HttpOnly")
            && (cookie.contains("path=/wp-content/plugins")
            || (cookie.contains("path=/wp-admin"))))) {
          return cookie;
        }
      }
    }
    return null;
  }

  private String getLocationHeader(Response response) {
    if (response.header("Location") != null) {
      if (response.header("Location").contains(SessionManager.GMS_REQUEST)) {
        persistGmsSessionToken(cookie);
      } else if (response.header("Location").contains(SessionManager.OFFERING_REQUEST)) {
        persistOfferingSessiontoken(cookie);
      }
    } else if (response.toString().contains(SessionManager.INTRANET_REQUEST)) {
      persistIntranetSessiontoken(cookie);
    }
    return null;
  }

  public static boolean arrayContainsUserId(Response response) {
    for (String cookie : response.headers().toMultimap().get(COOKIES_ARRAY)) {
      if (cookie.startsWith(COOKIE_USER_ID)) {
        return true;
      }
    }
    return false;
  }

  private void persistGmsSessionToken(String cookie) {
    sessionManager.setGmsToken(cookie);
    sessionManager.setGmsTokenActive(true);
  }

  private void persistIntranetSessiontoken(String cookie) {
    sessionManager.setIntranetToken(cookie);
    sessionManager.setIntranetTokenActive(true);
  }

  private void persistOfferingSessiontoken(String cookie) {
    sessionManager.setOfferingToken(cookie);
    sessionManager.setOfferingTokenActive(true);
  }
}