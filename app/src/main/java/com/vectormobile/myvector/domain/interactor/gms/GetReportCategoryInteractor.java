package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.entity.gms.request.GmsReportCategoryRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;

/**
 * Created by Erik Medina on 11/01/2017.
 */

public interface GetReportCategoryInteractor {

  void execute(GmsReportCategoryRequest gmsReportCategoryRequest, OnListRetrievedListener<GmsResponse> listener);
}
