package com.vectormobile.myvector.domain.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by ajruiz on 20/12/2016.
 */

public interface IntranetServiceXml {

  @FormUrlEncoded
  @POST(ServiceManager.BASE_INTRANET_URL + ServiceManager.INTRANET_LOGIN_ENDPOINT)
  Call<ResponseBody> userLogin(@Field("username") String username,
                               @Field("password") String password,
                               @Field("action") String redirect);

  @Headers("Content-Type: application/x-www-form-urlencoded")
  @POST
  Call<String> getIntranetNewsById(@Url String url);

  @GET(ServiceManager.INTRANET_USER_DATA_ENDPOINT)
  Call<String> getUserProfile();

  @FormUrlEncoded
  @POST(ServiceManager.BASE_INTRANET_URL + ServiceManager.INTRANET_SAVE_USER_INFO)
  Call<String> saveUserInfo(@Field("action") String action,
                            @Field("attr") String attribute,
                            @Field("val") String value);

}
