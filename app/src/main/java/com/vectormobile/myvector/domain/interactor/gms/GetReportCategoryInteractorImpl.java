package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.entity.gms.request.GmsReportCategoryRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.domain.repository.GmsRepository;

/**
 * Created by Erik Medina on 11/01/2017.
 */
public class GetReportCategoryInteractorImpl implements GetReportCategoryInteractor {

  private GmsRepository gmsRepository;

  public GetReportCategoryInteractorImpl(GmsRepository gmsRepository) {
    this.gmsRepository = gmsRepository;
  }

  @Override
  public void execute(GmsReportCategoryRequest gmsReportCategoryRequest, OnListRetrievedListener<GmsResponse> listener) {
    gmsRepository.getReportCategory(gmsReportCategoryRequest, listener);
  }
}
