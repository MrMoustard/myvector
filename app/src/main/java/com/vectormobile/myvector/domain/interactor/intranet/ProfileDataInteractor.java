package com.vectormobile.myvector.domain.interactor.intranet;

import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;

/**
 * Created by ajruiz on 11/01/2017.
 */

public interface ProfileDataInteractor {

  void execute(OnItemRetrievedListener listener);

}
