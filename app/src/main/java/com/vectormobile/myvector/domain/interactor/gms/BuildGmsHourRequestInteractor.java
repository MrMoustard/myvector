package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.entity.gms.request.GmsTimeLapseHourBodyRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;

/**
 * Created by ajruiz on 24/01/2017.
 */

public interface BuildGmsHourRequestInteractor {

  void execute(String action, String method, GmsTimeLapseHourBodyRequest gmsHourBodyRequest,
               OnListRetrievedListener<GmsResponse> listener);

}
