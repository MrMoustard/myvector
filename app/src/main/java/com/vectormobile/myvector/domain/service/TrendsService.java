package com.vectormobile.myvector.domain.service;

import com.vectormobile.myvector.domain.entity.offering.AuthenticationResponse;
import com.vectormobile.myvector.model.PostAbout;
import com.vectormobile.myvector.model.offering.SectionMenu;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

import java.util.List;

/**
 * Created by kurt on 30/11/16.
 */

public interface TrendsService {

  @FormUrlEncoded
  @POST(ServiceManager.LOGIN_ENDPOINT)
  Call<AuthenticationResponse> userLogin(@Field("username") String username,
                                         @Field("password") String password);

  @GET(ServiceManager.MENU_OFFERING_ENDPOINT)
  Call<SectionMenu> getSectionMenuList();

  @GET(ServiceManager.OFFERING_BY_CATEGORY_ENDPOINT)
  Call<List<PostAbout>> getOfferingNewsById(@Query("categories") int id,
                                            @Query("page") int page,
                                            @Query("per_page") int postsPerPage);

}
