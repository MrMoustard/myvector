package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.entity.gms.request.GmsJiraBrowserRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.domain.repository.GmsRepository;

/**
 * Created by ajruiz on 01/03/2017.
 */

public class BuildJiraBrowserBodyRequestInteractorImpl implements BuildJiraBrowserBodyRequestInteractor {

  private GmsRepository gmsRepository;

  public BuildJiraBrowserBodyRequestInteractorImpl(GmsRepository gmsRepository) {
    this.gmsRepository = gmsRepository;
  }

  @Override
  public void execute(String action, String method, GmsJiraBrowserRequest gmsJiraBrowserRequest,
                      OnListRetrievedListener<GmsResponse> listener) {
    gmsRepository.getJiraCodes(action, method, gmsJiraBrowserRequest, listener);
  }
}
