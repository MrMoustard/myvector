package com.vectormobile.myvector.domain.entity.gms.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Erik Medina on 04/01/2017.
 */

public class GmsRequest<T> {

  @SerializedName("action")
  @Expose
  protected String action;
  @SerializedName("method")
  @Expose
  protected String method;
  @SerializedName("data")
  @Expose
  protected List<T> data = null;
  @SerializedName("type")
  protected String type;
  @SerializedName("tid")
  protected int tid;

  public GmsRequest() {
    data = new ArrayList<>();
    type = "rpc";
  }

  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public String getMethod() {
    return method;
  }

  public void setMethod(String method) {
    this.method = method;
  }

  public List<T> getData() {
    return data;
  }

  public void setData(List<T> data) {
    this.data = data;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public int getTid() {
    return tid;
  }

  public void setTid(int tid) {
    this.tid = tid;
  }
}
