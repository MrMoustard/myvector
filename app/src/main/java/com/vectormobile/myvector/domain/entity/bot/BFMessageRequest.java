package com.vectormobile.myvector.domain.entity.bot;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by amarina on 22/03/2017.
 */

public class BFMessageRequest {
  @SerializedName("type")
  @Expose
  private String type = "message";
  @SerializedName("from")
  @Expose
  private BFUser from;
  @SerializedName("text")
  @Expose
  private String text;
  @SerializedName("localTimestamp")
  @Expose
  private String timestamp;

  public BFMessageRequest() {
    timestamp = (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())).format(new Date());
  }

  public String getType() {
    return type;
  }

  public BFUser getFrom() {
    return from;
  }

  public void setFrom(BFUser from) {
    this.from = from;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getTimestamp() {
    return timestamp;
  }

  @Override
  public String toString() {
    return "BFMessageRequest{" +
        "type='" + type + '\'' +
        ", from=" + from +
        ", text='" + text + '\'' +
        ", timestamp='" + timestamp + '\'' +
        '}';
  }
}
