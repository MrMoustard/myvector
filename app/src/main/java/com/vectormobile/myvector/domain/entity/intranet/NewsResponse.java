package com.vectormobile.myvector.domain.entity.intranet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ajruiz on 30/01/2017.
 */

public class NewsResponse {

  @SerializedName("html_results")
  @Expose
  public List<String> htmlResults = null;
  @SerializedName("posts_id")
  @Expose
  public List<Integer> postsId = null;
  @SerializedName("total")
  @Expose
  public String total;

  public List<String> getHtmlResults() {
    return htmlResults;
  }

  public void setHtmlResults(List<String> htmlResults) {
    this.htmlResults = htmlResults;
  }

  public String getTotal() {
    return total;
  }

  public void setTotal(String total) {
    this.total = total;
  }

  public List<Integer> getPostsId() {
    return postsId;
  }

  public void setPostsId(List<Integer> postsId) {
    this.postsId = postsId;
  }
}
