package com.vectormobile.myvector.domain.repository;

import com.vectormobile.myvector.domain.entity.gms.request.ExpenseRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsBodyRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsDayStateRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsExpenseBodyRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsFindExpenseByCreateTimeRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsJiraBrowserRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsJiraRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsReportCategoryRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsTimeLapseExpenseBodyRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsTimeLapseHourBodyRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsDayStateResult;
import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.interactor.listener.DownloadPdfListener;
import com.vectormobile.myvector.domain.interactor.listener.MultiActionGmsListener;
import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.model.gms.GmsNotification;

import java.util.List;

public interface GmsRepository {

  void validateLogin(String email, String password, OnItemRetrievedListener listener);

  void retrieveGmsNotifications(OnListRetrievedListener<GmsNotification> listener);

  void getCurrencies(OnListRetrievedListener<GmsResponse> listener);

  void getJiraCodesAssigned(GmsJiraRequest gmsJiraRequest,
                            OnListRetrievedListener<GmsResponse> listener);

  void getImputableProjects(String action, String method, GmsBodyRequest gmsBodyRequest,
                            OnListRetrievedListener<GmsResponse> listener);

  void getWorkOrders(String projectId, OnListRetrievedListener<GmsResponse> listener);

  void getExpenseCategories(String projectId, String workOrderId, OnListRetrievedListener<GmsResponse> listener);

  void saveExpense(ExpenseRequest expense, String action, String method,  OnListRetrievedListener<GmsResponse> listener);

  void sendExpense(GmsRequest gmsRequest, OnListRetrievedListener<GmsResponse> listener);

  void getReportCategory(GmsReportCategoryRequest gmsReportCategoryRequest, OnListRetrievedListener<GmsResponse> listener);

  void buildGmsExpenseRequest(String action, String method, GmsExpenseBodyRequest gmsExpenseBodyRequest,
                              OnListRetrievedListener<GmsResponse> listener);

  void buildGmsTimeLapseExpenseRequest(String action, String method, GmsTimeLapseExpenseBodyRequest bodyRequest,
                                       OnListRetrievedListener<GmsResponse> listener);

  void buildGmsHourRequest(String action, String method, GmsTimeLapseHourBodyRequest gmsHourBodyRequest,
                           OnListRetrievedListener<GmsResponse> listener);

  void sendExpenseWithImage(ExpenseRequest expenseRequest, GmsFindExpenseByCreateTimeRequest countRequest, String expenseAction, String expenseMethod, String countAction, MultiActionGmsListener multiActionGmsListener);

  void getJiraCodes(String action, String method, GmsJiraBrowserRequest gmsBodyRequest,
                            OnListRetrievedListener<GmsResponse> listener);

  void downloadPdfImputation(List<String> imputationIds, DownloadPdfListener downloadPdfListener);

  void getMonthDaysState(GmsDayStateRequest request, OnItemRetrievedListener<GmsDayStateResult> listener);
}
