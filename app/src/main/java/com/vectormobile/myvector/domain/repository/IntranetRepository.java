package com.vectormobile.myvector.domain.repository;

import com.vectormobile.myvector.domain.entity.intranet.UploadImageResponse;
import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;

import java.io.File;

/**
 * Created by ajruiz on 20/12/2016.
 */

public interface IntranetRepository {

  void validateLogin(String email, String password, OnItemRetrievedListener listener);

  void getIntranetNews(int page, OnItemRetrievedListener listener);

  void getIntranetNewsById(String id, OnItemRetrievedListener listener);

  void getUserProfile(OnItemRetrievedListener listener);

  void saveUserInfo(String attribute, String value, OnItemRetrievedListener listener);

  void sendImage(File photoProfile, OnItemRetrievedListener<UploadImageResponse> onItemRetrievedListener);
  void getContactsList(int page, String search, OnItemRetrievedListener listener);

}
