package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.domain.repository.GmsRepository;

/**
 * Created by Erik Medina on 09/01/2017.
 */
public class GetWorkOrderInteractorImpl implements GetWorkOrderInteractor {

  private GmsRepository gmsRepository;

  public GetWorkOrderInteractorImpl(GmsRepository gmsRepository) {
    this.gmsRepository = gmsRepository;
  }

  @Override
  public void execute(String projectId, OnListRetrievedListener<GmsResponse> listener) {
    gmsRepository.getWorkOrders(projectId, listener);
  }
}
