package com.vectormobile.myvector.domain.entity.gms.request;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by amarina on 31/01/2017.
 */

public class GmsMultiActionRequest {
  @Expose
  private List<GmsRequest> data = new ArrayList<>();

  public GmsMultiActionRequest(GmsRequest... requests) {
    Collections.addAll(data, requests);
  }

}
