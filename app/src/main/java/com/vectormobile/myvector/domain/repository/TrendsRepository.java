package com.vectormobile.myvector.domain.repository;

import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;


public interface TrendsRepository {

  void validateLogin(String email, String password, OnItemRetrievedListener listener);

  void buildOfferingMenu(OnItemRetrievedListener listener);

  void getOfferingNews(int id, int page, int postsPerPage, OnListRetrievedListener listener);

}
