package com.vectormobile.myvector.domain.entity.gms.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.vectormobile.myvector.util.date.DateUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Erik Medina on 09/01/2017.
 */

public class GmsBodyRequest {

  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("imputacionId")
  @Expose
  private Object imputationId;
  @SerializedName("trabajadorId")
  @Expose
  public Object employeeId;
  @SerializedName("proyectoId")
  @Expose
  public String projectId;
  @SerializedName("ordenTrabajoId")
  @Expose
  public String workOrderId;
  @SerializedName("categoria1Id")
  @Expose
  public String categoryId;
  @SerializedName("categoria2Id")
  @Expose
  public String category2Id;
  @SerializedName("fecha")
  @Expose
  private String date;
  @SerializedName("tipoAltaImputacion")
  @Expose
  public String imputationType;
  @SerializedName("unidades")
  @Expose
  public String units;
  @SerializedName("codigoExterno")
  @Expose
  public String externalCode;
  @SerializedName("imputacionJira")
  @Expose
  public String jiraImputation;
  @SerializedName("comentarioImputador")
  @Expose
  public String imputationComment;
  @SerializedName("comentarioAprobador")
  @Expose
  public String imputationApproving;
  @SerializedName("fechaDesde")
  @Expose
  public String startDate;
  @SerializedName("fechaHasta")
  @Expose
  public String endDate;

  public GmsBodyRequest() {
    this.imputationId = null;
    Calendar calendar = Calendar.getInstance();
    this.date = DateUtils.formatDateToGmsLargeString(calendar.getTime());

  }

  public GmsBodyRequest(Calendar calendar) {
    this.imputationId = null;
    this.date = DateUtils.formatDateToGmsLargeString(calendar.getTime());

  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(String categoryId) {
    this.categoryId = categoryId;
  }

  public String getWorkOrderId() {
    return workOrderId;
  }

  public void setWorkOrderId(String workOrderId) {
    this.workOrderId = workOrderId;
  }

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public Object getImputationId() {
    return imputationId;
  }

  public void setImputationId(Object imputationId) {
    this.imputationId = imputationId;
  }

  public Object getEmployeeId() {
    return employeeId;
  }

  public void setEmployeeId(Object employeeId) {
    this.employeeId = employeeId;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public String getImputationType() {
    return imputationType;
  }

  public void setImputationType(String imputationType) {
    this.imputationType = imputationType;
  }

  public String getCategory2Id() {
    return category2Id;
  }

  public void setCategory2Id(String category2Id) {
    this.category2Id = category2Id;
  }

  public String getExternalCode() {
    return externalCode;
  }

  public void setExternalCode(String externalCode) {
    this.externalCode = externalCode;
  }

  public String getImputationApproving() {
    return imputationApproving;
  }

  public void setImputationApproving(String imputationApprover) {
    this.imputationApproving = imputationApprover;
  }

  public String getImputationComment() {
    return imputationComment;
  }

  public void setImputationComment(String imputationComment) {
    this.imputationComment = imputationComment;
  }

  public String getJiraImputation() {
    return jiraImputation;
  }

  public void setJiraImputation(String jiraImputation) {
    this.jiraImputation = jiraImputation;
  }

  public String getUnits() {
    return units;
  }

  public void setUnits(String units) {
    this.units = units;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }
}
