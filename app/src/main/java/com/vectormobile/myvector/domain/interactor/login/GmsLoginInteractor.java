package com.vectormobile.myvector.domain.interactor.login;

import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;

/**
 * Created by ajruiz on 29/12/2016.
 */

public interface GmsLoginInteractor {

  void execute(String username, String password, OnItemRetrievedListener listener);

}
