package com.vectormobile.myvector.domain.repository;

import com.vectormobile.myvector.domain.interactor.botframework.listener.GetConversationHistoryListener;
import com.vectormobile.myvector.domain.interactor.botframework.listener.StartConversationListener;

/**
 * Created by amarina on 15/03/2017.
 */
public interface BFRepository {
  void startConversation(StartConversationListener listener);

  void sendMessage(String message);

  void getHistory(GetConversationHistoryListener getConversationHistoryListener);
}
