package com.vectormobile.myvector.domain.entity.gms.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ajruiz on 30/12/2016.
 */

public class NotificationRequest {

  @SerializedName("tipo")
  @Expose
  private String tipo;
  @SerializedName("fields")
  @Expose
  private List<Object> fields = null;
  @SerializedName("page")
  @Expose
  private Integer page;
  @SerializedName("start")
  @Expose
  private Integer start;
  @SerializedName("limit")
  @Expose
  private Integer limit;

  public String getTipo() {
    return tipo;
  }

  public void setTipo(String tipo) {
    this.tipo = tipo;
  }

  public List<Object> getFields() {
    return fields;
  }

  public void setFields(List<Object> fields) {
    this.fields = fields;
  }

  public Integer getPage() {
    return page;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  public Integer getStart() {
    return start;
  }

  public void setStart(Integer start) {
    this.start = start;
  }

  public Integer getLimit() {
    return limit;
  }

  public void setLimit(Integer limit) {
    this.limit = limit;
  }

}
