package com.vectormobile.myvector.domain.entity.gms.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ajruiz on 30/12/2016.
 */

public class Sort {

  @SerializedName("property")
  @Expose
  private String property;
  @SerializedName("direction")
  @Expose
  private String direction;

  public Sort(String property, String direction) {
    this.property = property;
    this.direction = direction;
  }

  public String getProperty() {
    return property;
  }

  public void setProperty(String property) {
    this.property = property;
  }

  public String getDirection() {
    return direction;
  }

  public void setDirection(String direction) {
    this.direction = direction;
  }

}
