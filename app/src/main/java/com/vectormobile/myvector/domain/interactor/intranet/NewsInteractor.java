package com.vectormobile.myvector.domain.interactor.intranet;

import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;

/**
 * Created by Erik Medina on 15/11/2016.
 */

public interface NewsInteractor {

  void execute(int page, OnItemRetrievedListener listener);

}
