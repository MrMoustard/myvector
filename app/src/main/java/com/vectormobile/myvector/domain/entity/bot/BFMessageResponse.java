package com.vectormobile.myvector.domain.entity.bot;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amarina on 22/03/2017.
 */
public class BFMessageResponse {
  @SerializedName("id")
  @Expose
  private String id;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return "BFMessageResponse{" +
        "id='" + id + '\'' +
        '}';
  }
}
