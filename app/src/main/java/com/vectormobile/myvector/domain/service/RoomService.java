package com.vectormobile.myvector.domain.service;

import com.vectormobile.myvector.domain.entity.bot.BFHistoryResponse;
import com.vectormobile.myvector.domain.entity.bot.BFMessageRequest;
import com.vectormobile.myvector.domain.entity.bot.BFMessageResponse;
import com.vectormobile.myvector.domain.entity.botframework.RsLoginResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by amarina on 15/03/2017.
 */
public interface RoomService {
  String PRIMARY_TOKEN_METADATA_NAME = "botPrimaryToken";

  @Headers("Content-Type: application/json")
  @POST(ServiceManager.BF_BOT_BASE_URL + ServiceManager.BF_BOT_START_CONVERSTAION)
  Call<RsLoginResponse> startConversation(@Header("Authorization") String metaData);

  @Headers("Content-Type: application/json")
  @GET(ServiceManager.BF_BOT_BASE_URL + ServiceManager.BF_BOT_START_CONVERSTAION + "/{conversationId}")
  Call<RsLoginResponse> reconectConversation(
      @Header("Authorization") String metaData,
      @Path("conversationId") String conversationId);

  @Headers("Content-Type: application/json")
  @POST(ServiceManager.BF_BOT_BASE_URL + ServiceManager.BF_BOT_START_CONVERSTAION + "/{conversationId}/activities")
  Call<BFMessageResponse> sendMessage(
      @Header("Authorization") String authentication,
      @Path("conversationId") String conversationId,
      @Body BFMessageRequest request);


  @Headers("Content-Type: application/json")
  @GET(ServiceManager.BF_BOT_BASE_URL + "conversations/{conversationId}/activities")
  Call<BFHistoryResponse> getHistoryByConversationId(
      @Header("Authorization") String authentication,
      @Path("conversationId") String conversacionId
  );
}
