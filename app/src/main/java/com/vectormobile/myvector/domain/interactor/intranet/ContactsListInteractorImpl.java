package com.vectormobile.myvector.domain.interactor.intranet;

import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.domain.repository.IntranetRepository;

/**
 * Created by ajruiz on 01/02/2017.
 */

public class ContactsListInteractorImpl implements ContactsListInteractor {

  private IntranetRepository repository;

  public ContactsListInteractorImpl(IntranetRepository repository) {
    this.repository = repository;
  }

  @Override
  public void execute(int page, String search, OnItemRetrievedListener listener) {
    repository.getContactsList(page, search, listener);
  }
}
