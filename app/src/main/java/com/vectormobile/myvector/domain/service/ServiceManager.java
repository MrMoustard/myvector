package com.vectormobile.myvector.domain.service;

/**
 * Created by ajruiz on 23/11/2016.
 */

public class ServiceManager {

  public static final String LOGIN_ENDPOINT = "offering/api/auth/generate_auth_cookie";
  public static final String MENU_OFFERING_ENDPOINT = "offering/wp-json/wp-api-menus/v2/menus/4";
  public static final String OFFERING_BY_CATEGORY_ENDPOINT = "/offering/wp-json/wp/v2/posts";

  // ---- GMS ----
  public static final String GMS_LOGIN_ENDPOINT = "j_spring_security_check";
  public static final String BASE_GMS_URL = "https://gms.vectoritcgroup.com/ngms-web/";
  public static final String GMS_ROUTER_ENDPOINT = "router";

  // ---- INTRANET_JSON ----
  public static final String BASE_INTRANET_URL = "https://intranet.vectoritcgroup.com/";
  public static final String INTRANET_LOGIN_ENDPOINT = "wp-admin/admin-ajax.php";
  public static final String INTRANET_USER_DATA_ENDPOINT = "el-grupo/vector-responsable/";
  public static final String INTRANET_SAVE_USER_INFO = "wp-admin/admin-ajax.php";

  // ---- BOT_RESERVA_DE_SALAS ----
  public static final String BF_BOT_BASE_URL = "https://directline.botframework.com/v3/directline/";
  public static final String BF_BOT_AUTHENTICATION = "tokens/generate";
  public static final String BF_BOT_START_CONVERSTAION = "conversations";

}
