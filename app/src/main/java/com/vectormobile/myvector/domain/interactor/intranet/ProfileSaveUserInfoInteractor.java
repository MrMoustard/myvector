package com.vectormobile.myvector.domain.interactor.intranet;

import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;

/**
 * Created by ajruiz on 30/01/2017.
 */

public interface ProfileSaveUserInfoInteractor {

  void execute(String attribute, String value, OnItemRetrievedListener listener);

}
