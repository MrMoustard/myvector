package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.model.gms.GmsNotification;

/**
 * Created by ajruiz on 30/12/2016.
 */

public interface RetrieveGmsNotificationsInteractor {

  void execute(OnListRetrievedListener<GmsNotification> listener);

}
