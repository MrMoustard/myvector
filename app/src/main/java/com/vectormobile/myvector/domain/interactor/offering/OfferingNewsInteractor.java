package com.vectormobile.myvector.domain.interactor.offering;

import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;

/**
 * Created by ajruiz on 18/11/2016.
 */

public interface OfferingNewsInteractor {

  void execute(int id, int page, int postsPerPage, OnListRetrievedListener listener);

}
