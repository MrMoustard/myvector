package com.vectormobile.myvector.domain.interactor.intranet;

import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.domain.repository.IntranetRepository;

/**
 * Created by ajruiz on 30/01/2017.
 */

public class ProfileSaveUserInfoInteractorImpl implements ProfileSaveUserInfoInteractor {

  private IntranetRepository repository;

  public ProfileSaveUserInfoInteractorImpl(IntranetRepository repository) {
    this.repository = repository;
  }

  @Override
  public void execute(String attribute, String value, OnItemRetrievedListener listener) {
    repository.saveUserInfo(attribute, value, listener);
  }

}
