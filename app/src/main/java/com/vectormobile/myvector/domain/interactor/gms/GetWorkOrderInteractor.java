package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;

/**
 * Created by Erik Medina on 09/01/2017.
 */

public interface GetWorkOrderInteractor {

  void execute(String projectId, OnListRetrievedListener<GmsResponse> listener);
}
