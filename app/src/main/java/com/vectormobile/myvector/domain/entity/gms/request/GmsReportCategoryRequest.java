package com.vectormobile.myvector.domain.entity.gms.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Erik Medina on 11/01/2017.
 */

public class GmsReportCategoryRequest {

  @SerializedName("imputacionId")
  @Expose
  private String imputationId;
  @SerializedName("proyectoId")
  @Expose
  private String projectId;
  @SerializedName("ordenTrabajoId")
  @Expose
  private String workOrderId;
  @SerializedName("categoria1Id")
  @Expose
  private String category1Id;
  @SerializedName("fecha")
  @Expose
  private String date;
  @SerializedName("page")
  @Expose
  private int page;
  @SerializedName("start")
  @Expose
  private int start;
  @SerializedName("limit")
  @Expose
  private int limit;

  public GmsReportCategoryRequest() {
    imputationId = null;
    page = 1;
    start = 0;
    limit = 25;
  }

  public String getImputationId() {
    return imputationId;
  }

  public void setImputationId(String imputationId) {
    this.imputationId = imputationId;
  }

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public String getWorkOrderId() {
    return workOrderId;
  }

  public void setWorkOrderId(String workOrderId) {
    this.workOrderId = workOrderId;
  }

  public String getCategory1Id() {
    return category1Id;
  }

  public void setCategory1Id(String category1Id) {
    this.category1Id = category1Id;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public int getPage() {
    return page;
  }

  public void setPage(int page) {
    this.page = page;
  }

  public int getStart() {
    return start;
  }

  public void setStart(int start) {
    this.start = start;
  }

  public int getLimit() {
    return limit;
  }

  public void setLimit(int limit) {
    this.limit = limit;
  }
}
