package com.vectormobile.myvector.domain.entity.gms.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Erik Medina on 05/01/2017.
 */
public class Record {

  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("text")
  @Expose
  private String text;
  @SerializedName("enabled")
  @Expose
  private String enabled;
  @SerializedName("fractionDigits")
  @Expose
  private int fractionDigits;
  @SerializedName("simbolo")
  @Expose
  private String symbol;
  @SerializedName("nombreProyecto")
  @Expose
  private String projectName;
  @SerializedName("proyectoNombre")
  @Expose
  private String projectNameHours;
  @SerializedName("nombreContrato")
  @Expose
  private String contractName;
  @SerializedName("nombreOrdenTrabajo")
  @Expose
  private String workOrderName;
  @SerializedName("categoria2Nombre")
  @Expose
  private String categoryName2;
  @SerializedName("ordenTrabajoNombre")
  @Expose
  private String workOrderNameHours;
  @SerializedName("nombreTrabajador")
  @Expose
  private String employeeName;
  @SerializedName("fecha")
  @Expose
  private long date;
  @SerializedName("fechaFormateada")
  @Expose
  private String fomattedDate;
  @SerializedName("importe")
  @Expose
  private double amount;
  @SerializedName("monedaNombre")
  @Expose
  private String currencyName;
  @SerializedName("importeReconocidoGasto")
  @Expose
  private double recognizedExpenseAmount;
  @SerializedName("monedaReconocidoGastoNombre")
  @Expose
  private String recognizedExpenseCurrencyName;
  @SerializedName("nombreCategoriaVenta")
  @Expose
  private String expenseCategoryName;
  @SerializedName("nombreCategoriaReporte")
  @Expose
  private String reportCategoryName;
  @SerializedName("estado")
  @Expose
  private String state;
  @SerializedName("estadoId")
  @Expose
  private String stateId;
  @SerializedName("fechaAceptacion")
  @Expose
  private long acceptanceDate;
  @SerializedName("fechaPago")
  @Expose
  private long paymentDate;
  @SerializedName("tipoTrabajador")
  @Expose
  private String employeeType;
  @SerializedName("fechaGasto")
  @Expose
  private long expenseDate;
  @SerializedName("fechaGastoFormateada")
  @Expose
  private String formattedExpenseDate;
  @SerializedName("tipoAltaImputacion")
  @Expose
  private Object imputationRegistrationType;
  @SerializedName("idProyecto")
  @Expose
  private int projectId;
  @SerializedName("idCliente")
  @Expose
  private int clientId;
  @SerializedName("idContrato")
  @Expose
  private int contractId;
  @SerializedName("idOrdenTrabajo")
  @Expose
  private int workOrderId;
  @SerializedName("idCategoriaVenta")
  @Expose
  private int saleCategoryId;
  @SerializedName("idCategoriaReporte")
  @Expose
  private int reportCategoryId;
  @SerializedName("idTrabajador")
  @Expose
  private int employeeId;
  @SerializedName("unidades")
  @Expose
  private int units;
  @SerializedName("precioUnitario")
  @Expose
  private double unitPrice;
  @SerializedName("precio")
  @Expose
  private double price;
  @SerializedName("moneda")
  @Expose
  private String currency;
  @SerializedName("monedaId")
  @Expose
  private String currencyId;
  @SerializedName("monedaSimbolo")
  @Expose
  private String currencySymbol;
  @SerializedName("monedaDecimales")
  @Expose
  private int currencyDecimals;
  @SerializedName("comentarioImputador")
  @Expose
  private String imputatorComment;
  @SerializedName("comentarioAprobador")
  @Expose
  private String approverComment;
  @SerializedName("estadoNombre")
  @Expose
  private String stateName;
  @SerializedName("fechaDesde")
  @Expose
  private long dateFrom;
  @SerializedName("fechaHasta")
  @Expose
  private long dateTo;
  @SerializedName("semanaFrom")
  @Expose
  private long weekFrom;
  @SerializedName("semanaTo")
  @Expose
  private long weekTo;
  @SerializedName("importeReconocido")
  @Expose
  private double recognizedAmount;
  @SerializedName("monedaReconocidoId")
  @Expose
  private String recognizedCurrencyId;
  @SerializedName("monedaReconocidoSimbolo")
  @Expose
  private String recognizedCurrencySymbol;
  @SerializedName("overrun")
  @Expose
  private boolean overrun;
  @SerializedName("overrunEditable")
  @Expose
  private Object overrunEditable;
  @SerializedName("monedaReconocidoGastoId")
  @Expose
  private String recognizedExpenseCurrencyId;
  @SerializedName("monedaReconocidoGastoSimbolo")
  @Expose
  private String recognizedExpenseCurrencySymbol;
  @SerializedName("codigoExterno")
  @Expose
  private String externalCode;
  @SerializedName("clave")
  @Expose
  public String key;
  @SerializedName("descripcion")
  @Expose
  public String description;
  @SerializedName("imputacionJira")
  @Expose
  public String jiraCode;
  @SerializedName("jirable")
  @Expose
  public boolean jirable;


  public String getStateId() {
    return stateId;
  }

  public void setStateId(String stateId) {
    this.stateId = stateId;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getEnabled() {
    return enabled;
  }

  public void setEnabled(String enabled) {
    this.enabled = enabled;
  }

  public int getFractionDigits() {
    return fractionDigits;
  }

  public void setFractionDigits(int fractionDigits) {
    this.fractionDigits = fractionDigits;
  }

  public String getSymbol() {
    return symbol;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }

  public String getProjectName() {
    return projectName;
  }

  public void setProjectName(String projectName) {
    this.projectName = projectName;
  }

  public String getContractName() {
    return contractName;
  }

  public void setContractName(String contractName) {
    this.contractName = contractName;
  }

  public String getWorkOrderName() {
    return workOrderName;
  }

  public void setWorkOrderName(String workOrderName) {
    this.workOrderName = workOrderName;
  }

  public String getCategoryName2() {
    return categoryName2;
  }

  public void setCategoryName2(String categoryName2) {
    this.categoryName2 = categoryName2;
  }

  public String getWorkOrderNameHours() {
    return workOrderNameHours;
  }

  public void setWorkOrderNameHours(String workOrderNameHours) {
    this.workOrderNameHours = workOrderNameHours;
  }

  public String getEmployeeName() {
    return employeeName;
  }

  public void setEmployeeName(String employeeName) {
    this.employeeName = employeeName;
  }

  public long getDate() {
    return date;
  }

  public void setDate(long date) {
    this.date = date;
  }

  public String getFomattedDate() {
    return fomattedDate;
  }

  public void setFomattedDate(String fomattedDate) {
    this.fomattedDate = fomattedDate;
  }

  public double getAmount() {
    return amount;
  }

  public void setAmount(double amount) {
    this.amount = amount;
  }

  public String getCurrencyName() {
    return currencyName;
  }

  public void setCurrencyName(String currencyName) {
    this.currencyName = currencyName;
  }

  public double getRecognizedExpenseAmount() {
    return recognizedExpenseAmount;
  }

  public void setRecognizedExpenseAmount(double recognizedExpenseAmount) {
    this.recognizedExpenseAmount = recognizedExpenseAmount;
  }

  public String getRecognizedExpenseCurrencyName() {
    return recognizedExpenseCurrencyName;
  }

  public void setRecognizedExpenseCurrencyName(String recognizedExpenseCurrencyName) {
    this.recognizedExpenseCurrencyName = recognizedExpenseCurrencyName;
  }

  public String getExpenseCategoryName() {
    return expenseCategoryName;
  }

  public void setExpenseCategoryName(String expenseCategoryName) {
    this.expenseCategoryName = expenseCategoryName;
  }

  public String getReportCategoryName() {
    return reportCategoryName;
  }

  public void setReportCategoryName(String reportCategoryName) {
    this.reportCategoryName = reportCategoryName;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public long getAcceptanceDate() {
    return acceptanceDate;
  }

  public void setAcceptanceDate(long acceptanceDate) {
    this.acceptanceDate = acceptanceDate;
  }

  public long getPaymentDate() {
    return paymentDate;
  }

  public void setPaymentDate(long paymentDate) {
    this.paymentDate = paymentDate;
  }

  public String getEmployeeType() {
    return employeeType;
  }

  public void setEmployeeType(String employeeType) {
    this.employeeType = employeeType;
  }

  public long getExpenseDate() {
    return expenseDate;
  }

  public void setExpenseDate(long expenseDate) {
    this.expenseDate = expenseDate;
  }

  public String getFormattedExpenseDate() {
    return formattedExpenseDate;
  }

  public void setFormattedExpenseDate(String formattedExpenseDate) {
    this.formattedExpenseDate = formattedExpenseDate;
  }

  public Object getImputationRegistrationType() {
    return imputationRegistrationType;
  }

  public void setImputationRegistrationType(Object imputationRegistrationType) {
    this.imputationRegistrationType = imputationRegistrationType;
  }

  public int getProjectId() {
    return projectId;
  }

  public void setProjectId(int projectId) {
    this.projectId = projectId;
  }

  public int getClientId() {
    return clientId;
  }

  public void setClientId(int clientId) {
    this.clientId = clientId;
  }

  public int getContractId() {
    return contractId;
  }

  public void setContractId(int contractId) {
    this.contractId = contractId;
  }

  public int getWorkOrderId() {
    return workOrderId;
  }

  public void setWorkOrderId(int workOrderId) {
    this.workOrderId = workOrderId;
  }

  public int getSaleCategoryId() {
    return saleCategoryId;
  }

  public void setSaleCategoryId(int saleCategoryId) {
    this.saleCategoryId = saleCategoryId;
  }

  public int getReportCategoryId() {
    return reportCategoryId;
  }

  public void setReportCategoryId(int reportCategoryId) {
    this.reportCategoryId = reportCategoryId;
  }

  public int getEmployeeId() {
    return employeeId;
  }

  public void setEmployeeId(int employeeId) {
    this.employeeId = employeeId;
  }

  public int getUnits() {
    return units;
  }

  public void setUnits(int units) {
    this.units = units;
  }

  public double getUnitPrice() {
    return unitPrice;
  }

  public void setUnitPrice(double unitPrice) {
    this.unitPrice = unitPrice;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public String getCurrencyId() {
    return currencyId;
  }

  public void setCurrencyId(String currencyId) {
    this.currencyId = currencyId;
  }

  public String getCurrencySymbol() {
    return currencySymbol;
  }

  public void setCurrencySymbol(String currencySymbol) {
    this.currencySymbol = currencySymbol;
  }

  public int getCurrencyDecimals() {
    return currencyDecimals;
  }

  public void setCurrencyDecimals(int currencyDecimals) {
    this.currencyDecimals = currencyDecimals;
  }

  public String getImputatorComment() {
    return imputatorComment;
  }

  public void setImputatorComment(String imputatorComment) {
    this.imputatorComment = imputatorComment;
  }

  public String getApproverComment() {
    return approverComment;
  }

  public void setApproverComment(String approverComment) {
    this.approverComment = approverComment;
  }

  public String getStateName() {
    return stateName;
  }

  public void setStateName(String stateName) {
    this.stateName = stateName;
  }

  public long getDateFrom() {
    return dateFrom;
  }

  public void setDateFrom(long dateFrom) {
    this.dateFrom = dateFrom;
  }

  public long getDateTo() {
    return dateTo;
  }

  public void setDateTo(long dateTo) {
    this.dateTo = dateTo;
  }

  public long getWeekFrom() {
    return weekFrom;
  }

  public void setWeekFrom(long weekFrom) {
    this.weekFrom = weekFrom;
  }

  public long getWeekTo() {
    return weekTo;
  }

  public void setWeekTo(long weekTo) {
    this.weekTo = weekTo;
  }

  public double getRecognizedAmount() {
    return recognizedAmount;
  }

  public void setRecognizedAmount(double recognizedAmount) {
    this.recognizedAmount = recognizedAmount;
  }

  public String getRecognizedCurrencyId() {
    return recognizedCurrencyId;
  }

  public void setRecognizedCurrencyId(String recognizedCurrencyId) {
    this.recognizedCurrencyId = recognizedCurrencyId;
  }

  public String getRecognizedCurrencySymbol() {
    return recognizedCurrencySymbol;
  }

  public void setRecognizedCurrencySymbol(String recognizedCurrencySymbol) {
    this.recognizedCurrencySymbol = recognizedCurrencySymbol;
  }

  public boolean isOverrun() {
    return overrun;
  }

  public void setOverrun(boolean overrun) {
    this.overrun = overrun;
  }

  public Object getOverrunEditable() {
    return overrunEditable;
  }

  public void setOverrunEditable(Object overrunEditable) {
    this.overrunEditable = overrunEditable;
  }

  public String getRecognizedExpenseCurrencyId() {
    return recognizedExpenseCurrencyId;
  }

  public void setRecognizedExpenseCurrencyId(String recognizedExpenseCurrencyId) {
    this.recognizedExpenseCurrencyId = recognizedExpenseCurrencyId;
  }

  public String getRecognizedExpenseCurrencySymbol() {
    return recognizedExpenseCurrencySymbol;
  }

  public void setRecognizedExpenseCurrencySymbol(String recognizedExpenseCurrencySymbol) {
    this.recognizedExpenseCurrencySymbol = recognizedExpenseCurrencySymbol;
  }

  public String getProjectNameHours() {
    return projectNameHours;
  }

  public void setProjectNameHours(String projectNameHours) {
    this.projectNameHours = projectNameHours;
  }

  public String getExternalCode() {
    return externalCode;
  }

  public void setExternalCode(String externalCode) {
    this.externalCode = externalCode;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getJiraCode() {
    return jiraCode;
  }

  public void setJiraCode(String jiraCode) {
    this.jiraCode = jiraCode;
  }

  public boolean isJirable() {
    return jirable;
  }

  public void setJirable(boolean jirable) {
    this.jirable = jirable;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Record record = (Record) o;

    return id != null ? id.equals(record.id) : record.id == null;

  }

  @Override
  public int hashCode() {
    return id != null ? id.hashCode() : 0;
  }
}
