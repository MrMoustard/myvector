package com.vectormobile.myvector.domain.interactor.intranet;

import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.domain.repository.IntranetRepository;

/**
 * Created by ajruiz on 11/01/2017.
 */

public class ProfileDataInteractorImpl implements ProfileDataInteractor {

  private IntranetRepository repository;

  public ProfileDataInteractorImpl(IntranetRepository repository) {
    this.repository = repository;
  }

  @Override
  public void execute(OnItemRetrievedListener listener) {
    repository.getUserProfile(listener);
  }

}
