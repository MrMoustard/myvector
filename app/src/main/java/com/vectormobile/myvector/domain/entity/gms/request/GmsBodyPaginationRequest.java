package com.vectormobile.myvector.domain.entity.gms.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ajruiz on 19/01/2017.
 */

public class GmsBodyPaginationRequest extends GmsBodyRequest {

  @SerializedName("page")
  @Expose
  private int page;
  @SerializedName("start")
  @Expose
  private int start;
  @SerializedName("limit")
  @Expose
  private int limit;

  public GmsBodyPaginationRequest() {
    this.page = 0;
    this.start = 0;
    this.limit = 25;
  }

  public int getLimit() {
    return limit;
  }

  public void setLimit(int limit) {
    this.limit = limit;
  }

  public int getPage() {
    return page;
  }

  public void setPage(int page) {
    this.page = page;
  }

  public int getStart() {
    return start;
  }

  public void setStart(int start) {
    this.start = start;
  }
}
