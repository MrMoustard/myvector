package com.vectormobile.myvector.domain.interactor.listener;

import java.util.List;

/**
 * Created by ajruiz on 22/12/2016.
 */

public interface OnListRetrievedListener<T> {

  void onSuccess(List<T> list);

  void onError(int errorId);

}
