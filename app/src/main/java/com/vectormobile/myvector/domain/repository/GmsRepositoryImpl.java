package com.vectormobile.myvector.domain.repository;

import com.google.gson.internal.LinkedTreeMap;

import com.vectormobile.myvector.R;
import com.vectormobile.myvector.domain.entity.gms.request.ExpenseRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsBodyPaginationRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsBodyRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsCurrencyRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsDayStateRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsExpenseBodyRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsExpenseCategoryRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsFindExpenseByCreateTimeRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsJiraBrowserRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsJiraRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsReportCategoryRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsTimeLapseExpenseBodyRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsTimeLapseHourBodyRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsWorkOrderRequest;
import com.vectormobile.myvector.domain.entity.gms.request.NotificationRequest;
import com.vectormobile.myvector.domain.entity.gms.request.Sort;
import com.vectormobile.myvector.domain.entity.gms.request.SortCriteria;
import com.vectormobile.myvector.domain.entity.gms.response.GmsDayStateResponse;
import com.vectormobile.myvector.domain.entity.gms.response.GmsDayStateResult;
import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.entity.gms.response.notification.NotificationEntity;
import com.vectormobile.myvector.domain.interactor.listener.DownloadPdfListener;
import com.vectormobile.myvector.domain.interactor.listener.MultiActionGmsListener;
import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.domain.service.GmsService;
import com.vectormobile.myvector.model.gms.GmsNotification;
import com.vectormobile.myvector.model.gms.GmsNotificationMapper;
import com.vectormobile.myvector.storage.ImageManager;
import com.vectormobile.myvector.util.text.TextUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GmsRepositoryImpl implements GmsRepository {

  private GmsService service;

  public GmsRepositoryImpl(GmsService service) {
    this.service = service;
  }

  @Override
  public void validateLogin(String email, String password, final OnItemRetrievedListener listener) {
    Call<ResponseBody> call = service.userLogin(email, password, false);
    call.enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        listener.onSuccess(null);
      }

      @Override
      public void onFailure(Call<ResponseBody> call, Throwable t) {
        listener.onError(R.string.error_gms_login_failure);
      }
    });
  }

  @Override
  public void retrieveGmsNotifications(final OnListRetrievedListener<GmsNotification> listener) {
    GmsRequest request = new GmsRequest();
    request.setAction(TextUtils.Gms.NOTIFICATION_CHECK_CONTROLLER);
    request.setMethod(TextUtils.Gms.NOTIFICATION_GET_NEW);
    request.setData(null);
    Call<List<GmsResponse>> call = service.getUserNotifications(request);
    call.enqueue(new Callback<List<GmsResponse>>() {
      @Override
      public void onResponse(Call<List<GmsResponse>> call, Response<List<GmsResponse>> response) {
        listener.onSuccess(parseNotificationResponse(response.body()));
      }

      @Override
      public void onFailure(Call<List<GmsResponse>> call, Throwable t) {
        listener.onError(R.string.error_gms_notification_failure);
      }
    });
  }

  private List<GmsRequest> prepareNotificationRequest() {
    List<GmsRequest> requests = new ArrayList<>();
    GmsRequest<GmsBodyPaginationRequest> itemRequest = new GmsRequest<>();
    NotificationRequest notificationRequest = new NotificationRequest();
    SortCriteria sortCriteria = new SortCriteria();

    itemRequest.setAction(TextUtils.Gms.NOTIFICATION_ACTION_INBOX_CONTROLLER);
    itemRequest.setMethod(TextUtils.Gms.NOTIFICATION_METHOD_LIST_ALL_ACTIVE_AS_OPTION);

    notificationRequest.setTipo(TextUtils.Gms.NOTIFICATION_TYPE_STATE);
    notificationRequest.setPage(1);
    notificationRequest.setStart(0);
    notificationRequest.setLimit(25);
    itemRequest.setData(null);
    itemRequest.setType(TextUtils.Gms.NOTIFICATION_TYPE);
    itemRequest.setTid(15);
    requests.add(itemRequest);


    itemRequest = new GmsRequest();
    itemRequest.setAction(TextUtils.Gms.NOTIFICATION_CHECK_CONTROLLER);
    itemRequest.setMethod(TextUtils.Gms.NOTIFICATION_METHOD_CHECK);
    itemRequest.setData(null);
    itemRequest.setType(TextUtils.Gms.NOTIFICATION_TYPE);
    itemRequest.setTid(16);
    requests.add(itemRequest);

    itemRequest = new GmsRequest();
    itemRequest.setAction(TextUtils.Gms.NOTIFICATION_ACTION_INBOX_CONTROLLER);
    itemRequest.setMethod(TextUtils.Gms.NOTIFICATION_METHOD_LIST);

    sortCriteria.setPage(1);
    sortCriteria.setStart(0);
    sortCriteria.setLimit(25);
    sortCriteria.setSort(Arrays.asList(new Sort("fechaEmision", "DESC")));

    itemRequest.setData(null);
    itemRequest.setType(TextUtils.Gms.NOTIFICATION_TYPE);
    itemRequest.setTid(17);
    requests.add(itemRequest);

    return requests;
  }

  private List<GmsNotification> parseNotificationResponse(List<GmsResponse> body) {
    List<GmsNotification> gmsNotifications = new ArrayList<>();
    for (GmsResponse itemResponse : body) {
      if (itemResponse.getType().equalsIgnoreCase(TextUtils.Gms.NOTIFICATION_TYPE)) {
        for (Object item : itemResponse.getResult().getRecords()) {
          gmsNotifications.add(GmsNotificationMapper.fromResponse(new NotificationEntity((LinkedTreeMap) item)));
        }
      }
    }
    return gmsNotifications;
  }

  @Override
  public void getCurrencies(final OnListRetrievedListener<GmsResponse> listener) {
    GmsCurrencyRequest currencyData = new GmsCurrencyRequest();
    List<GmsCurrencyRequest> currencyDataList = new ArrayList<>();
    currencyDataList.add(currencyData);

    GmsRequest<GmsCurrencyRequest> gmsRequest = new GmsRequest<>();
    gmsRequest.setAction(TextUtils.Gms.ACTION_COMBOS_CONTROLLER);
    gmsRequest.setMethod("list");
    gmsRequest.setData(currencyDataList);
    Call<List<GmsResponse>> call = service.getGmsData(gmsRequest);
    call.enqueue(new Callback<List<GmsResponse>>() {
      @Override
      public void onResponse(Call<List<GmsResponse>> call, Response<List<GmsResponse>> response) {
        if (response.isSuccessful()) {
          listener.onSuccess(response.body());
        } else {
          listener.onError(R.string.error_gms_currency_failure);
        }
      }

      @Override
      public void onFailure(Call<List<GmsResponse>> call, Throwable t) {
        listener.onError(R.string.error_gms_currency_failure);
      }
    });
  }

  @Override
  public void buildGmsExpenseRequest(String action, String method, GmsExpenseBodyRequest gmsExpenseBodyRequest,
                                     final OnListRetrievedListener<GmsResponse> listener) {

    List<GmsExpenseBodyRequest> gmsBodyRequests = new ArrayList<>();
    gmsBodyRequests.add(gmsExpenseBodyRequest);

    GmsRequest<GmsExpenseBodyRequest> gmsRequest = new GmsRequest<>();
    gmsRequest.setAction(action);
    gmsRequest.setMethod(method);
    gmsRequest.setData(gmsBodyRequests);

    Call<List<GmsResponse>> call = service.getGmsData(gmsRequest);
    call.enqueue(new Callback<List<GmsResponse>>() {
      @Override
      public void onResponse(Call<List<GmsResponse>> call, Response<List<GmsResponse>> response) {
        if (response.isSuccessful()) {
          listener.onSuccess(response.body());
        } else {
          listener.onError(R.string.error_gms_failure);
        }
      }

      @Override
      public void onFailure(Call<List<GmsResponse>> call, Throwable t) {
        listener.onError(R.string.error_gms_failure);
      }
    });
  }

  @Override
  public void buildGmsTimeLapseExpenseRequest(String action, String method, GmsTimeLapseExpenseBodyRequest bodyRequest,
                                              final OnListRetrievedListener<GmsResponse> listener) {

    List<GmsTimeLapseExpenseBodyRequest> gmsBodyRequests = new ArrayList<>();
    gmsBodyRequests.add(bodyRequest);

    GmsRequest<GmsTimeLapseExpenseBodyRequest> gmsRequest = new GmsRequest<>();
    gmsRequest.setAction(action);
    gmsRequest.setMethod(method);
    gmsRequest.setData(gmsBodyRequests);
    gmsRequest.setTid(14);

    Call<List<GmsResponse>> call = service.getGmsData(gmsRequest);
    call.enqueue(new Callback<List<GmsResponse>>() {
      @Override
      public void onResponse(Call<List<GmsResponse>> call, Response<List<GmsResponse>> response) {
        if (response.isSuccessful()) {
          listener.onSuccess(response.body());
        } else {
          listener.onError(R.string.error_gms_failure);
        }
      }

      @Override
      public void onFailure(Call<List<GmsResponse>> call, Throwable t) {
        listener.onError(R.string.error_gms_failure);
      }
    });
  }

  @Override
  public void buildGmsHourRequest(String action, String method, GmsTimeLapseHourBodyRequest gmsHourBodyRequest,
                                  final OnListRetrievedListener<GmsResponse> listener) {

    List<GmsTimeLapseHourBodyRequest> gmsBodyRequests = new ArrayList<>();
    gmsBodyRequests.add(gmsHourBodyRequest);

    GmsRequest<GmsTimeLapseHourBodyRequest> gmsRequest = new GmsRequest<>();
    gmsRequest.setAction(action);
    gmsRequest.setMethod(method);
    gmsRequest.setData(gmsBodyRequests);

    Call<List<GmsResponse>> call = service.getGmsData(gmsRequest);
    call.enqueue(new Callback<List<GmsResponse>>() {
      @Override
      public void onResponse(Call<List<GmsResponse>> call, Response<List<GmsResponse>> response) {
        if (response.isSuccessful()) {
          listener.onSuccess(response.body());
        } else {
          listener.onError(R.string.error_gms_failure);
        }
      }

      @Override
      public void onFailure(Call<List<GmsResponse>> call, Throwable t) {
        listener.onError(R.string.error_gms_failure);

      }
    });
  }

  @Override
  public void sendExpenseWithImage(
      ExpenseRequest expenseRequest,
      GmsFindExpenseByCreateTimeRequest countRequest,
      String expenseAction,
      String expenseMethod,
      String countAction,
      final MultiActionGmsListener multiActionGmsListener) {

    List<ExpenseRequest> expenses = new ArrayList<>();
    expenses.add(expenseRequest);

    GmsRequest<ExpenseRequest> gmsRequest = new GmsRequest<>();
    gmsRequest.setAction(expenseAction);
    gmsRequest.setMethod(expenseMethod);
    gmsRequest.setType("rpc");
    gmsRequest.setTid(25);
    gmsRequest.setData(expenses);
    GmsRequest<GmsFindExpenseByCreateTimeRequest> createDayRequest = new GmsRequest<>();
    createDayRequest.setAction(TextUtils.Gms.ACTION_IMPUTATION_EXPENSE_CONTROLLER);
    createDayRequest.setMethod(TextUtils.Gms.METHOD_LIST);
    createDayRequest.setData(Arrays.asList(countRequest));

    List<GmsRequest> gmsRequests = new ArrayList<>();
    gmsRequests.add(createDayRequest);
    gmsRequests.add(gmsRequest);
    gmsRequests.add(createDayRequest);
    Call<List<GmsResponse>> call = service.getGmsMultiActionData(gmsRequests);
    call.enqueue(new Callback<List<GmsResponse>>() {
      @Override
      public void onResponse(Call<List<GmsResponse>> call, Response<List<GmsResponse>> response) {
        if (response.isSuccessful() && checkResponse(response.body())) {
          multiActionGmsListener.onSuccess(response.body());
        } else {
          multiActionGmsListener.onUnknowError();
        }
      }

      @Override
      public void onFailure(Call<List<GmsResponse>> call, Throwable throwable) {
        multiActionGmsListener.onNetworkError();
      }
    });
  }

  @Override
  public void getJiraCodes(String action, String method, GmsJiraBrowserRequest gmsJiraBrowserRequest,
                           final OnListRetrievedListener<GmsResponse> listener) {
    List<GmsJiraBrowserRequest> gmsBodyRequests = new ArrayList<>();
    gmsBodyRequests.add(gmsJiraBrowserRequest);

    GmsRequest<GmsJiraBrowserRequest> gmsRequest = new GmsRequest<>();
    gmsRequest.setAction(action);
    gmsRequest.setMethod(method);
    gmsRequest.setData(gmsBodyRequests);

    Call<List<GmsResponse>> call = service.getGmsData(gmsRequest);
    call.enqueue(new Callback<List<GmsResponse>>() {

      @Override
      public void onResponse(Call<List<GmsResponse>> call, Response<List<GmsResponse>> response) {
        if (response.isSuccessful()) {
          listener.onSuccess(response.body());
        } else {
          listener.onError(R.string.error_gms_jiras_failure);
        }
      }

      @Override
      public void onFailure(Call<List<GmsResponse>> call, Throwable t) {
        listener.onError(R.string.error_gms_jiras_failure);
      }
    });
  }

  @Override
  public void getJiraCodesAssigned(GmsJiraRequest gmsJiraRequest,
                                   final OnListRetrievedListener<GmsResponse> listener) {

    Call<List<GmsResponse>> call = service.getGmsData(gmsJiraRequest);
    call.enqueue(new Callback<List<GmsResponse>>() {

      @Override
      public void onResponse(Call<List<GmsResponse>> call, Response<List<GmsResponse>> response) {
        if (response.isSuccessful()) {
          listener.onSuccess(response.body());
        } else {
          listener.onError(R.string.error_gms_jiras_failure);
        }
      }

      @Override
      public void onFailure(Call<List<GmsResponse>> call, Throwable t) {
        listener.onError(R.string.error_gms_jiras_failure);
      }
    });
  }

  private boolean checkResponse(List<GmsResponse> gmsResponses) {
    for (GmsResponse gmsResponse : gmsResponses) {
      if ("exception".equals(gmsResponse.getType()))
        return false;
    }
    return true;
  }

  @Override
  public void getImputableProjects(String action, String method, GmsBodyRequest gmsBodyRequest,
                                   final OnListRetrievedListener<GmsResponse> listener) {

    List<GmsBodyRequest> gmsBodyRequests = new ArrayList<>();
    gmsBodyRequests.add(gmsBodyRequest);

    GmsRequest<GmsBodyRequest> gmsRequest = new GmsRequest<>();
    gmsRequest.setAction(action);
    gmsRequest.setMethod(method);
    gmsRequest.setData(gmsBodyRequests);

    Call<List<GmsResponse>> call = service.getGmsData(gmsRequest);
    call.enqueue(new Callback<List<GmsResponse>>() {
      @Override
      public void onResponse(Call<List<GmsResponse>> call, Response<List<GmsResponse>> response) {
        if (response.isSuccessful()) {
          listener.onSuccess(response.body());
        } else {
          listener.onError(R.string.error_gms_projects_failure);
        }
      }

      @Override
      public void onFailure(Call<List<GmsResponse>> call, Throwable t) {
        listener.onError(R.string.error_gms_projects_failure);

      }
    });
  }

  @Override
  public void getWorkOrders(String projectId, final OnListRetrievedListener<GmsResponse> listener) {
    GmsWorkOrderRequest gmsWorkOrderRequest = new GmsWorkOrderRequest();
    gmsWorkOrderRequest.setProjectId(projectId);
    List<GmsWorkOrderRequest> workOrderData = new ArrayList<>();
    workOrderData.add(gmsWorkOrderRequest);

    GmsRequest<GmsWorkOrderRequest> gmsRequest = new GmsRequest<>();
    gmsRequest.setAction(TextUtils.Gms.ACTION_IMPUTATION_EXPENSE_CONTROLLER);
    gmsRequest.setMethod(TextUtils.Gms.IMPUTABLE_WORK_ORDER);
    gmsRequest.setData(workOrderData);

    Call<List<GmsResponse>> call = service.getGmsData(gmsRequest);
    call.enqueue(new Callback<List<GmsResponse>>() {
      @Override
      public void onResponse(Call<List<GmsResponse>> call, Response<List<GmsResponse>> response) {
        if (response.isSuccessful()) {
          listener.onSuccess(response.body());
        } else {
          listener.onError(R.string.error_gms_work_orders_failure);
        }
      }

      @Override
      public void onFailure(Call<List<GmsResponse>> call, Throwable t) {
        listener.onError(R.string.error_gms_work_orders_failure);
      }
    });
  }

  @Override
  public void getExpenseCategories(String projectId, String workOrderId, final OnListRetrievedListener<GmsResponse> listener) {
    GmsExpenseCategoryRequest gmsExpenseCategoryRequest = new GmsExpenseCategoryRequest();
    gmsExpenseCategoryRequest.setProjectId(projectId);
    gmsExpenseCategoryRequest.setWorkOrderId(workOrderId);

    List<GmsExpenseCategoryRequest> expenseCategoryData = new ArrayList<>();
    expenseCategoryData.add(gmsExpenseCategoryRequest);

    GmsRequest<GmsExpenseCategoryRequest> gmsRequest = new GmsRequest<>();
    gmsRequest.setAction(TextUtils.Gms.ACTION_IMPUTATION_EXPENSE_CONTROLLER);
    gmsRequest.setMethod(TextUtils.Gms.METHOD_IMPUTABLE_CATEGORIES_1);
    gmsRequest.setData(expenseCategoryData);

    Call<List<GmsResponse>> call = service.getGmsData(gmsRequest);
    call.enqueue(new Callback<List<GmsResponse>>() {
      @Override
      public void onResponse(Call<List<GmsResponse>> call, Response<List<GmsResponse>> response) {
        if (response.isSuccessful()) {
          listener.onSuccess(response.body());
        } else {
          listener.onError(R.string.error_gms_categories_failure);
        }
      }

      @Override
      public void onFailure(Call<List<GmsResponse>> call, Throwable t) {
        listener.onError(R.string.error_gms_categories_failure);
      }
    });
  }

  @Override
  public void saveExpense(ExpenseRequest expense, String action, String method, final OnListRetrievedListener<GmsResponse> listener) {
    List<ExpenseRequest> expenses = new ArrayList<>();
    expenses.add(expense);

    GmsRequest<ExpenseRequest> gmsRequest = new GmsRequest<>();
    gmsRequest.setAction(action);
    gmsRequest.setMethod(method);
    gmsRequest.setType("rpc");
    gmsRequest.setTid(25);
    gmsRequest.setData(expenses);

    Call<List<GmsResponse>> call = service.getGmsData(gmsRequest);
    call.enqueue(new Callback<List<GmsResponse>>() {
      @Override
      public void onResponse(Call<List<GmsResponse>> call, Response<List<GmsResponse>> response) {
        if (response.isSuccessful() && !"exception".equals(response.body().get(0).getType())) {
          listener.onSuccess(response.body());
        } else {
          listener.onError(R.string.error_gms_save_failure);
        }
      }

      @Override
      public void onFailure(Call<List<GmsResponse>> call, Throwable t) {
        listener.onError(R.string.error_gms_save_failure);
      }
    });
  }

  @Override
  public void sendExpense(GmsRequest gmsRequest, final OnListRetrievedListener<GmsResponse> listener) {
    Call<List<GmsResponse>> call = service.getGmsData(gmsRequest);
    call.enqueue(new Callback<List<GmsResponse>>() {
      @Override
      public void onResponse(Call<List<GmsResponse>> call, Response<List<GmsResponse>> response) {
        if (response.isSuccessful()) {
          listener.onSuccess(response.body());
        } else {
          listener.onError(R.string.error_gms_send_failure);
        }
      }

      @Override
      public void onFailure(Call<List<GmsResponse>> call, Throwable t) {
        listener.onError(R.string.error_gms_send_failure);
      }
    });
  }

  @Override
  public void getReportCategory(GmsReportCategoryRequest gmsReportCategoryRequest,
                                final OnListRetrievedListener<GmsResponse> listener) {

    List<GmsReportCategoryRequest> reportCategories = new ArrayList<>();
    reportCategories.add(gmsReportCategoryRequest);

    GmsRequest<GmsReportCategoryRequest> gmsRequest = new GmsRequest<>();
    gmsRequest.setAction(TextUtils.Gms.ACTION_IMPUTATION_EXPENSE_CONTROLLER);
    gmsRequest.setMethod(TextUtils.Gms.METHOD_IMPUTABLE_CATEGORIES_2);
    gmsRequest.setData(reportCategories);

    Call<List<GmsResponse>> call = service.getGmsData(gmsRequest);
    call.enqueue(new Callback<List<GmsResponse>>() {
      @Override
      public void onResponse(Call<List<GmsResponse>> call, Response<List<GmsResponse>> response) {
        if (response.isSuccessful()) {
          listener.onSuccess(response.body());
        } else {
          listener.onError(R.string.error_gms_failure);
        }
      }

      @Override
      public void onFailure(Call<List<GmsResponse>> call, Throwable t) {
        listener.onError(R.string.error_gms_failure);
      }
    });
  }

  @Override
  public void downloadPdfImputation(final List<String> imputationIds, final DownloadPdfListener downloadPdfListener) {
    Call<ResponseBody> callDownloadPdf = service.getImputationPdf("notaGasto.pdf", imputationIds);
    callDownloadPdf.enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        if (response.isSuccessful()) {
          if(writeResponseBodyToDisk(response.body())){
            downloadPdfListener.onSuccess(imputationIds);
          } else {
            downloadPdfListener.onSavePdfError();
          }

        } else {
          downloadPdfListener.onNetworkError();
        }
      }

      @Override
      public void onFailure(Call<ResponseBody> call, Throwable t) {
        downloadPdfListener.onNetworkError();
      }
    });
  }

  private boolean writeResponseBodyToDisk(ResponseBody body) {
    try {
      File xlsVectorImputation = ImageManager.getInstance().getFileToWrite("notaGasto.pdf");

      InputStream inputStream = null;
      OutputStream outputStream = null;

      try {
        byte[] fileReader = new byte[4096];

        long fileSize = body.contentLength();
        long fileSizeDownloaded = 0;

        inputStream = body.byteStream();
        outputStream = new FileOutputStream(xlsVectorImputation);

        while (true) {
          int read = inputStream.read(fileReader);

          if (read == -1) {
            break;
          }

          outputStream.write(fileReader, 0, read);

          fileSizeDownloaded += read;

          Timber.d("file download: " + fileSizeDownloaded + " of " + fileSize);
        }

        outputStream.flush();

        return true;
      } catch (IOException e) {
        return false;
      } finally {
        if (inputStream != null) {
          inputStream.close();
        }

        if (outputStream != null) {
          outputStream.close();
        }
      }
    } catch (IOException e) {
      return false;
    }
  }

  @Override
  public void getMonthDaysState(GmsDayStateRequest request, final OnItemRetrievedListener<GmsDayStateResult> listener) {
    Call<List<GmsDayStateResponse>> call = service.getMonthDaysState(request);
    call.enqueue(new Callback<List<GmsDayStateResponse>>() {
      @Override
      public void onResponse(Call<List<GmsDayStateResponse>> call, Response<List<GmsDayStateResponse>> response) {
        listener.onSuccess(response.body().get(0).getResult());
      }

      @Override
      public void onFailure(Call<List<GmsDayStateResponse>> call, Throwable t) {
        listener.onError(R.string.error_gms_day_state_failure);
      }
    });
  }
}
