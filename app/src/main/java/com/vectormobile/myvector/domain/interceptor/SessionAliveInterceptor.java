package com.vectormobile.myvector.domain.interceptor;

import com.vectormobile.myvector.App;
import com.vectormobile.myvector.storage.SessionManager;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import java.io.IOException;

/**
 * Created by amarina on 03/02/2017.
 */

public class SessionAliveInterceptor implements Interceptor {

  public static final int CUSTOM_CODE_SESSION_EXPIRED = 403;
  private SessionManager sessionManager;
  private App app;

  public SessionAliveInterceptor(SessionManager sessionManager, App app) {
    this.sessionManager = sessionManager;
    this.app = app;
  }

  @Override
  public Response intercept(Chain chain) throws IOException {
    Request request = chain.request();
    Response response = chain.proceed(request);
    if (request.url().toString().contains("indexFailLoginSimple.html")
        || request.url().toString().contains("intranet.vectoritcgroup.com/login/")) {
      sessionManager.logout();
      app.getCurrentActivity().forceFinishSession();
      return new Response.Builder()
          .code(CUSTOM_CODE_SESSION_EXPIRED)
          .message("Se ha caducado la sesión")
          .request(response.request())
          .protocol(response.protocol())
          .body(ResponseBody.create(MediaType.parse("text/plain"), "Se ha caducado la sesión"))
          .build();
    }
    return response;
  }
}
