package com.vectormobile.myvector.domain.repository;

import com.vectormobile.myvector.R;
import com.vectormobile.myvector.domain.entity.intranet.ContactsResponse;
import com.vectormobile.myvector.domain.entity.intranet.NewsResponse;
import com.vectormobile.myvector.domain.entity.intranet.UploadImageResponse;
import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.domain.interceptor.SessionAliveInterceptor;
import com.vectormobile.myvector.domain.service.IntranetServiceJson;
import com.vectormobile.myvector.domain.service.IntranetServiceXml;
import com.vectormobile.myvector.storage.SessionManager;
import com.vectormobile.myvector.util.text.TextUtils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import java.io.File;

/**
 * Created by ajruiz on 20/12/2016.
 */

public class IntranetRepositoryImpl implements IntranetRepository {

  public static final int NEWS_PER_PAGE = 3;
  public static final int CONTACTS_PER_PAGE = 5;
  public static final int REMAINING_CONTACTS = 2;
  public static final int FIRST_PAGE = 1;

  private IntranetServiceJson serviceJson;
  private IntranetServiceXml serviceXml;

  private SessionManager sessionManager;

  public IntranetRepositoryImpl(IntranetServiceJson serviceJson, IntranetServiceXml serviceXml, SessionManager sessionManager) {
    this.serviceJson = serviceJson;
    this.serviceXml = serviceXml;
    this.sessionManager = sessionManager;
  }

  @Override
  public void validateLogin(String email, String password, final OnItemRetrievedListener listener) {
    Call<ResponseBody> call = serviceXml.userLogin(email, password, TextUtils.Intranet.LOGIN_ACTION);
    call.enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        listener.onSuccess(null);
      }

      @Override
      public void onFailure(Call<ResponseBody> call, Throwable t) {
        listener.onError(R.string.error_intranet_login_failure);
      }
    });
  }

  @Override
  public void getIntranetNews(int page, final OnItemRetrievedListener listener) {
    Call<NewsResponse> call = serviceJson.getIntranetNews(TextUtils.Intranet.NEWS_CATEGORY,
        TextUtils.Intranet.NEWS_PER_PAGE, TextUtils.Intranet.ACTION_NEWS_SEARCH_POST);
    call.enqueue(new Callback<NewsResponse>() {
      @Override
      public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {
        listener.onSuccess(response.body().getHtmlResults());
      }

      @Override
      public void onFailure(Call<NewsResponse> call, Throwable t) {
        listener.onError(R.string.error_intranet_get_news_failure);
      }
    });
  }

  @Override
  public void getIntranetNewsById(String url, final OnItemRetrievedListener listener) {
    Call<String> call = serviceXml.getIntranetNewsById(url);
    call.enqueue(new Callback<String>() {
      @Override
      public void onResponse(Call<String> call, Response<String> response) {
        if (response.isSuccessful()) {
          listener.onSuccess(response.body());
        } else if (response.code() != SessionAliveInterceptor.CUSTOM_CODE_SESSION_EXPIRED) {
          listener.onError(R.string.error_intranet_get_piece_of_news_failure);
        }
      }

      @Override
      public void onFailure(Call<String> call, Throwable t) {
        listener.onError(R.string.error_intranet_get_piece_of_news_failure);
      }
    });
  }

  @Override
  public void getUserProfile(final OnItemRetrievedListener listener) {
    Call<String> call = serviceXml.getUserProfile();
    call.enqueue(new Callback<String>() {
      @Override
      public void onResponse(Call<String> call, Response<String> response) {
        if (response.isSuccessful()) {
          listener.onSuccess(response.body());
        } else if (response.code() != SessionAliveInterceptor.CUSTOM_CODE_SESSION_EXPIRED) {
          listener.onError(R.string.error_intranet_get_user_profile_failure);
        }
      }

      @Override
      public void onFailure(Call<String> call, Throwable t) {
        listener.onError(R.string.error_intranet_get_user_profile_failure);
      }
    });


  }

  @Override
  public void saveUserInfo(String attribute, String value, final OnItemRetrievedListener listener) {
    Call<String> call = serviceXml.saveUserInfo(TextUtils.Intranet.ACTION_SAVE_USER_INFO, attribute, value);
    call.enqueue(new Callback<String>() {
      @Override
      public void onResponse(Call<String> call, Response<String> response) {
        listener.onSuccess(null);
      }

      @Override
      public void onFailure(Call<String> call, Throwable t) {
        listener.onError(R.string.error_intranet_save_user_profile_failure);
      }
    });
  }

  @Override
  public void getContactsList(int page, String search, final OnItemRetrievedListener listener) {
    Call<ContactsResponse> call = serviceJson.getContacts(page, search, TextUtils.Intranet.CONTACTS_DEPARTMENTS,
        TextUtils.Intranet.CONTACTS_CITIES, TextUtils.Intranet.CONTACTS_SHOW,
        TextUtils.Intranet.ACTION_CONTACTS_SEARCH_DIRECTORY);
    call.enqueue(new Callback<ContactsResponse>() {
      @Override
      public void onResponse(Call<ContactsResponse> call, Response<ContactsResponse> response) {
        listener.onSuccess(response.body().getHtmlResults());
      }

      @Override
      public void onFailure(Call<ContactsResponse> call, Throwable t) {
        listener.onError(R.string.error_intranet_get_contacts_failure);
      }
    });
  }

  @Override
  public void sendImage(File photoProfile,
                        final OnItemRetrievedListener<UploadImageResponse> onItemRetrievedListener) {
    MultipartBody.Part file = MultipartBody.Part.createFormData("user_image", photoProfile.getName(), RequestBody.create(MediaType.parse("image/png"), photoProfile));
    MultipartBody.Part action = MultipartBody.Part.createFormData("action", "ivp_ajax_request_info_employee_save_image_user");

    Call<UploadImageResponse> call = serviceJson.sendImage(file, action);
    call.enqueue(new Callback<UploadImageResponse>() {
      @Override
      public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> response) {
        if (response.isSuccessful()) {
          if (response.body().isSuccess()) {
            onItemRetrievedListener.onSuccess(response.body());
          }
        } else {
          onItemRetrievedListener.onError(R.string.error_intranet_send_avatar_failure);
        }
      }

      @Override
      public void onFailure(Call<UploadImageResponse> call, Throwable t) {
        Timber.e(t, "Error: %s", t.getMessage());
        onItemRetrievedListener.onError(R.string.error_intranet_send_avatar_failure);
      }
    });
  }

}
