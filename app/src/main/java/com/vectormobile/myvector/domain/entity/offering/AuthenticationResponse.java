package com.vectormobile.myvector.domain.entity.offering;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.vectormobile.myvector.model.User;

/**
 * Created by Erik Medina on 10/11/2016.
 */
public class AuthenticationResponse {

  @SerializedName("status")
  @Expose
  private String status;
  @SerializedName("cookie")
  @Expose
  private String cookie;
  @SerializedName("cookie_name")
  @Expose
  private String cookieName;
  @SerializedName("user")
  @Expose
  private User user;

  @SerializedName("error")
  @Expose
  private String error;

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getCookie() {
    return cookie;
  }

  public void setCookie(String cookie) {
    this.cookie = cookie;
  }

  public String getCookieName() {
    return cookieName;
  }

  public void setCookieName(String cookieName) {
    this.cookieName = cookieName;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

  @Override
  public String toString() {
    return "AuthenticationResponse{" +
        "status='" + status + '\'' +
        ", cookie='" + cookie + '\'' +
        ", cookieName='" + cookieName + '\'' +
        ", user=" + user +
        ", error='" + error + '\'' +
        '}';
  }
}
