package com.vectormobile.myvector.domain.interactor.listener;

/**
 * Created by ajruiz on 22/12/2016.
 */

public interface OnItemRetrievedListener<T> {

  void onSuccess(T item);

  void onError(int errorId);

}
