package com.vectormobile.myvector.domain.interactor.login;

import com.vectormobile.myvector.storage.SessionManager;

/**
 * Created by amarina on 02/02/2017.
 */
public class LogoutInteractorImpl implements LogoutInteractor {
  private SessionManager sessionManager;

  public LogoutInteractorImpl(SessionManager sessionManager) {
    this.sessionManager = sessionManager;
  }

  @Override
  public void execute() {
    sessionManager.logout();
  }
}
