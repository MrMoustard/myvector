package com.vectormobile.myvector.domain.service;

import com.vectormobile.myvector.domain.entity.intranet.ContactsResponse;
import com.vectormobile.myvector.domain.entity.intranet.NewsResponse;
import com.vectormobile.myvector.domain.entity.intranet.UploadImageResponse;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by ajruiz on 20/12/2016.
 */

public interface IntranetServiceJson {

  @FormUrlEncoded
  @POST(ServiceManager.BASE_INTRANET_URL + ServiceManager.INTRANET_LOGIN_ENDPOINT)
  Call<NewsResponse> getIntranetNews(@Field("cat") String cat,
                                     @Field("posts_per_page") String postPerPage,
                                     @Field("action") String action);

  @FormUrlEncoded
  @POST(ServiceManager.BASE_INTRANET_URL + ServiceManager.INTRANET_LOGIN_ENDPOINT)
  Call<ContactsResponse> getContacts(@Field("page") int page,
                                     @Field("search") String search,
                                     @Field("deparments") String deparments,
                                     @Field("cities") String cities,
                                     @Field("show") String show,
                                     @Field("action") String action);

  @Headers({
      "Accept: */*",
      "Accept-Language: es-ES,es;q=0.8",
  })
  @Multipart
  @POST(ServiceManager.INTRANET_LOGIN_ENDPOINT)
  Call<UploadImageResponse> sendImage(
      @Part MultipartBody.Part photoProfile,
      @Part MultipartBody.Part action

  );
}