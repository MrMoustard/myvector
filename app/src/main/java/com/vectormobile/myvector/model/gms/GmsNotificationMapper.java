package com.vectormobile.myvector.model.gms;

import com.vectormobile.myvector.domain.entity.gms.response.notification.NotificationEntity;

/**
 * Created by ajruiz on 30/12/2016.
 */

public class GmsNotificationMapper {

  public static NotificationEntity.Request toRequest(GmsNotification gmsNotification) {
    NotificationEntity.Request request = new NotificationEntity.Request();
    request.id = gmsNotification.getId();
    request.dateOfIssue = gmsNotification.getDateOfIssue();
    request.origin = gmsNotification.getOrigin();
    request.leadIn = gmsNotification.getLeadIn();
    request.title = gmsNotification.getTitle();
    return request;
  }

  public static GmsNotification fromResponse(NotificationEntity.Response response) {
    GmsNotification gmsNotification = new GmsNotification();
    gmsNotification.setId(response.id);
    gmsNotification.setDateOfIssue(response.dateOfIssue);
    gmsNotification.setStatus(response.status);
    gmsNotification.setStatusText(response.statusText);
    gmsNotification.setOrigin(response.origin);
    gmsNotification.setLeadIn(response.leadIn);
    gmsNotification.setTitle(response.title);
    gmsNotification.setFolder(response.folder);
    gmsNotification.setFolderText(response.folderText);
    gmsNotification.setSendMail(response.sendMail);
    gmsNotification.setSendMailBox(response.sendMailBox);
    return gmsNotification;
  }

  public static GmsNotification fromResponse(NotificationEntity response) {
    GmsNotification gmsNotification = new GmsNotification();
    gmsNotification.setId(response.getId());
    gmsNotification.setDateOfIssue(response.getDateOfIssue());
    gmsNotification.setStatus(response.getStatus());
    gmsNotification.setStatusText(response.getStatusText());
    gmsNotification.setOrigin(response.getOrigin());
    gmsNotification.setLeadIn(response.getLeadIn());
    gmsNotification.setTitle(response.getTitle());
    gmsNotification.setFolder(response.getFolder());
    gmsNotification.setFolderText(response.getFolderText());
    gmsNotification.setSendMail(response.getSendMail());
    gmsNotification.setSendMailBox(response.getSendMailBox());
    return gmsNotification;
  }
}
