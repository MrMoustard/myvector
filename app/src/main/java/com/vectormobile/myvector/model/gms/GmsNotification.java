package com.vectormobile.myvector.model.gms;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ajruiz on 30/12/2016.
 */

public class GmsNotification {

  @SerializedName("id")
  @Expose
  private Double id;
  @SerializedName("fechaEmision")
  @Expose
  private String dateOfIssue;
  @SerializedName("estado")
  @Expose
  private String status;
  @SerializedName("estadoText")
  @Expose
  private String statusText;
  @SerializedName("origen")
  @Expose
  private String origin;
  @SerializedName("entradilla")
  @Expose
  private String leadIn;
  @SerializedName("titulo")
  @Expose
  private String title;
  @SerializedName("carpeta")
  @Expose
  private String folder;
  @SerializedName("carpetaText")
  @Expose
  private String folderText;
  @SerializedName("envioMail")
  @Expose
  private Boolean sendMail;
  @SerializedName("envioBuzon")
  @Expose
  private Boolean sendMailBox;

  public Double getId() {
    return id;
  }

  public void setId(Double id) {
    this.id = id;
  }

  public String getDateOfIssue() {
    return dateOfIssue;
  }

  public void setDateOfIssue(String dateOfIssue) {
    this.dateOfIssue = dateOfIssue;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getStatusText() {
    return statusText;
  }

  public void setStatusText(String statusText) {
    this.statusText = statusText;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public String getLeadIn() {
    return leadIn;
  }

  public void setLeadIn(String leadIn) {
    this.leadIn = leadIn;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getFolder() {
    return folder;
  }

  public void setFolder(String folder) {
    this.folder = folder;
  }

  public String getFolderText() {
    return folderText;
  }

  public void setFolderText(String folderText) {
    this.folderText = folderText;
  }

  public Boolean getSendMail() {
    return sendMail;
  }

  public void setSendMail(Boolean sendMail) {
    this.sendMail = sendMail;
  }

  public Boolean getSendMailBox() {
    return sendMailBox;
  }

  public void setSendMailBox(Boolean sendMailBox) {
    this.sendMailBox = sendMailBox;
  }

}
