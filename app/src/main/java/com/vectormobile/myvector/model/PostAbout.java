package com.vectormobile.myvector.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class PostAbout extends Post {


  @SerializedName("acf")
  @Expose
  private Acf acf;

  @SerializedName("gallery")
  @Expose
  private List<String> gallery_images;


  /**
   * @return The video embebed
   */
  public Acf getAcf() {
    return acf;
  }

  /**
   * @param tags The video embebed
   */
  public void setAcf(Acf acf) {
    this.acf = acf;
  }


  /**
   * @return The Gallery images
   */
  public List<String> getGalleryImages() {
    return gallery_images;
  }

  /**
   * @param tags The Gallery images
   */
  public void setGalleryImages(List<String> gallery_images) {
    this.gallery_images = gallery_images;
  }


}
