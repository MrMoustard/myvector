package com.vectormobile.myvector.model.news;

/**
 * Created by ajruiz on 21/12/2016.
 */

public class NewsItem {

  private String newsTitle;
  private String newsUrl;
  private String newsImage;

  public NewsItem() {
  }

  public NewsItem(String newsUrl, String newsImage, String newsTitle) {
    this.newsUrl = newsUrl;
    this.newsImage = newsImage;
    this.newsTitle = newsTitle;
  }

  public String getNewsImage() {
    return newsImage;
  }

  public void setNewsImage(String newsImage) {
    this.newsImage = newsImage;
  }

  public String getNewsTitle() {
    return newsTitle;
  }

  public void setNewsTitle(String newsTitle) {
    this.newsTitle = newsTitle;
  }

  public String getNewsUrl() {
    return newsUrl;
  }

  public void setNewsUrl(String newsUrl) {
    this.newsUrl = newsUrl;
  }

  public String getNewsItemImage() {
    return newsImage;
  }

  public void setNewsItemImage(String newsImage) {
    this.newsImage = newsImage;
  }

  public String getNewsItemTitle() {
    return newsTitle;
  }

  public void setNewsItemTitle(String newsTitle) {
    this.newsTitle = newsTitle;
  }
}
