package com.vectormobile.myvector.model.gms;

/**
 * Created by ajruiz on 01/03/2017.
 */

public class Jira {

  private String key;
  private String description;

  public Jira(String description, String key) {
    this.description = description;
    this.key = key;
  }

  public Jira() {
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }
}
