package com.vectormobile.myvector.model;

import android.content.Context;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.vectormobile.myvector.model.menuaction.VectorAction;

/**
 * Created by amarina on 07/11/2016.
 */

public class VectorItemMenu {
  private String label;
  private int bgImage;
  private VectorAction action;

  public VectorItemMenu(String label, int bgImage, VectorAction type) {
    this.label = label;
    this.bgImage = bgImage;
    this.action = type;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public int getBgImage() {
    return bgImage;
  }

  public void setBgImage(int bgImage) {
    this.bgImage = bgImage;
  }

  @Override
  public String toString() {
    return "VectorItemMenu{" +
        "label='" + label + '\'' +
        ", bgImage=" + bgImage +
        ", action=" + action +
        '}';
  }

  public void launchAction(Context context) {
    Answers.getInstance().logContentView(new ContentViewEvent()
        .putContentId("menu")
        .putContentName("" + action.name())
    );
    action.launchAction(context);
  }

}
