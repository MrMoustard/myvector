package com.vectormobile.myvector.model.intranet;

import android.os.Parcel;
import android.os.Parcelable;
import com.vectormobile.myvector.util.text.TextUtils;
import org.jsoup.nodes.Element;

/**
 * Created by ajruiz on 01/02/2017.
 */

public class Contact implements Parcelable {

  private String imageUrl;
  private String name;
  private String city;
  private String area;
  private String email;
  private String phone;
  private String cellphone;

  public Contact() {
  }

  public Contact(Element scrapElement) {
    parseElement(scrapElement);
  }

  public Contact(String area, String phone, String name, String imageUrl,
                 String email, String city, String cellphone) {
    this.area = area;
    this.phone = phone;
    this.name = name;
    this.imageUrl = imageUrl;
    this.email = email;
    this.city = city;
    this.cellphone = cellphone;
  }

  protected Contact(Parcel in) {
    imageUrl = in.readString();
    name = in.readString();
    city = in.readString();
    area = in.readString();
    email = in.readString();
    phone = in.readString();
    cellphone = in.readString();
  }

  private void parseElement(Element scrapElement) {
    setName(scrapElement.select("h3").text());
    setCity(TextUtils.extractUntilCharacter(scrapElement.select("div[class=location]").text(), "*"));
    setArea(TextUtils.extractFromCharacter(scrapElement.select("div[class=location]").text(), "*"));
    setEmail(scrapElement.select("a[class=btn-ivp]").attr("title"));
    if (TextUtils.checkStartWithNumber(TextUtils.extractUntilCharacter(scrapElement.select("div[class=contact]").text().trim(), "*")) ||
        TextUtils.extractUntilCharacter(scrapElement.select("div[class=contact]").text().trim(), "*").startsWith("+")) {
      setPhone(TextUtils.extractUntilCharacter(scrapElement.select("div[class=contact]").text().trim(), "*"));
      if (!TextUtils.extractFromCharacter(scrapElement.select("div[class=contact]").text().trim(), "*").matches("")) {
        if (TextUtils.checkStartWithNumber(TextUtils.extractFromCharacter(scrapElement.select("div[class=contact]").text().trim(), "*").trim())) {
          String cellPhone = TextUtils.extractFromCharacter(scrapElement.select("div[class=contact]").text().trim(), "*");
          setCellphone(TextUtils.extractUntilCharacter(cellPhone, "*").trim());
        }
      }
    }
    setImageUrl(TextUtils.extractStringBetweenCharacters(
        scrapElement.select("div[class=photo]").attr("style"), "'"));
  }

  public String getArea() {
    return area;
  }

  public void setArea(String area) {
    this.area = area;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getCellphone() {
    return cellphone;
  }

  public void setCellphone(String cellphone) {
    this.cellphone = cellphone;
  }

  @Override
  public String toString() {
    return "Contact{" +
        "area='" + area + '\'' +
        ", imageUrl='" + imageUrl + '\'' +
        ", name='" + name + '\'' +
        ", city='" + city + '\'' +
        ", email='" + email + '\'' +
        ", phone='" + phone + '\'' +
        ", cellphone='" + cellphone + '\'' +
        '}';
  }

  public static final Creator<Contact> CREATOR = new Creator<Contact>() {
    @Override
    public Contact createFromParcel(Parcel in) {
      return new Contact(in);
    }

    @Override
    public Contact[] newArray(int size) {
      return new Contact[size];
    }
  };

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel parcel, int i) {
    parcel.writeString(imageUrl);
    parcel.writeString(name);
    parcel.writeString(city);
    parcel.writeString(area);
    parcel.writeString(email);
    parcel.writeString(phone);
    parcel.writeString(cellphone);
  }
}
