package com.vectormobile.myvector.model.offering;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Links {

  @SerializedName("collection")
  @Expose
  public String collection;
  @SerializedName("self")
  @Expose
  public String self;

  public String getCollection() {
    return collection;
  }

  public void setCollection(String collection) {
    this.collection = collection;
  }

  public String getSelf() {
    return self;
  }

  public void setSelf(String self) {
    this.self = self;
  }
}
