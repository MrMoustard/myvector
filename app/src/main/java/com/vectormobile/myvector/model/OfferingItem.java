package com.vectormobile.myvector.model;

/**
 * Created by ajruiz on 03/11/2016.
 */

public class OfferingItem {

  private String offeringTittle;

  public OfferingItem() {
  }

  public OfferingItem(String offeringTittle) {
    this.offeringTittle = offeringTittle;
  }

  public String getOfferingTittle() {
    return offeringTittle;
  }

  public void setOfferingTittle(String offeringTittle) {
    this.offeringTittle = offeringTittle;
  }

  @Override
  public String toString() {
    return "OfferingItem{" +
        "offeringTittle='" + offeringTittle + '\'' +
        '}';
  }
}
