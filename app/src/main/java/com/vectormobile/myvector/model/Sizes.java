package com.vectormobile.myvector.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sizes {


  @SerializedName("thumbnail")
  @Expose
  public Thumbnail thumbnail;
  @SerializedName("medium")
  @Expose
  public Medium medium;
  @SerializedName("medium_large")
  @Expose
  public MediumLarge mediumLarge;
  @SerializedName("large")
  @Expose
  public Large large;
  @SerializedName("micro")
  @Expose
  public Micro micro;
  @SerializedName("menu")
  @Expose
  public Menu menu;
  @SerializedName("grid")
  @Expose
  public Grid grid;
  @SerializedName("grid-wide")
  @Expose
  public GridWide gridWide;
  @SerializedName("scroller")
  @Expose
  public Scroller scroller;
  @SerializedName("scroller-wide")
  @Expose
  public ScrollerWide scrollerWide;
  @SerializedName("headliner")
  @Expose
  public Headliner headliner;
  @SerializedName("single-180")
  @Expose
  public Single180 single180;
  @SerializedName("single-360")
  @Expose
  public Single360 single360;
  @SerializedName("single-1200")
  @Expose
  public Single1200 single1200;

  public Grid getGrid() {
    return grid;
  }

  public void setGrid(Grid grid) {
    this.grid = grid;
  }

  public GridWide getGridWide() {
    return gridWide;
  }

  public void setGridWide(GridWide gridWide) {
    this.gridWide = gridWide;
  }

  public Headliner getHeadliner() {
    return headliner;
  }

  public void setHeadliner(Headliner headliner) {
    this.headliner = headliner;
  }

  public Large getLarge() {
    return large;
  }

  public void setLarge(Large large) {
    this.large = large;
  }

  public Medium getMedium() {
    return medium;
  }

  public void setMedium(Medium medium) {
    this.medium = medium;
  }

  public MediumLarge getMediumLarge() {
    return mediumLarge;
  }

  public void setMediumLarge(MediumLarge mediumLarge) {
    this.mediumLarge = mediumLarge;
  }

  public Menu getMenu() {
    return menu;
  }

  public void setMenu(Menu menu) {
    this.menu = menu;
  }

  public Micro getMicro() {
    return micro;
  }

  public void setMicro(Micro micro) {
    this.micro = micro;
  }

  public Scroller getScroller() {
    return scroller;
  }

  public void setScroller(Scroller scroller) {
    this.scroller = scroller;
  }

  public ScrollerWide getScrollerWide() {
    return scrollerWide;
  }

  public void setScrollerWide(ScrollerWide scrollerWide) {
    this.scrollerWide = scrollerWide;
  }

  public Single1200 getSingle1200() {
    return single1200;
  }

  public void setSingle1200(Single1200 single1200) {
    this.single1200 = single1200;
  }

  public Single180 getSingle180() {
    return single180;
  }

  public void setSingle180(Single180 single180) {
    this.single180 = single180;
  }

  public Single360 getSingle360() {
    return single360;
  }

  public void setSingle360(Single360 single360) {
    this.single360 = single360;
  }

  public Thumbnail getThumbnail() {
    return thumbnail;
  }

  public void setThumbnail(Thumbnail thumbnail) {
    this.thumbnail = thumbnail;
  }
}
