package com.vectormobile.myvector.model.gms;

import com.vectormobile.myvector.util.date.DateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ajruiz on 25/01/2017.
 */

public class HourItemRecycler extends Hour {

  private boolean isHeader;

  private List<Date> weekBounds = new ArrayList<>();

  public HourItemRecycler(boolean isHeader, List<Date> weekBounds) {
    super(null, null, null, null, 0L, null, 0L, 0L, null, 0, false, null, null, null, null, false, null);
    this.isHeader = isHeader;
    this.weekBounds = weekBounds;
  }

  public HourItemRecycler(Hour hour, boolean isHeader, List<Date> weekBounds) {
    super(hour.getId(), hour.getComment(), hour.getDateFrom(), hour.getDateTo(), hour.getHourDate(),
        hour.getState(), hour.getWeekFrom(), hour.getWeekTo(), hour.getWorkOrderName(), hour.getUnits(),
        hour.isMassiveReport(), hour.getProjectName(), hour.getExternalCode(), hour.getCategoryName(),
        hour.getApproverComment(), hour.isJirable(), hour.getJiraCode());
    this.isHeader = isHeader;
    this.weekBounds = weekBounds;
  }


  public boolean isHeader() {
    return isHeader;
  }

  public void setHeader(boolean header) {
    isHeader = header;
  }

  public Date getWeekStarts() {
    return weekBounds.get(DateUtils.WEEK_BOUND_STARTS);
  }

  public Date getWeekEnds() {
    return weekBounds.get(DateUtils.WEEK_BOUND_ENDS);
  }

}
