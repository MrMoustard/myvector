package com.vectormobile.myvector.model.bot;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.stfalcon.chatkit.commons.models.IUser;

/**
 * Created by amarina on 14/03/2017.
 */

public class User implements IUser {
  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("name")
  @Expose
  private String name;

  @Override
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  @Override
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String getAvatar() {
    return "";
  }

  @Override
  public String toString() {
    return "User{" +
        "id='" + id + '\'' +
        ", name='" + name + '\'' +
        '}';
  }
}
