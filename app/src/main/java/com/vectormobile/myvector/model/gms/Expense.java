package com.vectormobile.myvector.model.gms;

import android.content.Context;
import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.storage.ImageManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by kurt on 05/12/16.
 */

public class Expense implements Comparable<Expense>, Parcelable {

  public static final String EXPENSE_SENT = "ENVIADO";
  public static final String EXPENSE_APPROVED = "APROBADO";
  public static final String EXPENSE_REJECTED = "RECHAZADO";
  public static final String EXPENSE_SAVED = "CREADO";

  private String id;
  private long expenseDate;
  private Date dateFrom;
  private Date dateTo;
  private String title;
  private String amount;
  private boolean isMassive;
  private String state;
  private String workOrderName;
  private String project;
  private String category;
  private int units;
  private double unitPrice;
  private String currencyId;
  private String comment;
  private long weekFrom;
  private long weekTo;
  private String approverComment;

  public Expense(String id, long expenseDate, Date dateFrom, Date dateTo, String title, String amount, boolean isMassive,
                 String state, String workOrderName, String project, String category, int units,
                 double unitPrice, String currencyId, String comment, long weekFrom, long weekTo,
                 String approverComment) {
    this.id = id;
    this.expenseDate = expenseDate;
    this.dateFrom = dateFrom;
    this.dateTo = dateTo;
    this.title = title;
    this.amount = amount;
    this.isMassive = isMassive;
    this.state = state;
    this.workOrderName = workOrderName;
    this.project = project;
    this.category = category;
    this.units = units;
    this.unitPrice = unitPrice;
    this.currencyId = currencyId;
    this.comment = comment;
    this.weekFrom = weekFrom;
    this.weekTo = weekTo;
    this.approverComment = approverComment;
  }

  protected Expense(Parcel in) {
    id = in.readString();
    expenseDate = in.readLong();
    long tmpDateFrom = in.readLong();
    dateFrom = tmpDateFrom != -1 ? new Date(tmpDateFrom) : null;
    long tmpDateTo = in.readLong();
    dateTo = tmpDateTo != -1 ? new Date(tmpDateTo) : null;
    title = in.readString();
    amount = in.readString();
    isMassive = in.readByte() != 0x00;
    state = in.readString();
    workOrderName = in.readString();
    project = in.readString();
    category = in.readString();
    units = in.readInt();
    unitPrice = in.readDouble();
    currencyId = in.readString();
    comment = in.readString();
    weekFrom = in.readLong();
    weekTo = in.readLong();
    approverComment = in.readString();
  }

  public Date getDateFrom() {
    return dateFrom;
  }

  public Date getDateTo() {
    return dateTo;
  }

  public String getDateString() {
    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
    return isMassive ?
        String.format(Locale.getDefault(), "%s - %s", formatter.format(dateFrom), formatter.format(dateTo)) :
        formatter.format(dateFrom);
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public long getExpenseDate() {
    return expenseDate;
  }

  public void setExpenseDate(long expenseDate) {
    this.expenseDate = expenseDate;
  }

  public void setDateFrom(Date date) {
    this.dateFrom = date;
  }

  public void setDateTo(Date date) {
    this.dateTo = date;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getAmount() {
    return amount;
  }

  public void setAmount(String amount) {
    this.amount = amount;
  }

  public boolean isMassive() {
    return isMassive;
  }

  public void setMassive(boolean massive) {
    isMassive = massive;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getWorkOrderName() {
    return workOrderName;
  }

  public void setWorkOrderName(String workOrderName) {
    this.workOrderName = workOrderName;
  }

  public String getProject() {
    return project;
  }

  public void setProject(String project) {
    this.project = project;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public int getUnits() {
    return units;
  }

  public void setUnits(int units) {
    this.units = units;
  }

  public double getUnitPrice() {
    return unitPrice;
  }

  public void setUnitPrice(double unitPrice) {
    this.unitPrice = unitPrice;
  }

  public String getCurrencyId() {
    return currencyId;
  }

  public void setCurrencyId(String currencyId) {
    this.currencyId = currencyId;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public long getWeekFrom() {
    return weekFrom;
  }

  public void setWeekFrom(Long weekFrom) {
    this.weekFrom = weekFrom;
  }

  public long getWeekTo() {
    return weekTo;
  }

  public void setWeekTo(long weekTo) {
    this.weekTo = weekTo;
  }

  public String getApproverComment() {
    return approverComment;
  }

  public void setApproverComment(String approverComment) {
    this.approverComment = approverComment;
  }

  public int getColour() {
    int colour = 0;
    switch (state) {
      case EXPENSE_SENT:
        colour = Color.BLUE;
        break;
      case EXPENSE_APPROVED:
        colour = Color.GREEN;
        break;
      case EXPENSE_REJECTED:
        colour = Color.RED;
        break;
      case EXPENSE_SAVED:
        colour = Color.GRAY;
        break;
    }
    return colour;
  }

  public String getStateString(Context context) {
    switch (state) {
      case EXPENSE_SENT:
        return context.getString(R.string.expense_sent_status_header_title);
      case EXPENSE_APPROVED:
        return context.getString(R.string.expense_approved_status_header_title);
      case EXPENSE_REJECTED:
        return context.getString(R.string.expense_rejected_status_header_title);
      case EXPENSE_SAVED:
        return context.getString(R.string.expense_saved_status_header_title);
    }
    return null;
  }

  public boolean isThereAnAttachedImage() {
    return ImageManager.getInstance().haveImage(id);
  }

  @Override
  public int compareTo(Expense expense) {
    return getDateFrom().compareTo(expense.getDateFrom());
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(id);
    dest.writeLong(expenseDate);
    dest.writeLong(dateFrom != null ? dateFrom.getTime() : -1L);
    dest.writeLong(dateTo != null ? dateTo.getTime() : -1L);
    dest.writeString(title);
    dest.writeString(amount);
    dest.writeByte((byte) (isMassive ? 0x01 : 0x00));
    dest.writeString(state);
    dest.writeString(workOrderName);
    dest.writeString(project);
    dest.writeString(category);
    dest.writeInt(units);
    dest.writeDouble(unitPrice);
    dest.writeString(currencyId);
    dest.writeString(comment);
    dest.writeLong(weekFrom);
    dest.writeLong(weekTo);
    dest.writeString(approverComment);
  }

  @SuppressWarnings("unused")
  public static final Parcelable.Creator<Expense> CREATOR = new Parcelable.Creator<Expense>() {
    @Override
    public Expense createFromParcel(Parcel in) {
      return new Expense(in);
    }

    @Override
    public Expense[] newArray(int size) {
      return new Expense[size];
    }
  };
}
