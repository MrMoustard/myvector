package com.vectormobile.myvector.model.gms;

import com.vectormobile.myvector.util.date.DateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by kurt on 05/12/16.
 */
public class ExpenseItemRecycler extends Expense {

  private boolean isHeader;

  private List<Date> weekBounds = new ArrayList<>();

  public ExpenseItemRecycler(boolean isHeader, List<Date> weekBounds) {
    super(null, 0L, null, null, null, null, false, null, null, null, null, 0, 0, null, null, 0L, 0L, null);
    this.isHeader = isHeader;
    this.weekBounds = weekBounds;
  }

  public ExpenseItemRecycler(Expense expense, boolean isHeader, List<Date> weekBounds) {
    super(expense.getId(), expense.getExpenseDate(), expense.getDateFrom(), expense.getDateTo(), expense.getTitle(), expense.getAmount(),
        expense.isMassive(), expense.getState(), expense.getWorkOrderName(), expense.getProject(),
        expense.getCategory(), expense.getUnits(), expense.getUnitPrice(), expense.getCurrencyId(),
        expense.getComment(), expense.getWeekFrom(), expense.getWeekTo(), expense.getApproverComment());
    this.isHeader = isHeader;
    this.weekBounds = weekBounds;
  }

  public boolean isHeader() {
    return isHeader;
  }

  public void setHeader(boolean header) {
    isHeader = header;
  }

  public Date getWeekStarts() {
    return weekBounds.get(DateUtils.WEEK_BOUND_STARTS);
  }

  public Date getWeekEnds() {
    return weekBounds.get(DateUtils.WEEK_BOUND_ENDS);
  }
}
