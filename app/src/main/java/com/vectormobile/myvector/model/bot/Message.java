package com.vectormobile.myvector.model.bot;

import com.stfalcon.chatkit.commons.models.IMessage;
import com.vectormobile.myvector.domain.entity.bot.BFButton;

import java.util.Date;
import java.util.List;

/**
 * Created by amarina on 14/03/2017.
 */
public class Message implements IMessage {
  private String id;
  private Date createdAt;
  private User user;
  private String text;
  private List<BFButton> buttons;

  @Override
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  @Override
  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  @Override
  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  @Override
  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public List<BFButton> getButtons() {
    return buttons;
  }

  public void setButtons(List<BFButton> buttons) {
    this.buttons = buttons;
  }

  @Override
  public String toString() {
    return "Message{" +
        "id='" + id + '\'' +
        ", createdAt=" + createdAt +
        ", user=" + user +
        ", text='" + text + '\'' +
        ", buttons=" + buttons +
        '}';
  }
}
