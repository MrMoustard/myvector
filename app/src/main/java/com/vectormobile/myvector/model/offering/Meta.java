package com.vectormobile.myvector.model.offering;

/**
 * Created by ajruiz on 15/11/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Meta {

  @SerializedName("links")
  @Expose
  private Links links;

  /**
   * @return The links
   */
  public Links getLinks() {
    return links;
  }

  /**
   * @param links The links
   */
  public void setLinks(Links links) {
    this.links = links;
  }

}