package com.vectormobile.myvector.model.offering;

/**
 * Created by ajruiz on 15/11/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.graphics.Color;
import com.vectormobile.myvector.util.text.TextUtils;

import java.util.Random;

public class Section {


  //VECTOR ITC GROUP - OFFERING

  private static final int INNOVACION_ID = 2;
  private static final int INDUSTRIES_ID = 3;
  private static final int CONSULTING_ID = 5;
  private static final int DIGITAL_ID = 1;
  private static final int TECHNOLOGY_ID = 6;
  private static final int SOLUTIONS_ID = 7;
  private static final int OPERATIONS_ID = 8;
  private static final int CORPORATE_ID = 9;

  //RADAR TECNOLOGICO

  private static final int LAST_NEWS_CAT_ID = -1;
  private static final int FINTECH_CAT_ID = 1286;
  private static final int ECONOMIA_COMPARTIDA_CAT_ID = 1703;
  private static final int STARTUPS_CAT_ID = 1441;
  private static final int CONSUMIDOR_DIGITAL_CAT_ID = 1558;
  private static final int CONSUMER_EXPIRIENCE_CAT_ID = 1700;
  private static final int BIG_DATA_CAT_ID = 24;
  private static final int IOT_CAT_ID = 1071;
  private static final int CIBERSEGURIDAD_CAT_ID = 1600;
  private static final int TECNOLOGIAS_EMERGENTES_CAT_ID = 1602;
  private static final int OMNICANALIDAD_CAT_ID = 1577;
  private static final int ECOMMERCE_CAT_ID = 1699;
  private static final int SOCIAL_MEDIA_GAMIFICACION_CAT_ID = 467;
  private static final int REAL_TIME_MARKETING_CAT_ID = 1704;
  private static final int EHEALTH_WEREABLES_CAT_ID = 1603;
  private static final int GESTION_INNOVACION_CAT_ID = 1599;
  private static final int TRANSF_DIGITAL_CAT_ID = 1714;
  private static final int NUEVAS_FORMAS_ORGANIZA_CAT_ID = 1601;


  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("order")
  @Expose
  private Integer order;
  @SerializedName("parent")
  @Expose
  private Integer parent;
  @SerializedName("title")
  @Expose
  private String title;
  @SerializedName("url")
  @Expose
  private String url;
  @SerializedName("attr")
  @Expose
  private String attr;
  @SerializedName("target")
  @Expose
  private String target;
  @SerializedName("classes")
  @Expose
  private String classes;
  @SerializedName("xfn")
  @Expose
  private String xfn;
  @SerializedName("description")
  @Expose
  private String description;
  @SerializedName("object_id")
  @Expose
  private Integer objectId;
  @SerializedName("object")
  @Expose
  private String object;
  @SerializedName("type")
  @Expose
  private String type;
  @SerializedName("type_label")
  @Expose
  private String typeLabel;

  public static int getColor(int objectId) {
    switch (objectId) {

      /**
       * SECCIONES VECTOR ITC GROUP - OFFERTING
       */

      case LAST_NEWS_CAT_ID:
        return Color.argb(255, 253, 20, 93);
      case INNOVACION_ID:
        return Color.argb(255, 230, 84, 0);
      case INDUSTRIES_ID:
        return Color.argb(255, 20, 34, 64);
      case CONSULTING_ID:
        return Color.argb(255, 149, 147, 0);
      case DIGITAL_ID:
        return Color.argb(255, 178, 76, 90);
      case TECHNOLOGY_ID:
        return Color.argb(255, 242, 139, 46);
      case SOLUTIONS_ID:
        return Color.argb(255, 0, 148, 202);
      case OPERATIONS_ID:
        return Color.argb(255, 96, 34, 90);
      case CORPORATE_ID:
        return Color.argb(255, 130, 135, 155);

      /**
       * SECCIONES RADAR TECNOLOGICO
       */

      case FINTECH_CAT_ID:
        return Color.argb(255, 211, 57, 23);
      case ECONOMIA_COMPARTIDA_CAT_ID:
        return Color.argb(255, 139, 158, 99);
      case STARTUPS_CAT_ID:
        return Color.argb(255, 232, 168, 32);
      case CONSUMIDOR_DIGITAL_CAT_ID:
        return Color.argb(255, 30, 115, 190);
      case CONSUMER_EXPIRIENCE_CAT_ID:
        return Color.argb(255, 96, 153, 47);
      case BIG_DATA_CAT_ID:
        return Color.argb(255, 65, 196, 17);
      case IOT_CAT_ID:
        return Color.argb(255, 209, 20, 174);
      case CIBERSEGURIDAD_CAT_ID:
        return Color.argb(255, 147, 147, 147);
      case TECNOLOGIAS_EMERGENTES_CAT_ID:
        return Color.argb(255, 221, 133, 130);
      case OMNICANALIDAD_CAT_ID:
        return Color.argb(255, 145, 111, 33);
      case ECOMMERCE_CAT_ID:
        return Color.argb(255, 129, 215, 66);
      case SOCIAL_MEDIA_GAMIFICACION_CAT_ID:
        return Color.argb(255, 110, 153, 47);
      case REAL_TIME_MARKETING_CAT_ID:
        return Color.argb(255, 255, 89, 89);
      case EHEALTH_WEREABLES_CAT_ID:
        return Color.argb(255, 221, 148, 148);
      case GESTION_INNOVACION_CAT_ID:
        return Color.argb(255, 237, 197, 21);
      case TRANSF_DIGITAL_CAT_ID:
        return Color.argb(255, 25, 113, 160);
      case NUEVAS_FORMAS_ORGANIZA_CAT_ID:
        return Color.argb(255, 165, 104, 38);

      default:
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        return color;
    }
  }

  /**
   * @return The id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id The id
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * @return The order
   */
  public Integer getOrder() {
    return order;
  }

  /**
   * @param order The order
   */
  public void setOrder(Integer order) {
    this.order = order;
  }

  /**
   * @return The parent
   */
  public Integer getParent() {
    return parent;
  }

  /**
   * @param parent The parent
   */
  public void setParent(Integer parent) {
    this.parent = parent;
  }

  /**
   * @return The title
   */
  public String getTitle() {
    return title;
  }

  /**
   * @param title The title
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * @return The url
   */
  public String getUrl() {
    return url;
  }

  /**
   * @param url The url
   */
  public void setUrl(String url) {
    this.url = url;
  }

  /**
   * @return The attr
   */
  public String getAttr() {
    return attr;
  }

  /**
   * @param attr The attr
   */
  public void setAttr(String attr) {
    this.attr = attr;
  }

  /**
   * @return The target
   */
  public String getTarget() {
    return target;
  }

  /**
   * @param target The target
   */
  public void setTarget(String target) {
    this.target = target;
  }

  /**
   * @return The classes
   */
  public String getClasses() {
    return classes;
  }

  /**
   * @param classes The classes
   */
  public void setClasses(String classes) {
    this.classes = classes;
  }

  /**
   * @return The xfn
   */
  public String getXfn() {
    return xfn;
  }

  /**
   * @param xfn The xfn
   */
  public void setXfn(String xfn) {
    this.xfn = xfn;
  }

  /**
   * @return The description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description The description
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * @return The objectId
   */
  public Integer getObjectId() {
    return objectId;
  }

  /**
   * @param objectId The object_id
   */
  public void setObjectId(Integer objectId) {
    this.objectId = objectId;
  }

  /**
   * @return The object
   */
  public String getObject() {
    return object;
  }

  /**
   * @param object The object
   */
  public void setObject(String object) {
    this.object = object;
  }

  /**
   * @return The type
   */
  public String getType() {
    return type;
  }

  /**
   * @param type The type
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * @return The typeLabel
   */
  public String getTypeLabel() {
    return typeLabel;
  }

  /**
   * @param typeLabel The type_label
   */
  public void setTypeLabel(String typeLabel) {
    this.typeLabel = typeLabel;
  }

  public int getColor() {
    return getColor(objectId);
  }

  private String properCase(String string) {
    if (string.length() == 0) return "";

    if (string.length() == 1) return string.toUpperCase();

    return TextUtils.capitalizeFirstLetter(string);
  }
}
