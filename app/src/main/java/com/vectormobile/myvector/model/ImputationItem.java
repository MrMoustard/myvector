package com.vectormobile.myvector.model;

/**
 * Created by Erik Medina on 03/11/2016.
 */

public class ImputationItem {

  private String state;
  private String project;
  private String workOrder;
  private int units;
  private long jiraCode;

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getProject() {
    return project;
  }

  public void setProject(String project) {
    this.project = project;
  }

  public String getWorkOrder() {
    return workOrder;
  }

  public void setWorkOrder(String workOrder) {
    this.workOrder = workOrder;
  }

  public int getUnits() {
    return units;
  }

  public void setUnits(int units) {
    this.units = units;
  }

  public long getJiraCode() {
    return jiraCode;
  }

  public void setJiraCode(long jiraCode) {
    this.jiraCode = jiraCode;
  }
}
