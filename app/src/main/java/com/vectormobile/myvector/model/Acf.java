package com.vectormobile.myvector.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by dsanchezc on 10/06/2016.
 */
public class Acf {

  @SerializedName("video")
  @Expose
  public String video;
  @SerializedName("video_url")
  @Expose
  public String videoUrl;
  @SerializedName("pdf")
  @Expose
  public String pdf;

  public String getPdf() {
    return pdf;
  }

  public void setPdf(String pdf) {
    this.pdf = pdf;
  }

  public String getVideo() {
    return video;
  }

  public void setVideo(String video) {
    this.video = video;
  }

  public String getVideoUrl() {
    return videoUrl;
  }

  public void setVideoUrl(String videoUrl) {
    this.videoUrl = videoUrl;
  }
}
