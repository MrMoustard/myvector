package com.vectormobile.myvector.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ajruiz on 21/11/2016.
 */

public class MediumLarge {
  @SerializedName("file")
  @Expose
  public String file;
  @SerializedName("width")
  @Expose
  public int width;
  @SerializedName("height")
  @Expose
  public int height;
  @SerializedName("mime-type")
  @Expose
  public String mimeType;
  @SerializedName("source_url")
  @Expose
  public String sourceUrl;

  public String getFile() {
    return file;
  }

  public void setFile(String file) {
    this.file = file;
  }

  public int getHeight() {
    return height;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public String getMimeType() {
    return mimeType;
  }

  public void setMimeType(String mimeType) {
    this.mimeType = mimeType;
  }

  public String getSourceUrl() {
    return sourceUrl;
  }

  public void setSourceUrl(String sourceUrl) {
    this.sourceUrl = sourceUrl;
  }

  public int getWidth() {
    return width;
  }

  public void setWidth(int width) {
    this.width = width;
  }
}
