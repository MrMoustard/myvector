package com.vectormobile.myvector.model;

/**
 * Created by ajruiz on 08/11/2016.
 */

public class UserAttributes {

  private String itemTitle;
  private String itemLabel;
  private int icon;
  private boolean editable;

  public UserAttributes(String itemTitle, String itemLabel, int icon, boolean editable) {
    this.icon = icon;
    this.itemLabel = itemLabel;
    this.itemTitle = itemTitle;
    this.editable = editable;
  }

  public boolean isEditable() {
    return editable;
  }

  public void setEditable(boolean editable) {
    this.editable = editable;
  }

  public int getIcon() {
    return icon;
  }

  public void setIcon(int icon) {
    this.icon = icon;
  }

  public String getItemLabel() {
    return itemLabel;
  }

  public void setItemLabel(String itemLabel) {
    this.itemLabel = itemLabel;
  }

  public String getItemTitle() {
    return itemTitle;
  }

  public void setItemTitle(String itemTitle) {
    this.itemTitle = itemTitle;
  }
}
