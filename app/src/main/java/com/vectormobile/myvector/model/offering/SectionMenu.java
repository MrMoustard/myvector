package com.vectormobile.myvector.model.offering;

/**
 * Created by ajruiz on 15/11/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SectionMenu {

  @SerializedName("ID")
  @Expose
  private Integer iD;
  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("slug")
  @Expose
  private String slug;
  @SerializedName("description")
  @Expose
  private String description;
  @SerializedName("count")
  @Expose
  private Integer count;
  @SerializedName("items")
  @Expose
  private List<Section> items = new ArrayList<Section>();
  @SerializedName("meta")
  @Expose
  private Meta meta;

  /**
   * @return The iD
   */
  public Integer getID() {
    return iD;
  }

  /**
   * @param iD The ID
   */
  public void setID(Integer iD) {
    this.iD = iD;
  }

  /**
   * @return The name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name The name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return The slug
   */
  public String getSlug() {
    return slug;
  }

  /**
   * @param slug The slug
   */
  public void setSlug(String slug) {
    this.slug = slug;
  }

  /**
   * @return The description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description The description
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * @return The count
   */
  public Integer getCount() {
    return count;
  }

  /**
   * @param count The count
   */
  public void setCount(Integer count) {
    this.count = count;
  }

  /**
   * @return The items
   */
  public List<Section> getItems() {
    return items;
  }

  /**
   * @param items The items
   */
  public void setItems(List<Section> items) {
    this.items = items;
  }

  /**
   * @return The meta
   */
  public Meta getMeta() {
    return meta;
  }

  /**
   * @param meta The meta
   */
  public void setMeta(Meta meta) {
    this.meta = meta;
  }

}
