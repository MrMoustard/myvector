package com.vectormobile.myvector.model;

/**
 * Created by amarina on 07/11/2016.
 */
public enum VectorAction {
  OPEN_IMPUTATION,
  OPEN_PROFILE,
  OPEN_OFFERING,
  OPEN_NEWS

}
