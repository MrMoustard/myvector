package com.vectormobile.myvector.model.menuaction;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import com.vectormobile.myvector.BuildConfig;
import com.vectormobile.myvector.ui.bot.RoomActivity;
import com.vectormobile.myvector.ui.browser.list.ContactsListActivity;
import com.vectormobile.myvector.ui.gms.expenses.list.ExpensesListActivity;
import com.vectormobile.myvector.ui.gms.hour.list.HoursListActivity;
import com.vectormobile.myvector.ui.news.NewsActivity;
import com.vectormobile.myvector.ui.offering.OfferingActivity;
import com.vectormobile.myvector.ui.profile.ProfileActivity;
import com.vectormobile.myvector.ui.rocketchat.RocketchatActivity;
import com.vectormobile.myvector.ui.outlook.agenda.OutlookAgendaActivity;
import com.vectormobile.myvector.ui.outlook.mail.OutlookMailActivity;
import timber.log.Timber;

/**
 * Created by amarina on 07/11/2016.
 */
public enum VectorAction {
  OPEN_TIME_IMPUTATION {
    public void launchAction(Context context) {
      context.startActivity(new Intent(context, HoursListActivity.class));
    }
  }, OPEN_EXPENSE_IMPUTATION {
    public void launchAction(Context context) {
      context.startActivity(new Intent(context, ExpensesListActivity.class));
    }
  },
  OPEN_PASSWORD {
    public void launchAction(Context context) {
//      context.startActivity(new Intent(context, PasswordActivity.class));
    }
  },
  OPEN_VECTORTALK {
    public void launchAction(Context context) {
      checkAndLaunchExternalApp(context, VECTOR_TALK_PACKAGE, VECTOR_TALK_MARKET_URL);
    }
  },
  OPEN_CONTACTS {
    public void launchAction(Context context) {
      context.startActivity(new Intent(context, ContactsListActivity.class));
    }
  },
  OPEN_EMAIL {
    public void launchAction(Context context) {
      context.startActivity(new Intent(context, OutlookMailActivity.class));
    }
  },
  OPEN_PROFILE {
    public void launchAction(Context context) {
      context.startActivity(new Intent(context, ProfileActivity.class));
    }
  },
  OPEN_OFFERING {
    @Override
    public void launchAction(Context context) {
      context.startActivity(new Intent(context, OfferingActivity.class));
    }
  },
  OPEN_RADAR {
    @Override
    public void launchAction(Context context) {
      checkAndLaunchExternalApp(context, RADAR_PACKAGE, RADAR_MARKET_URL);
    }
  },
  OPEN_CORPORATE_NEWS {
    @Override
    public void launchAction(Context context) {
      context.startActivity(new Intent(context, NewsActivity.class));
    }
  },
  OPEN_LYNDA {
    @Override
    public void launchAction(Context context) {
      checkAndLaunchExternalApp(context, LYNDA_PACKAGE, LYNDA_MARKET_URL);
    }
  },
  OPEN_ROCKETCHAT {
    @Override
    public void launchAction(Context context) {
      context.startActivity(new Intent(context, RocketchatActivity.class));
    }
  }, OPEN_AGENDA {
    @Override
    public void launchAction(Context context) {
      context.startActivity(new Intent(context, OutlookAgendaActivity.class));
    }
  }, OPEN_ROOM {
    @Override
    public void launchAction(Context context) {
      context.startActivity(new Intent(context, RoomActivity.class));
    }
  };

  public static String RADAR_PACKAGE = "com.vectormobile.trends";
  public static String RADAR_MARKET_URL = "market://details?id=com.vectormobile.trends";
  public static String VECTOR_TALK_PACKAGE = "com.vectormobile.vectortalk";
  public static String VECTOR_TALK_MARKET_URL = "market://details?id=com.vectormobile.vectortalk";
  public static String LYNDA_PACKAGE = "com.lynda.android.root";
  public static String LYNDA_MARKET_URL = "market://details?id=com.lynda.android.root";

  public abstract void launchAction(Context context);

  public void checkAndLaunchExternalApp(Context context, String packageName, String marketUrl) {
    Timber.d(BuildConfig.EASTER_EGG);

    PackageManager pm = context.getPackageManager();
    boolean installed;
    try {
      pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
      installed = true;
    } catch (PackageManager.NameNotFoundException e) {
      installed = false;
    }
    if (installed) {
      Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(packageName);
      if (launchIntent != null)
        context.startActivity(launchIntent);
    } else {
      context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(marketUrl)));
    }
  }
}
