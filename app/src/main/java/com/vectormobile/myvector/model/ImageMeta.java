package com.vectormobile.myvector.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ImageMeta {

  @SerializedName("aperture")
  @Expose
  public String aperture;
  @SerializedName("credit")
  @Expose
  public String credit;
  @SerializedName("camera")
  @Expose
  public String camera;
  @SerializedName("caption")
  @Expose
  public String caption;
  @SerializedName("created_timestamp")
  @Expose
  public String createdTimestamp;
  @SerializedName("copyright")
  @Expose
  public String copyright;
  @SerializedName("focal_length")
  @Expose
  public String focalLength;
  @SerializedName("iso")
  @Expose
  public String iso;
  @SerializedName("shutter_speed")
  @Expose
  public String shutterSpeed;
  @SerializedName("title")
  @Expose
  public String title;
  @SerializedName("orientation")
  @Expose
  public String orientation;
  @SerializedName("keywords")
  @Expose
  public List<Object> keywords = new ArrayList<Object>();

  public String getAperture() {
    return aperture;
  }

  public void setAperture(String aperture) {
    this.aperture = aperture;
  }

  public String getCamera() {
    return camera;
  }

  public void setCamera(String camera) {
    this.camera = camera;
  }

  public String getCaption() {
    return caption;
  }

  public void setCaption(String caption) {
    this.caption = caption;
  }

  public String getCopyright() {
    return copyright;
  }

  public void setCopyright(String copyright) {
    this.copyright = copyright;
  }

  public String getCreatedTimestamp() {
    return createdTimestamp;
  }

  public void setCreatedTimestamp(String createdTimestamp) {
    this.createdTimestamp = createdTimestamp;
  }

  public String getCredit() {
    return credit;
  }

  public void setCredit(String credit) {
    this.credit = credit;
  }

  public String getFocalLength() {
    return focalLength;
  }

  public void setFocalLength(String focalLength) {
    this.focalLength = focalLength;
  }

  public String getIso() {
    return iso;
  }

  public void setIso(String iso) {
    this.iso = iso;
  }

  public List<Object> getKeywords() {
    return keywords;
  }

  public void setKeywords(List<Object> keywords) {
    this.keywords = keywords;
  }

  public String getOrientation() {
    return orientation;
  }

  public void setOrientation(String orientation) {
    this.orientation = orientation;
  }

  public String getShutterSpeed() {
    return shutterSpeed;
  }

  public void setShutterSpeed(String shutterSpeed) {
    this.shutterSpeed = shutterSpeed;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }
}
