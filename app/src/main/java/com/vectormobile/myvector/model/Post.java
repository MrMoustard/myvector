package com.vectormobile.myvector.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Post {

  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("date")
  @Expose
  private String date;
  @SerializedName("date_gmt")
  @Expose
  private String dateGmt;
  @SerializedName("guid")
  @Expose
  private Guid guid;
  @SerializedName("modified")
  @Expose
  private String modified;
  @SerializedName("modified_gmt")
  @Expose
  private String modifiedGmt;
  @SerializedName("slug")
  @Expose
  private String slug;
  @SerializedName("type")
  @Expose
  private String type;
  @SerializedName("link")
  @Expose
  private String link;
  @SerializedName("title")
  @Expose
  private Title title;
  @SerializedName("content")
  @Expose
  private Content content;
  @SerializedName("excerpt")
  @Expose
  private Excerpt excerpt;
  @SerializedName("author")
  @Expose
  private Integer author;
  @SerializedName("featured_media")
  @Expose
  private Integer featuredMedia;
  @SerializedName("comment_status")
  @Expose
  private String commentStatus;
  @SerializedName("ping_status")
  @Expose
  private String pingStatus;
  @SerializedName("sticky")
  @Expose
  private Boolean sticky;
  @SerializedName("format")
  @Expose
  private String format;
  @SerializedName("categories")
  @Expose
  private List<Integer> categories = new ArrayList<Integer>();
  @SerializedName("tags")
  @Expose
  private List<Integer> tags = new ArrayList<Integer>();
  @SerializedName("better_featured_image")
  @Expose
  private BetterFeaturedImage betterFeaturedImage;
  @SerializedName("_links")
  @Expose
  private Links links;

  private String keys;
  private String pdfSource;

  /**
   * @return The id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id The id
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * @return The date
   */
  public String getDate() {
    return date;
  }

  /**
   * @param date The date
   */
  public void setDate(String date) {
    this.date = date;
  }

  /**
   * @return The dateGmt
   */
  public String getDateGmt() {
    return dateGmt;
  }

  /**
   * @param dateGmt The date_gmt
   */
  public void setDateGmt(String dateGmt) {
    this.dateGmt = dateGmt;
  }

  /**
   * @return The guid
   */
  public Guid getGuid() {
    return guid;
  }

  /**
   * @param guid The guid
   */
  public void setGuid(Guid guid) {
    this.guid = guid;
  }

  /**
   * @return The modified
   */
  public String getModified() {
    return modified;
  }

  /**
   * @param modified The modified
   */
  public void setModified(String modified) {
    this.modified = modified;
  }

  /**
   * @return The modifiedGmt
   */
  public String getModifiedGmt() {
    return modifiedGmt;
  }

  /**
   * @param modifiedGmt The modified_gmt
   */
  public void setModifiedGmt(String modifiedGmt) {
    this.modifiedGmt = modifiedGmt;
  }

  /**
   * @return The slug
   */
  public String getSlug() {
    return slug;
  }

  /**
   * @param slug The slug
   */
  public void setSlug(String slug) {
    this.slug = slug;
  }

  /**
   * @return The type
   */
  public String getType() {
    return type;
  }

  /**
   * @param type The type
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * @return The link
   */
  public String getLink() {
    return link;
  }

  /**
   * @param link The link
   */
  public void setLink(String link) {
    this.link = link;
  }

  /**
   * @return The title
   */
  public Title getTitle() {
    return title;
  }

  /**
   * @param title The title
   */
  public void setTitle(Title title) {
    this.title = title;
  }

  /**
   * @return The content
   */
  public Content getContent() {
    return content;
  }

  /**
   * @param content The content
   */
  public void setContent(Content content) {
    this.content = content;
  }

  /**
   * @return The excerpt
   */
  public Excerpt getExcerpt() {
    return excerpt;
  }

  /**
   * @param excerpt The excerpt
   */
  public void setExcerpt(Excerpt excerpt) {
    this.excerpt = excerpt;
  }

  /**
   * @return The author
   */
  public Integer getAuthor() {
    return author;
  }

  /**
   * @param author The author
   */
  public void setAuthor(Integer author) {
    this.author = author;
  }

  /**
   * @return The featuredMedia
   */
  public Integer getFeaturedMedia() {
    return featuredMedia;
  }

  /**
   * @param featuredMedia The featured_media
   */
  public void setFeaturedMedia(Integer featuredMedia) {
    this.featuredMedia = featuredMedia;
  }

  /**
   * @return The commentStatus
   */
  public String getCommentStatus() {
    return commentStatus;
  }

  /**
   * @param commentStatus The comment_status
   */
  public void setCommentStatus(String commentStatus) {
    this.commentStatus = commentStatus;
  }

  /**
   * @return The pingStatus
   */
  public String getPingStatus() {
    return pingStatus;
  }

  /**
   * @param pingStatus The ping_status
   */
  public void setPingStatus(String pingStatus) {
    this.pingStatus = pingStatus;
  }

  /**
   * @return The sticky
   */
  public Boolean getSticky() {
    return sticky;
  }

  /**
   * @param sticky The sticky
   */
  public void setSticky(Boolean sticky) {
    this.sticky = sticky;
  }

  /**
   * @return The format
   */
  public String getFormat() {
    return format;
  }

  /**
   * @param format The format
   */
  public void setFormat(String format) {
    this.format = format;
  }

  /**
   * @return The categories
   */
  public List<Integer> getSections() {
    return categories;
  }

  /**
   * @param categories The categories
   */
  public void setCategories(List<Integer> categories) {
    this.categories = categories;
  }

  /**
   * @return The tags
   */
  public List<Integer> getTags() {
    return tags;
  }

  /**
   * @param tags The tags
   */
  public void setTags(List<Integer> tags) {
    this.tags = tags;
  }

  /**
   * @return The betterFeaturedImage
   */
  public BetterFeaturedImage getBetterFeaturedImage() {
    return betterFeaturedImage;
  }

  /**
   * @param betterFeaturedImage The better_featured_image
   */
  public void setBetterFeaturedImage(BetterFeaturedImage betterFeaturedImage) {
    this.betterFeaturedImage = betterFeaturedImage;
  }

  /**
   * @return The links
   */
  public Links getLinks() {
    return links;
  }

  /**
   * @param links The _links
   */
  public void setLinks(Links links) {
    this.links = links;
  }

  public void parseContent() {
    content.setRendered("<head><style media=\"screen\" type=\"text/css\">\n" +
        "\n" +
        "    body {\n" +
        "        background-color: #FFF;\n" +
        "        padding: 10;\n" +
        "        margin: 0;\n" +
        "        font-family: Helvetica;\n" +
        "        font-size: 14px;\n" +
        "        max-width: 100%;\n" +
        "    }\n" +
        "\n" +
        "    iframe, img, div {\n" +
        "        max-width: 100%;\n" +
        "        height: auto;\n" +
        "        margin-top: 10px;\n" +
        "        margin-bottom: 10px;\n" +
        "    }\n" +
        "\n" +
        "    a {\n" +
        "        max-width: 80%;\n" +
        "        height: auto;\n" +
        "        margin-top: 10px;\n" +
        "        margin-bottom: 10px;\n" +
        "        font-size: 12px;\n" +
        "    }\n" +
        "\n" +
        "    ul {\n" +
        "        padding: 10px;\n" +
        "        margin: 0px;\n" +
        "        font-size: 14px;\n" +
        "    }\n" +
        "\n" +
        "    .btn {\n" +
        "        background: #3498db;\n" +
        "        background-image: -webkit-linear-gradient(top, #3498db, #2980b9);\n" +
        "        background-image: -moz-linear-gradient(top, #3498db, #2980b9);\n" +
        "        background-image: -ms-linear-gradient(top, #3498db, #2980b9);\n" +
        "        background-image: -o-linear-gradient(top, #3498db, #2980b9);\n" +
        "        background-image: linear-gradient(top bottom, #3498db, #2980b9);\n" +
        "        -webkit-border-radius: 14;\n" +
        "        -moz-border-radius: 14;\n" +
        "        border-radius: 14px;\n" +
        "        font-family: Arial;\n" +
        "        color: #ffffff;\n" +
        "        font-size: 14px;\n" +
        "        padding: 5px 10px 5px 10px;\n" +
        "        text-decoration: none;\n" +
        "    }\n" +
        "\n" +
        "    .btn:hover {\n" +
        "        background: #3cb0fd;\n" +
        "        background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);\n" +
        "        background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);\n" +
        "        background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);\n" +
        "        background-image: -o-linear-gradient(top, #3cb0fd, #3498db);\n" +
        "        background-image: linear-gradient(to bottom, #3cb0fd, #3498db);\n" +
        "        text-decoration: none;\n" +
        "    }\n" +
        "\n" +
        "    h4{\n" +
        "        color: #e8b038;\n" +
        "        background-color: #e9e9ea;     \n" +
        "        border: 1px solid #d5d6d7;\n" +
        "        border-radius: 10px;  \n" +
        "        max-width: 100%;   \n" +
        "        padding-left: 30px;   \n" +
        "        padding-top: 11px;  \n" +
        "    }\n" +
        "\n" +
        "    h4 ul{       \n" +
        "        margin-left: 0px;\n" +
        "        color: black;\n" +
        "    }\n" +
        "\n" +
        "    li{\n" +
        "        margin-bottom: 5px;\n" +
        "        font-weight: normal;\n" +
        "    }\n" +
        "pre{ display: inline-block; white-space:pre-wrap; font-size: 80%; }" +
        "</style></head>" +
        content.getRendered().replaceAll("text-align: justify;", "text-align: aling-left;"));
  }

  public String getKeys() {
    return keys;
  }

  public String parsePdfSource() {
    String url = null;
    Pattern p = Pattern.compile("http[^\\\"]+.pdf");
    Matcher m = p.matcher(content.getRendered());
    while (m.find()) {
      url = m.group(0);
    }
    return url;
  }

  @Override
  public String toString() {
    return "Post{" +
        "id=" + id +
        ", date='" + date + '\'' +
        ", dateGmt='" + dateGmt + '\'' +
        ", guid=" + guid +
        ", modified='" + modified + '\'' +
        ", modifiedGmt='" + modifiedGmt + '\'' +
        ", slug='" + slug + '\'' +
        ", type='" + type + '\'' +
        ", link='" + link + '\'' +
        ", title=" + title +
        ", content=" + content +
        ", excerpt=" + excerpt +
        ", author=" + author +
        ", featuredMedia=" + featuredMedia +
        ", commentStatus='" + commentStatus + '\'' +
        ", pingStatus='" + pingStatus + '\'' +
        ", sticky=" + sticky +
        ", format='" + format + '\'' +
        ", categories=" + categories +
        ", tags=" + tags +
        ", betterFeaturedImage=" + betterFeaturedImage +
        ", links=" + links +
        ", keys='" + keys + '\'' +
        ", pdfSource='" + pdfSource + '\'' +
        '}';
  }
}
