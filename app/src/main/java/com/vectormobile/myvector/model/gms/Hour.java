package com.vectormobile.myvector.model.gms;

import android.content.Context;
import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;
import com.vectormobile.myvector.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by ajruiz on 25/01/2017.
 */

public class Hour implements Comparable<Hour>, Parcelable {

  public static final String HOUR_SENT = "ENVIADO";
  public static final String HOUR_APPROVED = "APROBADO";
  public static final String HOUR_REJECTED = "RECHAZADO";
  public static final String HOUR_SAVED = "CREADO";

  private String id;
  private long hourDate;
  private Date dateFrom;
  private Date dateTo;
  private String state;
  private String workOrderName;
  private String projectName;
  private String categoryName;
  private String comment;
  private long weekFrom;
  private long weekTo;
  private int units;
  private boolean massiveReport;
  private String externalCode;
  private String approverComment;
  private boolean jirable;
  private String jiraCode;

  public Hour(String id, String comment, Date dateFrom, Date dateTo, long hourDate,
              String state, long weekFrom, long weekTo, String workOrderName,
              int units, boolean massiveReport, String projectName, String externalCode,
              String categoryName, String approverComment, boolean jirable, String jiraCode) {
    this.id = id;
    this.comment = comment;
    this.dateFrom = dateFrom;
    this.dateTo = dateTo;
    this.hourDate = hourDate;
    this.state = state;
    this.weekFrom = weekFrom;
    this.weekTo = weekTo;
    this.workOrderName = workOrderName;
    this.units = units;
    this.massiveReport = massiveReport;
    this.projectName = projectName;
    this.externalCode = externalCode;
    this.categoryName = categoryName;
    this.approverComment = approverComment;
    this.jirable = jirable;
    this.jiraCode = jiraCode;
  }

  protected Hour(Parcel in) {
    id = in.readString();
    state = in.readString();
    long tmpDateFrom = in.readLong();
    dateFrom = tmpDateFrom != -1 ? new Date(tmpDateFrom) : null;
    long tmpDateTo = in.readLong();
    dateTo = tmpDateTo != -1 ? new Date(tmpDateTo) : null;
    hourDate = in.readLong();
    workOrderName = in.readString();
    units = in.readInt();
    projectName = in.readString();
    comment = in.readString();
    externalCode = in.readString();
    categoryName = in.readString();
    approverComment = in.readString();
    jirable = in.readInt() == 1;
    jiraCode = in.readString();
  }

  public String getDateString() {
    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
    return massiveReport ?
        String.format(Locale.getDefault(), "%s - %s", formatter.format(dateFrom), formatter.format(dateTo)) :
        formatter.format(dateFrom);
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public Date getDateFrom() {
    return dateFrom;
  }

  public void setDateFrom(Date dateFrom) {
    this.dateFrom = dateFrom;
  }

  public Date getDateTo() {
    return dateTo;
  }

  public void setDateTo(Date dateTo) {
    this.dateTo = dateTo;
  }

  public long getHourDate() {
    return hourDate;
  }

  public void setHourDate(long hourDate) {
    this.hourDate = hourDate;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public long getWeekFrom() {
    return weekFrom;
  }

  public void setWeekFrom(long weekFrom) {
    this.weekFrom = weekFrom;
  }

  public long getWeekTo() {
    return weekTo;
  }

  public void setWeekTo(long weekTo) {
    this.weekTo = weekTo;
  }

  public String getWorkOrderName() {
    return workOrderName;
  }

  public void setWorkOrderName(String workOrderName) {
    this.workOrderName = workOrderName;
  }

  public String getCategoryName() {
    return categoryName;
  }

  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }

  public int getUnits() {
    return units;
  }

  public void setUnits(int units) {
    this.units = units;
  }

  public boolean isMassiveReport() {
    return massiveReport;
  }

  public void setMassiveReport(boolean massiveReport) {
    this.massiveReport = massiveReport;
  }

  public String getProjectName() {
    return projectName;
  }

  public void setProjectName(String projectName) {
    this.projectName = projectName;
  }

  public String getExternalCode() {
    return externalCode;
  }

  public void setExternalCode(String externalCode) {
    this.externalCode = externalCode;
  }

  public String getApproverComment() {
    return approverComment;
  }

  public void setApproverComment(String approverComment) {
    this.approverComment = approverComment;
  }

  public boolean isJirable() {
    return jirable;
  }

  public void setJirable(boolean jirable) {
    this.jirable = jirable;
  }

  public String getJiraCode() {
    return jiraCode;
  }

  public void setJiraCode(String jiraCode) {
    this.jiraCode = jiraCode;
  }

  public int getColour() {
    int colour = 0;
    switch (state) {
      case HOUR_SENT:
        colour = Color.BLUE;
        break;
      case HOUR_APPROVED:
        colour = Color.GREEN;
        break;
      case HOUR_REJECTED:
        colour = Color.RED;
        break;
      case HOUR_SAVED:
        colour = Color.GRAY;
        break;
    }
    return colour;
  }

  public String getStateString(Context context) {
    switch (state) {
      case HOUR_SENT:
        return context.getString(R.string.hour_sent_status_title);
      case HOUR_APPROVED:
        return context.getString(R.string.hour_approved_status_header_title);
      case HOUR_REJECTED:
        return context.getString(R.string.hour_rejected_status_header_title);
      case HOUR_SAVED:
        return context.getString(R.string.hour_saved_status_header_title);
    }
    return null;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel parcel, int i) {
    parcel.writeString(id);
    parcel.writeString(state);
    parcel.writeLong(dateFrom != null ? dateFrom.getTime() : -1L);
    parcel.writeLong(dateTo != null ? dateTo.getTime() : -1L);
    parcel.writeLong(hourDate);
    parcel.writeString(workOrderName);
    parcel.writeInt(units);
    parcel.writeString(projectName);
    parcel.writeString(comment);
    parcel.writeString(externalCode);
    parcel.writeString(categoryName);
    parcel.writeString(approverComment);
    parcel.writeInt(jirable ? 1 : 0);
    parcel.writeString(jiraCode);
  }

  @Override
  public int compareTo(Hour hour) {
    return getDateFrom().compareTo(hour.getDateFrom());
  }

  @SuppressWarnings("unused")
  public static final Parcelable.Creator<Hour> CREATOR = new Parcelable.Creator<Hour>() {
    @Override
    public Hour createFromParcel(Parcel in) {
      return new Hour(in);
    }

    @Override
    public Hour[] newArray(int size) {
      return new Hour[size];
    }
  };
}
