package com.vectormobile.myvector.storage;

import com.google.gson.Gson;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by ajruiz on 29/12/2016.
 */

public class PreferencesManager {

  public static final String KEY_SESSION_COOKIE = "SESSION_COOKIE";
  public static final String KEY_OFFERING_COOKIE = "OFFERING_COOKIE";
  public static final String KEY_OFFERING_COOKIE_AVAILABLE = "OFFERING_COOKIE_AVAILABLE";
  public static final String KEY_GMS_COOKIE = "GMS_COOKIE";
  public static final String KEY_GMS_COOKIE_AVAILABLE = "GMS_COOKIE_AVAILABLE";
  public static final String KEY_INTRANET_COOKIE = "INTRANET_COOKIE";
  public static final String KEY_INTRANET_COOKIE_AVAILABLE = "INTRANET_COOKIE_AVAILABLE";
  public static final String KEY_MUST_HIDE_RECYCLER_VIEW_TUTORIAL = "KEY_MUST_HIDE_RECYCLER_VIEW_TUTORIAL";
  private static final List<Class<?>> CLASSES = new ArrayList<>();
  private static final String KEY_USERNAME = "USERNAME";
  private static final String KEY_CONVERSAIONID = "CONVERSATION_ID";

  static {
    CLASSES.add(String.class);
    CLASSES.add(Boolean.class);
    CLASSES.add(Integer.class);
    CLASSES.add(Long.class);
    CLASSES.add(Float.class);
    CLASSES.add(Set.class);
  }

  private SharedPreferences prefs;
  private Gson gson;

  public PreferencesManager(Context context) {
    prefs = PreferenceManager.getDefaultSharedPreferences(context);
    gson = new Gson();
  }

  public void removeKeyFromPreferences(String key) {
    prefs.edit().remove(key).apply();
  }

  public void removeAll() {
    prefs.edit().clear().apply();
  }

  public <T> void persist(String key, T value) {
    final SharedPreferences.Editor ed = prefs.edit();
    if (value == null) {
      ed.putString(key, null);
    } else if (value instanceof String) {
      ed.putString(key, (String) value);
    } else if (value instanceof Boolean) {
      ed.putBoolean(key, (Boolean) value);
    } else if (value instanceof Integer) {
      ed.putInt(key, (Integer) value);
    } else if (value instanceof Long) {
      ed.putLong(key, (Long) value);
    } else if (value instanceof Float) {
      ed.putFloat(key, (Float) value);
    } else if (value instanceof Set) {
      if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
        throw new IllegalStateException(
            "You can add sets in the preferences only after API "
                + Build.VERSION_CODES.HONEYCOMB);
      }
      @SuppressWarnings({"unchecked", "unused"}) SharedPreferences.Editor
          soIcanAddSuppress = ed.putStringSet(key, (Set<String>) value);
    } else {
      throw new IllegalArgumentException(
          "The given value : " + value + " cannot be persisted");
    }
    ed.apply();
  }

  public <T> void persistObject(String key, T object) {
    final SharedPreferences.Editor ed = prefs.edit();
    if (object == null) {
      throw new IllegalArgumentException("Object is null");
    }
    if (key.equals("") || key == null) {
      throw new IllegalArgumentException("Key is empty or null");
    }
    ed.putString(key, gson.toJson(object));
    ed.apply();
  }

  public <T> T retrieve(String key, T defaultValue) {
    if (defaultValue == null) {
      if (!prefs.contains(key)) {
        return null;
      }
      // if the key does exist I get the value and..
      final Object value = prefs.getAll().get(key);
      // ..if null I return null
      if (value == null) {
        return null;
      }

      final Class<?> clazz = value.getClass();
      for (Class<?> cls : CLASSES) {
        if (clazz.isAssignableFrom(cls)) {
          try {
            return (T) clazz.cast(value);
          } catch (ClassCastException e) {
            String msg = "Value : "
                + value
                + " stored for key : "
                + key
                + " is not assignable to variable of given type.";
            throw new IllegalStateException(msg, e);
          }
        }
      }
      // that's really Illegal State I guess
      throw new IllegalStateException(
          "Unknown class for value :\n\t" + value + "\nstored in preferences");
    } else if (defaultValue instanceof String) {
      return (T) prefs.getString(key, (String) defaultValue);
    } else if (defaultValue instanceof Boolean) {
      return (T) (Boolean) prefs.getBoolean(key, (Boolean) defaultValue);
    } else if (defaultValue instanceof Integer) {
      return (T) (Integer) prefs.getInt(key, (Integer) defaultValue);
    } else if (defaultValue instanceof Long) {
      return (T) (Long) prefs.getLong(key, (Long) defaultValue);
    } else if (defaultValue instanceof Float) {
      return (T) (Float) prefs.getFloat(key, (Float) defaultValue);
    } else if (defaultValue instanceof Set) {
      if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
        throw new IllegalStateException("You can add sets in the preferences only after API"
            + Build.VERSION_CODES.HONEYCOMB);
      }
      return (T) prefs.getStringSet(key, (Set<String>) defaultValue);
    } else {
      throw new IllegalArgumentException(
          defaultValue + "cannot be persisted in SharedPreferences");
    }
  }

  public <T> T retrieveObject(String key, T defaultValue, Class<T> tClass) {
    String json = prefs.getString(key, null);
    if (json == null) {
      return defaultValue;
    } else {
      return gson.fromJson(json, tClass);
    }
  }

  /**
   * ******************************** SessionId ********************************
   */

  public void saveGmsToken(String cookie) {
    persist(KEY_GMS_COOKIE, cookie);
  }

  public String getGmsToken() {
    return retrieve(KEY_GMS_COOKIE, null);
  }

  public void saveIntranetToken(String cookie) {
    persist(KEY_INTRANET_COOKIE, cookie);
  }

  public String getIntranetToken() {
    return retrieve(KEY_INTRANET_COOKIE, null);
  }

  public void saveOfferingToken(String offeringToken) {
    persist(KEY_OFFERING_COOKIE, offeringToken);
  }

  public String getOfferingToken() {
    return retrieve(KEY_OFFERING_COOKIE, null);
  }

  public void saveGmsTokenAvailable(boolean gmsTokenActive) {
    persist(KEY_GMS_COOKIE_AVAILABLE, gmsTokenActive);
  }

  public boolean isGmsTokenAvailable() {
    return retrieve(KEY_GMS_COOKIE_AVAILABLE, false);
  }

  public void saveIntranetTokenAvailable(boolean intranetTokenActive) {
    persist(KEY_INTRANET_COOKIE_AVAILABLE, intranetTokenActive);
  }

  public boolean isIntranetTokenAvailable() {
    return retrieve(KEY_INTRANET_COOKIE_AVAILABLE, false);
  }

  public void saveOfferingTokenAvailable(boolean offeringTokenActive) {
    persist(KEY_OFFERING_COOKIE_AVAILABLE, offeringTokenActive);
  }

  public boolean isOfferingTokenAvailable() {
    return retrieve(KEY_OFFERING_COOKIE_AVAILABLE, false);
  }

  public void logout() {
    SharedPreferences.Editor editor = prefs.edit();
    editor.remove(KEY_GMS_COOKIE);
    editor.remove(KEY_INTRANET_COOKIE);
    editor.remove(KEY_OFFERING_COOKIE);
    editor.remove(KEY_GMS_COOKIE_AVAILABLE);
    editor.remove(KEY_INTRANET_COOKIE_AVAILABLE);
    editor.remove(KEY_OFFERING_COOKIE_AVAILABLE);
    editor.apply();
  }

  public void saveUsername(String username) {
    persist(KEY_USERNAME, username);
  }

  public String getUsername() {
    return retrieve(KEY_USERNAME, null);
  }

  public String getConversacionId() {
    return retrieve(KEY_CONVERSAIONID, null);
  }

  public void saveConversationId(String conversationId) {
    persist(KEY_CONVERSAIONID, conversationId);
  }
}
