package com.vectormobile.myvector.storage;

import android.os.AsyncTask;
import android.os.Environment;
import timber.log.Timber;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ajruiz on 14/11/2016.
 */

public class PdfDownloadAsyncTask extends AsyncTask<String, Void, String> {

  @Override
  protected String doInBackground(String... params) {
    if (params == null || params[0].isEmpty()) {
      return null;
    } else {
      String url = params[0];
      File f = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
      String ff = f.toString();
      File directory = new File(ff + "/" + url.substring(url.lastIndexOf('/') + 1));
      FileOutputStream file = null;
      try {
        file = new FileOutputStream(directory);
        URL scrapUrl = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) scrapUrl.openConnection();
        connection.setRequestMethod("GET");
        connection.setDoOutput(true);
        connection.connect();
        InputStream input = connection.getInputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = input.read(buffer)) > 0) {
          file.write(buffer, 0, len);
        }
      } catch (Exception ex) {
        Timber.e(ex, ex.getMessage());
        return null;
      } finally {
        try {
          if (file != null) {
            file.close();
          }
        } catch (IOException ioe) {
          ioe.printStackTrace();
          return null;
        }
      }
      return directory.getAbsolutePath();
    }
  }
}
