package com.vectormobile.myvector.storage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import timber.log.Timber;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by amarina on 30/01/2017.
 */
public class ImageManager {
  private static ImageManager instance = null;
  private String lastTempFile;

  private ImageManager() {
  }

  public static ImageManager getInstance() {
    if (instance == null) {
      instance = new ImageManager();
    }
    return instance;
  }

  public File getFileToWrite(String fileName) {
    File directory = new File(Environment.getExternalStorageDirectory().getPath());
    return new File(directory, fileName);
  }


  public boolean haveImage(String expenseId) {
    return getFileToWrite(expenseId).exists();
  }

  public void deleteTmpImage(String localImageFilename) {
    getFileToWrite(localImageFilename).delete();
    setLastTempFile(null);
  }

  public File createTmpImage(Context context) throws IOException {
    // Create an image file name
    Long timeStampLong = System.currentTimeMillis() / 1000;
    String timeStamp = timeStampLong.toString();
    String imageFileName = "JPG_" + timeStamp + "_";
    File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    File image = File.createTempFile(
        imageFileName,  /* prefix */
        ".jpg",         /* suffix */
        storageDir      /* directory */
    );

    // Save a file: path for use with ACTION_VIEW intents
    lastTempFile = image.getAbsolutePath();
    return image;
  }

  public String getLastTempFile() {
    return lastTempFile;
  }

  public void setLastTempFile(String lastTempFile) {
    this.lastTempFile = lastTempFile;
  }

  public void copyImage(String localImageFilename, String expenseId) {
    File sourceFile = new File(localImageFilename);
    File targetFile = getFileToWrite(expenseId);
    copyImageToGalleryFromTmpExpense(sourceFile, targetFile);

  }

  public void copyImageToGalleryFromTmpExpense(File sourceFile, File targetFile) {
    InputStream in = null;
    OutputStream out = null;
    try {
      in = new FileInputStream(sourceFile);
      out = new FileOutputStream(targetFile);

      // Transfer bytes from in to out
      byte[] buf = new byte[1024];
      int len;
      while ((len = in.read(buf)) > 0) {
        out.write(buf, 0, len);
      }

    } catch (FileNotFoundException fnfe) {
      Timber.e(fnfe, fnfe.getMessage());
    } catch (IOException e) {
      Timber.e(e, e.getMessage());
    } finally {
      try {
        if (in != null) {
          in.close();
        }

      } catch (IOException e) {
        Timber.e(e, e.getMessage());
      } finally {
        try {
          if (out != null) {
            out.close();
          }
        } catch (IOException e) {
          Timber.e(e, e.getMessage());
        }
      }
    }
  }

  public File convertBitmapToFile(Context context, Bitmap bitmap) throws IOException {
    File imageFile = createTmpImage(context);

    OutputStream os;
    try {
      os = new FileOutputStream(imageFile);
      bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
      os.flush();
      os.close();
    } catch (Exception e) {

    }
    return imageFile;
  }

  public String getImagePathFromGallery(Context context, Uri uri) {
    if (Build.VERSION.SDK_INT < 19) {
      return getPathUntilKitkat(context, uri);
    } else {
      return getPathFromKitkat(context, uri);
    }
  }

  @SuppressLint("NewApi")
  private String getPathFromKitkat(Context context, Uri uri) {
    String filePath = "";
    if (uri.getHost().contains("com.android.providers.media")) {
      String wholeID = DocumentsContract.getDocumentId(uri);
      String id = wholeID.split(":")[1];

      String[] column = {MediaStore.Images.Media.DATA};
      String sel = MediaStore.Images.Media._ID + "=?";

      Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
          column, sel, new String[]{id}, null);

      int columnIndex = cursor.getColumnIndex(column[0]);
      if (cursor.moveToFirst()) {
        filePath = cursor.getString(columnIndex);
      }
      cursor.close();
      return filePath;
    } else {
      return getPathUntilKitkat(context, uri);
    }
  }


  @SuppressLint("NewApi")
  private String getPathUntilKitkat(Context context, Uri uri) {
    String[] proj = {MediaStore.Images.Media.DATA};
    String result = null;
    CursorLoader cursorLoader = new CursorLoader(
        context,
        uri, proj, null, null, null);
    Cursor cursor = cursorLoader.loadInBackground();
    if (cursor != null) {
      int column_index =
          cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
      cursor.moveToFirst();
      result = cursor.getString(column_index);
    }
    return result;
  }

}
