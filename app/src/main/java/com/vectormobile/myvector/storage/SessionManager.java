package com.vectormobile.myvector.storage;

import com.vectormobile.myvector.R;
import com.vectormobile.myvector.domain.interceptor.ReceivedCookiesInterceptor;
import com.vectormobile.myvector.model.intranet.UserProfile;
import com.vectormobile.myvector.util.AppResources;
import okhttp3.Request;

/**
 * Created by ajruiz on 29/12/2016.
 */
public class SessionManager {

  private static final String COOKIE = "Cookie";
  public static final String INTRANET_HOST = "intranet.vectoritcgroup.com";
  public static final String GMS_HOST = "gms.vectoritcgroup.com";
  public static final String OFFERING_HOST = "trends.thinknowa.com";
  public static final String INTRANET_REQUEST = "https://intranet.vectoritcgroup.com/";
  public static final String GMS_REQUEST = "https://gms.vectoritcgroup.com/";
  public static final String OFFERING_REQUEST = "https://trends.thinknowa.com/";

  private AppResources appResource;
  private UserProfile userProfile;
  private PreferencesManager preferencesManager;

  public SessionManager(PreferencesManager preferencesManager, AppResources appResource) {
    this.preferencesManager = preferencesManager;
    this.appResource = appResource;
  }

  public String getGmsToken() {
    return buildCookieString(appResource.getString(R.string.session_id_cookie_value_gms), preferencesManager.getGmsToken());
  }

  public void setGmsToken(String gmsToken) {
    preferencesManager.saveGmsToken(trimCookie(gmsToken));
  }

  public boolean isGmsTokenActive() {
    return preferencesManager.isGmsTokenAvailable();
  }

  public void setGmsTokenActive(boolean gmsTokenActive) {
    preferencesManager.saveGmsTokenAvailable(gmsTokenActive);
  }

  public String getIntranetToken() {
    return buildCookieString(appResource.getString(R.string.session_id_cookie_value_intranet), preferencesManager.getIntranetToken());
  }

  public void setIntranetToken(String intranetToken) {
    preferencesManager.saveIntranetToken(trimIntranetCookie(intranetToken));
  }

  private String trimIntranetCookie(String value) {
    return value.substring(ReceivedCookiesInterceptor.COOKIE_SESSION_ID_WORDPRESS.length() + 1, value.indexOf(";"));
  }

  public boolean isIntranetTokenActive() {
    return preferencesManager.isIntranetTokenAvailable();
  }

  public void setIntranetTokenActive(boolean intranetTokenActive) {
    preferencesManager.saveIntranetTokenAvailable(intranetTokenActive);
  }

  public String getOfferingToken() {
    return preferencesManager.getOfferingToken();
  }

  public void setOfferingToken(String offeringToken) {
    preferencesManager.saveOfferingToken(offeringToken);
  }

  public boolean isOfferingTokenActive() {
    return preferencesManager.isOfferingTokenAvailable();
  }

  public void setOfferingTokenActive(boolean offeringTokenActive) {
    preferencesManager.saveOfferingTokenAvailable(offeringTokenActive);
  }

  public UserProfile getUserProfile() {
    return userProfile;
  }

  public void setUserProfile(UserProfile userProfile) {
    this.userProfile = userProfile;
  }

  public String buildCookieString(String cookie, String token) {
    return (token != null) ? String.format(cookie, token) : "";
  }

  private String trimCookie(String value) {
    return value.substring(ReceivedCookiesInterceptor.COOKIE_SESSION_ID.length() + 1, value.indexOf(";"));
  }

  public Request.Builder addCookieIfNeeded(Request request) {
    Request.Builder builder = request.newBuilder();
    if (needGmsCookie(request)) {
      builder.addHeader(COOKIE, getGmsToken());
    } else if (needIntranetCookie(request)) {
      builder.addHeader(COOKIE, getIntranetToken());
    } else if (needOfferingCookie(request)) {
      builder.addHeader(COOKIE, getOfferingToken());
    }
    return builder;
  }

  private boolean needOfferingCookie(Request request) {
    return OFFERING_HOST.equals(request.url().host()) && isOfferingTokenActive();
  }

  private boolean needIntranetCookie(Request request) {
    return INTRANET_HOST.equals(request.url().host()) && isIntranetTokenActive();
  }

  private boolean needGmsCookie(Request request) {
    return GMS_HOST.equals(request.url().host()) && isGmsTokenActive();
  }

  public boolean checkLogins() {
    return isGmsTokenActive() && isIntranetTokenActive() && isOfferingTokenActive();
  }

  public void logout() {
    preferencesManager.logout();
  }

  public void saveUsername(String username) {
    preferencesManager.saveUsername(username);
  }

    public String getUsername() {
        return preferencesManager.getUsername();
    }
}