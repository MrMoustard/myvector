package com.vectormobile.myvector.ui.news.detail;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.NewsComponent;
import com.vectormobile.myvector.ui.base.BaseFragment;
import com.vectormobile.myvector.ui.news.NewsActivity;
import com.vectormobile.myvector.ui.news.news.adapter.PicassoTrustAll;
import timber.log.Timber;

import javax.inject.Inject;

/**
 * Created by ajruiz on 11/11/2016.
 */

public class NewsDetailFragment extends BaseFragment<NewsDetailPresenter> implements NewsDetailView {

  @Inject
  NewsDetailPresenter presenter;

  @BindView(R.id.rl_news_detail)
  RelativeLayout mainRelativeLayout;
  @BindView(R.id.wb_news_detail)
  WebView webViewNewsItem;
  @BindView(R.id.iv_news_item_header_image)
  ImageView ivNewsItemHeaderImage;

  private ProgressDialog loadingDialog;
  private TextView tvNewsItemTitle;
  private String newsItemTitle;
  private String newsItemUrl;

  private void getIntentData() {
    Bundle bundle = getArguments();
    if (bundle != null) {
      newsItemTitle = bundle.getString(NewsActivity.KEY_NEWS_TITLE_SELECTED);
      newsItemUrl = bundle.getString(NewsActivity.KEY_NEWS_URL_SELECTED);
      Answers.getInstance().logContentView(
          new ContentViewEvent()
              .putContentId(newsItemUrl)
              .putContentName(newsItemTitle)
              .putContentType("intranet news")
      );
      presenter.getNewsById(newsItemUrl);
    }
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    createLoadingDialog();
    getIntentData();
  }

  @Override
  public void onPause() {
    super.onPause();
    NewsActivity.setNewsOpened(false);
    if (tvNewsItemTitle != null) {
      tvNewsItemTitle.setText(getResources().getString(R.string.menu_item_news));
    }
  }

  private void setTitle() {
    tvNewsItemTitle = (TextView) getActivity().findViewById(R.id.tv_news_title);
    tvNewsItemTitle.setText(newsItemTitle);
  }

  @Override
  protected void inject() {
    getComponent(NewsComponent.class).inject(this);
  }

  @Override
  protected NewsDetailPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.fragment_news_detail;
  }

  @Override
  public void finishLoadedContent(final String content) {
    webViewNewsItem.setVerticalScrollBarEnabled(false);
    webViewNewsItem.setHorizontalScrollBarEnabled(false);
    webViewNewsItem.loadData(content, "text/html; charset=utf-8", "UTF-8");
    webViewNewsItem.setWebViewClient(new WebViewClient() {
      @Override
      public boolean shouldOverrideUrlLoading(WebView view, String url) {
        try {
          Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
          startActivity(intent);
        } catch (ActivityNotFoundException anfe) {
          Timber.e(anfe, "Error: %s", anfe.getMessage());
        }
        return true;
      }

      public void onPageFinished(WebView view, String url) {
        hideLoadingDialog();
      }
    });
  }

  private void createLoadingDialog() {
    loadingDialog = new ProgressDialog(getActivity(), R.style.CustomLoadingDialogTheme);
    loadingDialog.setCancelable(true);
    loadingDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialog) {
        NewsActivity.setNewsOpened(false);
        getActivity().getSupportFragmentManager().popBackStack();
      }
    });
    loadingDialog.setCanceledOnTouchOutside(false);
    loadingDialog.show();
  }

  @Override
  public void hideLoadingDialog() {
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        if ((loadingDialog != null) && loadingDialog.isShowing()) {
          loadingDialog.dismiss();
          if (mainRelativeLayout != null) {
            mainRelativeLayout.setVisibility(View.VISIBLE);
            setTitle();
          }
        }
        loadingDialog = null;
      }
    }, 1000);
  }

  @Override
  public void newsDetailRetrieved(String content) {
    finishLoadedContent(content);
  }

  @Override
  public void newsDetailImage(String urlImage) {
    if (urlImage == null) return;

    PicassoTrustAll.getInstance(getContext())
        .load(urlImage)
        .into(ivNewsItemHeaderImage);
  }

  @Override
  public void closeNewsDetail() {
    getActivity().getSupportFragmentManager().popBackStack();
  }

  @Override
  public void showProfileErrorDialog() {
    new AlertDialog.Builder(getActivity())
        .setCancelable(false)
        .setTitle(getResources().getString(R.string.news_wrong_profile_title))
        .setMessage(getResources().getString(R.string.news_wrong_profile_message))
        .setPositiveButton(R.string.news_wrong_profile_button, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            loadingDialog.dismiss();
            getActivity().getSupportFragmentManager().popBackStackImmediate();
            dialog.dismiss();
          }
        })
        .show();
  }
}
