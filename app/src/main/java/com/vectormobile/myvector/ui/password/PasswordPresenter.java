package com.vectormobile.myvector.ui.password;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.base.BasePresenter;

import javax.inject.Inject;

/**
 * Created by Erik Medina on 14/12/2016.
 */
@ActivityScope
public class PasswordPresenter extends BasePresenter<PasswordView> {

  @Inject
  public PasswordPresenter(PasswordActivity activity) {
    super(activity);
  }
}
