package com.vectormobile.myvector.ui.offering.detail;

import android.text.Html;
import com.vectormobile.myvector.model.PostAbout;
import com.vectormobile.myvector.ui.base.BasePresenter;
import com.vectormobile.myvector.util.text.TextUtils;

import javax.inject.Inject;

/**
 * Created by ajruiz on 10/11/2016.
 */

public class OfferingDetailPresenter extends BasePresenter<OfferingDetailView> {

  PostAbout post;

  @Inject
  public OfferingDetailPresenter(OfferingDetailView fragment) {
    super(fragment);
  }

  public void setPost(PostAbout post) {
    this.post = post;
  }

  public String getTitlePost() {
    String title;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      title = Html.fromHtml(post.getTitle().getRendered(), Html.FROM_HTML_MODE_LEGACY).toString();
    } else {
      title = Html.fromHtml(post.getTitle().getRendered()).toString();
    }
    return TextUtils.capitalizeFirstLetter(title);
  }

  public String getAboutContent() {
    String about;
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      about = (Html.fromHtml(post.getContent().getRendered(), Html.FROM_HTML_MODE_LEGACY).toString());
    } else {
      about = (Html.fromHtml(post.getContent().getRendered()).toString());
    }
    return about;
  }

}
