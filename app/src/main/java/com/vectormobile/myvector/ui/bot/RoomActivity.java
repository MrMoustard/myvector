package com.vectormobile.myvector.ui.bot;

import com.google.gson.Gson;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import butterknife.BindView;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFactory;
import com.squareup.picasso.Picasso;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.DaggerRoomComponent;
import com.vectormobile.myvector.di.component.RoomComponent;
import com.vectormobile.myvector.di.module.RoomModule;
import com.vectormobile.myvector.domain.entity.bot.BFHistoryResponse;
import com.vectormobile.myvector.domain.entity.bot.BFResponse;
import com.vectormobile.myvector.domain.entity.bot.BFWebSocketResponse;
import com.vectormobile.myvector.domain.entity.bot.converter.MessageConverter;
import com.vectormobile.myvector.model.bot.Message;
import com.vectormobile.myvector.storage.PreferencesManager;
import com.vectormobile.myvector.ui.base.BaseActivity;
import com.vectormobile.myvector.util.connection.NaiveSSLContext;
import com.vectormobile.myvector.util.widget.CustomIncomingMessageClickListener;
import com.vectormobile.myvector.util.widget.CustomIncomingMessageViewHolder;
import timber.log.Timber;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by amarina on 14/03/2017.
 */
public class RoomActivity extends BaseActivity<RoomPresenter> implements RoomView, CustomIncomingMessageClickListener {
  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.messagesList)
  MessagesList messagesList;
  @BindView(R.id.input)
  MessageInput messageInput;
  @Inject
  RoomPresenter presenter;
  @Inject
  PreferencesManager preferencesManager;
  private RoomComponent component;
  private WebSocket webSocket;
  private MessagesListAdapter<Message> adapter;
  private MessagesListAdapter.HoldersConfig holdersConfig;
  private String username;
  private ImageLoader imageLoader = new ImageLoader() {
    @Override
    public void loadImage(ImageView imageView, String url) {
      Picasso.with(imageView.getContext())
          .load(url)
          .into(imageView);
    }
  };

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);
    username = preferencesManager.getUsername();
    holdersConfig = new MessagesListAdapter.HoldersConfig();
    holdersConfig.setIncoming(CustomIncomingMessageViewHolder.class, R.layout.item_custom_incoming_message);
    adapter = new MessagesListAdapter<>(username, holdersConfig, imageLoader);
    messagesList.setAdapter(adapter);
    messageInput.setInputListener(new MessageInput.InputListener() {
      @Override
      public boolean onSubmit(CharSequence input) {
//        Message ownMessage = new Message();
//        ownMessage.setText(input.toString());
//        ownMessage.setId(UUID.randomUUID().toString());
//        ownMessage.setCreatedAt(new Date());
//        User ownUser = new User();
//        ownUser.setId(username);
//        ownUser.setName(username);
//        ownMessage.setUser(ownUser);
//        adapter.addToStart(ownMessage, true);
        getPresenter().sendText(input);
        return true;
      }
    });
    getPresenter().startConversation();
  }

  @Override
  protected void injectModule() {
    component = DaggerRoomComponent.builder()
        .appComponent(((App) getApplication()).getComponent())
        .roomModule(new RoomModule(this))
        .build();
    component.inject(this);
  }

  @Override
  protected RoomPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.activity_room;
  }

  @Override
  public RoomComponent getComponent() {
    return component;
  }

  @Override
  protected void onPause() {
    if (webSocket != null && webSocket.isOpen()) {
      webSocket.disconnect();
    }
    super.onPause();
  }

  @Override
  public void openSocket(final String conversationId, String streamUrl) {
    try {
      webSocket = new WebSocketFactory().setSSLSocketFactory(NaiveSSLContext.getInstance("TLS").getSocketFactory()).createSocket(streamUrl, 5000);
      webSocket.setPingInterval(5 * 1000);

      Timber.d("ConversactionId: %s", conversationId);
      final Handler handler = new Handler();
      webSocket.addListener(new WebSocketAdapter() {
        @Override
        public void onConnectError(WebSocket websocket, WebSocketException exception) throws Exception {
          super.onConnectError(websocket, exception);
          Timber.e(exception, "Error de conexión %s", exception.getMessage());
        }

        @Override
        public void onError(WebSocket websocket, WebSocketException cause) throws Exception {
          Timber.e(cause, "Error desconocido %s", cause.getMessage());
        }

        @Override
        public void onConnected(WebSocket websocket, Map<String, List<String>> headers) throws Exception {
          super.onConnected(websocket, headers);
          Timber.e("Conectados <==> %s", headers);
          getPresenter().getConversationHistory();
        }

        @Override
        public void onTextMessage(WebSocket websocket, final String text) throws Exception {
          super.onTextMessage(websocket, text);
          Timber.d("Respuesta del socket: %s", text);
          if (text != null) {
            handler.post(new Runnable() {
              @Override
              public void run() {
                processWsResponse(text);
              }
            });
          }
        }
      });
      webSocket.connectAsynchronously();

    } catch (IOException e) {
      Timber.e(e, "Error creando el WebSocket: %s", e.getMessage());
//    } catch (NoSuchAlgorithmException e) {
//      Timber.e(e, "Error creando el WebSocket: %s", e.getMessage());
    } catch (NoSuchAlgorithmException e) {
      Timber.e(e, "Error al aplicar ssl: %s", e.getMessage());
    }
  }

  @Override
  public void setConversationHistory(BFHistoryResponse bfHistoryResponse) {
    List<Message> converstaionHistory = new ArrayList<>(bfHistoryResponse.getActivities().size());
    for (BFResponse bfResponse : bfHistoryResponse.getActivities()) {
      converstaionHistory.add(MessageConverter.wrap(bfResponse));
    }
    adapter.addToEnd(converstaionHistory, true);
  }

  public void processWsResponse(String text) {
    BFWebSocketResponse bfWebSocketResponse = new Gson().fromJson(text, BFWebSocketResponse.class);
    if (bfWebSocketResponse != null) {
      for (BFResponse bfResponse : bfWebSocketResponse.getActivities()) {
//        if (!bfResponse.getFrom().getId().equals(username)) {
        adapter.addToStart(MessageConverter.wrap(bfResponse), true);
//        }
      }
    }
  }

  @Override
  public void buttonClick(String value) {
    getPresenter().sendText(value);
  }
}
