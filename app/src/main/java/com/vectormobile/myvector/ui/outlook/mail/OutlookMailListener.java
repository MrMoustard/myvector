package com.vectormobile.myvector.ui.outlook.mail;

/**
 * Created by amarina on 13/03/2017.
 */

public interface OutlookMailListener {
  void onPageFinished();
}
