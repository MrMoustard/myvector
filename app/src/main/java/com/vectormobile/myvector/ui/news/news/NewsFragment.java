package com.vectormobile.myvector.ui.news.news;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import butterknife.BindView;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.NewsComponent;
import com.vectormobile.myvector.model.news.NewsItem;
import com.vectormobile.myvector.ui.base.BaseFragment;
import com.vectormobile.myvector.ui.news.NewsActivity;
import com.vectormobile.myvector.ui.news.detail.NewsDetailFragment;
import com.vectormobile.myvector.ui.news.news.adapter.NewsAdapter;
import com.vectormobile.myvector.ui.news.news.adapter.OnNewsItemClickListener;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by ajruiz on 11/11/2016.
 */

public class NewsFragment extends BaseFragment<NewsPresenter> implements NewsView, View.OnTouchListener, OnNewsItemClickListener {

  private static final int GRID_NUM_COLUMNS = 2;
  private static final int FIRST_PAGE = 1;
  @BindView(R.id.mainGridLayout)
  RelativeLayout gridLayout;
  @BindView(R.id.rv_news)
  RecyclerView rvNews;
  @Inject
  NewsPresenter presenter;
  NewsAdapter adapter;
  private ProgressDialog loadingDialog;
  private int totalWidthSpan;

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    adapter = new NewsAdapter(this, getContext(), this);
    adapter.setGroupPosition(1);
    rvNews.setAdapter(adapter);
    rvNews.setLayoutManager(new GridLayoutManager(getContext(), GRID_NUM_COLUMNS));

    GridLayoutManager manager = (GridLayoutManager) rvNews.getLayoutManager();
    totalWidthSpan = manager.getSpanCount();
    manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
      @Override
      public int getSpanSize(int position) {
        int spanSize = 1;
        if (adapter.getItemViewType(position) == 0) {
          spanSize = totalWidthSpan;
        }
        return spanSize;
      }
    });

    rvNews.setHasFixedSize(true);

    createLoadingDialog();

    presenter.getIntranetNews(FIRST_PAGE);
  }

  @Override
  protected void inject() {
    getComponent(NewsComponent.class).inject(this);
  }


  @Override
  public boolean onTouch(View view, MotionEvent motionEvent) {
    return false;
  }

  @Override
  protected NewsPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.fragment_news;
  }

  @Override
  public void fillNewsList(List<NewsItem> newsList) {
    adapter.setItems(newsList);
    hideLoadingDialog();
  }

  @Override
  public void loadNewPage(int page) {

  }

  @Override
  public void createLoadingDialog() {
    loadingDialog = new ProgressDialog(getActivity(), R.style.CustomLoadingDialogTheme);
    loadingDialog.setCancelable(true);
    loadingDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialog) {
        NewsActivity.setNewsOpened(false);
        getActivity().finish();
      }
    });
    loadingDialog.setCanceledOnTouchOutside(false);
    loadingDialog.show();
  }

  @Override
  public void hideLoadingDialog() {
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        if ((loadingDialog != null) && loadingDialog.isShowing()) {
          loadingDialog.dismiss();
        }
        loadingDialog = null;
      }
    }, 1000);
  }

  @Override
  public void onNewsItemClick(NewsItem newsItem) {
    if (!NewsActivity.isNewsOpened()) {
      NewsActivity.setNewsOpened(true);
      Bundle args = new Bundle();
      if (newsItem != null) {
        args.putString(NewsActivity.KEY_NEWS_URL_SELECTED, newsItem.getNewsUrl());
        args.putString(NewsActivity.KEY_NEWS_IMAGE_SELECTED, newsItem.getNewsItemImage());
        args.putString(NewsActivity.KEY_NEWS_TITLE_SELECTED, newsItem.getNewsItemTitle());
      }
      NewsDetailFragment newsDetailFragment = new NewsDetailFragment();
      newsDetailFragment.setArguments(args);
      getFragmentManager().beginTransaction().add(R.id.fl_news_container, newsDetailFragment)
          .addToBackStack(null).commit();
    }
  }
}
