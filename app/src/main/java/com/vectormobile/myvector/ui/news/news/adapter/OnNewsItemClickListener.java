package com.vectormobile.myvector.ui.news.news.adapter;

import com.vectormobile.myvector.model.news.NewsItem;

/**
 * Created by ajruiz on 11/11/2016.
 */

public interface OnNewsItemClickListener {
  void onNewsItemClick(NewsItem newsItem);
}
