package com.vectormobile.myvector.ui.gms.expenses.add;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.vectormobile.myvector.R;

import java.util.List;

/**
 * Created by Erik Medina on 13/02/2017.
 */

public class SpinnerAdapter extends ArrayAdapter<String> {

  private Context context;
  private List<String> idList;
  private List<String> nameList;
  private int textViewResourceId;
  private int groupViewResourceId;

  public SpinnerAdapter(Context context, int textViewResourceId, int groupViewResourceId, List<String> idList,
                        List<String> nameList) {
    super(context, textViewResourceId, idList);
    this.context = context;
    this.textViewResourceId = textViewResourceId;
    this.groupViewResourceId = groupViewResourceId;
    this.idList = idList;
    this.nameList = nameList;
  }

  @Override
  public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View row = inflater.inflate(groupViewResourceId, parent, false);
    TextView currencyId = (TextView) row.findViewById(R.id.tv_currency_id);
    currencyId.setText(idList.get(position));
    TextView currencyName = (TextView) row.findViewById(R.id.tv_currency_name);
    currencyName.setText(nameList.get(position));
    return row;
  }

  @Override
  public View getView(int position, View convertView, @NonNull ViewGroup parent) {
    return getCustomView(position, convertView, parent);
  }

  private View getCustomView(int position, View convertView, ViewGroup parent) {
    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    TextView currencyId = (TextView) inflater.inflate(textViewResourceId, parent, false);
    currencyId.setText(idList.get(position));
    return currencyId;
  }
}