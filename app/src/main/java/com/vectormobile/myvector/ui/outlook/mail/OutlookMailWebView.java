package com.vectormobile.myvector.ui.outlook.mail;

import android.content.Context;
import android.os.Build;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import javax.annotation.Nonnull;

/**
 * Created by amarina on 13/03/2017.
 */

public class OutlookMailWebView extends WebView {
  private final static String CORREO_URL = "http://correo.vectoritcgroup.com/";
  private StringBuilder javascriptFunction;
  private OutlookMailListener listener;
  private boolean firstLoadingDone = false;

  public OutlookMailWebView(Context context) {
    super(context);
    defaultSettings();
    loadUrl(CORREO_URL);
  }

  private void defaultSettings() {
    WebSettings settings = getSettings();
    settings.setJavaScriptEnabled(true);
    settings.setDomStorageEnabled(true);
    setWebViewClient(new WebViewClient() {

      @Override
      public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        if (url.contains("correo.vectoritcgroup.com") && javascriptFunction != null) {
          if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            evaluateJavascript(getLoginJavascript(), null);
          } else {
            loadUrl(getLoginJavascript());
          }
        }
        if (listener != null) {
          listener.onPageFinished();
        } else {
          firstLoadingDone = true;
        }
      }

      private String getLoginJavascript() {
        return javascriptFunction.toString();
      }

    });
  }

  public void loginOnOutlook(@Nonnull String username, @Nonnull String password) {
    javascriptFunction = new StringBuilder()
        .append("javascript:{")
        .append("document.getElementById('userNameInput').value='" + username + "@vectoritcgroup.com';")
        .append("document.getElementById('passwordInput').value='" + password + "';")
        .append("document.getElementById('submitButton').click();")
        .append("}");
    loadUrl(CORREO_URL);
  }

  public void setListener(OutlookMailListener listener) {
    this.listener = listener;
    if (firstLoadingDone && listener != null) {
      listener.onPageFinished();
    }
  }
}
