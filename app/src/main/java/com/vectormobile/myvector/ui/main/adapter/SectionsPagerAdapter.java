package com.vectormobile.myvector.ui.main.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.vectormobile.myvector.ui.menu.corporate.CorporateMenuFragment;
import com.vectormobile.myvector.ui.menu.news.NewsMenuFragment;
import com.vectormobile.myvector.ui.menu.personal.PersonalMenuFragment;


/**
 * Created by Erik Medina on 27/10/2016.
 */

public class SectionsPagerAdapter extends FragmentPagerAdapter {

  public SectionsPagerAdapter(FragmentManager fm) {
    super(fm);
  }

  @Override
  public Fragment getItem(int position) {
    switch (position) {
      case 0:
        return new PersonalMenuFragment();
      case 1:
        return new CorporateMenuFragment();
      case 2:
        return new NewsMenuFragment();
      default:
        return new PersonalMenuFragment();
    }
  }

  @Override
  public int getCount() {
    return 3;
  }

  @Override
  public CharSequence getPageTitle(int position) {
    switch (position) {
      case 0:
        break;
      case 1:
        break;
      case 2:
        break;
      default:
        break;
    }
    return null;
  }
}