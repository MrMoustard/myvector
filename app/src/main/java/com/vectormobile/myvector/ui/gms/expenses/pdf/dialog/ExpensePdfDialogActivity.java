package com.vectormobile.myvector.ui.gms.expenses.pdf.dialog;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.BuildConfig;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.DaggerExpensePdfDialogComponent;
import com.vectormobile.myvector.di.component.ExpensePdfDialogComponent;
import com.vectormobile.myvector.di.module.ExpensePdfDialogModule;
import com.vectormobile.myvector.model.gms.Expense;
import com.vectormobile.myvector.storage.ImageManager;
import com.vectormobile.myvector.ui.base.BaseActivity;
import com.vectormobile.myvector.ui.gms.expenses.pdf.dialog.adapter.AcceptedExpenseAdapter;
import com.vectormobile.myvector.util.date.DateUtils;
import com.vectormobile.myvector.util.text.TextUtils;
import com.vectormobile.myvector.util.widget.EndlessRecyclerViewScrollListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

/**
 * Created by kurt on 15/03/17.
 */

public class ExpensePdfDialogActivity extends BaseActivity<ExpensePdfDialogPresenter> implements ExpensePdfDialogView {

  public static final String EXTRA_DATE_FROM = "EXTRA_DATE_FROM";
  public static final String EXTRA_DATE_TO = "EXTRA_DATE_TO";

  private static final int FIRST_PAGE = 1;

  @Inject
  ExpensePdfDialogPresenter presenter;

  @BindView(R.id.pdf_dialog_text)
  TextView tvDialogHeader;
  @BindView(R.id.pdf_dialog_recycler)
  RecyclerView rvList;
  @BindView(R.id.pdf_dialog_no_items_text)
  TextView noItemsMessage;
  @BindView(R.id.pdf_dialog_cancel_button)
  Button cancelBtn;
  @BindView(R.id.pdf_dialog_send_button)
  Button sendBtn;
  @BindView(R.id.pdf_dialog_close_button)
  Button closeBtn;
  @BindView(R.id.pdf_dialog_pb)
  ProgressBar pbLoading;

  private ExpensePdfDialogComponent component;
  private AcceptedExpenseAdapter adapter;
  private EndlessRecyclerViewScrollListener recyclerScrollListener;

  private String dateFrom;
  private String dateTo;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setUpRecyclerView();

    dateFrom = getIntent().getExtras().getString(EXTRA_DATE_FROM);
    dateTo = getIntent().getExtras().getString(EXTRA_DATE_TO);
    presenter.getAcceptedExpenses(dateFrom, dateTo);
  }

  private void setUpRecyclerView() {
    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
    layoutManager.setAutoMeasureEnabled(true);
    rvList.setLayoutManager(layoutManager);
    rvList.setNestedScrollingEnabled(false);
    rvList.setHasFixedSize(false);
    adapter = new AcceptedExpenseAdapter();
    rvList.setAdapter(adapter);
    recyclerScrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
      @Override
      public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
        // Triggered only when new data needs to be appended to the list
        // Add whatever code is needed to append new items to the bottom of the list
        presenter.getNextPage(page);
      }
    };
    rvList.addOnScrollListener(recyclerScrollListener);
  }

  @Override
  protected void injectModule() {
    component = DaggerExpensePdfDialogComponent.builder()
        .appComponent(((App) getApplication()).getComponent())
        .expensePdfDialogModule(new ExpensePdfDialogModule(this))
        .build();
    component.inject(this);
  }

  @Override
  protected ExpensePdfDialogPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.dialog_expense_report_pdf_selector;
  }

  @Override
  public ExpensePdfDialogComponent getComponent() {
    return component;
  }

  @OnClick({R.id.pdf_dialog_cancel_button, R.id.pdf_dialog_close_button})
  public void cancelButtonHasBeenClicked() {
    finish();
  }

  @OnClick(R.id.pdf_dialog_send_button)
  public void sendButtonHasBeenClicked() {
    presenter.buildPdfAndSend(adapter.getExpenses());
  }

  @Override
  public void resetExpenses(int page) {
    if (page == FIRST_PAGE) {
      recyclerScrollListener.resetState();
      adapter.reset();
    }
  }

  @Override
  public void loadExpenses(List<Expense> expenses) {
    tvDialogHeader.setVisibility(View.VISIBLE);
    sendBtn.setVisibility(View.VISIBLE);
    cancelBtn.setVisibility(View.VISIBLE);
    adapter.addItems(expenses);
  }

  @Override
  public void showNoExpensesView(String lastDateFrom, String lastDateTo) {
    noItemsMessage.setText(String.format(
        Locale.getDefault(),
        getString(R.string.expense_pdf_dialog_no_item_text),
        TextUtils.formatStringDate(lastDateFrom, DateUtils.NO_ITEMS_GMS_DATE_FORMAT),
        TextUtils.formatStringDate(lastDateTo, DateUtils.NO_ITEMS_GMS_DATE_FORMAT)));
    tvDialogHeader.setVisibility(View.GONE);
    rvList.setVisibility(View.GONE);
    sendBtn.setVisibility(View.GONE);
    cancelBtn.setVisibility(View.GONE);
    noItemsMessage.setVisibility(View.VISIBLE);
    closeBtn.setVisibility(View.VISIBLE);
  }

  @Override
  public void showMessage(int errorId) {
    // Todo
  }

  @Override
  public void sharedPdfExpenseIds(List<String> expenseIds) {
    Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
    emailIntent.setType("text/plain");
    ArrayList<Uri> attachments = new ArrayList<>();
    attachments.add(Uri.fromFile(ImageManager.getInstance().getFileToWrite("notaGasto.pdf")));
    for (String expenseId : expenseIds) {
      if (ImageManager.getInstance().haveImage(expenseId)) {
        attachments.add(Uri.fromFile(ImageManager.getInstance().getFileToWrite(expenseId)));
      }
    }
    emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{BuildConfig.EMAIL_TO_EXPENSES_SHARED});
    emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.user_label_subject_share_pdf_expenses, dateFrom, dateTo));
    emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, attachments);
    startActivity(emailIntent);
  }

  @Override
  public void showProgressBar() {
    pbLoading.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideProgressBar() {
    pbLoading.setVisibility(View.GONE);
  }
}
