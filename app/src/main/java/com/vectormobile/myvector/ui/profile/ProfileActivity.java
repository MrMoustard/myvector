package com.vectormobile.myvector.ui.profile;

import static com.vectormobile.myvector.R.id.img_profile;
import static com.vectormobile.myvector.R.id.img_profile_editable;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.squareup.picasso.Picasso;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.DaggerProfileComponent;
import com.vectormobile.myvector.di.component.ProfileComponent;
import com.vectormobile.myvector.di.module.ProfileModule;
import com.vectormobile.myvector.model.UserAttributes;
import com.vectormobile.myvector.model.intranet.UserProfile;
import com.vectormobile.myvector.storage.ImageManager;
import com.vectormobile.myvector.ui.base.BaseActivity;
import com.vectormobile.myvector.ui.menu.ImagePickerDialogFragment;
import com.vectormobile.myvector.ui.news.news.adapter.PicassoTrustAll;
import com.vectormobile.myvector.ui.profile.adapter.OnUserAttributesClickListener;
import com.vectormobile.myvector.ui.profile.adapter.UserAdapter;
import com.vectormobile.myvector.util.text.TextUtils;
import de.hdodenhof.circleimageview.CircleImageView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.inject.Inject;

public class ProfileActivity extends BaseActivity<ProfilePresenter> implements ProfileView, OnUserAttributesClickListener {

  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.rv_profile)
  RecyclerView rvMyProfile;
  @Nullable
  @BindView(img_profile)
  CircleImageView imgProfile;
  @BindView(img_profile_editable)
  ImageView imgProfileEditable;
  @BindView(R.id.iv_action_button)
  ImageView saveButton;
  @Nullable
  @BindView(R.id.tv_field_editable)
  TextView tvEditFields;
  @BindView(R.id.rl_log_out)
  RelativeLayout rlLogOut;
  @BindView(R.id.tv_profile_user_name)
  TextView tvProfileUserName;
  @BindView(R.id.container)
  RelativeLayout rlContainer;

  @Inject
  ProfilePresenter presenter;
  @Inject
  ImageManager imageManager;

  private ProgressDialog loadingDialog;
  private ProfileComponent component;
  private UserAdapter adapter;
  private BottomSheetDialogFragment bottomSheetDialogFragment;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);

    bottomSheetDialogFragment = new ImagePickerDialogFragment();

    adapter = new UserAdapter(this, this);
    rvMyProfile.setAdapter(adapter);
    rvMyProfile.setLayoutManager(new LinearLayoutManager(this));

    presenter.getUserProfile();
  }

  private void setAvatarFromInternalStorage() {
    imgProfileEditable.setVisibility(View.INVISIBLE);
    assert imgProfile != null;
    imgProfile.setVisibility(View.VISIBLE);
    Bitmap bmp = BitmapFactory.decodeFile(presenter.getAvatarFromInternalStorage(this));
    imgProfile.setImageBitmap(bmp);
  }

  private void initBottomSheet() {
    bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
  }

  @OnClick({img_profile_editable, img_profile})
  public void editMyProfile() {
    initBottomSheet();
  }

  @OnClick(R.id.iv_action_button)
  public void onSaveButtonClicked() {
    presenter.saveButtonClicked(adapter.getCellphone(), adapter.getPhone());
  }

  @OnClick(R.id.rl_log_out)
  public void logOutClicked() {
    new AlertDialog.Builder(this)
        .setTitle(getResources().getString(R.string.profile_logout_title))
        .setMessage(getResources().getString(R.string.profile_logout_message))
        .setPositiveButton(R.string.profile_logout_close_session, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            presenter.logout();
            navigateToLogin();
            Answers.getInstance().logCustom(new CustomEvent("logout"));
          }
        })
        .setNegativeButton(R.string.profile_logout_cancel, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {

          }
        })
        .show();
  }


  @Override
  public void clearItemFocus() {
    adapter.clearAllItemsViewsFocus();
  }

  @Override
  protected void onPause() {
    super.onPause();
    if (!bottomSheetDialogFragment.isHidden() && bottomSheetDialogFragment.getFragmentManager() != null) {
      bottomSheetDialogFragment.dismiss();
    }
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    adapter.clearArrays();
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    int REQUEST_IMAGE_CAPTURE = 1;
    int REQUEST_IMAGE_GALLERY = 2;

    if (resultCode == RESULT_OK) {
      File sourceFile = null;
      if (requestCode == REQUEST_IMAGE_CAPTURE) {
        sourceFile = new File(ImageManager.getInstance().getLastTempFile());

      } else if (requestCode == REQUEST_IMAGE_GALLERY) {
        String url = data.getData().toString();
        if (url.startsWith(TextUtils.Intranet.GOOGLE_PHOTOS_URL)) {
          InputStream is = null;
          try {
            is = getContentResolver().openInputStream(Uri.parse(data.getData().toString()));
            Bitmap bitmap = BitmapFactory.decodeStream(is);
            sourceFile = (imageManager.convertBitmapToFile(this, bitmap));
          } catch (IOException e) {
            e.printStackTrace();
          } finally {
            try {
              assert is != null;
              is.close();
            } catch (IOException e) {
              e.printStackTrace();
            }
          }
        } else {
          sourceFile = new File(imageManager.getImagePathFromGallery(this, data.getData()));
        }
      }
      if (sourceFile != null) {
        imgProfileEditable.setVisibility(View.INVISIBLE);
        assert imgProfile != null;
        imgProfile.setVisibility(View.VISIBLE);
        Picasso.with(this)
            .load(sourceFile)
            .into(imgProfile);
        presenter.sendImage(sourceFile);
      }
    }
  }

  @Override
  protected void injectModule() {
    component = DaggerProfileComponent.builder()
        .appComponent(((App) getApplication()).getComponent())
        .profileModule(new ProfileModule(this))
        .build();
    component.inject(this);
  }

  @Override
  protected ProfilePresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.activity_profile;
  }

  @Override
  public ProfileComponent getComponent() {
    return component;
  }

  @Override
  public void onItemClick(UserAttributes item, int position) {
    presenter.editButtonClicked();
  }

  @Override
  public void onKeyboardActionDoneClicked() {
    presenter.keyboardActionDoneClicked(adapter.getCellphone(), adapter.getPhone());
  }

  @Override
  public void showSaveButton() {
    saveButton.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideSaveButton() {
    saveButton.setVisibility(View.INVISIBLE);
  }

  @Override
  public void userDataRetrieved(UserProfile userProfile) {
    tvProfileUserName.setText(userProfile.getName());
    adapter.setItems(presenter.setUserAttributes(userProfile));
    if (presenter.loadAvatarFromIntranet(userProfile.getAvatarUrl())) {
      setAvatarFromIntranet(userProfile.getAvatarUrl());
    }
  }

  private void setAvatarFromIntranet(String url) {
    PicassoTrustAll.getInstance(this)
        .load(url)
        .placeholder(ContextCompat.getDrawable(this, R.drawable.placeholder_profile_normal))
        .error(ContextCompat.getDrawable(this, R.drawable.placeholder_profile_normal))
        .into(imgProfile);
    imgProfileEditable.setVisibility(View.INVISIBLE);
    assert imgProfile != null;
    imgProfile.setVisibility(View.VISIBLE);
  }

  @Override
  public void showProgressBar() {
    loadingDialog = new ProgressDialog(this, R.style.CustomLoadingDialogTheme);
    loadingDialog.setCancelable(true);
    loadingDialog.show();
  }

  @Override
  public void hideProgressBar() {
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        if ((loadingDialog != null) && loadingDialog.isShowing()) {
          loadingDialog.dismiss();
        }
        loadingDialog = null;
      }
    }, 1000);
  }

  @Override
  public void showMessage(int messageId) {
    View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
    Snackbar.make(rootView, messageId, Snackbar.LENGTH_SHORT).show();
  }

  @Override
  public void showMessageAction(int messageId) {
    View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
    Snackbar.make(rootView, messageId, Snackbar.LENGTH_INDEFINITE)
        .setAction(getString(R.string.error_retry), new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            presenter.getUserProfile();
          }
        })
        .show();
  }

  @Override
  public void showInvalidProfileDialog() {
    new AlertDialog.Builder(this)
        .setTitle(getResources().getString(R.string.profile_wrong_profile_title))
        .setMessage(getResources().getString(R.string.profile_wrong_profile_message))
        .setPositiveButton(R.string.profile_wrong_profile_button, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
          }
        })
        .show();
  }

  @Override
  public void setFocus(int position) {
    adapter.setFocus(position);
  }

  @Override
  public void showProfile() {
    rlContainer.setVisibility(View.VISIBLE);
  }

}
