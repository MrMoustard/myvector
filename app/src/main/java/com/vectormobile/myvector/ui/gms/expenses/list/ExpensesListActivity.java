package com.vectormobile.myvector.ui.gms.expenses.list;

import static com.vectormobile.myvector.ui.gms.expenses.add.AddExpenseActivity.EXTRA_DETAILS_EXPENSE_DATE;
import static com.vectormobile.myvector.ui.gms.expenses.add.AddExpenseActivity.EXTRA_DETAILS_ITEM_COUNTER;
import static com.vectormobile.myvector.ui.gms.expenses.add.AddExpenseActivity.EXTRA_DETAILS_ITEM_ID;
import static com.vectormobile.myvector.ui.gms.expenses.add.AddExpenseActivity.OPERATION_FINALIZED;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.bignerdranch.android.multiselector.MultiSelector;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.DaggerExpensesListComponent;
import com.vectormobile.myvector.di.component.ExpensesListComponent;
import com.vectormobile.myvector.di.module.ExpensesListModule;
import com.vectormobile.myvector.domain.entity.gms.response.GmsDayStateResult;
import com.vectormobile.myvector.model.gms.Expense;
import com.vectormobile.myvector.ui.base.BaseActivity;
import com.vectormobile.myvector.ui.gms.expenses.add.AddExpenseActivity;
import com.vectormobile.myvector.ui.gms.expenses.detail.ExpenseDetailActivity;
import com.vectormobile.myvector.ui.gms.expenses.list.adapter.ExpenseAdapter;
import com.vectormobile.myvector.ui.gms.expenses.list.adapter.OnItemClickListener;
import com.vectormobile.myvector.ui.gms.expenses.pdf.ExpensePdfActivity;
import com.vectormobile.myvector.util.date.DateUtils;
import com.vectormobile.myvector.util.text.TextUtils;
import com.vectormobile.myvector.util.widget.EndlessRecyclerViewScrollListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by kurt on 05/12/16.
 */

public class ExpensesListActivity extends BaseActivity<ExpensesListPresenter>
    implements ExpensesListView, OnItemClickListener<Expense> {

  public static final int UPDATE_EXPENSE_REQUEST = 2;
  public static final int DETAIL_EXPENSE_REQUEST = 3;
  public static final String REMOVED_EXPENSE = "removed";
  private static final int ADD_EXPENSE_REQUEST = 1;
  private static final String UPDATED_EXPENSE = "updated";
  private static final String SENT_EXPENSE = "sent";
  private static final int FIRST_PAGE = 1;

  @BindView(R.id.cl_imputation_list)
  CoordinatorLayout clImputationList;
  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.ly_toolbar)
  LinearLayout secondToolbar;
  @BindView(R.id.b_action_mode_remove)
  ImageView bActionModeRemove;
  @BindView(R.id.toolbar_action_mode)
  LinearLayout toolbarActionMode;
  @BindView(R.id.tv_section_title)
  TextView sectionTitle;
  @BindView(R.id.mini_calendar_view)
  CompactCalendarView calendarView;
  @BindView(R.id.rv_imputations_list)
  RecyclerView rvList;
  @BindView(R.id.rl_tutorial_message)
  RelativeLayout tutorialLayout;
  @BindView(R.id.fab_add_item)
  FloatingActionButton fab;
  @BindView(R.id.pb_massive_loading)
  ProgressBar pbMassiveLoading;

  @BindView(R.id.no_items)
  LinearLayout noItemsLayout;
  @BindView(R.id.no_items_text)
  TextView noItemsMessage;
  @BindView(R.id.recent_items)
  Button lastItemsButton;

  @Inject
  ExpensesListPresenter presenter;

  String date = null;
  private ExpensesListComponent component;
  private List<Expense> expenses = new ArrayList<>();
  private List<String> expenseIdList;
  private Map<String, Event> miniCalendarEventsMap = new HashMap<>();
  private boolean isMiniCalendarOpen = false;
  private EndlessRecyclerViewScrollListener recyclerScrollListener;
  private ExpenseAdapter expenseAdapter;
  private boolean multiSelectionMode = false;
  private MultiSelector multiSelector = new MultiSelector();
  private ProgressDialog loadingDialog;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setUpActionBar();
    setUpMiniCalendarView();
    setUpRecyclerView();

    sectionTitle.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        presenter.monthTitleHasBeenClicked(sectionTitle.getText().toString());
      }
    });
    presenter.getCurrentMonthExpenses();
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    String action;
    if (resultCode == Activity.RESULT_OK) {
      if (requestCode == ADD_EXPENSE_REQUEST) {
        date = data.getExtras().getString(OPERATION_FINALIZED);
        presenter.onExpenseSaved();
        Answers.getInstance().logCustom(
            new CustomEvent("SaveExpense")
                .putCustomAttribute("action", "created")
                .putCustomAttribute("success", "true")
        );
      } else if (requestCode == UPDATE_EXPENSE_REQUEST) {
        action = data.getExtras().getString(OPERATION_FINALIZED);
        date = data.getExtras().getString(EXTRA_DETAILS_EXPENSE_DATE);
        int itemCounter = data.getExtras().getInt(EXTRA_DETAILS_ITEM_COUNTER);
        if (action != null) {
          if (action.equals(UPDATED_EXPENSE)) {
            Answers.getInstance().logCustom(
                new CustomEvent("SaveExpense")
                    .putCustomAttribute("action", "updated")
                    .putCustomAttribute("success", "true")
            );
            presenter.onExpenseUpdated();
          } else if (action.equals(SENT_EXPENSE)) {
            Answers.getInstance().logCustom(
                new CustomEvent("SaveExpense")
                    .putCustomAttribute("action", "sent")
                    .putCustomAttribute("success", "true")
            );
            presenter.onExpenseSent(itemCounter);
          } else if (action.equals(REMOVED_EXPENSE)) {
            Answers.getInstance().logCustom(
                new CustomEvent("SaveExpense")
                    .putCustomAttribute("action", "removed")
                    .putCustomAttribute("success", "true")
            );
            presenter.onExpenseRemoved(itemCounter);
          } else if (action.equals(AddExpenseActivity.DUPLICATE)) {
            date = DateUtils.formatCalendarDateToGmsShortDate(date);
            presenter.onExpenseDuplicated();
          }
        }
      } else if (requestCode == DETAIL_EXPENSE_REQUEST) {
        action = data.getExtras().getString(OPERATION_FINALIZED);
        date = data.getExtras().getString(EXTRA_DETAILS_EXPENSE_DATE);
        if (action != null) {
          if (action.equals(UPDATED_EXPENSE)) {
            Answers.getInstance().logCustom(
                new CustomEvent("SaveExpense")
                    .putCustomAttribute("action", "updated")
                    .putCustomAttribute("success", "true")
            );
            presenter.onExpenseUpdated();
          } else if (action.equals(REMOVED_EXPENSE)) {
            Answers.getInstance().logCustom(
                new CustomEvent("SaveExpense")
                    .putCustomAttribute("action", "removed")
                    .putCustomAttribute("success", "true")
            );
            String itemId = data.getExtras().getString(EXTRA_DETAILS_ITEM_ID);
            presenter.removeExpenses(Collections.singletonList(itemId));
          } else if (action.equals(AddExpenseActivity.DUPLICATE_ACTION)) {
            presenter.onExpenseDuplicated();
          }
        }
      }
      presenter.getWeekExpenses(date);
    }
  }

  private void setUpActionBar() {
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);
  }

  private void setUpMiniCalendarView() {
    // show days from other months as greyed out days
    calendarView.displayOtherMonthDays(false);
    calendarView.shouldDrawIndicatorsBelowSelectedDays(true);
    calendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
      @Override
      public void onDayClick(Date dateClicked) {
        presenter.onMiniCalendarDayClick(dateClicked, calendarView.getEvents(dateClicked));
      }

      @Override
      public void onMonthScroll(Date firstDayOfNewMonth) {
        sectionTitle.setText(setMonthNameTitle(firstDayOfNewMonth));
        presenter.getSelectedMonthExpenses(firstDayOfNewMonth);
        //presenter.getSelectedDaysState(firstDayOfNewMonth);
      }
    });
  }

  private void setUpRecyclerView() {
    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
    layoutManager.setAutoMeasureEnabled(true);
    rvList.setLayoutManager(layoutManager);
    rvList.setNestedScrollingEnabled(false);
    rvList.setHasFixedSize(false);
    expenseAdapter = new ExpenseAdapter(this, this);
    expenseAdapter.setMultiSelector(multiSelector);
    rvList.setAdapter(expenseAdapter);
    recyclerScrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
      @Override
      public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
        // Triggered only when new data needs to be appended to the list
        // Add whatever code is needed to append new items to the bottom of the list
        presenter.getNextPageExpenses(page);
      }
    };
    rvList.addOnScrollListener(recyclerScrollListener);
  }

  @Override
  public void onBackPressed() {
    if (multiSelectionMode) {
      finishMultiSelectionMode();
    } else {
      super.onBackPressed();
    }
  }

  @Override
  public void loadExpenses() {
    expenseAdapter.addItems(expenses);
    noItemsLayout.setVisibility(View.GONE);
    rvList.setVisibility(View.VISIBLE);
    presenter.showTutorial();
  }

  @Override
  public void loadMiniCalendarEventsArray() {
    miniCalendarEventsMap.putAll(buildEventsHashMap());
    calendarView.removeAllEvents();

    populateMiniCalendarEventByState();
    calendarView.invalidate();
  }

  private Map<String, Event> buildEventsHashMap() {
    Map<String, Event> eventHashMap = new HashMap<>();
    for (Expense expense : expenses) {
      eventHashMap.put(expense.getId(), new Event(expense.getColour(), expense.getDateFrom().getTime(), expense));
    }
    return eventHashMap;
  }

  private void populateMiniCalendarEventByState() {
    Map<String, Event> saved = new HashMap<>();
    Map<String, Event> sent = new HashMap<>();
    Map<String, Event> approved = new HashMap<>();
    Map<String, Event> rejected = new HashMap<>();

    for (Map.Entry<String, Event> entry : miniCalendarEventsMap.entrySet()) {
      if (Expense.EXPENSE_SAVED.equals(((Expense) entry.getValue().getData()).getState())) {
        saved.put(((Expense) entry.getValue().getData()).getDateString(), entry.getValue());
      } else if (Expense.EXPENSE_SENT.equals(((Expense) entry.getValue().getData()).getState())) {
        sent.put(((Expense) entry.getValue().getData()).getDateString(), entry.getValue());
      } else if (Expense.EXPENSE_APPROVED.equals(((Expense) entry.getValue().getData()).getState())) {
        approved.put(((Expense) entry.getValue().getData()).getDateString(), entry.getValue());
      } else if (Expense.EXPENSE_REJECTED.equals(((Expense) entry.getValue().getData()).getState())) {
        rejected.put(((Expense) entry.getValue().getData()).getDateString(), entry.getValue());
      }
    }

    calendarView.addEvents(new ArrayList<>(rejected.values()));
    calendarView.addEvents(new ArrayList<>(saved.values()));
    calendarView.addEvents(new ArrayList<>(sent.values()));
    calendarView.addEvents(new ArrayList<>(approved.values()));
  }

  @Override
  public void decorateMiniCalendar(GmsDayStateResult dayStateResult) {
//    for (Day item : dayStateResult.getDays()) {
//      Calendar calendar = Calendar.getInstance();
//      calendar.setTimeInMillis(item.getDate());
//      if (weekendsDecorator.shouldDecorate(CalendarDay.from(calendar))) {
//        weekendsDecorator.decorate();
//      }
//    }
  }

  @Override
  protected void injectModule() {
    component = DaggerExpensesListComponent.builder()
        .appComponent(((App) getApplication()).getComponent())
        .expensesListModule(new ExpensesListModule(this))
        .build();
    component.inject(this);
  }

  @Override
  protected ExpensesListPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.activity_imputation_list;
  }

  @Override
  public ExpensesListComponent getComponent() {
    return component;
  }

  @OnClick(R.id.img_mini_calendar)
  public void miniCalendarButtonHasBeenClicked(View view) {
    expenseAdapter.closeSwipedItems();
    presenter.miniCalendarButtonHasBeenClicked(isMiniCalendarOpen);
  }

  @OnClick(R.id.img_toolbar_pdf)
  public void buildPdfReportButtonHasBeenClicked() {
    Intent iExpensePdf = new Intent(this, ExpensePdfActivity.class);
    Answers.getInstance().logCustom(new CustomEvent("OpenSentPdfActivity"));
    startActivity(iExpensePdf);
  }

  @Override
  public void showMiniCalendarView() {
    isMiniCalendarOpen = true;
    calendarView.setVisibility(View.VISIBLE);
    calendarView.showCalendar();
    sectionTitle.setText(setMonthNameTitle(calendarView.getFirstDayOfCurrentMonth()));
  }

  @Override
  public void hideMiniCalendarView() {
    if (isMiniCalendarOpen) {
      isMiniCalendarOpen = false;
      calendarView.hideCalendar();
      sectionTitle.setText(getString(R.string.expense_list_toolbar_title));
    }
  }

  private String setMonthNameTitle(Date date) {
    SimpleDateFormat formatter = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());
    return TextUtils.capitalizeFirstLetter(formatter.format(date));
  }

  @Override
  public void showTutorial() {
    tutorialLayout.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideTutorial() {
    tutorialLayout.setVisibility(View.GONE);
  }

  @Override
  public void showMassiveLoading() {
    pbMassiveLoading.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideMassiveLoading() {
    pbMassiveLoading.setVisibility(View.GONE);
  }

  @Override
  public void showNoExpensesView(String dateFrom, String dateTo, int timeLapse) {
    if (timeLapse == ExpensesListPresenter.TODAY_TIME_LAPSE) {
      noItemsMessage.setText(String.format(
          Locale.getDefault(),
          getString(R.string.expense_no_items_today_text),
          TextUtils.formatStringDate(dateFrom, DateUtils.NO_ITEMS_GMS_DATE_FORMAT)));
    } else {
      noItemsMessage.setText(String.format(
          Locale.getDefault(),
          getString((timeLapse == ExpensesListPresenter.WEEK_TIME_LAPSE) ?
              R.string.expense_no_items_week_text : R.string.expense_no_items_month_text),
          TextUtils.formatStringDate(dateFrom, DateUtils.NO_ITEMS_GMS_DATE_FORMAT),
          TextUtils.formatStringDate(dateTo, DateUtils.NO_ITEMS_GMS_DATE_FORMAT)));
    }
    noItemsLayout.setVisibility(View.VISIBLE);
    rvList.setVisibility(View.GONE);
  }

  @Override
  public void setExpensesList(List<Expense> expenses) {
    this.expenses = expenses;
  }

  @Override
  public void resetExpenses(int page) {
    if (page == FIRST_PAGE) {
      recyclerScrollListener.resetState();
      expenseAdapter.reset();
      rvList.getAdapter().notifyDataSetChanged();
    }
  }

  @Override
  public void goToAddExpense() {
    hideMiniCalendarView();
    Intent intent = new Intent(this, AddExpenseActivity.class);
    intent.putExtra(AddExpenseActivity.EXTRA_ACTION, AddExpenseActivity.CREATE_ACTION);
    startActivityForResult(intent, ADD_EXPENSE_REQUEST);
  }

  @OnClick(R.id.iv_close_message)
  public void onCloseTutorialButtonHasBeenClicked() {
    presenter.closeTutorial();
  }

  @OnClick(R.id.recent_items)
  public void onRecentItemsButtonHasBeenClicked() {
    presenter.getCurrentMonthExpenses();
  }

  @Override
  public void onItemClick(Expense expense) {
    if (expense.getState().equals(Expense.EXPENSE_SENT) ||
        expense.getState().equals(Expense.EXPENSE_APPROVED) ||
        expense.getState().equals(Expense.EXPENSE_REJECTED)) {
      Intent iExpenseDetail = new Intent(this, ExpenseDetailActivity.class);
      iExpenseDetail.putExtra(AddExpenseActivity.EXTRA_DETAILS_EXPENSE, expense);
      startActivityForResult(iExpenseDetail, DETAIL_EXPENSE_REQUEST);
    } else {
      Intent iAddExpense = new Intent(this, AddExpenseActivity.class);
      iAddExpense.putExtra(AddExpenseActivity.EXTRA_ACTION, AddExpenseActivity.UPDATE_ACTION);
      iAddExpense.putExtra(AddExpenseActivity.EXTRA_DETAILS_EXPENSE, expense);
      startActivityForResult(iAddExpense, UPDATE_EXPENSE_REQUEST);
    }
  }

  public void sendExpense(String expenseId) {
    List<String> idList = new ArrayList<>();
    idList.add(expenseId);
    presenter.sendExpenses(idList);
  }

  public void removeExpense(String expenseId) {
    List<String> idList = new ArrayList<>();
    idList.add(expenseId);
    presenter.removeExpenses(idList);
  }

  @OnClick(R.id.fab_add_item)
  public void addItemButtonHasBeenClicked(View view) {
    presenter.fabClicked(multiSelectionMode, multiSelector, expenseAdapter);
  }

  @Override
  public void refreshList() {
    if (date != null) {
      presenter.getWeekExpenses(date);
    } else {
      presenter.getCurrentMonthExpenses();
    }
  }

  @Override
  public void removeAdapterItems() {
    expenseAdapter.removeItems(multiSelector.getSelectedPositions());
  }

  @Override
  public void setAdapterItemsSent() {
    expenseAdapter.setItemsSent(multiSelector.getSelectedPositions());
  }

  @Override
  public void showMessage(int messageId) {
    showMessage(getString(messageId));
  }

  @Override
  public void showMessage(String message) {
    Snackbar.make(clImputationList, message, Snackbar.LENGTH_SHORT).show();
  }

  @Override
  public void showProgressBar() {
    rvList.setVisibility(View.GONE);
    loadingDialog = new ProgressDialog(this, R.style.CustomLoadingDialogTheme);
    loadingDialog.setCancelable(true);
    loadingDialog.show();
  }

  @Override
  public void hideProgressBar() {
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        if ((loadingDialog != null) && loadingDialog.isShowing()) {
          loadingDialog.dismiss();
        }
        loadingDialog = null;
      }
    }, 1000);
    rvList.setVisibility(View.VISIBLE);
  }

  @OnClick(R.id.b_action_mode_remove)
  public void onRemoveExpensesButtonClicked() {
    expenseIdList = new ArrayList<>();
    for (int position : multiSelector.getSelectedPositions()) {
      expenseIdList.add(expenseAdapter.getItemsToShow().get(position).getId());
    }
    presenter.removeExpenses(expenseIdList);
  }

  public void initializeMultiSelectionMode() {
    multiSelectionMode = true;
    multiSelector.setSelectable(true);
    toolbarActionMode.setVisibility(View.VISIBLE);
    secondToolbar.setVisibility(View.GONE);
    fab.setImageDrawable(ContextCompat.getDrawable(ExpensesListActivity.this, R.drawable.ic_send));
  }

  @Override
  public void finishMultiSelectionMode() {
    expenseAdapter.enableSwipe();
    multiSelectionMode = false;
    multiSelector.clearSelections();
    multiSelector.setSelectable(false);
    toolbarActionMode.setVisibility(View.GONE);
    secondToolbar.setVisibility(View.VISIBLE);
    fab.setImageDrawable(ContextCompat.getDrawable(ExpensesListActivity.this, R.drawable.ic_add));
  }
}
