package com.vectormobile.myvector.ui.menu.news;

import com.vectormobile.myvector.ui.base.BaseMenuPresenter;

/**
 * Created by ajruiz on 10/11/2016.
 */
public class NewsMenuPresenter extends BaseMenuPresenter<NewsMenuFragment> {
  public NewsMenuPresenter(NewsMenuFragment fragment) {
    super(fragment);
  }
}
