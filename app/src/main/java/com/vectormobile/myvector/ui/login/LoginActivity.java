package com.vectormobile.myvector.ui.login;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.DaggerLoginComponent;
import com.vectormobile.myvector.di.component.LoginComponent;
import com.vectormobile.myvector.di.module.LoginModule;
import com.vectormobile.myvector.ui.base.BaseActivity;
import com.vectormobile.myvector.ui.main.MainActivity;
import com.vectormobile.myvector.ui.rocketchat.RocketWebView;
import com.vectormobile.myvector.ui.outlook.mail.OutlookMailWebView;
import com.vectormobile.myvector.util.text.TextUtils;

import javax.inject.Inject;

/**
 * Created by Erik Medina on 27/10/2016.
 */
public class LoginActivity extends BaseActivity<LoginPresenter>
    implements LoginView {

  @BindView(R.id.et_email_login)
  EditText etEmailLogin;
  @BindView(R.id.et_password_login)
  EditText etPasswordLogin;
  @BindView(R.id.b_login)
  Button bLogin;
  @BindView(R.id.pb_login)
  ProgressBar pbLogin;

  @Inject
  LoginPresenter presenter;
  @Inject
  RocketWebView rocketWebView;
  @Inject
  OutlookMailWebView outlookSectionWebView;

  private LoginComponent component;

  @Override
  public void onResume() {
    super.onResume();
    if (presenter.needLogin()) {
      setInitialsValues();
    } else {
      goToMainActivity();
    }
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
  }

  private void setInitialsValues() {
    if (etEmailLogin.getText().toString().isEmpty()) {
      etEmailLogin.setText(presenter.getSavedUsername());
    }

    etPasswordLogin.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_GO) {
          onLoginButtonClicked();
          return true;
        }
        return false;
      }
    });
  }

  @Override
  public void onPause() {
    super.onPause();
  }

  @Override
  protected void injectModule() {
    component = DaggerLoginComponent.builder()
        .appComponent(((App) getApplication()).getComponent())
        .loginModule(new LoginModule(this))
        .build();
    component.inject(this);
  }

  @Override
  protected LoginPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.activity_login;
  }

  @Override
  public LoginComponent getComponent() {
    return component;
  }

  @OnClick(R.id.b_login)
  public void onLoginButtonClicked() {
    presenter.sendLogin(etEmailLogin.getText().toString(), etPasswordLogin.getText().toString());
  }

  @Override
  public void goToMainActivity() {
    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
    startActivity(intent);
  }

  @Override
  public void showProgressBar(boolean isShowed) {
    if (isShowed) {
      pbLogin.setVisibility(View.VISIBLE);
    } else {
      if (pbLogin != null)
        pbLogin.setVisibility(View.GONE);
    }
  }

  @Override
  public void enableLoginButton() {
    bLogin.setEnabled(true);
    bLogin.animate()
        .scaleX(1F)
        .scaleY(1F)
        .start();
  }

  @Override
  public void showMessage(int messageId) {
    View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
    Snackbar.make(rootView, messageId, Snackbar.LENGTH_SHORT).show();
  }

  @Override
  public void hideBtnLogin() {
    bLogin.setEnabled(false);
    bLogin.animate()
        .scaleX(0F)
        .scaleY(0F)
        .start();
  }

  @Override
  public void showLoginError() {
    showError(R.string.error_login_failure);
  }

  @Override
  public void hideKeyboard() {
    TextUtils.hideKeyboard(this, etPasswordLogin);
  }

  @Override
  public void loginOnEmbeddedWebViews(String username, String password) {
    rocketWebView.loginOnRocketchat(username, password);
    outlookSectionWebView.loginOnOutlook(username, password);
  }
}
