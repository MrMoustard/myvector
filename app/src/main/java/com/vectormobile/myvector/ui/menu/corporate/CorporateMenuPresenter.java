package com.vectormobile.myvector.ui.menu.corporate;

import com.vectormobile.myvector.ui.base.BaseMenuPresenter;

import javax.inject.Inject;

/**
 * Created by amarina on 08/11/2016.
 */
public class CorporateMenuPresenter extends BaseMenuPresenter<CorporateMenuFragment> {
  @Inject
  public CorporateMenuPresenter(CorporateMenuFragment fragment) {
    super(fragment);
  }
}
