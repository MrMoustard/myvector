package com.vectormobile.myvector.ui.menu.news;

import android.support.v7.widget.RecyclerView;
import android.widget.RelativeLayout;
import butterknife.BindView;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.MainComponent;
import com.vectormobile.myvector.model.VectorItemMenu;
import com.vectormobile.myvector.model.menuaction.VectorAction;
import com.vectormobile.myvector.ui.base.BaseMenuFragment;

import java.util.ArrayList;

/**
 * Created by ajruiz on 10/11/2016.
 */

public class NewsMenuFragment extends BaseMenuFragment<NewsMenuPresenter> implements NewsMenuView {
  @BindView(R.id.rv_news_menu)
  RecyclerView rvNews;
  @BindView(R.id.fragment_container)
  RelativeLayout rlContainer;

  private NewsMenuPresenter presenter;

  public NewsMenuFragment() {
    this.presenter = new NewsMenuPresenter(this);
  }

  @Override
  protected void loadItems() {
    items = new ArrayList<>();
    items.add(new VectorItemMenu(getString(R.string.menu_item_lynda), R.drawable.menu_bg_lynda, VectorAction.OPEN_LYNDA));
    items.add(new VectorItemMenu(getString(R.string.menu_item_news), R.drawable.menu_bg_news, VectorAction.OPEN_CORPORATE_NEWS));
    items.add(new VectorItemMenu(getString(R.string.menu_item_offering), R.drawable.menu_bg_offering, VectorAction.OPEN_OFFERING));
    items.add(new VectorItemMenu(getString(R.string.menu_item_radar), R.drawable.menu_bg_radar, VectorAction.OPEN_RADAR));

  }

  @Override
  protected RelativeLayout getRelativeLayoutContainer() {
    return rlContainer;
  }

  @Override
  public RecyclerView getRecyclerView() {
    return rvNews;
  }

  @Override
  protected void inject() {
    getComponent(MainComponent.class).inject(this);
  }

  @Override
  protected NewsMenuPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.fragment_news_menu;
  }
}
