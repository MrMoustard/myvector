package com.vectormobile.myvector.ui.gms.hour.list;

import static com.vectormobile.myvector.ui.gms.expenses.list.ExpensesListPresenter.MONTH_BOUND_ENDS;
import static com.vectormobile.myvector.ui.gms.expenses.list.ExpensesListPresenter.MONTH_BOUND_STARTS;
import static com.vectormobile.myvector.ui.gms.expenses.list.ExpensesListPresenter.MONTH_TIME_LAPSE;
import static com.vectormobile.myvector.ui.gms.expenses.list.ExpensesListPresenter.TODAY_TIME_LAPSE;
import static com.vectormobile.myvector.ui.gms.expenses.list.ExpensesListPresenter.WEEK_TIME_LAPSE;

import com.bignerdranch.android.multiselector.MultiSelector;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.domain.entity.gms.request.GmsRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsTimeLapseHourBodyRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.entity.gms.response.Record;
import com.vectormobile.myvector.domain.entity.gms.response.Result;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsHourRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsRequestInteractor;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.model.gms.Hour;
import com.vectormobile.myvector.storage.PreferencesManager;
import com.vectormobile.myvector.ui.base.BasePresenter;
import com.vectormobile.myvector.ui.gms.hour.list.adapter.HourAdapter;
import com.vectormobile.myvector.util.date.DateUtils;
import com.vectormobile.myvector.util.AppResources;
import com.vectormobile.myvector.util.text.TextUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

/**
 * Created by ajruiz on 24/01/2017.
 */

public class HoursListPresenter extends BasePresenter<HoursListView> {

  private static final int FIRST_ITEM = 0;

  private BuildGmsHourRequestInteractor interactor;
  private BuildGmsRequestInteractor buildGmsRequestInteractor;
  private PreferencesManager preferencesManager;
  private AppResources appResources;

  private String lastDateFrom;
  private String lastDateTo;
  private int lastTimeLapse;
  private int lastPage;

  @Inject
  public HoursListPresenter(HoursListActivity activity, BuildGmsHourRequestInteractor interactor,
                            BuildGmsRequestInteractor buildGmsRequestInteractor,
                            PreferencesManager preferencesManager,
                            AppResources appResources) {

    super(activity);
    this.interactor = interactor;
    this.buildGmsRequestInteractor = buildGmsRequestInteractor;
    this.preferencesManager = preferencesManager;
    this.appResources = appResources;
  }

  public void miniCalendarButtonHasBeenClicked(boolean isMiniCalendarOpen) {
    if (getView() != null) {
      if (!isMiniCalendarOpen) {
        getView().showMiniCalendarView();
      } else {
        getView().hideMiniCalendarView();
      }
    }
  }

  public void onMiniCalendarDayClick(Date dateClicked, List<Event> events) {
    if (isToday(dateClicked)) {
      getTodayImputations();
    } else {
      view.showProgressBar();
      List<String> weekBounds = buildWeekBounds(dateClicked);
      getTimeLapseImputations(
          weekBounds.get(DateUtils.WEEK_BOUND_STARTS),
          weekBounds.get(DateUtils.WEEK_BOUND_ENDS),
          WEEK_TIME_LAPSE);
    }
    view.hideMiniCalendarView();
  }

  public void monthTitleHasBeenClicked(String month) {
    if (!month.equalsIgnoreCase(appResources.getString(R.string.menu_item_time_imputation))) {
      view.hideMiniCalendarView();
      view.showProgressBar();
      List<String> monthBounds = DateUtils.buildMonthBounds(month);
      getTimeLapseImputations(
          monthBounds.get(MONTH_BOUND_STARTS),
          monthBounds.get(MONTH_BOUND_ENDS),
          MONTH_TIME_LAPSE);
    }
  }

  private boolean isToday(Date date) {
    return DateUtils.formatDateToCompactString(date, DateUtils.NO_ITEMS_GMS_DATE_FORMAT)
        .equals(DateUtils.formatDateToCompactString(new Date(), DateUtils.NO_ITEMS_GMS_DATE_FORMAT));
  }

  private List<String> buildWeekBounds(Date date) {
    return DateUtils.getStringWeekBounds(date, DateUtils.GMS_SHORT_DATE_FORMAT);
  }

  public void getTodayImputations() {
    view.showProgressBar();
    String today = DateUtils.formatDateToCompactString(new Date(), DateUtils.GMS_SHORT_DATE_FORMAT);
    getTimeLapseImputations(today, today, TODAY_TIME_LAPSE);
  }

  public void getWeekImputations(String stringDate) {
    view.showProgressBar();
    Date date = DateUtils.parseStringToDate(stringDate, DateUtils.GMS_SHORT_DATE_FORMAT);
    if (date != null) {
      List<String> weekBounds = buildWeekBounds(date);
      getTimeLapseImputations(
          weekBounds.get(DateUtils.WEEK_BOUND_STARTS),
          weekBounds.get(DateUtils.WEEK_BOUND_ENDS),
          WEEK_TIME_LAPSE);
    }
  }

  public void getCurrentMonthImputations() {
    view.showProgressBar();
    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.MONTH_DATE_FORMAT, Locale.getDefault());
    String month = sdf.format(calendar.getTime());
    List<String> monthBounds = DateUtils.buildMonthBounds(month);
    getTimeLapseImputations(
        monthBounds.get(MONTH_BOUND_STARTS),
        monthBounds.get(MONTH_BOUND_ENDS),
        MONTH_TIME_LAPSE);
  }

  public void getSelectedMonthImputations(Date firstDayOfNewMonth) {
    view.showProgressBar();
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(firstDayOfNewMonth);
    SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.MONTH_DATE_FORMAT, Locale.getDefault());
    String month = sdf.format(calendar.getTime());
    List<String> monthBounds = DateUtils.buildMonthBounds(month);
    getTimeLapseImputations(
        monthBounds.get(MONTH_BOUND_STARTS),
        monthBounds.get(MONTH_BOUND_ENDS),
        MONTH_TIME_LAPSE);
  }

  private void getTimeLapseImputations(final String dateFrom, final String dateTo, final int timeLapse) {
    if (!dateFrom.equals(lastDateFrom) || !dateTo.equals(lastDateTo)) {
      view.resetImputations(lastPage);
      lastPage = 1;
      lastDateFrom = dateFrom;
      lastDateTo = dateTo;
      lastTimeLapse = timeLapse;
    }
    GmsTimeLapseHourBodyRequest bodyRequest = new GmsTimeLapseHourBodyRequest(lastDateFrom, lastDateTo);
    bodyRequest.setPage(lastPage);
    interactor.execute(TextUtils.Gms.ACTION_IMPUTATION_HOUR_CONTROLLER,
        TextUtils.Gms.METHOD_LIST, bodyRequest, new OnListRetrievedListener<GmsResponse>() {
          @Override
          public void onSuccess(List<GmsResponse> list) {
            if (getView() != null) {
              getView().hideProgressBar();
              if (areThereImputations(list)) {
                getView().resetImputations(lastPage);
                getView().setHoursList(buildImputationsList(list));
                getView().loadHours();
                getView().loadMiniCalendarEventsArray();
              } else {
                if (lastPage == 1) {
                  getView().hideTutorial();
                  getView().showNoHoursView(lastDateFrom, lastDateTo, timeLapse);
                }
                lastPage = 1;
              }
            }
          }

          @Override
          public void onError(int errorId) {
            if (getView() != null) {
              getView().showMessage(errorId);
              getView().hideProgressBar();
            }
          }
        });
  }

  private boolean areThereImputations(List<GmsResponse> list) {
    return !(list.get(0)).getResult().getRecords().isEmpty();
  }

  private List<Hour> buildImputationsList(List<GmsResponse> list) {
    List<Hour> hours = new ArrayList<>(list.get(0).getResult().getRecords().size());
    for (Record record : list.get(0).getResult().getRecords()) {
      Hour hour = new Hour(
          record.getId(),
          record.getImputatorComment(),
          DateUtils.convertMillisecondsToDate(record.getDate()),
          DateUtils.convertMillisecondsToDate(record.getDate()),
          record.getDate(),
          record.getStateId(),
          record.getWeekFrom(),
          record.getWeekTo(),
          record.getWorkOrderNameHours(),
          record.getUnits(),
          false,
          record.getProjectNameHours(),
          record.getExternalCode(),
          record.getCategoryName2(),
          record.getApproverComment(),
          record.isJirable(),
          record.getJiraCode()
      );
      hours.add(hour);
    }
    return hours;
  }

  public void sendImputation(final List<String> hourIdList) {
    getView().showMassiveLoading();
    GmsRequest<List<String>> gmsRequest = new GmsRequest<>();
    gmsRequest.setAction(TextUtils.Gms.ACTION_IMPUTATION_HOUR_CONTROLLER);
    gmsRequest.setMethod(TextUtils.Gms.METHOD_SEND);
    gmsRequest.getData().add(hourIdList);
    buildGmsRequestInteractor.execute(gmsRequest, new OnListRetrievedListener<GmsResponse>() {
      @Override
      public void onSuccess(List<GmsResponse> list) {
        Result result = list.get(FIRST_ITEM).getResult();
        getView().hideMassiveLoading();
        if (result != null) {
          if (result.isSuccess() && getView() != null) {
            getView().setAdapterItemsSent();
            getView().finishMultiSelectionMode();
            getView().showMessage(appResources.getQuantityString(R.plurals.hour_message_sent_hour, hourIdList.size()));
          } else {
            view.showDialogMessage(result.getMessage());
          }
        } else {
          view.showError(R.string.error_gms_send_failure);
          getView().hideMassiveLoading();
        }
      }

      @Override
      public void onError(int errorId) {
        if (getView() != null) {
          getView().showError(errorId);
        }
      }
    });
  }

  public void removeImputation(final String hourId) {
    view.showProgressBar();
    GmsRequest<List<String>> gmsRequest = new GmsRequest<>();
    gmsRequest.setAction(TextUtils.Gms.ACTION_IMPUTATION_HOUR_CONTROLLER);
    gmsRequest.setMethod(TextUtils.Gms.METHOD_REMOVE);
    gmsRequest.getData().add(Collections.singletonList(hourId));
    buildGmsRequestInteractor.execute(gmsRequest, new OnListRetrievedListener<GmsResponse>() {
      @Override
      public void onSuccess(List<GmsResponse> list) {
        Result result = list.get(FIRST_ITEM).getResult();
        getView().hideProgressBar();
        if (result != null) {
          if (result.isSuccess() && getView() != null) {
            getView().removeItemFromAdapter(hourId);
            getView().showMessage(appResources.getQuantityString(R.plurals.hour_message_removed_hour, 1));
          }
        } else {
          view.showError(R.string.error_gms_eliminate_failure);
        }
      }

      @Override
      public void onError(int errorId) {
        if (getView() != null) {
          getView().showError(errorId);
          getView().hideProgressBar();
        }
      }
    });
  }

  public void removeImputations(final List<String> hourIdList) {
    view.showMassiveLoading();
    GmsRequest<List<String>> gmsRequest = new GmsRequest<>();
    gmsRequest.setAction(TextUtils.Gms.ACTION_IMPUTATION_HOUR_CONTROLLER);
    gmsRequest.setMethod(TextUtils.Gms.METHOD_REMOVE);
    gmsRequest.getData().add(hourIdList);
    buildGmsRequestInteractor.execute(gmsRequest, new OnListRetrievedListener<GmsResponse>() {
      @Override
      public void onSuccess(List<GmsResponse> list) {
        Result result = list.get(FIRST_ITEM).getResult();
        getView().hideMassiveLoading();
        if (result != null) {
          if (result.isSuccess() && getView() != null) {
            getView().removeAdapterItems(hourIdList);
            getView().finishMultiSelectionMode();
            getView().showMessage(appResources.getQuantityString(R.plurals.hour_message_removed_hour, hourIdList.size()));
          }
        } else {
          view.showError(R.string.error_gms_eliminate_failure);
        }
      }

      @Override
      public void onError(int errorId) {
        if (getView() != null) {
          getView().showError(errorId);
          getView().hideMassiveLoading();
        }
      }
    });
  }

  public void fabClicked(boolean isMultiSelectionMode, MultiSelector multiSelector, HourAdapter hourAdapter) {
    if (!isMultiSelectionMode) {
      view.goToAddImputation();
    } else {
      List<String> hourIdList = new ArrayList<>();
      for (int position : multiSelector.getSelectedPositions()) {
        hourIdList.add(hourAdapter.getItemsToShow().get(position).getId());
      }
      sendImputation(hourIdList);
    }
  }

  public void onImputationSaved() {
    view.showMessage(R.string.hour_message_saved_hour);
  }

  public void onImputationUpdated() {
    view.showMessage(R.string.hour_message_updated_hour);
  }

  public void onImputationSent(int itemCounter) {
    view.showMessage(appResources.getQuantityString(R.plurals.hour_message_sent_hour, itemCounter));
  }

  public void onImputationRemoved(int itemCounter) {
    view.showMessage(appResources.getQuantityString(R.plurals.hour_message_removed_hour, itemCounter));
  }

  public void onImputationDuplicated() {
    view.showMessage(R.string.hour_message_duplicated_hour);
  }

  public void getNextPageHours(int page) {
    lastPage = page;
    getTimeLapseImputations(lastDateFrom, lastDateTo, lastTimeLapse);
  }

  public void showTutorial() {
    if (!preferencesManager.retrieve(PreferencesManager.KEY_MUST_HIDE_RECYCLER_VIEW_TUTORIAL, false)) {
      view.showTutorial();
    }
  }

  public void closeTutorial() {
    preferencesManager.persist(PreferencesManager.KEY_MUST_HIDE_RECYCLER_VIEW_TUTORIAL, true);
    view.hideTutorial();
  }
}
