package com.vectormobile.myvector.ui.offering.menu;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.BindView;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.OfferingComponent;
import com.vectormobile.myvector.model.offering.Section;
import com.vectormobile.myvector.model.offering.SectionMenu;
import com.vectormobile.myvector.ui.base.BaseFragment;
import com.vectormobile.myvector.ui.offering.OfferingActivity;
import com.vectormobile.myvector.ui.offering.menu.adapter.OfferingMenuAdapter;
import com.vectormobile.myvector.ui.offering.menu.adapter.OnItemClickListener;
import com.vectormobile.myvector.ui.offering.news.OfferingNewsFragment;

import javax.inject.Inject;

/**
 * Created by ajruiz on 03/11/2016.
 */

public class OfferingMenuFragment extends BaseFragment<OfferingMenuPresenter> implements OfferingMenuView, OnItemClickListener {

  OnMenuItemSelectedListener mCallback;
  @Inject
  OfferingMenuPresenter presenter;
  @BindView(R.id.rv_offering_menu)
  RecyclerView rvOfferingMenu;
  private ProgressDialog loadingDialog;
  private OfferingMenuAdapter adapter;
  private OfferingNewsFragment offeringNewsFragment;

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);

    Activity activity = null;
    if (context instanceof Activity) {
      activity = (Activity) context;
    }

    try {
      mCallback = (OnMenuItemSelectedListener) activity;
    } catch (ClassCastException e) {
      throw new ClassCastException(activity.toString()
          + " must implement OnMenuItemSelectedListener");
    }
  }

  @Override
  protected void inject() {
    getComponent(OfferingComponent.class).inject(this);
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    adapter = new OfferingMenuAdapter(this, getContext());

    rvOfferingMenu.setAdapter(adapter);
    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getActivity());
    rvOfferingMenu.setLayoutManager(mLayoutManager);

    createLoadingDialog();
    presenter.getOfferingMenuList();
  }

  @Override
  protected OfferingMenuPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.fragment_offering_menu;
  }

  @Override
  public void onOfferingMenuRetrieved(SectionMenu sectionMenu) {
    adapter.setOfferingMenuList(sectionMenu.getItems());
    hideLoadingDialog();
  }

  private void createLoadingDialog() {
    loadingDialog = new ProgressDialog(getActivity(), R.style.CustomLoadingDialogTheme);
    loadingDialog.setCancelable(true);
    loadingDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialog) {
        getActivity().finish();
      }
    });
    loadingDialog.setCanceledOnTouchOutside(false);
    loadingDialog.show();
  }

  @Override
  public void hideLoadingDialog() {
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        if ((loadingDialog != null) && loadingDialog.isShowing()) {
          loadingDialog.dismiss();
        }
        loadingDialog = null;
      }
    }, 1000);
  }

  @Override
  public void onItemClick(Section item) {
    if(!OfferingActivity.getNewsOpened()){
      OfferingActivity.setNewsOpened(true);
      Bundle args = new Bundle();
      if (item != null) {
        args.putInt(OfferingActivity.KEY_OFFERING_SECTION_ID, item.getObjectId());
        args.putString(OfferingActivity.KEY_OFFERING_SECTION_TITLE, item.getTitle());
        args.putInt(OfferingActivity.KEY_OFFERING_SECTION_COLOR, item.getColor());
      }
      offeringNewsFragment = new OfferingNewsFragment();
      offeringNewsFragment.setArguments(args);
      getFragmentManager().beginTransaction().add(R.id.fl_offering_container, offeringNewsFragment).addToBackStack(null).commit();
      mCallback.onMenuItemSelected();
    }
  }

  public OfferingNewsFragment getOfferingNewsFragment() {
    return offeringNewsFragment;
  }

  public interface OnMenuItemSelectedListener {

    void onMenuItemSelected();
  }
}
