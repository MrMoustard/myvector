package com.vectormobile.myvector.ui.gms.expenses.pdf.dialog;

import android.content.Intent;
import android.net.Uri;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.domain.entity.gms.request.GmsTimeLapseExpenseBodyRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.entity.gms.response.Record;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsTimeLapseExpenseRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.SharedPdfImputationInteractor;
import com.vectormobile.myvector.domain.interactor.listener.DownloadPdfListener;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.model.gms.Expense;
import com.vectormobile.myvector.storage.ImageManager;
import com.vectormobile.myvector.ui.base.BasePresenter;
import com.vectormobile.myvector.util.date.DateUtils;
import com.vectormobile.myvector.util.text.TextUtils;
import timber.log.Timber;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by kurt on 15/03/17.
 */

public class ExpensePdfDialogPresenter extends BasePresenter<ExpensePdfDialogView> {

  private BuildGmsTimeLapseExpenseRequestInteractor interactor;
  private SharedPdfImputationInteractor sharedPdfImputationInteractor;
  private String lastDateFrom;
  private String lastDateTo;
  private int lastPage;

  @Inject
  public ExpensePdfDialogPresenter(ExpensePdfDialogActivity activity,
                                   BuildGmsTimeLapseExpenseRequestInteractor interactor, SharedPdfImputationInteractor sharedPdfImputationInteractor) {
    super(activity);
    this.interactor = interactor;
    this.sharedPdfImputationInteractor = sharedPdfImputationInteractor;
  }

  public void getAcceptedExpenses(String dateFrom, String dateTo) {
    view.showProgressBar();
    getTimeLapseExpenses(
        DateUtils.formatCalendarDateToGmsShortDate(dateFrom),
        DateUtils.formatCalendarDateToGmsShortDate(dateTo));
  }

  public void getNextPage(int page) {
    lastPage = page;
    getTimeLapseExpenses(lastDateFrom, lastDateTo);
  }

  private void getTimeLapseExpenses(final String dateFrom, final String dateTo) {
    if (areDifferentBounds(dateFrom, dateTo)) {
      view.resetExpenses(lastPage);
      lastPage = 1;
      lastDateFrom = dateFrom;
      lastDateTo = dateTo;
    }
    GmsTimeLapseExpenseBodyRequest bodyRequest = new GmsTimeLapseExpenseBodyRequest(lastDateFrom, lastDateTo);
    bodyRequest.setPage(lastPage);
    interactor.execute(TextUtils.Gms.ACTION_IMPUTATION_EXPENSE_CONTROLLER,
        TextUtils.Gms.METHOD_LIST, bodyRequest, new OnListRetrievedListener<GmsResponse>() {
          @Override
          public void onSuccess(List<GmsResponse> list) {
            if (getView() != null) {
              getView().hideProgressBar();
              if (areThereExpenses(list)) {
                getView().resetExpenses(lastPage);
                List<Expense> acceptedExpenses = buildAcceptedExpensesList(list);
                if (!acceptedExpenses.isEmpty()) {
                  getView().loadExpenses(acceptedExpenses);
                } else {
                  getView().showNoExpensesView(lastDateFrom, lastDateTo);
                }
              } else {
                if (lastPage == 1) {
                  getView().showNoExpensesView(lastDateFrom, lastDateTo);
                }
                lastPage = 1;
              }
            }
          }

          @Override
          public void onError(int errorId) {
            if (getView() != null) {
              getView().showMessage(errorId);
              getView().hideProgressBar();
            }
          }
        });
  }

  private boolean areDifferentBounds(String dateFrom, String dateTo) {
    return !dateFrom.equals(lastDateFrom) || !dateTo.equals(lastDateTo);
  }

  private boolean areThereExpenses(List<GmsResponse> list) {
    return !(list.get(0)).getResult().getRecords().isEmpty();
  }

  private List<Expense> buildAcceptedExpensesList(List<GmsResponse> list) {
    List<Expense> expenses = new ArrayList<>();
    for (Record record : list.get(0).getResult().getRecords()) {
      if (record.getState().equals(Expense.EXPENSE_APPROVED)) {
        Expense expense = new Expense(
            record.getId(),
            record.getExpenseDate(),
            DateUtils.convertMillisecondsToDate(record.getExpenseDate()),
            DateUtils.convertMillisecondsToDate(record.getDateTo()),
            record.getReportCategoryName(),
            String.valueOf(record.getAmount()),
            false,
            record.getState(),
            record.getWorkOrderName(),
            record.getProjectName(),
            record.getExpenseCategoryName(),
            record.getUnits(),
            record.getUnitPrice(),
            record.getCurrencyId(),
            record.getImputatorComment(),
            record.getWeekFrom(),
            record.getWeekTo(),
            record.getApproverComment()
        );
        expenses.add(expense);
      }
    }
    return expenses;
  }

  public void buildPdfAndSend(List<Expense> expenses) {
    List<String> expenseIds = buildAcceptedExpenseIdList(expenses);
    sharedPdfImputationInteractor.execute(expenseIds, new DownloadPdfListener() {
      @Override
      public void onNetworkError() {
        getView().showError(R.string.error_connection);

      }

      @Override
      public void onSavePdfError() {
        Timber.d("Se ha hecho correctamente");
      }

      @Override
      public void onSuccess(List<String> expenseIds) {
        getView().sharedPdfExpenseIds(expenseIds);
      }
    });
  }

  private List<String> buildAcceptedExpenseIdList(List<Expense> expenses) {
    List<String> expenseIds = new ArrayList<>(expenses.size());
    for (Expense item : expenses) {
      expenseIds.add(item.getId());
    }
    return expenseIds;
  }
}
