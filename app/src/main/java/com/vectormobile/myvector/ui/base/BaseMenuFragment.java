package com.vectormobile.myvector.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import com.vectormobile.myvector.model.VectorItemMenu;
import com.vectormobile.myvector.widget.animatedmenu.AnimatedMenuAdapter;
import com.vectormobile.myvector.widget.animatedmenu.VectorMenuListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amarina on 07/11/2016.
 */

public abstract class BaseMenuFragment<P extends BaseMenuPresenter> extends BaseFragment<P> {

  protected AnimatedMenuAdapter adapter;
  protected List<VectorItemMenu> items = new ArrayList();

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    adapter = new AnimatedMenuAdapter(getContext(), getRelativeLayoutContainer());
    loadItems();
    if (isAdded())
      adapter.setItems(items);
    final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext()) {
      @Override
      public int scrollVerticallyBy(int dy, RecyclerView.Recycler recycler, RecyclerView.State state) {
        super.scrollVerticallyBy(0, recycler, state);

        if (dy > 10 && adapter.isScrollAvailable()) {
          adapter.scrollDownAnimation(getRecyclerView());
        } else if (dy < -10 && adapter.isScrollAvailable()) {
          adapter.scrollUpAnimation(getRecyclerView());
        }
        return super.scrollVerticallyBy(0, recycler, state);
      }

      @Override
      public boolean canScrollVertically() {
        return adapter.isScrollAvailable();
      }
    };

    getRecyclerView().setLayoutManager(linearLayoutManager);
    getRecyclerView().setAdapter(adapter);
    getRecyclerView().scrollToPosition(calculateCenterOfInfinityScroll());
    getRecyclerView().setHasFixedSize(false);

    adapter.setHeaderItem(calculateCenterOfInfinityScroll());
    adapter.setListener(new VectorMenuListener() {
      @Override
      public void onItemMenuClick(VectorItemMenu itemMenu) {
        if (adapter.isScrollAvailable())
          itemMenu.launchAction(getContext());
      }
    });
  }


  @Override
  public void onResume() {
    super.onResume();
  }

  private int calculateCenterOfInfinityScroll() {
    int middle = Integer.MAX_VALUE / 2;
    return middle - (Integer.MAX_VALUE % items.size());
  }


  protected abstract void loadItems();

  protected abstract RelativeLayout getRelativeLayoutContainer();

  public abstract RecyclerView getRecyclerView();
}