package com.vectormobile.myvector.ui.menu.personal;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.base.BaseMenuPresenter;

import javax.inject.Inject;

/**
 * VectorDigital.
 *
 * @author -   AMarinaG
 * @since -   28/10/16
 */
@ActivityScope
public class PersonalMenuPresenter extends BaseMenuPresenter<PersonalMenuFragment> {
  @Inject
  public PersonalMenuPresenter(PersonalMenuFragment fragment) {
    super(fragment);
  }
}
