package com.vectormobile.myvector.ui.gms.expenses.pdf.dialog.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.model.gms.Expense;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * Created by kurt on 05/12/16.
 */
public class AcceptedExpenseAdapter extends RecyclerView.Adapter<AcceptedExpenseAdapter.ItemViewHolder> {

  private List<Expense> expenses;

  public AcceptedExpenseAdapter() {
    this.expenses = new ArrayList<>();
  }

  private List<Expense> sortExpensesListByExpenseDate(List<Expense> expenses) {
    Collections.sort(expenses, new Comparator<Expense>() {
      public int compare(Expense expense1, Expense expense2) {
        return Long.valueOf(expense1.getExpenseDate()).compareTo(expense2.getExpenseDate());
      }
    });
    return expenses;
  }

  @Override
  public AcceptedExpenseAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_accepted_expense_row, parent, false);
    return new ItemViewHolder(view);
  }

  @Override
  public void onBindViewHolder(AcceptedExpenseAdapter.ItemViewHolder holder, int pos) {
    Expense item = expenses.get(pos);
    holder.date.setText(item.getDateString());
    holder.title.setText(item.getTitle());
    holder.amount.setText(String.format(Locale.getDefault(), "%s %s", item.getAmount(), item.getCurrencyId()));
    holder.cameraIcon.setVisibility(item.isThereAnAttachedImage() ? View.VISIBLE : View.INVISIBLE);
  }

  @Override
  public int getItemCount() {
    return expenses.size();
  }

  public List<Expense> getExpenses() {
    return expenses;
  }

  public void addItems(List<Expense> expenses) {
    this.expenses.addAll(sortExpensesListByExpenseDate(expenses));
    this.notifyDataSetChanged();
  }

  public void reset() {
    expenses.clear();
    this.notifyDataSetChanged();
  }

  static public class ItemViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.item_date)
    TextView date;
    @BindView(R.id.item_title)
    TextView title;
    @BindView(R.id.mini_camera_img)
    ImageView cameraIcon;
    @BindView(R.id.item_amount)
    TextView amount;

    public ItemViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
