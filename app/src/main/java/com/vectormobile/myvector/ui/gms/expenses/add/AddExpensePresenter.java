package com.vectormobile.myvector.ui.gms.expenses.add;

import android.content.Context;
import android.graphics.Bitmap;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.domain.entity.gms.request.ExpenseRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsBodyRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsFindExpenseByCreateTimeRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsReportCategoryRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.entity.gms.response.Record;
import com.vectormobile.myvector.domain.entity.gms.response.Result;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsBodyRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.GetCurrenciesInteractor;
import com.vectormobile.myvector.domain.interactor.gms.GetExpenseCategoriesInteractor;
import com.vectormobile.myvector.domain.interactor.gms.GetReportCategoryInteractor;
import com.vectormobile.myvector.domain.interactor.gms.GetWorkOrderInteractor;
import com.vectormobile.myvector.domain.interactor.gms.SaveExpenseInteractor;
import com.vectormobile.myvector.domain.interactor.gms.SaveExpenseWithImageInteractor;
import com.vectormobile.myvector.domain.interactor.listener.MultiActionGmsListener;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.model.gms.Expense;
import com.vectormobile.myvector.storage.ImageManager;
import com.vectormobile.myvector.ui.base.BasePresenter;
import com.vectormobile.myvector.util.date.DateUtils;
import com.vectormobile.myvector.util.text.TextUtils;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.inject.Inject;

/**
 * Created by Erik Medina on 07/12/2016.
 */

public class AddExpensePresenter extends BasePresenter<AddExpenseView> {

  public static final int ONLY_ONE_ITEM_WAS_AFFECTED = 1;
  public static final int SAVE_ONLY = 100;
  private static final int FIRST_ITEM = 0;
  private static final int MINIMUM_UNITS_VALUE = 1;
  private static final int SAVE_AND_SEND = 101;

  private SaveExpenseInteractor saveExpenseInteractor;
  private SaveExpenseWithImageInteractor saveExpenseWithImageInteractor;
  private BuildGmsRequestInteractor buildGmsRequestInteractor;
  private BuildGmsBodyRequestInteractor buildGmsBodyRequestInteractor;
  private GetWorkOrderInteractor getWorkOrderInteractor;
  private GetExpenseCategoriesInteractor getExpenseCategoriesInteractor;
  private GetCurrenciesInteractor getCurrenciesInteractor;
  private GetReportCategoryInteractor getReportCategoryInteractor;
  private ImageManager imageManager;
  private List<Record> projects;
  private List<Record> workOrders;
  private List<Record> expenseCategories;
  private List<Record> currencies;
  private String expenseDate;
  private String expenseDateFrom;
  private String expenseDateTo;
  private String date;
  private int projectPositionSelected;
  private int workOrderPositionSelected;
  private int expenseCategoryPositionSelected;
  private String expenseUnits;
  private String unitPrice;
  private int currencyPositionSelected;
  private String comment;
  private String projectSelectedId;
  private String workOrderSelectedId;
  private String expenseCategorySelectedId;
  private String reportCategoryId;
  private Expense expense;
  private boolean dataChanged;
  private boolean dataSaved = true;
  private boolean firsTime = true;

  @Inject
  public AddExpensePresenter(AddExpenseActivity activity,
                             SaveExpenseInteractor saveExpenseInteractor,
                             GetCurrenciesInteractor getCurrenciesInteractor,
                             BuildGmsBodyRequestInteractor buildGmsBodyRequestInteractor,
                             SaveExpenseWithImageInteractor saveExpenseWithImageInteractor,
                             GetWorkOrderInteractor getWorkOrderInteractor,
                             GetExpenseCategoriesInteractor getExpenseCategoriesInteractor,
                             GetReportCategoryInteractor getReportCategoryInteractor,
                             BuildGmsRequestInteractor buildGmsRequestInteractor) {
    super(activity);
    this.saveExpenseInteractor = saveExpenseInteractor;
    this.saveExpenseWithImageInteractor = saveExpenseWithImageInteractor;
    this.getCurrenciesInteractor = getCurrenciesInteractor;
    this.buildGmsBodyRequestInteractor = buildGmsBodyRequestInteractor;
    this.getWorkOrderInteractor = getWorkOrderInteractor;
    this.getExpenseCategoriesInteractor = getExpenseCategoriesInteractor;
    this.getReportCategoryInteractor = getReportCategoryInteractor;
    this.buildGmsRequestInteractor = buildGmsRequestInteractor;
    this.imageManager = ImageManager.getInstance();

    workOrders = new ArrayList<>();
    expenseCategories = new ArrayList<>();
  }

  void initializeView(int action) {
    setDefaultDateFrom(action);
    view.makeCommentFieldScrollable();
    view.setMinimumValueForUnits(MINIMUM_UNITS_VALUE);
    view.setUnits(MINIMUM_UNITS_VALUE);
    view.setFabBehaviour();
  }

  void getProjects() {
    GmsBodyRequest gmsBodyRequest = new GmsBodyRequest(getDateTo());
    buildGmsBodyRequestInteractor.execute(TextUtils.Gms.ACTION_IMPUTATION_EXPENSE_CONTROLLER,
        TextUtils.Gms.IMPUTABLE_PROJECTS, gmsBodyRequest,
        new OnListRetrievedListener<GmsResponse>() {
          @Override
          public void onSuccess(List<GmsResponse> list) {
            if (list.get(FIRST_ITEM).getResult() != null) {
              projects = list.get(FIRST_ITEM).getResult().getRecords();
              view.populateProjects(projects);
              if (expense != null) {
                view.setProjectItem(expense.getProject());
              }
            }
          }

          @Override
          public void onError(int errorId) {
            gmsServiceError(errorId);
          }
        });
  }

  void getWorkOrders(int projectPositionSelected) {
    workOrders.clear();
    if (projectPositionSelected > -1) {
      projectSelectedId = projects.get(projectPositionSelected).getId();
      getWorkOrderInteractor.execute(projectSelectedId, new OnListRetrievedListener<GmsResponse>() {
        @Override
        public void onSuccess(List<GmsResponse> list) {
          workOrders = list.get(FIRST_ITEM).getResult().getRecords();
          view.populateWorkOrders(workOrders);
          if (expense != null) {
            view.setWorkOrderItem(expense.getWorkOrderName());
          }
        }

        @Override
        public void onError(int errorId) {
          gmsServiceError(errorId);
        }
      });
    } else {
      view.populateWorkOrders(workOrders);
    }
  }

  void getExpenseCategories(int workOrderPositionSelected) {
    expenseCategories.clear();
    if (workOrderPositionSelected > -1) {
      workOrderSelectedId = workOrders.get(workOrderPositionSelected).getId();
      getExpenseCategoriesInteractor.execute(projectSelectedId, workOrderSelectedId,
          new OnListRetrievedListener<GmsResponse>() {
            @Override
            public void onSuccess(List<GmsResponse> list) {
              if (list.get(FIRST_ITEM).getResult() != null) {
                expenseCategories = fixCategoriesList(list.get(FIRST_ITEM).getResult().getRecords());
                view.populateExpenseCategories(expenseCategories);
                if (expense != null) {
                  view.setCategoryItem(expense.getCategory());
                }
              }
            }

            @Override
            public void onError(int errorId) {
              gmsServiceError(errorId);
            }
          });
    } else {
      view.populateExpenseCategories(expenseCategories);
    }
  }

  private Calendar getDateTo() {
    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.CALENDAR_DATE_FORMAT, Locale.getDefault());
    try {
      calendar.setTime(sdf.parse(view.getExpenseDateFrom()));
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return calendar;
  }

  private List<Record> fixCategoriesList(List<Record> records) {
    List<Record> result = new ArrayList<>();
    Set<String> titles = new HashSet<>();
    Collections.reverse(records);

    for (Record item : records) {
      if (titles.add(item.getId())) {
        result.add(item);
      }
    }

    Collections.reverse(result);
    return result;
  }

  void getCurrencies() {
    getCurrenciesInteractor.execute(new OnListRetrievedListener<GmsResponse>() {
      @Override
      public void onSuccess(List<GmsResponse> list) {
        currencies = list.get(FIRST_ITEM).getResult().getRecords();
        view.populateCurrencies(currencies);
        if (expense != null) {
          view.setCurrencyItem(expense.getCurrencyId());
        }
      }

      @Override
      public void onError(int errorId) {
        gmsServiceError(errorId);
      }
    });
  }

  void getReportCategory(int expenseCategoryPositionSelected) {
    if (expenseCategoryPositionSelected > -1 && expenseCategoryPositionSelected < expenseCategories.size()) {
      expenseCategorySelectedId = expenseCategories.get(expenseCategoryPositionSelected).getId();
      disableCategoryFields(expenseCategories.get(expenseCategoryPositionSelected));
      GmsReportCategoryRequest gmsReportCategoryRequest = new GmsReportCategoryRequest();
      gmsReportCategoryRequest.setProjectId(projectSelectedId);
      gmsReportCategoryRequest.setWorkOrderId(workOrderSelectedId);
      gmsReportCategoryRequest.setCategory1Id(expenseCategorySelectedId);
      gmsReportCategoryRequest.setDate(DateUtils.formatDateToGmsLargeString(new Date()));

      getReportCategoryInteractor
          .execute(gmsReportCategoryRequest, new OnListRetrievedListener<GmsResponse>() {
            @Override
            public void onSuccess(List<GmsResponse> reportCategoriesList) {
              reportCategoryId = reportCategoriesList.get(FIRST_ITEM).getResult().getRecords().get(FIRST_ITEM).getId();
            }

            @Override
            public void onError(int errorId) {
              gmsServiceError(errorId);
            }
          });
    }
  }

  private void disableCategoryFields(Record expenseCategory) {
    if (expenseCategory.getPrice() != 0) {
      view.blockCategoryFields(expenseCategory);
    } else {
      view.releaseCategoryFields();
    }
  }

  void saveExpense(int action) {
    view.showProgressBar();
    final ExpenseRequest expenseRequest = createExpenseRequest();
    if (expenseRequest != null) {
      if (ImageManager.getInstance().getLastTempFile() != null) {
        saveExpenseWithImage(expenseRequest, action);
      } else {
        saveExpenseWithoutImage(expenseRequest, action);
      }
    } else {
      view.hideProgressBar();
      view.showMessage(R.string.error_incomplete_data);
    }
  }

  private void saveExpenseWithImage(ExpenseRequest expenseRequest, final int action) {
    String countAction = TextUtils.Gms.METHOD_LIST;
    Calendar calendar = Calendar.getInstance();
    DateUtils.setToMidnight(calendar);

    GmsFindExpenseByCreateTimeRequest countRequest = new GmsFindExpenseByCreateTimeRequest(calendar.getTimeInMillis());
    String expenseAction = TextUtils.Gms.ACTION_IMPUTATION_EXPENSE_CONTROLLER;
    final String expenseMethod;
    if (dataChanged && expense != null) {
      if (!"".equals(this.expense.getId())) {
        expenseMethod = TextUtils.Gms.METHOD_CREATE;
      } else {
        expenseMethod = TextUtils.Gms.METHOD_UPDATE;
      }
    } else {
      expenseMethod = TextUtils.Gms.METHOD_CREATE;
    }
    saveExpenseWithImageInteractor.execute(expenseRequest, countRequest, expenseAction, expenseMethod, countAction,
        new MultiActionGmsListener() {
          @Override
          public void onSuccess(List<GmsResponse> body) {
            try {
              // send 3 actions, response count, added & count
              GmsResponse preCountResponse = body.get(0);
              GmsResponse addedExponseResponse = body.get(1);
              GmsResponse proCountResponse = body.get(2);

              if (preCountResponse.getResult().getTotal() < proCountResponse.getResult().getTotal()) {
                List<String> idsToApply = new ArrayList<String>();
                List<Record> preRecords = preCountResponse.getResult().getRecords();
                List<Record> proRecords = proCountResponse.getResult().getRecords();
                for (Record proRecord : proRecords) {
                  if (!preRecords.contains(proRecord)) {
                    idsToApply.add(proRecord.getId());
                  }
                }
                if (!idsToApply.isEmpty()) {
                  for (String expenseId : idsToApply) {
                    imageManager.copyImage(ImageManager.getInstance().getLastTempFile(), expenseId);
                  }
                }
                imageManager.deleteTmpImage(ImageManager.getInstance().getLastTempFile());
              } else if (expense.getId() != null) {
                imageManager.copyImage(ImageManager.getInstance().getLastTempFile(), expense.getId());
                imageManager.deleteTmpImage(ImageManager.getInstance().getLastTempFile());
              }
              view.hideProgressBar();
              if (TextUtils.Gms.METHOD_CREATE.equals(addedExponseResponse.getMethod())) {
                dataSaved = true;
                if (action == SAVE_AND_SEND)
                  sendExpense(Collections.singletonList(expense.getId()));
                else
                  view.returnResult(expenseDateFrom, expenseDateFrom, ONLY_ONE_ITEM_WAS_AFFECTED);
              } else if (TextUtils.Gms.METHOD_UPDATE.equals(addedExponseResponse.getMethod())) {
                dataChanged = false;
                if (action == SAVE_AND_SEND)
                  sendExpense(Collections.singletonList(expense.getId()));
                else
                  view.returnResult(TextUtils.Gms.IMPUTATION_UPDATED, expenseDateFrom, ONLY_ONE_ITEM_WAS_AFFECTED);
              }
            } catch (NullPointerException npe) {
              getView().showNetworkError();
              view.hideProgressBar();
            }
          }

          @Override
          public void onUnknowError() {
            getView().showNetworkError();
            view.hideProgressBar();
          }

          @Override
          public void onNetworkError() {
            getView().showNetworkError();
            view.hideProgressBar();
          }
        });
  }

  private void saveExpenseWithoutImage(ExpenseRequest expenseRequest, final int action) {
    String countAction = TextUtils.Gms.ACTION_IMPUTATION_EXPENSE_CONTROLLER;
    final String expenseMethod;
    if (dataChanged && expense != null) {
      if ("".equals(this.expense.getId())) {
        expenseMethod = TextUtils.Gms.METHOD_CREATE;
      } else {
        expenseMethod = TextUtils.Gms.METHOD_UPDATE;
      }
    } else {
      expenseMethod = TextUtils.Gms.METHOD_CREATE;
    }
    if (expenseRequest != null) {
      saveExpenseInteractor.execute(expenseRequest, countAction, expenseMethod, new OnListRetrievedListener<GmsResponse>() {
        @Override
        public void onSuccess(List<GmsResponse> list) {
          view.hideProgressBar();
          if (expenseMethod.equals(TextUtils.Gms.METHOD_CREATE)) {
            dataSaved = true;
            if (action == SAVE_AND_SEND)
              sendExpense(Collections.singletonList(expense.getId()));
            else {
              view.returnResult(expenseDateFrom, expenseDateFrom, ONLY_ONE_ITEM_WAS_AFFECTED);
            }
          } else if (expenseMethod.equals(TextUtils.Gms.METHOD_UPDATE)) {
            dataChanged = false;
            if (action == SAVE_AND_SEND)
              sendExpense(Collections.singletonList(expense.getId()));
            else {
              view.returnResult(TextUtils.Gms.IMPUTATION_UPDATED, expenseDateFrom, ONLY_ONE_ITEM_WAS_AFFECTED);
            }
          }
        }

        @Override
        public void onError(int errorId) {
          gmsServiceError(errorId);
          view.hideProgressBar();
        }
      });
    } else {
      view.hideProgressBar();
      view.showMessage(R.string.error_incomplete_data);
    }
  }

  private ExpenseRequest createExpenseRequest() {
    getFormData();
    if (isValidFormData()) {
      return buildExpenseRequestEntity();
    }
    return null;
  }

  private void getFormData() {
    expenseDate = DateUtils.formatCalendarDateToGmsShortDate(view.getExpenseDateFrom());
    expenseDateFrom = DateUtils.formatCalendarDateToGmsShortDate(view.getExpenseDateFrom());
    expenseDateTo = DateUtils.formatCalendarDateToGmsShortDate(view.getExpenseDateTo());
    date = DateUtils.formatCalendarDateToGmsShortDate(DateUtils.getCalendarFormattedCurrentDate());
    projectPositionSelected = view.getProjectPositionSelected();
    workOrderPositionSelected = view.getWorkOrderPositionSelected();
    expenseCategoryPositionSelected = view.getExpenseCategoryPositionSelected();
    expenseUnits = view.getExpenseUnits();
    unitPrice = view.getUnitPrice();
    currencyPositionSelected = view.getCurrencyPositionSelected();
    comment = view.getComment();
  }

  private boolean isValidFormData() {
    return isValidDate(expenseDate) &&
        isValidProjectPositionSelected(projectPositionSelected) &&
        isValidWorkOrderPositionSelected(workOrderPositionSelected) &&
        isValidExpenseCategoryPositionSelected(expenseCategoryPositionSelected) &&
        isValidExpenseUnits(expenseUnits) &&
        isValidAmountPerUnit(unitPrice) &&
        isValidCurrencyPositionSelected(currencyPositionSelected);
  }

  private ExpenseRequest buildExpenseRequestEntity() {
    ExpenseRequest expenseRequest = new ExpenseRequest();
    if (isValidDate(expenseDateTo)) {
      expenseRequest.setImputationType(TextUtils.Gms.IMPUTATION_MASSIVE);
      expenseRequest.setDateFrom(expenseDateFrom);
      expenseRequest.setDateTo(expenseDateTo);
    } else {
      expenseRequest.setImputationType(TextUtils.Gms.IMPUTATION_INDIVIDUAL);
      expenseRequest.setExpenseDate(expenseDate);
    }
    if (expense != null) {
      expenseRequest.setId(expense.getId());
      expenseRequest.setApproverComment(expense.getApproverComment());
    }
    expenseRequest.setDate(date);
    expenseRequest.setProjectId(projectSelectedId);
    expenseRequest
        .setSaleCategoryId(expenseCategories.get(expenseCategoryPositionSelected).getId());
    expenseRequest.setUnitPrice(unitPrice);
    expenseRequest.setCurrencyId(currencies.get(currencyPositionSelected).getId());
    expenseRequest.setWorkOrderId(workOrders.get(workOrderPositionSelected).getId());
    expenseRequest.setReportCategoryId(reportCategoryId);
    expenseRequest.setUnits(expenseUnits);
    expenseRequest.setImputationComment(comment);
    return expenseRequest;
  }

  private boolean isValidDate(String date) {
    return date != null && !date.isEmpty();
  }

  private boolean isValidProjectPositionSelected(int projectPositionSelected) {
    if (projectPositionSelected > -1) {
      projectSelectedId = projects.get(projectPositionSelected).getId();
      return true;
    }
    return false;
  }

  private boolean isValidWorkOrderPositionSelected(int workOrderPositionSelected) {
    if (workOrderPositionSelected > -1) {
      workOrderSelectedId = workOrders.get(workOrderPositionSelected).getId();
      return true;
    }
    return false;
  }

  private boolean isValidExpenseCategoryPositionSelected(int expenseCategoryPositionSelected) {
    if (expenseCategoryPositionSelected > -1) {
      expenseCategorySelectedId = expenseCategories.get(expenseCategoryPositionSelected).getId();
      return true;
    }
    return false;
  }

  private boolean isValidExpenseUnits(String expenseUnits) {
    try {
      return Integer.parseInt(expenseUnits) > 0;
    } catch (NumberFormatException e) {
      view.showMessage(R.string.error_incomplete_data);
      return false;
    }
  }

  private boolean isValidAmountPerUnit(String amountPerUnit) {
    if (!amountPerUnit.isEmpty()) {
      return true;
    } else {
      view.showMessage(R.string.error_incomplete_data);
      return false;
    }
  }

  private boolean isValidCurrencyPositionSelected(int currencyPositionSelected) {
    return currencyPositionSelected > -1;
  }

  void onCalendarIconClicked(int calendarType, Calendar date) {
    view.showDatePickerDialog(calendarType, date);
  }

  void sendExpense(final List<String> expensesIds) {
    if (!dataChanged) {
      GmsRequest<List<String>> gmsRequest = new GmsRequest<>();
      gmsRequest.setAction(TextUtils.Gms.ACTION_IMPUTATION_EXPENSE_CONTROLLER);
      gmsRequest.setMethod(TextUtils.Gms.METHOD_SEND);
      gmsRequest.getData().add(expensesIds);
      buildGmsRequestInteractor.execute(gmsRequest, new OnListRetrievedListener<GmsResponse>() {
        @Override
        public void onSuccess(List<GmsResponse> list) {
          firsTime = true;
          Result result = list.get(FIRST_ITEM).getResult();
          if (result != null) {
            if (result.isSuccess()) {
              view.hideProgressBar();
              expenseDateFrom = DateUtils.formatCalendarDateToGmsShortDate(view.getExpenseDateFrom());
              view.returnResult(TextUtils.Gms.IMPUTATION_SENT, expenseDateFrom, expensesIds.size());
              Answers.getInstance().logCustom(
                  new CustomEvent("SendExpense")
                      .putCustomAttribute("expenses List", expensesIds.toString())
                      .putCustomAttribute("success", "true")
              );
            }
          } else {
            Answers.getInstance().logCustom(
                new CustomEvent("SendExpense")
                    .putCustomAttribute("expenses List", expensesIds.toString())
                    .putCustomAttribute("success", "false")
            );
            view.showError(R.string.error_gms_send_failure);
          }
        }

        @Override
        public void onError(int errorId) {
          view.showError(errorId);
        }
      });
    } else {
      saveExpense(SAVE_AND_SEND);
    }
  }

  public void removeImputation(final List<String> expensesIds) {
    view.showProgressBar();
    GmsRequest<List<String>> gmsRequest = new GmsRequest<>();
    gmsRequest.setAction(TextUtils.Gms.ACTION_IMPUTATION_EXPENSE_CONTROLLER);
    gmsRequest.setMethod(TextUtils.Gms.METHOD_REMOVE);
    gmsRequest.getData().add(expensesIds);
    buildGmsRequestInteractor.execute(gmsRequest, new OnListRetrievedListener<GmsResponse>() {
      @Override
      public void onSuccess(List<GmsResponse> list) {
        view.hideProgressBar();
        Result result = list.get(FIRST_ITEM).getResult();
        if (result != null) {
          if (result.isSuccess()) {
            expenseDateFrom = DateUtils.formatCalendarDateToGmsShortDate(view.getExpenseDateFrom());
            view.returnResult(TextUtils.Gms.IMPUTATION_REMOVED, expenseDateFrom, expensesIds.size());
          }
        } else {
          view.showError(R.string.error_gms_eliminate_failure);
        }
      }

      @Override
      public void onError(int errorId) {
        view.hideProgressBar();
        view.showError(R.string.error_gms_eliminate_failure);
      }
    });
  }

  private void gmsServiceError(int errorId) {
    view.showError(errorId);
  }

  void compareDates(int calendarType, String stringFrom, String stringTo) {
    if (stringTo != null && !stringTo.isEmpty()) {
      SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.CALENDAR_DATE_FORMAT,
          Locale.getDefault());
      try {
        Date dateFrom = sdf.parse(stringFrom);
        Date dateTo = sdf.parse(stringTo);
        if (calendarType == TextUtils.Gms.CALENDAR_EXPENSE_FROM) {
          if (dateFrom.after(dateTo)) {
            view.setDateTo(stringFrom);
          }
        } else if (calendarType == TextUtils.Gms.CALENDAR_EXPENSE_TO) {
          if (dateTo.before(dateFrom)) {
            view.setDateFrom(stringTo);
          }
        }
      } catch (ParseException e) {
        e.printStackTrace();
      }
    }
  }

  void fillForm(int action, Expense expense) {
    if (action != AddExpenseActivity.CREATE_ACTION) {
      this.expense = expense;

      setDefaultDateFrom(action);
      getProjects();
      view.setUnits(expense.getUnits());
      view.setUnitPrice(expense.getUnitPrice());
      getCurrencies();
      view.setComment(expense.getComment());
      if (imageManager.haveImage(expense.getId()) && action != AddExpenseActivity.DUPLICATE_ACTION) {
        view.checkReadPermission(expense.getId());
      }
    }
    if (action == AddExpenseActivity.DUPLICATE_ACTION) {
      this.expense.setId("");
    }
  }

  private void setDefaultDateFrom(int action) {
    if (action == AddExpenseActivity.CREATE_ACTION) {
      view.setDefaultDate(DateUtils.getCalendarFormattedCurrentDate());
    } else {
      view.setDefaultDate(TextUtils.capitalizeFirstLetter(DateUtils.formatDateToLargeString(expense.getDateFrom())));
    }
  }

  public void loadImage(String expenseId) {
    view.loadImage(imageManager.getFileToWrite(expenseId));

  }

  public File getImageToShare(Context context, Bitmap bitmap) {
    try {
      return imageManager.convertBitmapToFile(context, bitmap);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  void dataChanged() {
    dataChanged = true;
    if (expense != null && firsTime) {
      firsTime = false;
    }
  }

  public boolean isDataChanged() {
    return dataChanged;
  }

  public void setDataChanged(boolean dataChanged) {
    this.dataChanged = dataChanged;
  }

  public boolean isFirsTime() {
    return firsTime;
  }

  public boolean isDataSaved() {
    return dataSaved;
  }

  public void setDataNotSaved() {
    dataSaved = false;
  }

  public void shouldShowHiddenButtons(int action) {
    if (action == AddExpenseActivity.UPDATE_ACTION)
      view.showHiddenButtons();
  }

  public void setUpActionBarTitle(int action) {
    switch (action) {
      case AddExpenseActivity.DUPLICATE_ACTION:
        view.setTitle(R.string.expense_duplicate_expense_toolbar_title);
        break;

      case AddExpenseActivity.UPDATE_ACTION:
        view.setTitle(R.string.expense_edit_expense_toolbar_title);
        break;
    }
  }

  public void shouldShowDuplicateButton(int action) {
    if (action == AddExpenseActivity.UPDATE_ACTION) {
      view.showDuplicateButton();
    }
  }
}
