package com.vectormobile.myvector.ui.profile;

import android.content.Context;
import android.content.ContextWrapper;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.domain.entity.intranet.UploadImageResponse;
import com.vectormobile.myvector.domain.interactor.intranet.ProfileDataInteractor;
import com.vectormobile.myvector.domain.interactor.intranet.ProfileSaveUserInfoInteractor;
import com.vectormobile.myvector.domain.interactor.intranet.SendProfileImageInteractor;
import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.domain.interactor.login.LogoutInteractor;
import com.vectormobile.myvector.model.UserAttributes;
import com.vectormobile.myvector.model.intranet.UserProfile;
import com.vectormobile.myvector.ui.base.BasePresenter;
import com.vectormobile.myvector.util.AppResources;
import com.vectormobile.myvector.util.text.TextUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * VectorDigital.
 *
 * @author -   AMarinaG
 * @since -   28/10/16
 */
@ActivityScope
public class ProfilePresenter extends BasePresenter<ProfileView> {

  private static int PHONE_POSITION = 1;
  private static int CELLPHONE_POSITION = 2;

  private static List<UserAttributes> userAttributes;

  private AppResources appResources;
  private ProfileDataInteractor profileDataInteractor;
  private ProfileSaveUserInfoInteractor profileSaveUserInfoInteractor;
  private SendProfileImageInteractor sendProfileImageInteractor;
  private LogoutInteractor logoutInteractor;
  private String phone;
  private String cellphone;

  @Inject
  public ProfilePresenter(ProfileActivity activity,
                          ProfileDataInteractor profileDataInteractor,
                          ProfileSaveUserInfoInteractor profileSaveUserInfoInteractor,
                          SendProfileImageInteractor sendProfileImageInteractor,
                          LogoutInteractor logoutInteractor,
                          AppResources appResources) {
    super(activity);
    this.profileDataInteractor = profileDataInteractor;
    this.profileSaveUserInfoInteractor = profileSaveUserInfoInteractor;
    this.sendProfileImageInteractor = sendProfileImageInteractor;
    this.logoutInteractor = logoutInteractor;
    this.appResources = appResources;
    userAttributes = new ArrayList<>();
  }

  public void saveButtonClicked(String cellphone, String phone) {
    savechanges(cellphone, phone);
  }

  public void editButtonClicked() {
    view.showSaveButton();
  }

  public void keyboardActionDoneClicked(String cellphone, String phone) {
    savechanges(cellphone, phone);
  }

  private void savechanges(String cellphone, String phone) {
    view.clearItemFocus();
    if (cellphone.length() < 2) {
      view.setFocus(CELLPHONE_POSITION);
      view.showMessage(R.string.profile_wrong_phone_number);
    } else if (phone.length() < 2) {
      view.setFocus(PHONE_POSITION);
      view.showMessage(R.string.profile_wrong_phone_number);
    } else {
      view.hideSaveButton();
      if (!TextUtils.compareStrings(phone, this.phone)) {
        saveUserInfo(TextUtils.Intranet.ATTRIBUTE_PHONE_SAVE_USER_INFO, phone);
        this.phone = phone;
      }
      if (!TextUtils.compareStrings(cellphone, this.cellphone)) {
        saveUserInfo(TextUtils.Intranet.ATTRIBUTE_CELLPHONE_SAVE_USER_INFO, cellphone);
        this.cellphone = cellphone;
      }
    }
  }

  public void getUserProfile() {
    view.showProgressBar();
    profileDataInteractor.execute(new OnItemRetrievedListener() {
      @Override
      public void onSuccess(Object item) {
        if (getView() != null) {
          UserProfile userProfile = parseUserDataContent(item.toString());
          if (isAValidProfile(userProfile)) {
            getView().userDataRetrieved(userProfile);
          } else {
            getView().showInvalidProfileDialog();
          }
          getView().showProfile();
          getView().hideProgressBar();
        }
      }

      @Override
      public void onError(int errorId) {
        if (getView() != null) {
          getView().hideProgressBar();
          getView().showMessageAction(errorId);
        }
      }
    });
  }

  private boolean isAValidProfile(UserProfile userProfile) {
    return userProfile.getName() != null && !userProfile.getName().isEmpty();
  }

  public void saveUserInfo(String attribute, String value) {
    profileSaveUserInfoInteractor.execute(attribute, value, new OnItemRetrievedListener() {
      @Override
      public void onSuccess(Object item) {
        if (getView() != null) {
          getView().showMessage(R.string.profile_data_saved_correctly);
        }
      }

      @Override
      public void onError(int errorId) {
        if (getView() != null) {
          getView().showError(errorId);
        }
      }
    });
  }

  private UserProfile parseUserDataContent(String content) {
    UserProfile userProfile = new UserProfile();
    Document doc = Jsoup.parseBodyFragment(content);
    doc.select("script").remove();
    doc.select("script").remove();
    doc.select("span.glyphicon glyphicon-edit").remove();
    Elements elementUser = doc.select("div[id=info-profile]");

    //GET User Data
    if (elementUser != null && elementUser.size() > 0) {
      phone = elementUser.select("div#user-info-phone").text();
      cellphone = elementUser.select("div#user-info-cellphone").text();
      userProfile.setAvatarUrl(elementUser.select("img[src]").get(0).attr("src"));
      userProfile.setName(elementUser.select("h4").html());
      userProfile.setEmail(elementUser.select("div#user-info-email").html());
      userProfile.setPhone(elementUser.select("div#user-info-phone").text());
      userProfile.setMobile(elementUser.select("div#user-info-cellphone").text());
      userProfile.setDepartment(elementUser.select("span[class=info]").get(0).text());
      userProfile.setCenter(elementUser.select("span[class=info]").get(1).text());
      //Not available yet
      userProfile.setArea(appResources.getString(R.string.error_not_available));
      userProfile.setResponsible(appResources.getString(R.string.error_not_available));
      userProfile.setCompany(appResources.getString(R.string.error_not_available));
    }
    return userProfile;
  }

  public boolean loadAvatarFromIntranet(String url) {
    return url != null && !url.contains(TextUtils.Intranet.DEFAULT_USER_PROFILE_IMAGE);
  }

  public String getAvatarFromInternalStorage(Context context) {
    ContextWrapper cw = new ContextWrapper(context);
    File directory = cw.getDir("media", Context.MODE_PRIVATE);
    File file = new File(directory, TextUtils.Intranet.DEFAULT_AVATAR_FILE_NAME);
    if (file.isFile()) {
      return new File(directory, TextUtils.Intranet.DEFAULT_AVATAR_FILE_NAME).toString();
    }
    return null;
  }

  public void sendImage(File photoProfile) {
    sendProfileImageInteractor.execute(photoProfile, new OnItemRetrievedListener<UploadImageResponse>() {
      @Override
      public void onSuccess(UploadImageResponse item) {
        if (getView() != null) {
          getView().showMessage(R.string.profile_avatar_saved_successfully);
        }
      }

      @Override
      public void onError(int errorId) {
        if (getView() != null) {
          getView().showError(errorId);
        }
      }
    });
  }

  public void logout() {
    logoutInteractor.execute();
  }

  public List<UserAttributes> setUserAttributes(UserProfile userProfile) {
    userAttributes.clear();
    userAttributes.add(new UserAttributes(appResources.getString(R.string.profile_email), userProfile.getEmail(), R.drawable.ic_email, false));
    userAttributes.add(new UserAttributes(appResources.getString(R.string.profile_phone), userProfile.getPhone(), R.drawable.ic_phone, true));
    userAttributes.add(new UserAttributes(appResources.getString(R.string.profile_mobile), userProfile.getMobile(), R.drawable.ic_mobile, true));
    userAttributes.add(new UserAttributes(appResources.getString(R.string.profile_department), userProfile.getDepartment(), R.drawable.ic_department, false));
    userAttributes.add(new UserAttributes(appResources.getString(R.string.profile_area), userProfile.getArea(), R.drawable.ic_area, false));
    userAttributes.add(new UserAttributes(appResources.getString(R.string.profile_center), userProfile.getCenter(), R.drawable.ic_center, false));
    userAttributes.add(new UserAttributes(appResources.getString(R.string.profile_company), userProfile.getCompany(), R.drawable.ic_company, false));
    return userAttributes;
  }
}
