package com.vectormobile.myvector.ui.gms.hour.add.jira.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.model.gms.Jira;
import com.vectormobile.myvector.ui.gms.hour.add.jira.JiraBrowserActivity;
import com.vectormobile.myvector.ui.gms.hour.add.jira.JiraBrowserView;
import com.vectormobile.myvector.util.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ajruiz on 01/03/2017.
 */

public class JiraAdapter extends RecyclerView.Adapter<JiraAdapter.JiraViewHolder> {

  private static List<Jira> jiraList;
  private int page = 1;
  private JiraBrowserView jiraBrowserView;
  private OnJiraClickListener listener;

  public JiraAdapter(JiraBrowserView jiraBrowserView, OnJiraClickListener listener) {
    this.jiraBrowserView = jiraBrowserView;
    this.listener = listener;
  }

  @Override
  public JiraViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_jira_row, parent, false);
    return new JiraViewHolder(view);
  }

  @Override
  public void onBindViewHolder(JiraViewHolder holder, int position) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(jiraList.get(position).getKey());
    stringBuilder.append(TextUtils.Intranet.CONTACTS_WHITE_SPACE);
    stringBuilder.append(jiraList.get(position).getDescription());
    holder.tvJiraTitle.setText(stringBuilder);

    holder.bind(jiraList.get(position), listener);
  }

  public void setItems(List<Jira> list) {
    jiraList = new ArrayList<>(list.size());
    jiraList.addAll(list);
    notifyDataSetChanged();
  }

  public void clearItems() {
    if (jiraList != null) {
      jiraList.clear();
    }
    page = JiraBrowserActivity.FIRST_PAGE;
    notifyDataSetChanged();
  }

  public int getPage() {
    return page;
  }

  @Override
  public void onAttachedToRecyclerView(RecyclerView recyclerView) {
    super.onAttachedToRecyclerView(recyclerView);
  }

  @Override
  public int getItemCount() {
    if (jiraList != null) {
      return jiraList.size();
    } else {
      return 0;
    }
  }

  static class JiraViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.ll_item_jira_row)
    LinearLayout llJiraRow;
    @BindView(R.id.tv_jira_title)
    TextView tvJiraTitle;

    public JiraViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }

    public void bind(final Jira jira, final OnJiraClickListener listener) {
      llJiraRow.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          listener.onJiraClick(jira);
        }
      });
    }

  }


}
