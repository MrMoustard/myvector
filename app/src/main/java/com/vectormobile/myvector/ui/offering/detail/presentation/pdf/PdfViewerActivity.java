package com.vectormobile.myvector.ui.offering.detail.presentation.pdf;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.storage.PdfDownloadAsyncTask;

import java.io.File;
import java.lang.annotation.Annotation;

/**
 * Created by ajruiz on 23/11/2016.
 */

public class PdfViewerActivity extends AppCompatActivity implements PermissionChecker.PermissionResult {

  @BindView(R.id.wb_pdf)
  WebView webViewPdf;
  @BindString(R.string.offering_pdf_saved)
  String pdfSavedMessage;

  public static final String KEY_URL = "url";
  public static final String GOOGLE_DOCS_URL = "http://drive.google.com/viewerng/viewer?embedded=true&url=";
  public static final String KEY_TITLE = "title";
  private static final int REQUEST_EXTERNAL_STORAGE = 1;
  private static String[] PERMISSIONS_STORAGE = {
      Manifest.permission.READ_EXTERNAL_STORAGE,
      Manifest.permission.WRITE_EXTERNAL_STORAGE
  };

  private static String url;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(getLayoutResourceId());
    ButterKnife.bind(this);

    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(true);
    getSupportActionBar().setTitle(getIntent().getExtras().getString(KEY_TITLE));

    url = getIntent().getExtras().getString(KEY_URL);

    webViewPdf = new WebView(PdfViewerActivity.this);
    webViewPdf.getSettings().setJavaScriptEnabled(true);
    webViewPdf.getSettings().setPluginState(WebSettings.PluginState.ON);
    webViewPdf.setWebViewClient(new Callback());
    url = getIntent().getExtras().getString(KEY_URL);
    webViewPdf.loadUrl(url);
    setContentView(webViewPdf);

  }

  protected int getLayoutResourceId() {
    return R.layout.activity_pdf;
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_pdf, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        onBackPressed();
        break;
      case R.id.action_download:
        verifyStoragePermissions(this);
        break;
    }
    return true;
  }

  private void showSuccesfulTask() {
    View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
    Snackbar.make(rootView, pdfSavedMessage, Snackbar.LENGTH_LONG).show();
  }

  private void shareIt(File directory) {
    Intent i = new Intent(android.content.Intent.ACTION_SEND);
    i.setType("application/pdf");

    Uri uri = Uri.fromFile(directory);
    i.putExtra(Intent.EXTRA_STREAM, uri);

    startActivity(Intent.createChooser(i, "Share via"));
  }

  public void verifyStoragePermissions(Activity activity) {
    // Check if we have write permission
    int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

    if (permission != PackageManager.PERMISSION_GRANTED) {
      // We don't have permission so prompt the user
      ActivityCompat.requestPermissions(
          activity,
          PERMISSIONS_STORAGE,
          REQUEST_EXTERNAL_STORAGE
      );
    } else {
      downloadPdfOnBackground();
    }
  }

  private void downloadPdfOnBackground() {
    new PdfDownloadAsyncTask() {
      @Override
      protected void onPreExecute() {
        super.onPreExecute();
//          showProgressBar();
      }

      @Override
      protected void onPostExecute(String filePath) {
        super.onPostExecute(filePath);

        if (filePath!=null) {
          showSuccesfulTask();
        } else {
          View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
          Snackbar.make(rootView, getString(R.string.error_not_available), Snackbar.LENGTH_LONG).show();
        }
      }
    }.execute(url);
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
    switch (requestCode) {
      case REQUEST_EXTERNAL_STORAGE: {
        if (grantResults.length > 0
            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          // permission was granted
          downloadPdfOnBackground();
        }
      }
    }
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return null;
  }


  private class Callback extends WebViewClient {
    @Override
    public boolean shouldOverrideUrlLoading(
        WebView view, String url) {
      return (false);
    }
  }

}
