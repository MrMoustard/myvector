package com.vectormobile.myvector.ui.gms.expenses.pdf;

import com.vectormobile.myvector.ui.base.BasePresenter;
import com.vectormobile.myvector.util.date.DateUtils;
import com.vectormobile.myvector.util.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

/**
 * Created by kurt on 15/03/17.
 */

public class ExpensePdfPresenter extends BasePresenter<ExpensePdfView> {

  @Inject
  public ExpensePdfPresenter(ExpensePdfActivity activity) {
    super(activity);
  }

  public void setDefaultDateFrom() {
    view.setDateFrom(DateUtils.getExpensePdfDefaultStartDate());
  }

  public void setDefaultDateTo() {
    view.setDateTo(DateUtils.getCalendarFormattedCurrentDate());
  }

  void onCalendarIconClicked(int calendarType, Calendar date) {
    view.showDatePickerDialog(calendarType, date);
  }

  void compareDates(int calendarType, String stringFrom, String stringTo) {
    if (stringTo != null && !stringTo.isEmpty()) {
      SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.CALENDAR_DATE_FORMAT,
          Locale.getDefault());
      try {
        Date dateFrom = sdf.parse(stringFrom);
        Date dateTo = sdf.parse(stringTo);
        if (calendarType == TextUtils.Gms.CALENDAR_EXPENSE_FROM) {
          if (dateFrom.after(dateTo)) {
            view.setDateTo(stringFrom);
          }
        } else if (calendarType == TextUtils.Gms.CALENDAR_EXPENSE_TO) {
          if (dateTo.before(dateFrom)) {
            view.setDateFrom(stringTo);
          }
        }
      } catch (ParseException e) {
        e.printStackTrace();
      }
    }
  }

  public void buildAndSend() {
    view.navigateToDialog();
  }
}
