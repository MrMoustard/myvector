package com.vectormobile.myvector.ui.browser.detail.adapter;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.model.UserAttributes;
import com.vectormobile.myvector.ui.browser.detail.ContactsDetailActivity;
import com.vectormobile.myvector.ui.profile.adapter.OnUserAttributesClickListener;
import com.vectormobile.myvector.util.AppResources;
import com.vectormobile.myvector.util.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ajruiz on 20/02/2017.
 */

public class ContactAdapter extends
    RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {

  private static List<UserAttributes> items;
  private final OnUserAttributesClickListener listener;

  private AppResources appResources;

  public ContactAdapter(OnUserAttributesClickListener listener, AppResources appResources) {
    this.listener = listener;
    this.appResources = appResources;
    items = new ArrayList<>();
  }

  @Override
  public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact_detail_row, parent, false);
    return new ContactViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final ContactViewHolder holder, int position) {
    holder.textInputLayout.setHint(items.get(position).getItemTitle());
    holder.tittle.setText(TextUtils.ellipsizeText(items.get(position).getItemLabel(), ContactsDetailActivity.MAX_TEXT_SIZE));
    holder.icon.setImageResource(items.get(position).getIcon());
    if (items.get(position).isEditable()) {
      holder.editable.setVisibility(View.VISIBLE);
      if (position == 0) {
        holder.editable.setText(appResources.getString(R.string.browser_contact_action_email));
      } else if (position == 1 || position == 2) {
        holder.editable.setText(R.string.browser_contact_action_call);
      }
    } else {
      holder.editable.setVisibility(View.GONE);
    }
    holder.bind(items.get(position), position, listener);
  }

  @Override
  public void onAttachedToRecyclerView(RecyclerView recyclerView) {
    super.onAttachedToRecyclerView(recyclerView);
  }

  @Override
  public int getItemCount() {
    return items.size();
  }

  public void setItems(List<UserAttributes> userProfile) {
    items.clear();
    items.addAll(userProfile);
    notifyDataSetChanged();
  }

  public void clearArrays() {
    items.clear();
  }

  static class ContactViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.et_item_profile_title)
    TextInputEditText tittle;
    @BindView(R.id.input_item_profile)
    TextInputLayout textInputLayout;
    @BindView(R.id.img_item_user)
    ImageView icon;
    @BindView(R.id.tv_field_editable)
    TextView editable;

    ContactViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }

    public void bind(final UserAttributes atributes, final int position, final OnUserAttributesClickListener listener) {
      editable.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          listener.onItemClick(atributes, position);
        }
      });

    }


  }

}

