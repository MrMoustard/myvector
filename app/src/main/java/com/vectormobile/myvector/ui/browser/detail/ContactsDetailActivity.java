package com.vectormobile.myvector.ui.browser.detail;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.ContactsDetailComponent;
import com.vectormobile.myvector.di.component.DaggerContactsDetailComponent;
import com.vectormobile.myvector.di.module.ContactsDetailModule;
import com.vectormobile.myvector.model.UserAttributes;
import com.vectormobile.myvector.model.intranet.Contact;
import com.vectormobile.myvector.ui.base.BaseActivity;
import com.vectormobile.myvector.ui.browser.detail.adapter.ContactAdapter;
import com.vectormobile.myvector.ui.browser.list.ContactsListActivity;
import com.vectormobile.myvector.ui.news.news.adapter.PicassoTrustAll;
import com.vectormobile.myvector.ui.profile.adapter.OnUserAttributesClickListener;
import com.vectormobile.myvector.util.AppResourcesImpl;
import com.vectormobile.myvector.util.text.TextUtils;

import javax.inject.Inject;

/**
 * Created by ajruiz on 03/02/2017.
 */

public class ContactsDetailActivity extends BaseActivity<ContactsDetailPresenter> implements ContactsDetailView,
    OnUserAttributesClickListener {

  public static int MAX_TEXT_SIZE = 20;

  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.rl_contacts_detail)
  RelativeLayout rlContactsDetail;
  @BindView(R.id.img_contacts_detail_image)
  ImageView imgContactDetail;
  @BindView(R.id.expanded_image)
  ImageView ivExpandedView;
  @BindView(R.id.tv_contact_name)
  TextView tvContactName;
  @BindView(R.id.rv_contacts)
  RecyclerView rvContactDetail;

  @Inject
  ContactsDetailPresenter presenter;

  private ContactsDetailComponent component;
  private Contact contact;
  private ContactAdapter adapter;
  private Animator animator;
  private int shortAnimationDuration;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);

    adapter = new ContactAdapter(this, new AppResourcesImpl(this));
    rvContactDetail.setAdapter(adapter);
    rvContactDetail.setLayoutManager(new LinearLayoutManager(this));

    contact = getIntent().getExtras().getParcelable(ContactsListActivity.EXTRA_DETAILS_CONTACTS);
    if (contact != null) {
      setContactImage(contact.getImageUrl());
      setContactName(contact.getName());
      setContactDetails(contact);
    }

    rlContactsDetail.setVisibility(View.VISIBLE);

    shortAnimationDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);
  }

  @Override
  protected void onResume() {
    super.onResume();
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        TextUtils.hideKeyboard(ContactsDetailActivity.this);
      }
    }, 300);

  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    adapter.clearArrays();
  }

  @Override
  protected void injectModule() {
    component = DaggerContactsDetailComponent.builder()
        .appComponent(((App) getApplication()).getComponent())
        .contactsDetailModule(new ContactsDetailModule(this))
        .build();
    component.inject(this);
  }

  @Override
  protected ContactsDetailPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.activity_contacts_detail;
  }

  @Override
  public ContactsDetailComponent getComponent() {
    return component;
  }

  @Override
  public void setContactImage(String imageUrl) {
    if (!imageUrl.contains(TextUtils.Intranet.DEFAULT_AVATAR_CONTACTS_FILE_NAME)) {
      PicassoTrustAll.getInstance(this)
          .load(imageUrl)
          .placeholder(R.drawable.placeholder_profile_normal_vector_wrapper)
          .error(R.drawable.placeholder_profile_normal_vector_wrapper)
          .into(imgContactDetail);
    }

  }

  @OnClick(R.id.img_contacts_detail_image)
  public void expandImage() {
    if (!contact.getImageUrl().contains(TextUtils.Intranet.DEFAULT_USER_PROFILE_IMAGE)) {
      zoomImageFromThumb(imgContactDetail, imgContactDetail.getDrawable());
    }
  }

  private void zoomImageFromThumb(final ImageView imgContactDetail, Drawable imageDrawable) {
    // If there's an animation in progress, cancel it
    // immediately and proceed with this one.
    if (animator != null) {
      animator.cancel();
    }
    if (imageDrawable != null) {
      ivExpandedView.setImageDrawable(imageDrawable);
    } else {
      ivExpandedView.setImageResource(R.drawable.placeholder_profile_normal_vector_wrapper);
    }
    // Calculate the starting and ending bounds for the zoomed-in image.
    // This step involves lots of math. Yay, math.
    final Rect startBounds = new Rect();
    final Rect finalBounds = new Rect();
    final Point globalOffset = new Point();

    // The start bounds are the global visible rectangle of the thumbnail,
    // and the final bounds are the global visible rectangle of the container
    // view. Also set the container view's offset as the origin for the
    // bounds, since that's the origin for the positioning animation
    // properties (X, Y).
    imgContactDetail.getGlobalVisibleRect(startBounds);
    findViewById(R.id.coordinator_layout_contacts_detail)
        .getGlobalVisibleRect(finalBounds, globalOffset);
    startBounds.offset(-globalOffset.x, -globalOffset.y);
    finalBounds.offset(-globalOffset.x, -globalOffset.y);

    // Adjust the start bounds to be the same aspect ratio as the final
    // bounds using the "center crop" technique. This prevents undesirable
    // stretching during the animation. Also calculate the start scaling
    // factor (the end scaling factor is always 1.0).
    float startScale;
    if ((float) finalBounds.width() / finalBounds.height()
        > (float) startBounds.width() / startBounds.height()) {
      // Extend start bounds horizontally
      startScale = (float) startBounds.height() / finalBounds.height();
      float startWidth = startScale * finalBounds.width();
      float deltaWidth = (startWidth - startBounds.width()) / 2;
      startBounds.left -= deltaWidth;
      startBounds.right += deltaWidth;
    } else {
      // Extend start bounds vertically
      startScale = (float) startBounds.width() / finalBounds.width();
      float startHeight = startScale * finalBounds.height();
      float deltaHeight = (startHeight - startBounds.height()) / 2;
      startBounds.top -= deltaHeight;
      startBounds.bottom += deltaHeight;
    }

    // Hide the thumbnail and show the zoomed-in view. When the animation
    // begins, it will position the zoomed-in view in the place of the
    // thumbnail.
    imgContactDetail.setAlpha(0f);
    ivExpandedView.setVisibility(View.VISIBLE);

    // Set the pivot point for SCALE_X and SCALE_Y transformations
    // to the top-left corner of the zoomed-in view (the default
    // is the center of the view).
    ivExpandedView.setPivotX(0f);
    ivExpandedView.setPivotY(0f);

    // Construct and run the parallel animation of the four translation and
    // scale properties (X, Y, SCALE_X, and SCALE_Y).
    AnimatorSet set = new AnimatorSet();
    set
        .play(ObjectAnimator.ofFloat(ivExpandedView, View.X,
            startBounds.left, finalBounds.left))
        .with(ObjectAnimator.ofFloat(ivExpandedView, View.Y,
            startBounds.top, finalBounds.top))
        .with(ObjectAnimator.ofFloat(ivExpandedView, View.SCALE_X,
            startScale, 1f)).with(ObjectAnimator.ofFloat(ivExpandedView,
        View.SCALE_Y, startScale, 1f));
    set.setDuration(shortAnimationDuration);
    set.setInterpolator(new DecelerateInterpolator());
    set.addListener(new AnimatorListenerAdapter() {
      @Override
      public void onAnimationEnd(Animator animation) {
        animator = null;
      }

      @Override
      public void onAnimationCancel(Animator animation) {
        animator = null;
      }
    });
    set.start();
    animator = set;
    // Upon clicking the zoomed-in image, it should zoom back down
    // to the original bounds and show the thumbnail instead of
    // the expanded image.
    final float startScaleFinal = startScale;

    ivExpandedView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (animator != null) {
          animator.cancel();
        }

        // Animate the four positioning/sizing properties in parallel,
        // back to their original values.
        AnimatorSet set = new AnimatorSet();
        set.play(ObjectAnimator
            .ofFloat(ivExpandedView, View.X, startBounds.left))
            .with(ObjectAnimator
                .ofFloat(ivExpandedView,
                    View.Y, startBounds.top))
            .with(ObjectAnimator
                .ofFloat(ivExpandedView,
                    View.SCALE_X, startScaleFinal))
            .with(ObjectAnimator
                .ofFloat(ivExpandedView,
                    View.SCALE_Y, startScaleFinal));
        set.setDuration(shortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
          @Override
          public void onAnimationEnd(Animator animation) {
            imgContactDetail.setAlpha(1f);
            ivExpandedView.setVisibility(View.GONE);
            animator = null;
          }

          @Override
          public void onAnimationCancel(Animator animation) {
            imgContactDetail.setAlpha(1f);
            ivExpandedView.setVisibility(View.GONE);
            animator = null;
          }
        });
        set.start();
        animator = set;
      }
    });
  }

  @Override
  public void setContactName(String name) {
    tvContactName.setText(name);
  }

  @Override
  public void setContactDetails(Contact contact) {
    adapter.setItems(presenter.setUserAttributes(contact));
  }

  @OnClick(R.id.iv_toolbar_share)
  public void onShareButtonClicked() {
    Intent intent = new Intent();
    intent.setAction(Intent.ACTION_SEND);
    intent.putExtra(Intent.EXTRA_TEXT, presenter.formatShareContactInfo(contact));
    intent.setType("text/plain");
    startActivity(Intent.createChooser(intent, getResources().getText(R.string.browser_share_contact)));
  }

  @Override
  public void onItemClick(UserAttributes item, int position) {
    if (item.getItemLabel().contains(TextUtils.Intranet.CONTACTS_EMAIL)) {
      actionSendEmail(item.getItemLabel());
    } else if (TextUtils.checkStartWithNumber(item.getItemLabel()) || item.getItemLabel().
        startsWith(TextUtils.Intranet.CONTACTS_COUNTRY_CODE_PLUS)) {
      actionCall(item.getItemLabel());
    }
  }

  private void actionSendEmail(String email) {
    Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
        TextUtils.Intranet.CONTACTS_EMAIL_SCHEME, email, null));
    startActivity(Intent.createChooser(emailIntent, TextUtils.Intranet.CONTACTS_EMAIL_INTENT_TITLE));
  }

  private void actionCall(String number) {
    Intent intent = new Intent(Intent.ACTION_DIAL);
    intent.setData(Uri.parse("tel:" + number));
    startActivity(intent);
  }

  @Override
  public void onKeyboardActionDoneClicked() {

  }

}
