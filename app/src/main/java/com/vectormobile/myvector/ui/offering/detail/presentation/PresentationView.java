package com.vectormobile.myvector.ui.offering.detail.presentation;

import com.vectormobile.myvector.ui.base.PresenterView;

/**
 * Created by ajruiz on 23/11/2016.
 */

public interface PresentationView extends PresenterView {
}
