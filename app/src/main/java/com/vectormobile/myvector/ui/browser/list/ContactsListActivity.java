package com.vectormobile.myvector.ui.browser.list;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.SearchEvent;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.ContactsListComponent;
import com.vectormobile.myvector.di.component.DaggerContactsListComponent;
import com.vectormobile.myvector.di.module.ContactsListModule;
import com.vectormobile.myvector.domain.repository.IntranetRepositoryImpl;
import com.vectormobile.myvector.model.intranet.Contact;
import com.vectormobile.myvector.ui.base.BaseActivity;
import com.vectormobile.myvector.ui.browser.detail.ContactsDetailActivity;
import com.vectormobile.myvector.ui.browser.list.adapter.ContactsListAdapter;
import com.vectormobile.myvector.ui.browser.list.adapter.OnContactClickListener;
import com.vectormobile.myvector.util.text.TextUtils;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by ajruiz on 01/02/2017.
 */

public class ContactsListActivity extends BaseActivity<ContactsListPresenter> implements ContactsListView,
    OnContactClickListener {

  public static final String EXTRA_DETAILS_CONTACTS = "EXTRA_DETAILS_CONTACTS";

  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.rv_contacts)
  RecyclerView rvContacts;
  @BindView(R.id.et_contacts_search)
  EditText etContactsSearch;
  @BindView(R.id.img_contacts_filter)
  ImageView imgContactsFilter;
  @BindView(R.id.tv_without_results)
  TextView tvWithoutResults;

  @Inject
  ContactsListPresenter presenter;

  private ContactsListComponent component;
  private ContactsListAdapter adapter;
  private ProgressDialog loadingDialog;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);

    adapter = new ContactsListAdapter(this, this, this);
    rvContacts.setAdapter(adapter);
    rvContacts.setLayoutManager(new LinearLayoutManager(this));

    etContactsSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
          searchContacts();
          return true;
        }
        return false;
      }
    });

    etContactsSearch.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
        requestKeyboard(etContactsSearch);
        imgContactsFilter.setVisibility(View.VISIBLE);
        final int DRAWABLE_LEFT = 0;
        final int DRAWABLE_RIGHT = 2;
        if (event.getAction() == MotionEvent.ACTION_UP) {
          if (event.getRawX() >= (etContactsSearch.getRight() -
              etContactsSearch.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
            etContactsSearch.setText("");
            return true;
          }
        }
        return false;
      }
    });

  }

  @Override
  protected void injectModule() {
    component = DaggerContactsListComponent.builder()
        .appComponent(((App) getApplication()).getComponent())
        .contactsListModule(new ContactsListModule(this))
        .build();
    component.inject(this);

  }

  private void searchContacts() {
    if (!etContactsSearch.getText().toString().isEmpty()) {
      showProgressBar();
      TextUtils.hideKeyboard(this, etContactsSearch);
      adapter.clearItems();
      presenter.getContactsList(IntranetRepositoryImpl.FIRST_PAGE,
          presenter.formatSearchQuery(etContactsSearch.getText().toString()));
      Answers.getInstance().logSearch(new SearchEvent()
      .putQuery(etContactsSearch.getText().toString())
      .putCustomAttribute("QueryOn", "browser"));
    }
  }

  @Override
  protected ContactsListPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.activity_contacts;
  }

  @Override
  public ContactsListComponent getComponent() {
    return component;
  }

  @Override
  public void loadNewPage(int page) {
    presenter.getContactsList(page, presenter.formatSearchQuery(etContactsSearch.getText().toString()));
  }

  @Override
  public void showProgressBar() {
    loadingDialog = new ProgressDialog(this, R.style.CustomLoadingDialogTheme);
    loadingDialog.setCancelable(true);
    loadingDialog.show();
  }

  @Override
  public void hideProgressBar() {
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        if ((loadingDialog != null) && loadingDialog.isShowing()) {
          loadingDialog.dismiss();
        }
        loadingDialog = null;
      }
    }, 1000);
  }

  @Override
  public void fillContactsList(List<Contact> contact) {
    if (imgContactsFilter != null) imgContactsFilter.setVisibility(View.GONE);
    if (contact.size() > 0) {
      adapter.setItems(contact);
      tvWithoutResults.setVisibility(View.GONE);
    } else {
      if (adapter.getPage() == ContactsListAdapter.FIRST_PAGE) {
        tvWithoutResults.setVisibility(View.VISIBLE);
      }
    }
  }

  @Override
  public void search(String search) {
    presenter.getContactsList(IntranetRepositoryImpl.FIRST_PAGE, search);
  }

  @Override
  public void onContactClick(Contact contact) {
    if (contact != null) {
      Intent contactDetail = new Intent(this, ContactsDetailActivity.class);
      contactDetail.putExtra(EXTRA_DETAILS_CONTACTS, contact);
      startActivity(contactDetail);
    }

  }

  public void requestKeyboard(EditText editText) {
    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
    imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
  }
}
