package com.vectormobile.myvector.ui.base;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

public abstract class BaseFragment<P extends BasePresenter> extends Fragment implements PresenterView {
  Unbinder unbinder;

  @CallSuper
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View rootView = inflater.inflate(getLayoutResourceId(), container, false);
    unbinder = ButterKnife.bind(this, rootView);
    Timber.d("%s::onCreateView", getClass().getSimpleName());
    return rootView;
  }

  @CallSuper
  @Override
  public void onCreate(Bundle bundle) {
    super.onCreate(bundle);
    Timber.d("%s::onCreate", getClass().getSimpleName());
    inject();
    getPresenter().create(bundle);
  }

  protected <C> C getComponent(Class<C> componentType){
    return componentType.cast(((BaseActivity)getActivity()).getComponent());
  }
  protected abstract void inject();

  protected abstract P getPresenter();

  @CallSuper
  @Override
  public void onDestroyView() {
    unbinder.unbind();
    getPresenter().destroy();
    Timber.d("%s::onDestroyView", getClass().getSimpleName());
    super.onDestroyView();
  }

  @Override
  public void onPause() {
    super.onPause();
    Timber.d("%s::onPause", getClass().getSimpleName());
    getPresenter().dropView();
  }

  @Override
  public void onResume() {
    super.onResume();
    Timber.d("%s::onResume", getClass().getSimpleName());
    getPresenter().takeView(this);
  }

  @Override
  public void showError(int errorId) {
    ((BaseActivity) getActivity()).showError(errorId);
  }

  protected abstract int getLayoutResourceId();
}
