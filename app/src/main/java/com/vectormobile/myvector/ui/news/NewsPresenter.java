package com.vectormobile.myvector.ui.news;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.base.BasePresenter;

import javax.inject.Inject;

/**
 * Created by ajruiz on 11/11/2016.
 */
@ActivityScope
public class NewsPresenter extends BasePresenter<NewsView> {

  @Inject
  public NewsPresenter(NewsActivity activity) {
    super(activity);
  }
}
