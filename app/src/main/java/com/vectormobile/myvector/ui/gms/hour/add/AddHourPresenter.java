package com.vectormobile.myvector.ui.gms.hour.add;

import com.vectormobile.myvector.R;
import com.vectormobile.myvector.domain.entity.gms.request.GmsBodyRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsJiraRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.entity.gms.response.Record;
import com.vectormobile.myvector.domain.entity.gms.response.Result;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsBodyRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsJiraRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsRequestInteractor;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.model.gms.Hour;
import com.vectormobile.myvector.ui.base.BasePresenter;
import com.vectormobile.myvector.util.date.DateUtils;
import com.vectormobile.myvector.util.text.TextUtils;
import com.vectormobile.myvector.util.text.TextUtils.Gms;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

/**
 * Created by Erik Medina on 13/01/2017.
 */

public class AddHourPresenter extends BasePresenter<AddHourView> {

  public static final int SAVE_ONLY = 100;
  private static final int SAVE_AND_SEND = 101;

  private static final int ONLY_ONE_ITEM_WAS_AFFECTED = 1;

  private BuildGmsBodyRequestInteractor interactor;
  private BuildGmsRequestInteractor buildGmsRequestInteractor;
  private BuildGmsJiraRequestInteractor buildGmsJiraRequestInteractor;

  private List<Record> workOrders;
  private List<Record> projects;
  private List<Record> categories;
  private String projectSelectedId;
  private String workOrderSelectedId;
  private String categorySelectedId;

  private String hourDateFrom;
  private String hourDateTo;

  private String imputationHours;
  private String imputationExternalCode;
  private String imputationComment;
  private String imputationHoursDateFrom;
  private String imputationHoursDateTo;
  private String imputationCategoryFirstId;
  private String imputationCategorySecondId;
  private String imputationJiraCode;

  private int projectPositionSelected;
  private int workOrderPositionSelected;
  private int categoryPositionSelected;
  private Hour hour;
  private boolean dataChanged;
  private boolean dataSaved = true;
  private final int FIRST_ITEM = 0;
  private final int MINIMUM_HOURS_VALUE = 1;
  private List<Record> jiraCodes;

  @Inject
  public AddHourPresenter(AddHourActivity activity, BuildGmsBodyRequestInteractor interactor,
                          BuildGmsRequestInteractor buildGmsRequestInteractor,
                          BuildGmsJiraRequestInteractor buildGmsJiraRequestInteractor) {
    super(activity);
    this.interactor = interactor;
    this.buildGmsRequestInteractor = buildGmsRequestInteractor;
    this.buildGmsJiraRequestInteractor = buildGmsJiraRequestInteractor;
    workOrders = new ArrayList<>();
    projects = new ArrayList<>();
    categories = new ArrayList<>();
  }

  public void onCalendarIconClicked(int calendarType, Calendar date) {
    view.showDatePickerDialog(calendarType, date);
  }

  public void initializeView(int action) {
    setDefaultDateFrom(action);
    view.makeCommentFieldScrollable();
    setDefaultHours(action);
  }

  public void getProjects() {
    GmsBodyRequest gmsBodyRequest = new GmsBodyRequest(getDateTo());
    interactor.execute(TextUtils.Gms.ACTION_IMPUTATION_HOUR_CONTROLLER,
        TextUtils.Gms.IMPUTABLE_PROJECTS, gmsBodyRequest,
        new OnListRetrievedListener<GmsResponse>() {
          @Override
          public void onSuccess(List<GmsResponse> list) {
            if (list.get(FIRST_ITEM).getResult() != null) {
              projects = list.get(FIRST_ITEM).getResult().getRecords();
              view.populateProjects(projects);
              if (hour != null) {
                view.setProjectItem(hour.getProjectName());
              }
            }
          }

          @Override
          public void onError(int errorId) {
          }
        });
  }

  public void getWorkOrders(int projectPositionSelected) {
    workOrders.clear();
    if (projectPositionSelected > -1) {
      projectSelectedId = projects.get(projectPositionSelected).getId();
      GmsBodyRequest gmsBodyRequest = new GmsBodyRequest(getDateTo());
      gmsBodyRequest.setProjectId(projectSelectedId);
      interactor.execute(TextUtils.Gms.ACTION_IMPUTATION_HOUR_CONTROLLER,
          TextUtils.Gms.IMPUTABLE_WORK_ORDER, gmsBodyRequest,
          new OnListRetrievedListener<GmsResponse>() {
            @Override
            public void onSuccess(List<GmsResponse> list) {
              if (list.get(FIRST_ITEM).getResult() != null) {
                workOrders = list.get(FIRST_ITEM).getResult().getRecords();
                view.populateWorkOrders(workOrders);
                if (hour != null) {
                  view.setWorkOrderItem(hour.getWorkOrderName());
                }
              }
            }

            @Override
            public void onError(int errorId) {
              view.showError(errorId);
            }
          });
    } else {
      view.populateWorkOrders(workOrders);
    }
  }

  private Calendar getDateTo() {
    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.CALENDAR_DATE_FORMAT, Locale.getDefault());
    try {
      calendar.setTime(sdf.parse(view.getDateFrom()));
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return calendar;
  }

  public void getJiraCodes(int projectPositionSelected) {
    if (projectPositionSelected > -1 && projectPositionSelected < projects.size()) {
      projectSelectedId = projects.get(projectPositionSelected).getId();
      GmsJiraRequest gmsBodyRequest = new GmsJiraRequest();
      gmsBodyRequest.getData().add(projectSelectedId);
      gmsBodyRequest.getData().add("");
      gmsBodyRequest.getData().add("");
      buildGmsJiraRequestInteractor.execute(gmsBodyRequest, new OnListRetrievedListener<GmsResponse>() {
        @Override
        public void onSuccess(List<GmsResponse> list) {
          if (getView() != null) {
            if (list.get(0).getResult().getRecords().size() > 0 &&
                list.get(0).getResult().getRecords().get(0).getText() != null) {
              jiraCodes = list.get(0).getResult().getRecords();
              getView().populateJiraCodes(getJiraNames(jiraCodes));
            } else {
              getView().hideJiraSpinner();
            }
          }
        }

        @Override
        public void onError(int errorId) {
          if (getView() != null) {
            getView().hideJiraSpinner();
          }
        }
      });
    }
  }

  public void getImputationFirstCategory(final int workOrderPositionSelected) {
    if (workOrderPositionSelected > -1 && workOrderPositionSelected < workOrders.size()) {
      categories.clear();
      workOrderSelectedId = String.valueOf(workOrders.get(workOrderPositionSelected).getId());
      GmsBodyRequest gmsBodyRequest = new GmsBodyRequest();
      gmsBodyRequest.setWorkOrderId(workOrderSelectedId);
      gmsBodyRequest.setProjectId(projectSelectedId);
      interactor.execute(TextUtils.Gms.ACTION_IMPUTATION_HOUR_CONTROLLER,
          TextUtils.Gms.METHOD_IMPUTABLE_CATEGORIES_1, gmsBodyRequest,
          new OnListRetrievedListener<GmsResponse>() {
            @Override
            public void onSuccess(List<GmsResponse> list) {
              if (list.get(FIRST_ITEM).getResult() != null) {
                categories = list.get(FIRST_ITEM).getResult().getRecords();
                imputationCategoryFirstId = categories.get(FIRST_ITEM).getId();
                view.populateImputationCategory(categories);
                getImputationSecondCategory(workOrderPositionSelected);
              }
            }

            @Override
            public void onError(int errorId) {
              view.showError(errorId);
            }
          });
    }
  }

  private void getImputationSecondCategory(final int workOrderPositionSelected) {
    GmsBodyRequest gmsBodyRequest = new GmsBodyRequest();
    gmsBodyRequest.setWorkOrderId(workOrderSelectedId);
    gmsBodyRequest.setProjectId(projectSelectedId);
    gmsBodyRequest.setCategoryId(imputationCategoryFirstId);
    interactor.execute(TextUtils.Gms.ACTION_IMPUTATION_HOUR_CONTROLLER,
        TextUtils.Gms.METHOD_IMPUTABLE_CATEGORIES_2, gmsBodyRequest,
        new OnListRetrievedListener<GmsResponse>() {
          @Override
          public void onSuccess(List<GmsResponse> list) {
            if (list.get(FIRST_ITEM).getResult() != null) {
              categories = list.get(FIRST_ITEM).getResult().getRecords();
              if (categories.size() > view.getCategorySecondIdPosition()) {
                imputationCategorySecondId = categories.get(view.getCategorySecondIdPosition())
                    .getId();
              } else {
                imputationCategorySecondId = categories.get(FIRST_ITEM).getId();
              }
              if (workOrderPositionSelected > -1 && categories.size() >= workOrderPositionSelected) {
                view.showImputationCategorySpinner(true);
                view.populateImputationCategory(categories);
              } else {
                view.showImputationCategorySpinner(false);
              }
              if (hour != null) {
                view.setCategoryItem(hour.getCategoryName());
              }
            }
          }

          @Override
          public void onError(int errorId) {
            view.showError(errorId);
          }
        });
  }

  public void updateCategorySecondId() {
    imputationCategorySecondId = categories.get(view.getCategorySecondIdPosition()).getId();
  }

  private boolean isValidFormData() {
    getFormData();
    if (isValidHours()) {
      if (isValidProjectPositionSelected(projectPositionSelected) &&
          isValidWorkOrderPositionSelected(workOrderPositionSelected) &&
          isValidCategoryPositionSelected(categoryPositionSelected)) {
        return true;
      } else {
        view.showMessage(R.string.error_incomplete_data);
        return false;
      }
    } else {
      view.showMessage(R.string.hour_error_max_hours_message);
      return false;
    }
  }

  private boolean isValidProjectPositionSelected(int projectPositionSelected) {
    if (projectPositionSelected > -1) {
      projectSelectedId = projects.get(projectPositionSelected).getId();
      return true;
    }
    return false;
  }

  private boolean isValidWorkOrderPositionSelected(int workOrderPositionSelected) {
    if (workOrderPositionSelected > -1) {
      workOrderSelectedId = workOrders.get(workOrderPositionSelected).getId();
      return true;
    }
    return false;
  }

  private boolean isValidCategoryPositionSelected(int categoryPositionSelected) {
    if (view.isCategoriesSpinnerVisible()) {
      if (categoryPositionSelected > -1) {
        categorySelectedId = categories.get(categoryPositionSelected).getId();
        return true;
      }
      return false;
    }
    return true;
  }

  private boolean isValidHours() {
    if (imputationHours != null && !imputationHours.isEmpty()) {
      int hours = Integer.parseInt(imputationHours);
      if (hours <= 8 && hours >= 1) {
        return true;
      }
    }
    return false;
  }

  private boolean isValidDate(String date) {
    return date != null && !date.isEmpty();
  }

  public void saveImputation(final int action) {
    view.showProgressBar();
    GmsBodyRequest gmsBodyRequest = createHoursImputationRequest();
    String countAction = TextUtils.Gms.ACTION_IMPUTATION_HOUR_CONTROLLER;
    final String method;
    if (dataChanged && hour != null) {
      if ("".equals(this.hour.getId())) {
        method = TextUtils.Gms.METHOD_CREATE;
      } else {
        method = TextUtils.Gms.METHOD_UPDATE;
      }
    } else {
      method = TextUtils.Gms.METHOD_CREATE;
    }
    if (gmsBodyRequest != null) {
      interactor.execute(countAction, method, gmsBodyRequest, new OnListRetrievedListener<GmsResponse>() {
            @Override
            public void onSuccess(List<GmsResponse> list) {
              if (getView() != null) {
                getView().hideProgressBar();
                if (method.equals(TextUtils.Gms.METHOD_CREATE)) {
                  dataSaved = true;
                  if (action == SAVE_AND_SEND)
                    sendImputation(Collections.singletonList(hour.getId()));
                  else
                    getView().returnResult(Gms.IMPUTATION_SAVED, hourDateFrom, ONLY_ONE_ITEM_WAS_AFFECTED);
                } else if (method.equals(TextUtils.Gms.METHOD_UPDATE)) {
                  dataChanged = false;
                  if (action == SAVE_AND_SEND)
                    sendImputation(Collections.singletonList(hour.getId()));
                  else
                    getView().returnResult(TextUtils.Gms.IMPUTATION_UPDATED, hourDateFrom, ONLY_ONE_ITEM_WAS_AFFECTED);
                }
              }
            }

            @Override
            public void onError(int errorId) {
              if (getView() != null) {
                getView().hideProgressBar();
                getView().showError(errorId);
              }
            }
          }
      );
    } else {
      view.hideProgressBar();
    }
  }

  private GmsBodyRequest createHoursImputationRequest() {
    GmsBodyRequest gmsBodyRequest = null;
    if (isValidFormData()) {
      gmsBodyRequest = new GmsBodyRequest();
      if (isValidDate(imputationHoursDateTo)) {
        gmsBodyRequest.setImputationType(TextUtils.Gms.IMPUTATION_MASSIVE);
        gmsBodyRequest.setStartDate(imputationHoursDateFrom);
        gmsBodyRequest.setEndDate(imputationHoursDateTo);
      } else {
        gmsBodyRequest.setImputationType(TextUtils.Gms.IMPUTATION_INDIVIDUAL);
        gmsBodyRequest.setDate(imputationHoursDateFrom);
      }
      if (hour != null) {
        gmsBodyRequest.setId(hour.getId());
      }
      gmsBodyRequest.setProjectId(projectSelectedId);
      gmsBodyRequest.setUnits(imputationHours);
      gmsBodyRequest.setWorkOrderId(workOrderSelectedId);
      gmsBodyRequest.setCategoryId(imputationCategoryFirstId);
      gmsBodyRequest.setCategory2Id(imputationCategorySecondId);
      gmsBodyRequest.setExternalCode(imputationExternalCode);
      gmsBodyRequest.setImputationComment(imputationComment);
      gmsBodyRequest.setJiraImputation(imputationJiraCode);
    }
    return gmsBodyRequest;
  }

  private void getFormData() {
    hourDateFrom = DateUtils.formatCalendarDateToGmsShortDate(view.getHourImputationDateFrom());
    hourDateTo = DateUtils.formatCalendarDateToGmsShortDate(view.getHourImputationDateTo());
    imputationHours = view.getHours();
    imputationExternalCode = view.getExternalCode();
    imputationComment = view.getComment();
    projectPositionSelected = view.getProjectPositionSelected();
    workOrderPositionSelected = view.getWorkOrderPositionSelected();
    imputationJiraCode = view.getJiraCode();
    if (view.isCategoriesSpinnerVisible()) {
      categoryPositionSelected = view.getCategoryPositionSelected();
    }
    imputationHoursDateFrom = DateUtils
        .formatCalendarDateToGmsShortDate(view.getHourImputationDateFrom());
    imputationHoursDateTo = DateUtils
        .formatCalendarDateToGmsShortDate(view.getHourImputationDateTo());
  }

  public void sendImputation(final List<String> imputationIdsList) {
    if (!dataChanged) {
      view.showProgressBar();
      GmsRequest<List<String>> gmsRequest = new GmsRequest<>();
      gmsRequest.setAction(Gms.ACTION_IMPUTATION_HOUR_CONTROLLER);
      gmsRequest.setMethod(Gms.METHOD_SEND);
      gmsRequest.getData().add(imputationIdsList);
      buildGmsRequestInteractor.execute(gmsRequest, new OnListRetrievedListener<GmsResponse>() {
        @Override
        public void onSuccess(List<GmsResponse> list) {
          if (getView() != null) {
            getView().hideProgressBar();
            Result result = list.get(FIRST_ITEM).getResult();
            if (result != null) {
              if (result.isSuccess()) {
                hourDateFrom = DateUtils
                    .formatCalendarDateToGmsShortDate(view.getHourImputationDateFrom());
                getView().returnResult(Gms.IMPUTATION_SENT, hourDateFrom, imputationIdsList.size());
              } else {
                getView().showDialogMessage(result.getMessage());
              }
            }
          } else {
            view.showError(R.string.error_gms_send_failure);
          }
        }

        @Override
        public void onError(int errorId) {
          if (getView() != null) {
            getView().hideProgressBar();
            getView().showError(errorId);
          }
        }
      });
    } else {
      saveImputation(SAVE_AND_SEND);
    }
  }

  public void removeImputation(final List<String> imputationIdsList) {
    view.showProgressBar();
    final GmsRequest<List<String>> gmsRequest = new GmsRequest<>();
    gmsRequest.setAction(TextUtils.Gms.ACTION_IMPUTATION_HOUR_CONTROLLER);
    gmsRequest.setMethod(TextUtils.Gms.METHOD_REMOVE);
    gmsRequest.getData().add(imputationIdsList);
    buildGmsRequestInteractor.execute(gmsRequest, new OnListRetrievedListener<GmsResponse>() {
      @Override
      public void onSuccess(List<GmsResponse> list) {
        if (getView() != null) {
          getView().hideProgressBar();
          Result result = list.get(FIRST_ITEM).getResult();
          if (result != null) {
            if (result.isSuccess()) {
              hourDateFrom = DateUtils
                  .formatCalendarDateToGmsShortDate(view.getHourImputationDateFrom());
              getView().returnResult(TextUtils.Gms.IMPUTATION_REMOVED, hourDateFrom, imputationIdsList.size());
            }
          }
        } else {
          view.showError(R.string.error_gms_eliminate_failure);
        }
      }

      @Override
      public void onError(int errorId) {
        if (getView() != null) {
          getView().hideProgressBar();
          getView().showError(errorId);
        }
      }
    });
  }

  void compareDates(int calendarType, String stringFrom, String stringTo) {
    if (stringTo != null && !stringTo.isEmpty()) {
      SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.CALENDAR_DATE_FORMAT,
          Locale.getDefault());
      try {
        Date dateFrom = sdf.parse(stringFrom);
        Date dateTo = sdf.parse(stringTo);
        if (calendarType == TextUtils.Gms.CALENDAR_EXPENSE_FROM) {
          if (dateFrom.after(dateTo)) {
            view.setDateTo(stringFrom);
          }
        } else if (calendarType == TextUtils.Gms.CALENDAR_EXPENSE_TO) {
          if (dateTo.before(dateFrom)) {
            view.setDateFrom(stringTo);
          }
        }
      } catch (ParseException e) {
        e.printStackTrace();
      }
    }
  }

  public void fillForm(int action, Hour hour) {
    if (action != AddHourActivity.CREATE_ACTION) {
      this.hour = hour;

      setDefaultDateFrom(action);
      getProjects();
      view.setHours(hour.getUnits());
      view.setComment(hour.getComment());
    }
    if (action == AddHourActivity.DUPLICATE_ACTION) {
      this.hour.setId("");
    }
  }

  private void setDefaultDateFrom(int action) {
    if (action == AddHourActivity.CREATE_ACTION) {
      view.setDefaultDate(DateUtils.getCalendarFormattedCurrentDate());
    } else {
      view.setDefaultDate(TextUtils.capitalizeFirstLetter(DateUtils.formatDateToLargeString(hour.getDateFrom())));
    }
  }

  private void setDefaultHours(int action) {
    if (action == AddHourActivity.CREATE_ACTION) {
      view.setMinimumValueForHours(MINIMUM_HOURS_VALUE);
      view.setHours(MINIMUM_HOURS_VALUE);
    } else {
      view.setHours(hour.getUnits());
    }
  }

  public void dataChanged() {
    dataChanged = true;
  }

  public boolean isDataChanged() {
    return dataChanged;
  }

  public boolean isDataSaved() {
    return dataSaved;
  }

  public void setDataNotSaved() {
    dataSaved = false;
  }

  private List<Record> getJiraNames(List<Record> records) {
    List<Record> namesList = new ArrayList<>(records.size());
    for (Record record : records) {
      Record jira = new Record();
      jira.setId(record.getId());
      jira.setText(record.getText());
      namesList.add(jira);
    }
    return namesList;
  }

  public String getProjectSelectedId() {
    return projectSelectedId;
  }

  public void setUpActionBarTitle(int action) {
    switch (action) {
      case AddHourActivity.DUPLICATE_ACTION:
        view.setTitle(R.string.hour_duplicate_hour_toolbar_title);
        break;

      case AddHourActivity.UPDATE_ACTION:
        view.setTitle(R.string.hour_edit_hour_toolbar_title);
        break;
    }
  }

  public void shouldShowHiddenButtons(int action) {
    if (action == AddHourActivity.UPDATE_ACTION)
      view.showHiddenButtons();
  }

  public void shouldShowDuplicateButton(int action) {
    if (action == AddHourActivity.UPDATE_ACTION) {
      view.showDuplicateButton();
    }
  }
}
