package com.vectormobile.myvector.ui.news.news;

import com.vectormobile.myvector.model.news.NewsItem;
import com.vectormobile.myvector.ui.base.PresenterView;

import java.util.List;

/**
 * Created by ajruiz on 11/11/2016.
 */

public interface NewsView extends PresenterView {

  void fillNewsList(List<NewsItem> newsItem);

  void loadNewPage(int page);

  void createLoadingDialog();

  void hideLoadingDialog();

}
