package com.vectormobile.myvector.ui.browser.detail;

import com.vectormobile.myvector.R;
import com.vectormobile.myvector.model.UserAttributes;
import com.vectormobile.myvector.model.intranet.Contact;
import com.vectormobile.myvector.ui.base.BasePresenter;
import com.vectormobile.myvector.util.AppResources;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by ajruiz on 03/02/2017.
 */

public class ContactsDetailPresenter extends BasePresenter<ContactsDetailView> {

  private static List<UserAttributes> userAttributes;

  private AppResources appResource;

  @Inject
  public ContactsDetailPresenter(ContactsDetailActivity activity, AppResources appResource) {
    super(activity);
    userAttributes = new ArrayList<>();
    this.appResource = appResource;
  }

  public List<UserAttributes> setUserAttributes(Contact contact) {
    userAttributes.clear();
    userAttributes.add(new UserAttributes(appResource.getString(R.string.profile_email), contact.getEmail(), R.drawable.ic_email, true));
    if (contact.getPhone() == null) {
      userAttributes.add(new UserAttributes(appResource.getString(R.string.profile_phone),
          appResource.getString(R.string.error_not_available), R.drawable.ic_phone, false));
    } else {
      userAttributes.add(new UserAttributes(appResource.getString(R.string.profile_phone), contact.getPhone(), R.drawable.ic_phone, true));
    }
    if (contact.getCellphone() == null || contact.getCellphone().matches("")) {
      userAttributes.add(new UserAttributes(appResource.getString(R.string.profile_mobile),
          appResource.getString(R.string.error_not_available), R.drawable.ic_mobile, false));
    } else {
      userAttributes.add(new UserAttributes(appResource.getString(R.string.profile_mobile), contact.getCellphone(), R.drawable.ic_mobile, true));
    }
    userAttributes.add(new UserAttributes(appResource.getString(R.string.profile_area), contact.getArea(), R.drawable.ic_area, false));
    userAttributes.add(new UserAttributes(appResource.getString(R.string.profile_center), contact.getCity(), R.drawable.ic_center, false));

    return userAttributes;
  }

  public String formatShareContactInfo(Contact contact) {
    StringBuilder stringBuilder = new StringBuilder();
    if (contact.getName() != null) {
      stringBuilder.append(contact.getName()).append("\n");
    }
    if (contact.getEmail() != null) {
      stringBuilder.append(appResource.getString(R.string.profile_email)).append(
          ": ").append(contact.getEmail()).append("\n");
    }
    if (contact.getPhone() != null) {
      stringBuilder.append(appResource.getString(R.string.profile_phone)).append(
          ": ").append(contact.getPhone()).append("\n");
    }
    if (contact.getCellphone() != null) {
      stringBuilder.append(appResource.getString(R.string.profile_mobile)).append(
          ": ").append(contact.getCellphone()).append("\n");
    }
    return stringBuilder.toString();
  }
}
