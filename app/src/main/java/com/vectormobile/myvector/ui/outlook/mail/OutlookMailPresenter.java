package com.vectormobile.myvector.ui.outlook.mail;

import com.vectormobile.myvector.ui.base.BasePresenter;

import javax.inject.Inject;

/**
 * Created by amarina on 13/03/2017.
 */
public class OutlookMailPresenter extends BasePresenter<OutlookMailView> {
  @Inject
  public OutlookMailPresenter(OutlookMailActivity activity) {
    super(activity);
  }
}
