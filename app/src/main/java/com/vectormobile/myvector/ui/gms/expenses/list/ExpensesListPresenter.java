package com.vectormobile.myvector.ui.gms.expenses.list;

import static com.vectormobile.myvector.util.date.DateUtils.formatDateToCompactString;

import android.net.ParseException;
import com.bignerdranch.android.multiselector.MultiSelector;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.domain.entity.gms.request.GmsDayStateRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsRequest;
import com.vectormobile.myvector.domain.entity.gms.request.GmsTimeLapseExpenseBodyRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsDayStateResult;
import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.entity.gms.response.Record;
import com.vectormobile.myvector.domain.entity.gms.response.Result;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsDayStateRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.BuildGmsTimeLapseExpenseRequestInteractor;
import com.vectormobile.myvector.domain.interactor.gms.SharedPdfImputationInteractor;
import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.model.gms.Expense;
import com.vectormobile.myvector.storage.PreferencesManager;
import com.vectormobile.myvector.ui.base.BasePresenter;
import com.vectormobile.myvector.ui.gms.expenses.list.adapter.ExpenseAdapter;
import com.vectormobile.myvector.util.AppResources;
import com.vectormobile.myvector.util.date.DateUtils;
import com.vectormobile.myvector.util.text.TextUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

/**
 * Created by kurt on 05/12/16.
 */

public class ExpensesListPresenter extends BasePresenter<ExpensesListView> {

  public static final int MONTH_BOUND_STARTS = 0;
  public static final int MONTH_BOUND_ENDS = 1;

  public static final int WEEK_TIME_LAPSE = 0;
  public static final int MONTH_TIME_LAPSE = 1;
  public static final int TODAY_TIME_LAPSE = 2;

  public static final int FIRST_ITEM = 0;

  private AppResources appResources;
  private PreferencesManager preferencesManager;

  private BuildGmsTimeLapseExpenseRequestInteractor interactor;
  private BuildGmsRequestInteractor buildGmsRequestInteractor;
  private BuildGmsDayStateRequestInteractor dayStateRequestInteractor;
  private SharedPdfImputationInteractor sharedPdfImputationInteractor;
  private String lastDateFrom;
  private String lastDateTo;
  private int lastTimeLapse;
  private int lastPage;

  @Inject
  public ExpensesListPresenter(ExpensesListActivity view,
                               BuildGmsTimeLapseExpenseRequestInteractor interactor,
                               BuildGmsRequestInteractor buildGmsRequestInteractor,
                               BuildGmsDayStateRequestInteractor dayStateRequestInteractor,
                               PreferencesManager preferencesManager,
                               AppResources appResources,
                               SharedPdfImputationInteractor sharedPdfImputationInteractor) {
    super(view);
    this.interactor = interactor;
    this.buildGmsRequestInteractor = buildGmsRequestInteractor;
    this.dayStateRequestInteractor = dayStateRequestInteractor;
    this.preferencesManager = preferencesManager;
    this.appResources = appResources;
    this.sharedPdfImputationInteractor = sharedPdfImputationInteractor;
  }

  public void miniCalendarButtonHasBeenClicked(boolean isMiniCalendarOpen) {
    if (getView() != null) {
      if (!isMiniCalendarOpen) {
        getView().showMiniCalendarView();
      } else {
        getView().hideMiniCalendarView();
      }
    }
  }

  public void onMiniCalendarDayClick(Date dateClicked, List<Event> events) {
    if (isToday(dateClicked)) {
      getTodayExpenses();
    } else {
      List<String> weekBounds = buildWeekBounds(dateClicked);
      getTimeLapseExpenses(
          weekBounds.get(DateUtils.WEEK_BOUND_STARTS),
          weekBounds.get(DateUtils.WEEK_BOUND_ENDS),
          WEEK_TIME_LAPSE);
    }
    view.hideMiniCalendarView();
  }

  public void monthTitleHasBeenClicked(String month) {
    if (!month.equalsIgnoreCase(appResources.getString(R.string.menu_item_expense_imputation))) {
      view.hideMiniCalendarView();
      view.showProgressBar();
      List<String> monthBounds = DateUtils.buildMonthBounds(month);
      getTimeLapseExpenses(
          monthBounds.get(MONTH_BOUND_STARTS),
          monthBounds.get(MONTH_BOUND_ENDS),
          MONTH_TIME_LAPSE);
    }
  }

  private boolean isToday(Date date) {
    return formatDateToCompactString(date, DateUtils.NO_ITEMS_GMS_DATE_FORMAT)
        .equals(formatDateToCompactString(new Date(), DateUtils.NO_ITEMS_GMS_DATE_FORMAT));
  }

  private List<String> buildWeekBounds(Date date) {
    return DateUtils.getStringWeekBounds(date, DateUtils.GMS_SHORT_DATE_FORMAT);
  }

  void getTodayExpenses() {
    view.showProgressBar();
    String today = formatDateToCompactString(new Date(), DateUtils.GMS_SHORT_DATE_FORMAT);
    getTimeLapseExpenses(today, today, TODAY_TIME_LAPSE);
  }

  void getWeekExpenses(String sDate) {
    view.showProgressBar();
    Date date = DateUtils.parseStringToDate(sDate, DateUtils.GMS_SHORT_DATE_FORMAT);
    if (date != null) {
      List<String> weekBounds = buildWeekBounds(date);
      getTimeLapseExpenses(
          weekBounds.get(DateUtils.WEEK_BOUND_STARTS),
          weekBounds.get(DateUtils.WEEK_BOUND_ENDS),
          WEEK_TIME_LAPSE);
    }
  }

  void getCurrentMonthExpenses() {
    view.showProgressBar();
    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.MONTH_DATE_FORMAT, Locale.getDefault());
    String month = sdf.format(calendar.getTime());
    List<String> monthBounds = DateUtils.buildMonthBounds(month);
    getTimeLapseExpenses(
        monthBounds.get(MONTH_BOUND_STARTS),
        monthBounds.get(MONTH_BOUND_ENDS),
        MONTH_TIME_LAPSE);
  }

  public void getSelectedMonthExpenses(Date firstDayOfNewMonth) {
    view.showProgressBar();
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(firstDayOfNewMonth);
    SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.MONTH_DATE_FORMAT, Locale.getDefault());
    String month = sdf.format(calendar.getTime());
    List<String> monthBounds = DateUtils.buildMonthBounds(month);
    getTimeLapseExpenses(
        monthBounds.get(MONTH_BOUND_STARTS),
        monthBounds.get(MONTH_BOUND_ENDS),
        MONTH_TIME_LAPSE);
  }

  private void getTimeLapseExpenses(final String dateFrom, final String dateTo, final int timeLapse) {
    if (areDifferentBounds(dateFrom, dateTo)) {
      view.resetExpenses(lastPage);
      lastPage = 1;
      lastDateFrom = dateFrom;
      lastDateTo = dateTo;
      lastTimeLapse = timeLapse;
    }
    GmsTimeLapseExpenseBodyRequest bodyRequest = new GmsTimeLapseExpenseBodyRequest(lastDateFrom, lastDateTo);
    bodyRequest.setPage(lastPage);
    interactor.execute(TextUtils.Gms.ACTION_IMPUTATION_EXPENSE_CONTROLLER,
        TextUtils.Gms.METHOD_LIST, bodyRequest, new OnListRetrievedListener<GmsResponse>() {
          @Override
          public void onSuccess(List<GmsResponse> list) {
            if (getView() != null) {
              getView().hideProgressBar();
              if (areThereExpenses(list)) {
                getView().resetExpenses(lastPage);
                getView().setExpensesList(buildExpensesList(list));
                getView().loadExpenses();
                try {
                  getView().loadMiniCalendarEventsArray();
                } catch (ParseException e) {
                  e.printStackTrace();
                }
              } else {
                if (lastPage == 1) {
                  getView().hideTutorial();
                  getView().showNoExpensesView(lastDateFrom, lastDateTo, lastTimeLapse);
                }
                lastPage = 1;
              }
            }
          }

          @Override
          public void onError(int errorId) {
            if (getView() != null) {
              getView().showMessage(errorId);
              getView().hideProgressBar();
            }
          }
        });
  }

  private boolean areDifferentBounds(String dateFrom, String dateTo) {
    return !dateFrom.equals(lastDateFrom) || !dateTo.equals(lastDateTo);
  }

  private boolean areThereExpenses(List<GmsResponse> list) {
    return !(list.get(0)).getResult().getRecords().isEmpty();
  }

  private List<Expense> buildExpensesList(List<GmsResponse> list) {
    List<Expense> expenses = new ArrayList<>();
    for (Record record : list.get(0).getResult().getRecords()) {
      Expense expense = new Expense(
          record.getId(),
          record.getExpenseDate(),
          DateUtils.convertMillisecondsToDate(record.getExpenseDate()),
          DateUtils.convertMillisecondsToDate(record.getDateTo()),
          record.getReportCategoryName(),
          String.valueOf(record.getAmount()),
          false,
          record.getState(),
          record.getWorkOrderName(),
          record.getProjectName(),
          record.getExpenseCategoryName(),
          record.getUnits(),
          record.getUnitPrice(),
          record.getCurrencyId(),
          record.getImputatorComment(),
          record.getWeekFrom(),
          record.getWeekTo(),
          record.getApproverComment()
      );
      expenses.add(expense);
    }
    return expenses;
  }

  public void sendExpenses(final List<String> expenseIdList) {
    view.showMassiveLoading();
    GmsRequest<List<String>> gmsRequest = new GmsRequest<>();
    gmsRequest.setAction(TextUtils.Gms.ACTION_IMPUTATION_EXPENSE_CONTROLLER);
    gmsRequest.setMethod(TextUtils.Gms.METHOD_SEND);
    gmsRequest.getData().add(expenseIdList);
    buildGmsRequestInteractor.execute(gmsRequest, new OnListRetrievedListener<GmsResponse>() {
      @Override
      public void onSuccess(List<GmsResponse> list) {
        Result result = list.get(FIRST_ITEM).getResult();
        view.hideMassiveLoading();
        if (result != null) {
          if (result.isSuccess()) {
            view.setAdapterItemsSent();
            view.finishMultiSelectionMode();
            view.showMessage(appResources.getQuantityString(R.plurals.expense_message_sent_expense, expenseIdList.size()));
          }
        } else {
          view.showError(R.string.error_gms_send_failure);
        }
      }

      @Override
      public void onError(int errorId) {
        view.showError(errorId);
        view.hideMassiveLoading();
      }
    });
  }

  public void removeExpenses(final List<String> expenseIdList) {
    view.showMassiveLoading();
    GmsRequest<List<String>> gmsRequest = new GmsRequest<>();
    gmsRequest.setAction(TextUtils.Gms.ACTION_IMPUTATION_EXPENSE_CONTROLLER);
    gmsRequest.setMethod(TextUtils.Gms.METHOD_REMOVE);
    gmsRequest.getData().add(expenseIdList);
    buildGmsRequestInteractor.execute(gmsRequest, new OnListRetrievedListener<GmsResponse>() {
      @Override
      public void onSuccess(List<GmsResponse> list) {
        Result result = list.get(FIRST_ITEM).getResult();
        view.hideMassiveLoading();
        if (result != null) {
          if (result.isSuccess()) {
            view.removeAdapterItems();
            view.finishMultiSelectionMode();
            view.showMessage(appResources.getQuantityString(R.plurals.expense_message_removed_expense, expenseIdList.size()));
          }
        } else {
          view.showError(R.string.error_gms_eliminate_failure);
          view.hideMassiveLoading();
        }
      }

      @Override
      public void onError(int errorId) {
        view.showError(errorId);
      }
    });
  }

  public void fabClicked(boolean isMultiSelectionMode, MultiSelector multiSelector,
                         ExpenseAdapter expenseAdapter) {
    if (!isMultiSelectionMode) {
      view.goToAddExpense();
    } else {
      List<String> expenseIdList = new ArrayList<>();
      for (int position : multiSelector.getSelectedPositions()) {
        expenseIdList.add(expenseAdapter.getItemsToShow().get(position).getId());
      }
      sendExpenses(expenseIdList);
    }
  }

  public void onExpenseSaved() {
    view.showMessage(R.string.expense_message_saved_expense);
  }

  public void onExpenseUpdated() {
    view.showMessage(R.string.expense_message_updated_expense);
  }

  public void onExpenseSent(int itemCounter) {
    view.showMessage(appResources.getQuantityString(R.plurals.expense_message_sent_expense, itemCounter));
  }

  public void onExpenseRemoved(int itemCounter) {
    view.showMessage(appResources.getQuantityString(R.plurals.expense_message_removed_expense, itemCounter));
  }

  public void onExpenseDuplicated() {
    view.showMessage(R.string.expense_message_duplicated_expense);
  }

  public void getNextPageExpenses(int page) {
    lastPage = page;
    getTimeLapseExpenses(lastDateFrom, lastDateTo, lastTimeLapse);
  }

  public void showTutorial() {
    if (!preferencesManager.retrieve(PreferencesManager.KEY_MUST_HIDE_RECYCLER_VIEW_TUTORIAL, false)) {
      view.showTutorial();
    }
  }

  public void closeTutorial() {
    preferencesManager.persist(PreferencesManager.KEY_MUST_HIDE_RECYCLER_VIEW_TUTORIAL, true);
    view.hideTutorial();
  }

  public void getSelectedDaysState(Date firstDayOfNewMonth) {
    view.showProgressBar();
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(firstDayOfNewMonth);

    SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.GMS_STATE_DATE_FORMAT, Locale.getDefault());
    String monthStarts = sdf.format(calendar.getTime());

    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
    String monthEnds = sdf.format(calendar.getTime());

    getTimeLapseDaysState(monthStarts, monthEnds);
  }

  private void getTimeLapseDaysState(String starts, String ends) {
    GmsDayStateRequest bodyRequest = new GmsDayStateRequest(starts, ends);
    dayStateRequestInteractor.execute(bodyRequest, new OnItemRetrievedListener<GmsDayStateResult>() {
      @Override
      public void onSuccess(GmsDayStateResult result) {
        if (getView() != null) {
          getView().hideProgressBar();
          getView().decorateMiniCalendar(result);
        }
      }

      @Override
      public void onError(int errorId) {
        if (getView() != null) {
          getView().showMessage(errorId);
          getView().hideProgressBar();
        }
      }
    });
  }
}
