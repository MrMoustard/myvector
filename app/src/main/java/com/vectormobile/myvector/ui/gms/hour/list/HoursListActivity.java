package com.vectormobile.myvector.ui.gms.hour.list;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.bignerdranch.android.multiselector.MultiSelector;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.DaggerHoursListComponent;
import com.vectormobile.myvector.di.component.HoursListComponent;
import com.vectormobile.myvector.di.module.HoursListModule;
import com.vectormobile.myvector.model.gms.Hour;
import com.vectormobile.myvector.ui.base.BaseActivity;
import com.vectormobile.myvector.ui.gms.expenses.list.ExpensesListPresenter;
import com.vectormobile.myvector.ui.gms.expenses.list.adapter.OnItemClickListener;
import com.vectormobile.myvector.ui.gms.hour.add.AddHourActivity;
import com.vectormobile.myvector.ui.gms.hour.detail.HourDetailActivity;
import com.vectormobile.myvector.ui.gms.hour.list.adapter.HourAdapter;
import com.vectormobile.myvector.util.date.DateUtils;
import com.vectormobile.myvector.util.text.TextUtils;
import com.vectormobile.myvector.util.widget.EndlessRecyclerViewScrollListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by ajruiz on 24/01/2017.
 */

public class HoursListActivity extends BaseActivity<HoursListPresenter>
    implements HoursListView, OnItemClickListener<Hour> {

  public static final String EXTRA_DETAILS_HOUR_DATE = "date";
  public static final String EXTRA_DETAILS_ITEM_COUNTER = "items_counter";

  private static final int ADD_HOUR_REQUEST = 1;
  public static final int UPDATE_HOUR_REQUEST = 2;
  public static final int DETAIL_HOUR_REQUEST = 3;
  public static final int DUPLICATE_HOUR_REQUEST = 4;

  private static final String OPERATION_FINALIZED = "operationFinalized";
  private static final String UPDATED_HOUR = "updated";
  private static final String SENT_HOUR = "sent";
  private static final String REMOVED_HOUR = "removed";

  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.ly_toolbar)
  LinearLayout secondToolbar;
  @BindView(R.id.b_action_mode_remove)
  ImageView bActionModeRemove;
  @BindView(R.id.toolbar_action_mode)
  LinearLayout toolbarActionMode;
  @BindView(R.id.tv_section_title)
  TextView sectionTitle;
  @BindView(R.id.mini_calendar_view)
  CompactCalendarView calendarView;
  @BindView(R.id.fab_add_item)
  FloatingActionButton fab;
  @BindView(R.id.rv_imputations_list)
  RecyclerView rvList;
  @BindView(R.id.rl_tutorial_message)
  RelativeLayout tutorialLayout;
  @Inject
  HoursListPresenter presenter;

  @BindView(R.id.img_mini_calendar)
  ImageView imgMiniCalendar;
  @BindView(R.id.img_toolbar_pdf)
  ImageView ivPdfIcon;
  @BindView(R.id.no_items_text)
  TextView noItemsMessage;
  @BindView(R.id.recent_items)
  Button lastItemsButton;
  @BindView(R.id.no_items)
  LinearLayout noItemsLayout;
  @BindView(R.id.cl_imputation_list)
  CoordinatorLayout clImputationList;
  @BindView(R.id.pb_massive_loading)
  ProgressBar pbMassiveLoading;

  String date = null;
  private HoursListComponent component;
  private List<Hour> hours = new ArrayList<>();
  private Map<String, Event> miniCalendarEventsMap = new HashMap<>();
  private EndlessRecyclerViewScrollListener recyclerScrollListener;
  private HourAdapter hourAdapter;
  private boolean isMiniCalendarOpen = false;
  private List<String> hourIdList;
  private boolean multiSelectionMode = false;
  private MultiSelector multiSelector = new MultiSelector();
  private ProgressDialog loadingDialog;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setUpActionBar();
    setUpMiniCalendarView();
    setUpRecyclerView();
    ivPdfIcon.setVisibility(View.GONE);

    sectionTitle.setText(getString(R.string.hour_list_toolbar_title));
    sectionTitle.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        presenter.monthTitleHasBeenClicked(sectionTitle.getText().toString());
      }
    });
    presenter.getCurrentMonthImputations();
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    String action = null;
    if (resultCode == Activity.RESULT_OK) {
      if (requestCode == ADD_HOUR_REQUEST) {
        date = data.getExtras().getString(EXTRA_DETAILS_HOUR_DATE);
        presenter.onImputationSaved();
      } else if (requestCode == UPDATE_HOUR_REQUEST) {
        action = data.getExtras().getString(OPERATION_FINALIZED);
        date = data.getExtras().getString(EXTRA_DETAILS_HOUR_DATE);
        int itemCounter = data.getExtras().getInt(EXTRA_DETAILS_ITEM_COUNTER);
        if (action != null) {
          if (action.equals(UPDATED_HOUR)) {
            presenter.onImputationUpdated();
          } else if (action.equals(SENT_HOUR)) {
            presenter.onImputationSent(itemCounter);
          } else if (action.equals(REMOVED_HOUR)) {
            presenter.onImputationRemoved(itemCounter);
          } else if (action.equals(AddHourActivity.DUPLICATE)) {
            date = DateUtils.formatCalendarDateToGmsShortDate(date);
            presenter.onImputationDuplicated();
          }
        }
      } else if (requestCode == DETAIL_HOUR_REQUEST) {
        action = data.getExtras().getString(OPERATION_FINALIZED);
        date = data.getExtras().getString(EXTRA_DETAILS_HOUR_DATE);
        if (action != null) {
          if (action.equals(UPDATED_HOUR)) {
            presenter.onImputationUpdated();
          } else if (action.equals(REMOVED_HOUR)) {
            String itemId = data.getExtras().getString(AddHourActivity.EXTRA_DETAILS_ITEM_ID);
            presenter.removeImputation(itemId);
          } else if (action.equals(AddHourActivity.DUPLICATE)) {
            presenter.onImputationDuplicated();
            presenter.getWeekImputations(date);
          }
        }
      }
      if (requestCode != DETAIL_HOUR_REQUEST && action != null && !action.equals(REMOVED_HOUR)) {
        presenter.getWeekImputations(date);
      }
    }
  }

  private void setUpActionBar() {
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);
  }

  private void setUpMiniCalendarView() {
    // show days from other months as greyed out days
    calendarView.displayOtherMonthDays(false);
    calendarView.shouldDrawIndicatorsBelowSelectedDays(true);
    calendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
      @Override
      public void onDayClick(Date dateClicked) {
        presenter.onMiniCalendarDayClick(dateClicked, calendarView.getEvents(dateClicked));
      }

      @Override
      public void onMonthScroll(Date firstDayOfNewMonth) {
        sectionTitle.setText(setMonthNameTitle(firstDayOfNewMonth));
        presenter.getSelectedMonthImputations(firstDayOfNewMonth);
      }
    });
  }

  private void setUpRecyclerView() {
    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
    layoutManager.setAutoMeasureEnabled(true);
    rvList.setLayoutManager(layoutManager);
    rvList.setNestedScrollingEnabled(false);
    rvList.setHasFixedSize(false);
    hourAdapter = new HourAdapter(this, this);
    hourAdapter.setMultiSelector(multiSelector);
    rvList.setAdapter(hourAdapter);
    recyclerScrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
      @Override
      public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
        // Triggered only when new data needs to be appended to the list
        // Add whatever code is needed to append new items to the bottom of the list
        presenter.getNextPageHours(page);
      }
    };
    rvList.addOnScrollListener(recyclerScrollListener);
  }

  @Override
  public void onBackPressed() {
    if (multiSelectionMode) {
      finishMultiSelectionMode();
    } else {
      super.onBackPressed();
    }
  }

  @Override
  protected void injectModule() {
    component = DaggerHoursListComponent.builder()
        .appComponent(((App) getApplication()).getComponent())
        .hoursListModule(new HoursListModule(this))
        .build();
    component.inject(this);
  }

  private String setMonthNameTitle(Date date) {
    SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());
    return TextUtils.capitalizeFirstLetter(dateFormatForMonth.format(date));
  }

  @OnClick(R.id.img_mini_calendar)
  public void miniCalendarButtonHasBeenClicked(View view) {
    hourAdapter.closeSwipedItems();
    presenter.miniCalendarButtonHasBeenClicked(isMiniCalendarOpen);
  }

  @Override
  public void showMiniCalendarView() {
    isMiniCalendarOpen = true;
    calendarView.setVisibility(View.VISIBLE);
    calendarView.showCalendar();
    sectionTitle.setText(setMonthNameTitle(calendarView.getFirstDayOfCurrentMonth()));
  }

  @Override
  public void hideMiniCalendarView() {
    if (isMiniCalendarOpen) {
      isMiniCalendarOpen = false;
      calendarView.hideCalendar();
      sectionTitle.setText(getString(R.string.hour_list_toolbar_title));
    }
  }

  @Override
  public void setHoursList(List<Hour> hours) {
    this.hours = hours;
  }

  @Override
  public void loadHours() {
    hourAdapter.addItems(hours);
    noItemsLayout.setVisibility(View.GONE);
    rvList.setVisibility(View.VISIBLE);
    presenter.showTutorial();
  }

  @Override
  public void showTutorial() {
    tutorialLayout.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideTutorial() {
    tutorialLayout.setVisibility(View.GONE);
  }

  @Override
  public void showMassiveLoading() {
    pbMassiveLoading.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideMassiveLoading() {
    pbMassiveLoading.setVisibility(View.GONE);
  }

  @Override
  public void showNoHoursView(String dateFrom, String dateTo, int timeLapse) {
    if (timeLapse == ExpensesListPresenter.TODAY_TIME_LAPSE) {
      noItemsMessage.setText(String.format(
          Locale.getDefault(),
          getString(R.string.hour_no_items_today_text),
          TextUtils.formatStringDate(dateFrom, DateUtils.NO_ITEMS_GMS_DATE_FORMAT)));

    } else {
      noItemsMessage.setText(String.format(
          Locale.getDefault(),
          getString((timeLapse == ExpensesListPresenter.WEEK_TIME_LAPSE) ?
              R.string.hour_no_items_week_text : R.string.hour_no_items_month_text),
          TextUtils.formatStringDate(dateFrom, DateUtils.NO_ITEMS_GMS_DATE_FORMAT),
          TextUtils.formatStringDate(dateTo, DateUtils.NO_ITEMS_GMS_DATE_FORMAT)));
    }
    noItemsLayout.setVisibility(View.VISIBLE);
    rvList.setVisibility(View.GONE);
  }

  @Override
  public void resetImputations(int page) {
    if (page == 1) {
      hourAdapter.reset();
      recyclerScrollListener.resetState();
    }
  }

  @Override
  public void goToAddImputation() {
    hideMiniCalendarView();
    Intent intent = new Intent(this, AddHourActivity.class);
    intent.putExtra(AddHourActivity.EXTRA_ACTION, AddHourActivity.CREATE_ACTION);
    startActivityForResult(intent, ADD_HOUR_REQUEST);
  }

  @Override
  public void loadMiniCalendarEventsArray() {
    miniCalendarEventsMap.putAll(buildEventsHashMap());
    calendarView.removeAllEvents();

    populateMiniCalendarByState();
    calendarView.invalidate();
  }

  private Map<String, Event> buildEventsHashMap() {
    Map<String, Event> eventHashMap = new HashMap<>();
    for (Hour hour : hours) {
      eventHashMap.put(hour.getId(), new Event(hour.getColour(), hour.getDateFrom().getTime(), hour));
    }
    return eventHashMap;
  }

  private void populateMiniCalendarByState() {
    Map<String, Event> saved = new HashMap<>();
    Map<String, Event> sent = new HashMap<>();
    Map<String, Event> approved = new HashMap<>();
    Map<String, Event> rejected = new HashMap<>();

    for (Map.Entry<String, Event> entry : miniCalendarEventsMap.entrySet()) {
      if (Hour.HOUR_SAVED.equals(((Hour) entry.getValue().getData()).getState())) {
        saved.put(((Hour) entry.getValue().getData()).getDateString(), entry.getValue());
      } else if (Hour.HOUR_SENT.equals(((Hour) entry.getValue().getData()).getState())) {
        sent.put(((Hour) entry.getValue().getData()).getDateString(), entry.getValue());
      } else if (Hour.HOUR_APPROVED.equals(((Hour) entry.getValue().getData()).getState())) {
        approved.put(((Hour) entry.getValue().getData()).getDateString(), entry.getValue());
      } else if (Hour.HOUR_REJECTED.equals(((Hour) entry.getValue().getData()).getState())) {
        rejected.put(((Hour) entry.getValue().getData()).getDateString(), entry.getValue());
      }
    }

    calendarView.addEvents(new ArrayList<>(rejected.values()));
    calendarView.addEvents(new ArrayList<>(saved.values()));
    calendarView.addEvents(new ArrayList<>(sent.values()));
    calendarView.addEvents(new ArrayList<>(approved.values()));
  }

  @Override
  protected HoursListPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.activity_imputation_list;
  }

  @Override
  public HoursListComponent getComponent() {
    return component;
  }

  @OnClick(R.id.recent_items)
  public void onRecentItemsButtonHasBeenClicked() {
    presenter.getCurrentMonthImputations();
  }

  @Override
  public void onItemClick(Hour hour) {
    if (hour.getState().equals(Hour.HOUR_SENT) ||
        hour.getState().equals(Hour.HOUR_APPROVED) ||
        hour.getState().equals(Hour.HOUR_REJECTED)) {
      Intent iHourDetail = new Intent(this, HourDetailActivity.class);
      iHourDetail.putExtra(HourDetailActivity.EXTRA_DETAILS_HOUR, hour);
      startActivityForResult(iHourDetail, DETAIL_HOUR_REQUEST);
    } else {
      Intent iAddHour = new Intent(this, AddHourActivity.class);
      iAddHour.putExtra(AddHourActivity.EXTRA_ACTION, AddHourActivity.UPDATE_ACTION);
      iAddHour.putExtra(AddHourActivity.EXTRA_DETAILS_HOUR, hour);
      startActivityForResult(iAddHour, UPDATE_HOUR_REQUEST);
    }
  }

  public void sendImputation(String id) {
    List<String> idList = new ArrayList<>();
    idList.add(id);
    presenter.sendImputation(idList);
  }

  public void removeImputation(String id) {
    List<String> idList = new ArrayList<>();
    idList.add(id);
    presenter.removeImputations(idList);
  }

  @OnClick(R.id.iv_close_message)
  public void onCloseTutorialButtonHasBeenClicked() {
    presenter.closeTutorial();
  }

  @OnClick(R.id.fab_add_item)
  public void addItemButtonHasBeenClicked(View view) {
    presenter.fabClicked(multiSelectionMode, multiSelector, hourAdapter);
  }

  @Override
  public void refreshList() {
    if (date != null) {
      presenter.getWeekImputations(date);
    } else {
      presenter.getCurrentMonthImputations();
    }
  }

  @Override
  public void removeAdapterItems(List<String> idList) {
    if (multiSelector.getSelectedPositions().size() > 0) {
      hourAdapter.removeItems(multiSelector.getSelectedPositions());
    } else {
      hourAdapter.removeItems(castToIntegerList(idList));
    }
  }

  @Override
  public void removeItemFromAdapter(String hourId) {
    hourAdapter.removeItem(hourId);
  }

  private List<Integer> castToIntegerList(List<String> idList) {
    List<Integer> integerList = new ArrayList<>(idList.size());
    for (String item : idList) {
      integerList.add(Integer.parseInt(item));
    }
    return integerList;
  }

  @Override
  public void setAdapterItemsSent() {
    hourAdapter.setItemsSent(multiSelector.getSelectedPositions());
  }

  @Override
  public void showMessage(int messageId) {
    showMessage(getString(messageId));
  }

  @Override
  public void showMessage(String message) {
    Snackbar.make(clImputationList, message, Snackbar.LENGTH_SHORT).show();
  }

  @Override
  public void showDialogMessage(String message) {
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setMessage(message)
        .setCancelable(false)
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
          }
        });
    AlertDialog alert = builder.create();
    alert.show();
  }

  @Override
  public void showProgressBar() {
    rvList.setVisibility(View.GONE);
    loadingDialog = new ProgressDialog(this, R.style.CustomLoadingDialogTheme);
    loadingDialog.setCancelable(true);
    loadingDialog.show();
  }

  @Override
  public void hideProgressBar() {
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        if ((loadingDialog != null) && loadingDialog.isShowing()) {
          loadingDialog.dismiss();
        }
        loadingDialog = null;
      }
    }, 1000);
    rvList.setVisibility(View.VISIBLE);
  }

  @OnClick(R.id.b_action_mode_remove)
  public void onRemoveButtonClicked() {
    hourIdList = new ArrayList<>();
    for (int position : multiSelector.getSelectedPositions()) {
      hourIdList.add(hourAdapter.getItemsToShow().get(position).getId());
    }
    presenter.removeImputations(hourIdList);
  }

  public void initializeMultiSelectionMode() {
    multiSelectionMode = true;
    multiSelector.setSelectable(true);
    toolbarActionMode.setVisibility(View.VISIBLE);
    secondToolbar.setVisibility(View.GONE);
    fab.setImageDrawable(ContextCompat.getDrawable(HoursListActivity.this, R.drawable.ic_send));
  }

  @Override
  public void finishMultiSelectionMode() {
    hourAdapter.enableSwipe();
    multiSelectionMode = false;
    multiSelector.clearSelections();
    multiSelector.setSelectable(false);
    toolbarActionMode.setVisibility(View.GONE);
    secondToolbar.setVisibility(View.VISIBLE);
    fab.setImageDrawable(ContextCompat.getDrawable(HoursListActivity.this, R.drawable.ic_add));
  }
}
