package com.vectormobile.myvector.ui.base;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.ui.login.LoginActivity;
import com.vectormobile.myvector.ui.main.MainActivity;

import javax.inject.Inject;

/**
 * VectorDigital.
 *
 * @author -   AMarinaG
 * @since -   28/10/16
 */

public abstract class BaseActivity<P extends BasePresenter> extends AppCompatActivity
    implements PresenterView {

  @Nullable
  @BindView(R.id.toolbar)
  Toolbar toolbar;

  @Inject
  App app;

  private Unbinder unbinder;

  @CallSuper
  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(getLayoutResourceId());
    AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    injectModule();
    unbinder = ButterKnife.bind(this);
    getPresenter().create(savedInstanceState);
  }

  protected abstract void injectModule();

  protected abstract P getPresenter();

  protected abstract int getLayoutResourceId();

  public abstract <C> C getComponent();

  @Optional
  @OnClick(R.id.toolbar)
  public void goToMainActivity() {
    Intent i = new Intent(this, MainActivity.class);
    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    getApplicationContext().startActivity(i);
  }

  @CallSuper
  @Override
  public void onDestroy() {
    super.onDestroy();
    unbinder.unbind();
    app.mustDie(this);
    app.setCurrentActivity(null);
    app.setAppComponent(null);
  }

  @CallSuper
  @Override
  protected void onResume() {
    ((App) getApplication()).setCurrentActivity(this);
    super.onResume();
    getPresenter().takeView(this);
  }

  @CallSuper
  @Override
  protected void onPause() {
    super.onPause();
    getPresenter().dropView();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        onBackPressed();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void showError(int errorId) {
    View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
    Snackbar.make(rootView, errorId, Snackbar.LENGTH_SHORT).show();
  }

  public void forceFinishSession() {
    if (!(this instanceof LoginActivity)) {
      sessionExpiredDialog();
    }
  }

  private void sessionExpiredDialog() {
    this.runOnUiThread(new Runnable() {
      public void run() {
        new AlertDialog.Builder(BaseActivity.this)
            .setTitle(getString(R.string.expired_session_dialog_title))
            .setMessage(getString(R.string.expired_session_dialog_message))
            .setPositiveButton(R.string.expired_session_dialog_button, new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                navigateToLogin();
              }
            })
            .show();
      }
    });
  }

  public void navigateToLogin() {
    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    intent.putExtra("EXIT", true);
    startActivity(intent);
    finishAffinity();
  }
}