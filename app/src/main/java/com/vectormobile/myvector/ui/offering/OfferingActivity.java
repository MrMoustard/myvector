package com.vectormobile.myvector.ui.offering;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.BindView;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.DaggerOfferingComponent;
import com.vectormobile.myvector.di.component.OfferingComponent;
import com.vectormobile.myvector.di.module.OfferingModule;
import com.vectormobile.myvector.ui.base.BaseActivity;
import com.vectormobile.myvector.ui.offering.menu.OfferingMenuFragment;
import com.vectormobile.myvector.ui.offering.news.OfferingNewsFragment;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by ajruiz on 03/11/2016.
 */

public class OfferingActivity extends BaseActivity<OfferingPresenter> implements OfferingView,
    AppBarLayout.OnOffsetChangedListener, OfferingMenuFragment.OnMenuItemSelectedListener,
    OfferingNewsFragment.OnNewsItemSelectedListener {

  public static final int FIRST_PAGE = 1;
  public static final String KEY_OFFERING_SECTION_ID = "OFFERING_SECTION_ID";
  public static final String KEY_OFFERING_SECTION_COLOR = "OFFERING_SECTION_COLOR";
  public static final String KEY_OFFERING_SECTION_TITLE = "OFFERING_SECTION_TITLE";
  public static final String KEY_OFFERING_POST_SELECTED = "OFFERING_POST_SELECTED";

  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.fl_offering_container)
  FrameLayout fragmentContainer;
  @BindView(R.id.appbar)
  AppBarLayout barLayout;
  @BindView(R.id.tv_section_title)
  TextView tvSectionTitle;
  @BindView(R.id.nsv_offering)
  NestedScrollView nsvOffering;

  @Inject
  OfferingPresenter presenter;

  private OfferingComponent component;
  private static Boolean newsOpened = false;
  private static Boolean newsDetailOpened = false;

  private State currentState = State.IDLE;
  OfferingMenuFragment offeringMenuFragment;

  private int menuScrollPosition = 0, newsScrollPosition = 0, toolbarPartialHeight = 0,
      menuToolbarPartialHeight = 0, newsToolbarPartialHeight = 0;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);

    setOfferingComercialFragment();

    barLayout.addOnOffsetChangedListener(this);
  }

  public void setOfferingComercialFragment() {
    offeringMenuFragment = new OfferingMenuFragment();
    getSupportFragmentManager().beginTransaction().replace(R.id.fl_offering_container, offeringMenuFragment).commit();
  }

  @Override
  public void onBackPressed() {
    if (offeringMenuFragment.getOfferingNewsFragment() != null
        && offeringMenuFragment.getOfferingNewsFragment().getOfferingDetailFragment() != null
        && offeringMenuFragment.getOfferingNewsFragment().getOfferingDetailFragment().isVisible()) {
      if (offeringMenuFragment.getOfferingNewsFragment().getOfferingDetailFragment().isFullScreen()) {
        offeringMenuFragment.getOfferingNewsFragment().getOfferingDetailFragment().getYouTubePlayer().setFullscreen(false);
      } else {
        if (offeringMenuFragment.getOfferingNewsFragment().getOfferingDetailFragment().getYouTubePlayer() != null) {
          offeringMenuFragment.getOfferingNewsFragment().getOfferingDetailFragment().getYouTubePlayer().release();
        }
        expandToolbarPartially(newsToolbarPartialHeight);
        scrollTo(newsScrollPosition);
        super.onBackPressed();
      }
    } else if (offeringMenuFragment.getOfferingNewsFragment() != null
        && offeringMenuFragment.getOfferingNewsFragment().isVisible()) {
      expandToolbarPartially(menuToolbarPartialHeight);
      scrollTo(menuScrollPosition);
      super.onBackPressed();
    } else {
      super.onBackPressed();
    }
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        onBackPressed();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  protected void injectModule() {
    component = DaggerOfferingComponent.builder()
        .appComponent(((App) getApplication()).getComponent())
        .offeringModule(new OfferingModule(this))
        .build();
    component.inject(this);
  }

  public static Boolean getNewsOpened() {
    return newsOpened;
  }

  public static void setNewsOpened(Boolean newsOpened) {
    OfferingActivity.newsOpened = newsOpened;
  }

  public static Boolean getNewsDetailOpened() {
    return newsDetailOpened;
  }

  public static void setNewsDetailOpened(Boolean newsDetailOpened) {
    OfferingActivity.newsDetailOpened = newsDetailOpened;
  }

  @Override
  protected OfferingPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.activity_offering;
  }

  @Override
  public OfferingComponent getComponent() {
    return component;
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    List<Fragment> fragments = getSupportFragmentManager().getFragments();
    if (fragments != null) {
      for (Fragment fragment : fragments) {
        fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
      }
    }
  }

  @Override
  public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
    toolbarPartialHeight = verticalOffset;
    if (verticalOffset == 0) {
      if (currentState != State.EXPANDED) {
        tvSectionTitle.setSingleLine(false);
      }
      currentState = State.EXPANDED;
    } else if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
      if (currentState != State.COLLAPSED) {
        tvSectionTitle.setSingleLine(true);
        tvSectionTitle.setEllipsize(TextUtils.TruncateAt.END);
      }
      currentState = State.COLLAPSED;
    } else {
      currentState = State.IDLE;
    }
  }

  @Override
  public void onMenuItemSelected() {
    menuToolbarPartialHeight = toolbarPartialHeight;
    menuScrollPosition = nsvOffering.getScrollY();
    barLayout.setExpanded(true);
    nsvOffering.scrollTo(0, 0);
  }

  @Override
  public void onNewsItemSelected() {
    newsToolbarPartialHeight = toolbarPartialHeight;
    newsScrollPosition = nsvOffering.getScrollY();
    barLayout.setExpanded(true);
    nsvOffering.scrollTo(0, 0);
  }

  private void scrollTo(int scrollPosition) {
    nsvOffering.scrollTo(0, scrollPosition);
  }

  private void expandToolbarPartially(int partialHeight) {
    CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) barLayout.getLayoutParams();
    AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) params.getBehavior();
    if (behavior != null) {
      behavior.setTopAndBottomOffset(partialHeight);
    }
  }

  public enum State {
    EXPANDED,
    COLLAPSED,
    IDLE
  }
}
