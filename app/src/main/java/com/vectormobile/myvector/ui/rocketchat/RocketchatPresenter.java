package com.vectormobile.myvector.ui.rocketchat;

import com.vectormobile.myvector.ui.base.BasePresenter;

import javax.inject.Inject;

/**
 * Created by amarina on 26/01/2017.
 */
public class RocketchatPresenter extends BasePresenter<RocketchatView> {
  @Inject
  public RocketchatPresenter(RocketchatActivity activity) {
    super(activity);
  }
}
