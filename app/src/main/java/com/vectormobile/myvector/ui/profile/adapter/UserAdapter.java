package com.vectormobile.myvector.ui.profile.adapter;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.model.UserAttributes;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ajruiz on 08/11/2016.
 */

public class UserAdapter extends
    RecyclerView.Adapter<UserAdapter.ProfileViewHolder> {

  private static final int PHONE_POSITION = 1;
  private static final int CELLPHONE_POSITION = 2;
  private static final int MAX_PHONE_NUMBER_LENGTH = 15;

  private static List<UserAttributes> items;
  private static List<ProfileViewHolder> views;
  private Context context;
  private final OnUserAttributesClickListener listener;

  public UserAdapter(Context context, OnUserAttributesClickListener listener) {
    this.context = context;
    this.listener = listener;
    items = new ArrayList<>();
    views = new ArrayList<>();
  }

  @Override
  public ProfileViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_profile_row, parent, false);
    return new ProfileViewHolder(v, context);
  }

  @Override
  public void onBindViewHolder(final ProfileViewHolder holder, int position) {
    holder.textInputLayout.setHint(items.get(position).getItemTitle());
    holder.tittle.setText(items.get(position).getItemLabel());
    holder.icon.setImageResource(items.get(position).getIcon());
    if (items.get(position).isEditable()) {
      holder.editable.setVisibility(View.VISIBLE);
    } else {
      holder.editable.setVisibility(View.GONE);
    }
    holder.bind(items.get(position), position, listener);
    views.add(holder);
  }

  @Override
  public void onAttachedToRecyclerView(RecyclerView recyclerView) {
    super.onAttachedToRecyclerView(recyclerView);
  }

  @Override
  public int getItemCount() {
    return items.size();
  }

  public void setItems(List<UserAttributes> userProfile) {
    items.clear();
    items.addAll(userProfile);
    notifyDataSetChanged();
  }

  public void clearArrays() {
    views.clear();
    items.clear();
  }

  public void clearAllItemsViewsFocus() {
    for (int i = 0; i < views.size(); i++) {
      views.get(i).tittle.setEnabled(false);
      views.get(i).tittle.clearFocus();
      views.get(i).textInputLayout.setEnabled(false);
      views.get(i).icon.setBackground(ContextCompat.getDrawable(context, R.drawable.circle_item_blue));
      views.get(i).textInputLayout.setHintTextAppearance(R.style.EditTextProfile);
      if (items.get(i).isEditable()) {
        views.get(i).editable.setVisibility(View.VISIBLE);
      }
    }
  }

  public void setFocus(int position){
    views.get(position).tittle.setEnabled(true);
    views.get(position).tittle.requestFocus();
    views.get(position).textInputLayout.setEnabled(true);
    views.get(position).textInputLayout.setHintTextAppearance(R.style.EditTextProfileActivated);
    views.get(position).icon.setBackground(ContextCompat.getDrawable(context, R.drawable.circle_item_orange));
    views.get(position).editable.setVisibility(View.GONE);
  }

  public String getCellphone() {
    return views.get(CELLPHONE_POSITION).tittle.getText().toString();
  }

  public String getPhone() {
    return views.get(PHONE_POSITION).tittle.getText().toString();
  }

  static class ProfileViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.et_item_profile_title)
    TextInputEditText tittle;
    @BindView(R.id.input_item_profile)
    TextInputLayout textInputLayout;
    @BindView(R.id.img_item_user)
    ImageView icon;
    @BindView(R.id.tv_field_editable)
    TextView editable;
    Context context;

    ProfileViewHolder(View itemView, Context context) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      this.context = context;
    }


    public void disableOtherViews() {
      for (int i = 0; i < views.size(); i++) {
        views.get(i).tittle.setEnabled(false);
        views.get(i).tittle.clearFocus();
        views.get(i).tittle.setSelectAllOnFocus(false);
        views.get(i).icon.setBackground(ContextCompat.getDrawable(context, R.drawable.circle_item_blue));
        if (items.get(i).isEditable()) {
          views.get(i).editable.setVisibility(View.VISIBLE);
          views.get(i).tittle.setSelectAllOnFocus(true);
        }
      }
    }

    public void requestKeyboard(EditText editText) {
      InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
      imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    public void bind(final UserAttributes atributes, final int position, final OnUserAttributesClickListener listener) {
      editable.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          disableOtherViews();
          tittle.setEnabled(true);
          tittle.requestFocus();
          tittle.setFilters(new InputFilter[] {new InputFilter.LengthFilter(MAX_PHONE_NUMBER_LENGTH)});
          textInputLayout.setHintTextAppearance(R.style.EditTextProfileActivated);
          icon.setBackground(ContextCompat.getDrawable(context, R.drawable.circle_item_orange));
          editable.setVisibility(View.GONE);

          requestKeyboard(tittle);

          listener.onItemClick(atributes, position);

          tittle.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
              if (actionId == EditorInfo.IME_ACTION_DONE) {
                listener.onKeyboardActionDoneClicked();
                return true;
              } else {
                return false;
              }
            }
          });

        }
      });

    }


  }

}
