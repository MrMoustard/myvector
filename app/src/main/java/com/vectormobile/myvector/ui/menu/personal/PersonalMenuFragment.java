package com.vectormobile.myvector.ui.menu.personal;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import butterknife.BindView;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.MainComponent;
import com.vectormobile.myvector.model.VectorItemMenu;
import com.vectormobile.myvector.model.menuaction.VectorAction;
import com.vectormobile.myvector.ui.base.BaseMenuFragment;

import java.util.ArrayList;


/**
 * Created by Erik Medina on 27/10/2016.
 */
public class PersonalMenuFragment extends BaseMenuFragment<PersonalMenuPresenter> implements PersonalMenuView {
  @BindView(R.id.rv_personal_menu)
  RecyclerView rvPersonalMenu;
  @BindView(R.id.fragment_container)
  RelativeLayout rlContainer;

  private PersonalMenuPresenter presenter;

  public PersonalMenuFragment() {
    presenter = new PersonalMenuPresenter(this);
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
  }

  @Override
  protected void loadItems() {
    items = new ArrayList<>();
    items.add(new VectorItemMenu(getString(R.string.menu_item_profile), R.drawable.menu_bg_profile, VectorAction.OPEN_PROFILE));
    items.add(new VectorItemMenu(getString(R.string.menu_item_book_room), R.drawable.menu_bg_password, VectorAction.OPEN_ROOM));
    items.add(new VectorItemMenu(getString(R.string.menu_item_time_imputation), R.drawable.menu_bg_time_imputation, VectorAction.OPEN_TIME_IMPUTATION));
    items.add(new VectorItemMenu(getString(R.string.menu_item_expense_imputation), R.drawable.menu_bg_expense_imputation, VectorAction.OPEN_EXPENSE_IMPUTATION));
  }

  @Override
  protected RelativeLayout getRelativeLayoutContainer() {
    return rlContainer;
  }

  @Override
  public RecyclerView getRecyclerView() {
    return rvPersonalMenu;
  }

  @Override
  protected void inject() {
    getComponent(MainComponent.class).inject(this);
  }

  @Override
  protected PersonalMenuPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.fragment_personal_menu;
  }
}

