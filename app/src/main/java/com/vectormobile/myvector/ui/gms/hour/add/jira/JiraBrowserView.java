package com.vectormobile.myvector.ui.gms.hour.add.jira;

import com.vectormobile.myvector.model.gms.Jira;
import com.vectormobile.myvector.ui.base.PresenterView;

import java.util.List;

/**
 * Created by ajruiz on 28/02/2017.
 */

public interface JiraBrowserView extends PresenterView {

  void jiraCodesRetrieved(List<Jira> jiraCodes);

  void messageNoResults();

  void returnResult(String extra, String jira);

  void showMessage(int messageId);

  void showProgressBar();

  void hideProgressBar();

}
