package com.vectormobile.myvector.ui.login;

import com.vectormobile.myvector.ui.base.PresenterView;

/**
 * VectorDigital.
 *
 * @author -   AMarinaG
 * @since -   28/10/16
 */
public interface LoginView extends PresenterView {

  void goToMainActivity();

  void showProgressBar(boolean isShowed);

  void enableLoginButton();

  void showMessage(int messageId);

  void hideBtnLogin();

  void showLoginError();

  void hideKeyboard();

  void loginOnEmbeddedWebViews(String username, String password);
}
