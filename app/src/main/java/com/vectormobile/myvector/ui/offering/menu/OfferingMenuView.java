package com.vectormobile.myvector.ui.offering.menu;

import com.vectormobile.myvector.model.offering.SectionMenu;
import com.vectormobile.myvector.ui.base.PresenterView;

/**
 * Created by ajruiz on 03/11/2016.
 */

public interface OfferingMenuView extends PresenterView {

  void onOfferingMenuRetrieved(SectionMenu sectionMenu);

  void hideLoadingDialog();

}
