package com.vectormobile.myvector.ui.profile.adapter;

import com.vectormobile.myvector.model.UserAttributes;

/**
 * Created by ajruiz on 25/11/2016.
 */

public interface OnUserAttributesClickListener {

  void onItemClick(UserAttributes item, int position);

  void onKeyboardActionDoneClicked();
}
