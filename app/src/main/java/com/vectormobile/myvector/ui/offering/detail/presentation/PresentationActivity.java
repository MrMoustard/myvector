package com.vectormobile.myvector.ui.offering.detail.presentation;

import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import butterknife.BindView;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.DaggerPresentationComponent;
import com.vectormobile.myvector.di.component.PresentationComponent;
import com.vectormobile.myvector.di.module.PresentationModule;
import com.vectormobile.myvector.ui.base.BaseActivity;
import com.vectormobile.myvector.ui.offering.detail.presentation.adapter.ImagePagerAdapter;
import com.vectormobile.myvector.ui.offering.detail.presentation.adapter.MiniaturePresentationAdapter;

import javax.inject.Inject;

/**
 * Created by ajruiz on 22/11/2016.
 */

public class PresentationActivity extends BaseActivity<PresentationPresenter> implements PresentationView,
    View.OnClickListener, ViewPager.OnPageChangeListener {

  @Inject
  PresentationPresenter presenter;

  @BindView(R.id.gallery_top_bar)
  RelativeLayout galleryTopBar;
  @BindView(R.id.view_pager)
  ViewPager viewPager;
  @BindView(R.id.gallery)
  LinearLayout gallery;
  @BindView(R.id.gallery_scroll)
  HorizontalScrollView horizontalScrollView;
  @BindView(R.id.imgBtnClose)
  ImageButton imgBtnBack;

  private PresentationComponent component;

  public static final String GALLERY_IMAGES = "GALLERY_IMAGES";
  public static final String PRESENTATION_PDF_URL = "GALLERY_PDF_URL";

  private float pixels;
  private String[] urls;
  private String url;
  private View previusMiniatureSelected;
  private int currentPage = -1;
  private boolean clicked = true;
  private boolean isGalleryHide = false;
  Thread thread;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    imgBtnBack.setOnClickListener(this);

    getInitValues();
    initAndFillViewPager();
    initAndFillMiniatures();

  }

  @Override
  public void onDestroy() {
    thread.interrupt();
    super.onDestroy();
  }

  @Override
  protected void injectModule() {
    component = DaggerPresentationComponent.builder()
        .appComponent(((App) getApplication()).getComponent())
        .presentationModule(new PresentationModule(this))
        .build();
    component.inject(this);

  }

  @Override
  protected void onStop() {
    super.onStop();
    thread.interrupt();
  }

  @Override
  protected PresentationPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.activity_presentation;
  }

  @Override
  public PresentationComponent getComponent() {
    return component;
  }

  @Override
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.imgBtnClose:
        finish();
        break;
      case R.id.imgViewSlide:
        hideOrShowDisplayElements();
        break;
      default:
        clicked = true;
        int position = 0;
        if (v.getTag() instanceof Integer) {
          position = (Integer) v.getTag();
          hightlightSelected(v, position);
        }
    }
  }

  private void hideOrShowDisplayElements() {
    TranslateAnimation animate1;
    TranslateAnimation animate2;

    if (!isGalleryHide) {
      animate1 = new TranslateAnimation(0, 0, 0, horizontalScrollView.getHeight());
      animate2 = new TranslateAnimation(0, 0, 0, -galleryTopBar.getHeight() - pixels);

      animate1.setAnimationListener(new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
          horizontalScrollView.setVisibility(View.GONE);
          galleryTopBar.setVisibility(View.GONE);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }
      });

    } else {
      animate1 = new TranslateAnimation(0, 0, horizontalScrollView.getHeight(), 0);
      animate2 = new TranslateAnimation(0, 0, -galleryTopBar.getHeight() - pixels, 0);

      animate1.setAnimationListener(new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {
          horizontalScrollView.setVisibility(View.VISIBLE);
          galleryTopBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onAnimationEnd(Animation animation) {
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }
      });
    }
    animate1.setDuration(500);
    animate2.setDuration(500);
    horizontalScrollView.startAnimation(animate1);
    galleryTopBar.startAnimation(animate2);

    isGalleryHide = !isGalleryHide;
  }

  public void getInitValues() {
    urls = getIntent().getExtras().getStringArray(GALLERY_IMAGES);
    url = getIntent().getExtras().getString(PRESENTATION_PDF_URL);
    pixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 19, getResources().getDisplayMetrics());
  }

  private void initAndFillViewPager() {
    viewPager.addOnPageChangeListener(this);
    viewPager.setKeepScreenOn(true);

    viewPager.setAdapter(new ImagePagerAdapter(this, urls, this));
    MiniaturePresentationAdapter miniatureAdapter = new MiniaturePresentationAdapter(this, urls, this);

    for (int i = 0; i < urls.length; i++) {
      gallery.addView(miniatureAdapter.getView(i, null, null));
    }

    hightlightSelected(gallery.getChildAt(0), 0);
  }

  private void initAndFillMiniatures() {
    thread = new Thread(new Runnable() {
      @Override
      public void run() {
        synchronized (this) {
          while (!thread.isInterrupted()) {
            runOnUiThread(new Runnable() {
              @Override
              public void run() {
                Rect scrollBounds = new Rect();
                if (horizontalScrollView != null) {
                  horizontalScrollView.getHitRect(scrollBounds);
                  for (int i = 0; i < urls.length; i++) {
                    View view = gallery.getChildAt(i);
                    View imageView = view.findViewById(R.id.iv_gallery_miniature);
                    if (view.getGlobalVisibleRect(scrollBounds)) {
                      if (imageView.getTag() instanceof Integer && (int) imageView.getTag() == 0) {
                        imageView.setVisibility(View.VISIBLE);
                        imageView.setTag(1);
                      }
                    } else {
                      if (imageView.getTag() instanceof Integer && (int) imageView.getTag() == 1) {
                        imageView.setVisibility(View.INVISIBLE);
                        imageView.setTag(0);
                      }
                    }
                  }
                }
              }
            });
            try {
              wait(100);
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
        }
      }
    });
    thread.start();
  }

  private void hightlightSelected(View view, int position) {
    if (currentPage != position) {
      currentPage = position;
      view.findViewById(R.id.selecctor_gallery_miniature).setVisibility(View.VISIBLE);
      view.findViewById(R.id.tv_gallery_number).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
      if (previusMiniatureSelected != null) {
        previusMiniatureSelected.findViewById(R.id.selecctor_gallery_miniature).setVisibility(View.INVISIBLE);
        previusMiniatureSelected.findViewById(R.id.tv_gallery_number).setBackgroundColor(Color.BLACK);
      }
      previusMiniatureSelected = view;
      viewPager.setCurrentItem(position);
      clicked = false;
    }
  }

  @Override
  public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

  }

  @Override
  public void onPageSelected(int position) {
    if (clicked) {
      hightlightSelected(gallery.getChildAt(position), position);
      Rect scrollBounds = new Rect();
      horizontalScrollView.getHitRect(scrollBounds);
      if (gallery.getChildAt(position).getGlobalVisibleRect(scrollBounds)) {
        // imageView is within the visible window
      } else {
        horizontalScrollView.smoothScrollTo(position * gallery.getChildAt(position).getWidth(), 0);
      }
    }
  }

  @Override
  public void onPageScrollStateChanged(int state) {
    if (state == 2) {
      clicked = true;
    }
  }
}
