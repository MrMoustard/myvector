package com.vectormobile.myvector.ui.gms.expenses.detail;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.squareup.picasso.Picasso;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.DaggerExpenseDetailComponent;
import com.vectormobile.myvector.di.component.ExpenseDetailComponent;
import com.vectormobile.myvector.di.module.ExpenseDetailModule;
import com.vectormobile.myvector.model.gms.Expense;
import com.vectormobile.myvector.ui.base.BaseActivity;
import com.vectormobile.myvector.ui.gms.expenses.add.AddExpenseActivity;
import com.vectormobile.myvector.ui.gms.expenses.list.ExpensesListActivity;
import com.vectormobile.myvector.ui.gms.hour.list.HoursListActivity;
import com.vectormobile.myvector.util.date.DateUtils;
import com.vectormobile.myvector.util.text.TextUtils;

import java.io.File;
import java.text.DecimalFormat;

import javax.inject.Inject;

/**
 * Created by kurt on 12/12/16.
 */

public class ExpenseDetailActivity extends BaseActivity<ExpenseDetailPresenter> implements ExpenseDetailView {

  public static final String EXTRA_DETAILS_EXPENSE = "expense";

  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.tv_section_title)
  TextView sectionTitle;
  @BindView(R.id.iv_action_button)
  ImageView actionButton;

  @BindView(R.id.ll_header_add_expense)
  RelativeLayout imageHeader;

  @BindView(R.id.status_image)
  ImageView statusImage;
  @BindView(R.id.status_message_layout)
  RelativeLayout statusLayout;
  @BindView(R.id.status_title)
  TextView statusTitle;
  @BindView(R.id.status_message)
  TextView statusMessage;
  @BindView(R.id.status_separator)
  View statusSeparator;
  @BindView(R.id.status_delete_button)
  Button statusDeleteExpenseBtn;
  @BindView(R.id.status_edit_button)
  Button statusEditExpenseBtn;

  @BindView(R.id.detail_date_from_value)
  TextView dateFrom;
  @BindView(R.id.detail_date_to_value)
  TextView dateTo;

  @Inject
  ExpenseDetailPresenter presenter;
  @BindView(R.id.ly_news_toolbar)
  RelativeLayout lyNewsToolbar;
  @BindView(R.id.appbar)
  AppBarLayout appbar;
  @BindView(R.id.iv_header_icon_add_expense)
  ImageView ivHeaderIconAddExpense;
  @BindView(R.id.iv_header_add_expense)
  ImageView ivHeaderAddExpense;
  @BindView(R.id.detail_date_from_header)
  TextView detailDateFromHeader;
  @BindView(R.id.detail_date_to_header)
  TextView detailDateToHeader;
  @BindView(R.id.detail_work_order_header)
  TextView detailWorkOrderHeader;
  @BindView(R.id.detail_work_order_value)
  TextView detailWorkOrderValue;
  @BindView(R.id.detail_project_header)
  TextView detailProjectHeader;
  @BindView(R.id.detail_project_value)
  TextView detailProjectValue;
  @BindView(R.id.detail_expense_category_header)
  TextView detailExpenseCategoryHeader;
  @BindView(R.id.detail_expense_category_value)
  TextView detailExpenseCategoryValue;
  @BindView(R.id.detail_units_header)
  TextView detailUnitsHeader;
  @BindView(R.id.detail_units_value)
  TextView detailUnitsValue;
  @BindView(R.id.detail_unit_amount_header)
  TextView detailUnitAmountHeader;
  @BindView(R.id.detail_unit_amount_value)
  TextView detailUnitAmountValue;
  @BindView(R.id.detail_currency_value)
  TextView detailCurrencyValue;
  @BindView(R.id.detail_comment_header)
  TextView detailCommentHeader;
  @BindView(R.id.detail_comment_value)
  TextView detailCommentValue;
  @BindView(R.id.container)
  LinearLayout container;
  @BindView(R.id.detail_date_to_separator)
  View dateToSeparator;
  @BindView(R.id.expanded_image)
  ImageView ivExpandedView;
  @BindView(R.id.scrollView)
  NestedScrollView scrollView;

  private ExpenseDetailComponent component;
  private Expense expense;
  private Animator animator;
  private int shortAnimationDuration;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);

    expense = getIntent().getExtras().getParcelable(EXTRA_DETAILS_EXPENSE);

    configUi();
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode == Activity.RESULT_OK) {
      String action;
      if (requestCode == HoursListActivity.DUPLICATE_HOUR_REQUEST) {
        action = AddExpenseActivity.DUPLICATE;
      } else {
        action = data.getExtras().getString(AddExpenseActivity.OPERATION_FINALIZED);
      }
      String date = data.getExtras().getString(AddExpenseActivity.EXTRA_DETAILS_EXPENSE_DATE);
      int itemCounter = data.getExtras().getInt(AddExpenseActivity.EXTRA_DETAILS_ITEM_COUNTER);
      returnResult(action, date, itemCounter);
      finish();
    }
  }

  private void returnResult(String action, String date, int itemCounter) {
    Intent returnIntent = new Intent();
    returnIntent.putExtra(AddExpenseActivity.OPERATION_FINALIZED, action);
    returnIntent.putExtra(AddExpenseActivity.EXTRA_DETAILS_EXPENSE_DATE, date);
    returnIntent.putExtra(AddExpenseActivity.EXTRA_DETAILS_ITEM_COUNTER, itemCounter);
    setResult(Activity.RESULT_OK, returnIntent);
  }

  private void returnResult(String action, String date, String itemId) {
    Intent returnIntent = new Intent();
    returnIntent.putExtra(AddExpenseActivity.OPERATION_FINALIZED, action);
    returnIntent.putExtra(AddExpenseActivity.EXTRA_DETAILS_EXPENSE_DATE, date);
    returnIntent.putExtra(AddExpenseActivity.EXTRA_DETAILS_ITEM_ID, itemId);
    setResult(Activity.RESULT_OK, returnIntent);
  }

  private void configUi() {
    setToolbarStatusTitle();
    configImageHeader();
    configStatusMessage();
    hideActionButton();
    hideDateTo();

    statusMessage.setText(expense.getApproverComment());
    dateFrom.setText(TextUtils.capitalizeFirstLetter(
        DateUtils.formatDateToLargeString(expense.getDateFrom())));
    detailWorkOrderValue.setText(expense.getWorkOrderName());
    detailProjectValue.setText(expense.getProject());
    detailExpenseCategoryValue.setText(expense.getCategory());
    detailUnitsValue.setText(String.valueOf(expense.getUnits()));
    DecimalFormat df = new DecimalFormat("0.00");
    String doubleFormatted = df.format(expense.getUnitPrice());
    detailUnitAmountValue.setText(doubleFormatted);
    detailCurrencyValue.setText(expense.getCurrencyId());
    detailCommentValue.setText(expense.getComment());
  }

  private void hideDateTo() {
    if (!expense.isMassive()) {
      detailDateToHeader.setVisibility(View.GONE);
      dateTo.setVisibility(View.GONE);
      dateToSeparator.setVisibility(View.GONE);
    }
  }

  private void setToolbarStatusTitle() {
    sectionTitle.setText(expense.getStateString(this));
  }

  private void hideActionButton() {
    actionButton.setVisibility(View.GONE);
  }

  private void configImageHeader() {
    presenter.checkImageHeader(expense);
  }

  private void configStatusMessage() {
    switch (expense.getState()) {
      case Expense.EXPENSE_SENT:
        setSentStatusMessage();
        break;
      case Expense.EXPENSE_APPROVED:
        setApprovedStatusMessage();
        break;
      case Expense.EXPENSE_REJECTED:
        setRejectedStatusMessage();
        break;
    }
  }

  private void setSentStatusMessage() {
    statusImage.setImageResource(R.drawable.ic_send);
    statusImage.setBackgroundResource(R.drawable.circle_item_blue);
    statusTitle.setText(getString(R.string.expense_sent_status_message_title));
    statusMessage.setVisibility(View.GONE);
    statusSeparator.setVisibility(View.GONE);
    statusDeleteExpenseBtn.setVisibility(View.GONE);
    statusEditExpenseBtn.setVisibility(View.GONE);
    statusLayout.setVisibility(View.VISIBLE);

  }

  private void setApprovedStatusMessage() {
    statusImage.setImageResource(R.drawable.ic_accept);
    statusImage.setBackgroundResource(R.drawable.circle_item_green);
    if (expense.getApproverComment().isEmpty()) {
      statusTitle.setText(String.format("%s%s", getString(R.string.expense_approved_status_message_title), "."));
    } else {
      statusTitle.setText(String.format("%s%s", getString(R.string.expense_approved_status_message_title), ":"));
    }
    statusSeparator.setVisibility(View.GONE);
    statusDeleteExpenseBtn.setVisibility(View.GONE);
    statusEditExpenseBtn.setVisibility(View.GONE);
    statusLayout.setVisibility(View.VISIBLE);
  }

  private void setRejectedStatusMessage() {
    statusImage.setImageResource(R.drawable.ic_reject);
    statusImage.setBackgroundResource(R.drawable.circle_item_red);
    if (expense.getApproverComment().isEmpty()) {
      statusTitle.setText(String.format("%s%s", getString(R.string.expense_rejected_status_message_title), "."));
    } else {
      statusTitle.setText(String.format("%s%s", getString(R.string.expense_rejected_status_message_title), ":"));
    }
    statusLayout.setVisibility(View.VISIBLE);
    statusDeleteExpenseBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        returnResult(
            ExpensesListActivity.REMOVED_EXPENSE,
            DateUtils.formatDateToCompactString(expense.getDateFrom(), DateUtils.GMS_SHORT_DATE_FORMAT),
            expense.getId());
        finish();
      }
    });
    statusEditExpenseBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent iAddExpense = new Intent(ExpenseDetailActivity.this, AddExpenseActivity.class);
        iAddExpense.putExtra(AddExpenseActivity.EXTRA_ACTION, AddExpenseActivity.UPDATE_ACTION);
        iAddExpense.putExtra(AddExpenseActivity.EXTRA_DETAILS_EXPENSE, expense);
        startActivityForResult(iAddExpense, ExpensesListActivity.UPDATE_EXPENSE_REQUEST);
      }
    });

  }

  @OnClick(R.id.iv_header_add_expense)
  public void expandImage() {
    zoomImageFromThumb(ivHeaderAddExpense, ivHeaderAddExpense.getDrawable());
  }

  @OnClick(R.id.iv_duplicate_button)
  public void onDuplicateButtonHasBeenClicked() {
    Intent iAddExpense = new Intent(ExpenseDetailActivity.this, AddExpenseActivity.class);
    iAddExpense.putExtra(AddExpenseActivity.EXTRA_ACTION, AddExpenseActivity.DUPLICATE_ACTION);
    iAddExpense.putExtra(AddExpenseActivity.EXTRA_DETAILS_EXPENSE, expense);
    startActivityForResult(iAddExpense, AddExpenseActivity.DUPLICATE_ACTION);
  }

  private void zoomImageFromThumb(final ImageView ivHeaderAddExpense, Drawable imageDrawable) {
    // If there's an animation in progress, cancel it
    // immediately and proceed with this one.
    if (animator != null) {
      animator.cancel();
    }
    if (imageDrawable != null) {
      ivExpandedView.setImageDrawable(imageDrawable);
    } else {
      ivExpandedView.setImageResource(R.drawable.placeholder_add_expense_header);
    }
    // Calculate the starting and ending bounds for the zoomed-in image.
    // This step involves lots of math. Yay, math.
    final Rect startBounds = new Rect();
    final Rect finalBounds = new Rect();
    final Point globalOffset = new Point();

    // The start bounds are the global visible rectangle of the thumbnail,
    // and the final bounds are the global visible rectangle of the container
    // view. Also set the container view's offset as the origin for the
    // bounds, since that's the origin for the positioning animation
    // properties (X, Y).
    ivHeaderAddExpense.getGlobalVisibleRect(startBounds);
    findViewById(R.id.container)
        .getGlobalVisibleRect(finalBounds, globalOffset);
    startBounds.offset(-globalOffset.x, -globalOffset.y);
    finalBounds.offset(-globalOffset.x, -globalOffset.y);

    // Adjust the start bounds to be the same aspect ratio as the final
    // bounds using the "center crop" technique. This prevents undesirable
    // stretching during the animation. Also calculate the start scaling
    // factor (the end scaling factor is always 1.0).
    float startScale;
    if ((float) finalBounds.width() / finalBounds.height()
        > (float) startBounds.width() / startBounds.height()) {
      // Extend start bounds horizontally
      startScale = (float) startBounds.height() / finalBounds.height();
      float startWidth = startScale * finalBounds.width();
      float deltaWidth = (startWidth - startBounds.width()) / 2;
      startBounds.left -= deltaWidth;
      startBounds.right += deltaWidth;
    } else {
      // Extend start bounds vertically
      startScale = (float) startBounds.width() / finalBounds.width();
      float startHeight = startScale * finalBounds.height();
      float deltaHeight = (startHeight - startBounds.height()) / 2;
      startBounds.top -= deltaHeight;
      startBounds.bottom += deltaHeight;
    }

    // Hide the thumbnail and show the zoomed-in view. When the animation
    // begins, it will position the zoomed-in view in the place of the
    // thumbnail.
    ivHeaderAddExpense.setAlpha(0f);
    ivExpandedView.setVisibility(View.VISIBLE);

    // Set the pivot point for SCALE_X and SCALE_Y transformations
    // to the top-left corner of the zoomed-in view (the default
    // is the center of the view).
    ivExpandedView.setPivotX(0f);
    ivExpandedView.setPivotY(0f);

    // Construct and run the parallel animation of the four translation and
    // scale properties (X, Y, SCALE_X, and SCALE_Y).
    AnimatorSet set = new AnimatorSet();
    set
        .play(ObjectAnimator.ofFloat(ivExpandedView, View.X,
            startBounds.left, finalBounds.left))
        .with(ObjectAnimator.ofFloat(ivExpandedView, View.Y,
            startBounds.top, finalBounds.top))
        .with(ObjectAnimator.ofFloat(ivExpandedView, View.SCALE_X,
            startScale, 1f)).with(ObjectAnimator.ofFloat(ivExpandedView,
        View.SCALE_Y, startScale, 1f));
    set.setDuration(shortAnimationDuration);
    set.setInterpolator(new DecelerateInterpolator());
    set.addListener(new AnimatorListenerAdapter() {
      @Override
      public void onAnimationEnd(Animator animation) {
        animator = null;
      }

      @Override
      public void onAnimationCancel(Animator animation) {
        animator = null;
      }
    });
    set.start();
    animator = set;
    // Upon clicking the zoomed-in image, it should zoom back down
    // to the original bounds and show the thumbnail instead of
    // the expanded image.
    final float startScaleFinal = startScale;

    ivExpandedView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (animator != null) {
          animator.cancel();
        }

        // Animate the four positioning/sizing properties in parallel,
        // back to their original values.
        AnimatorSet set = new AnimatorSet();
        set.play(ObjectAnimator
            .ofFloat(ivExpandedView, View.X, startBounds.left))
            .with(ObjectAnimator
                .ofFloat(ivExpandedView,
                    View.Y, startBounds.top))
            .with(ObjectAnimator
                .ofFloat(ivExpandedView,
                    View.SCALE_X, startScaleFinal))
            .with(ObjectAnimator
                .ofFloat(ivExpandedView,
                    View.SCALE_Y, startScaleFinal));
        set.setDuration(shortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
          @Override
          public void onAnimationEnd(Animator animation) {
            ivHeaderAddExpense.setAlpha(1f);
            ivExpandedView.setVisibility(View.GONE);
            animator = null;
          }

          @Override
          public void onAnimationCancel(Animator animation) {
            ivHeaderAddExpense.setAlpha(1f);
            ivExpandedView.setVisibility(View.GONE);
            animator = null;
          }
        });
        set.start();
        animator = set;
      }
    });
  }

  @Override
  protected void injectModule() {
    component = DaggerExpenseDetailComponent.builder()
        .appComponent(((App) getApplication()).getComponent())
        .expenseDetailModule(new ExpenseDetailModule(this))
        .build();
    component.inject(this);
  }

  @Override
  protected ExpenseDetailPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.activity_expense_detail;
  }

  @Override
  public ExpenseDetailComponent getComponent() {
    return component;
  }

  @Override
  public void checkReadPermission(final String id) {
    Dexter.withActivity(this)
        .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
        .withListener(new PermissionListener() {
          @Override
          public void onPermissionGranted(PermissionGrantedResponse response) {
            presenter.loadImage(id);
          }

          @Override
          public void onPermissionDenied(PermissionDeniedResponse response) {
            Snackbar
                .make(toolbar, getResources().getString(R.string.permissions_denied_error_show_image),
                    Snackbar.LENGTH_INDEFINITE)
                .show();
          }

          @Override
          public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
            token.continuePermissionRequest();
          }
        })
        .onSameThread()
        .check();
  }

  @Override
  public void loadImage(File fileToWrite) {
    Picasso.with(this)
        .load(fileToWrite)
        .fit()
        .centerCrop()
        .into(ivHeaderAddExpense);
    ivHeaderAddExpense.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideHeaderImage() {
    imageHeader.setVisibility(View.GONE);
  }
}
