package com.vectormobile.myvector.ui.gms.hour.list;

import com.vectormobile.myvector.model.gms.Hour;
import com.vectormobile.myvector.ui.base.PresenterView;

import java.util.List;

/**
 * Created by ajruiz on 24/01/2017.
 */

public interface HoursListView extends PresenterView {

  void loadMiniCalendarEventsArray();

  void showMiniCalendarView();

  void hideMiniCalendarView();

  void setHoursList(List<Hour> hours);

  void loadHours();

  void showNoHoursView(String dateFrom, String dateTo, int timeLapse);

  void resetImputations(int page);

  void goToAddImputation();

  void finishMultiSelectionMode();

  void refreshList();

  void removeAdapterItems(List<String> idList);

  void removeItemFromAdapter(String hourId);

  void setAdapterItemsSent();

  void showMessage(int messageId);

  void showMessage(String message);

  void showDialogMessage(String message);

  void showProgressBar();

  void hideProgressBar();

  void showTutorial();

  void hideTutorial();

  void showMassiveLoading();

  void hideMassiveLoading();

}
