package com.vectormobile.myvector.ui.gms.hour.detail;

import com.vectormobile.myvector.ui.base.BasePresenter;

import javax.inject.Inject;

/**
 * Created by ajruiz on 26/01/2017.
 */

public class HourDetailPresenter extends BasePresenter<HourDetailView> {

  @Inject
  public HourDetailPresenter(HourDetailActivity view) {
    super(view);
  }
}
