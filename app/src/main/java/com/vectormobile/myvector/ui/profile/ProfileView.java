package com.vectormobile.myvector.ui.profile;

import com.vectormobile.myvector.model.intranet.UserProfile;
import com.vectormobile.myvector.ui.base.PresenterView;

/**
 * VectorDigital.
 *
 * @author -   AMarinaG
 * @since -   28/10/16
 */
public interface ProfileView extends PresenterView {

  void clearItemFocus();

  void showSaveButton();

  void hideSaveButton();

  void userDataRetrieved(UserProfile userProfile);

  void showProgressBar();

  void hideProgressBar();

  void showMessage(int messageId);

  void showMessageAction(int messageId);

  void showProfile();

  void showInvalidProfileDialog();

  void setFocus(int position);
}
