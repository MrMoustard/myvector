package com.vectormobile.myvector.ui.news.detail;

import com.vectormobile.myvector.ui.base.PresenterView;

/**
 * Created by ajruiz on 11/11/2016.
 */

public interface NewsDetailView extends PresenterView {

  void finishLoadedContent(String content);

  void hideLoadingDialog();

  void newsDetailRetrieved(String content);

  void newsDetailImage(String image);

  void closeNewsDetail();

  void showProfileErrorDialog();
}
