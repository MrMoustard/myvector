package com.vectormobile.myvector.ui.calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.widget.DatePicker;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.util.date.DateUtils;
import com.vectormobile.myvector.util.text.TextUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Erik Medina on 12/12/2016.
 */

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

  public static final String CALENDAR_TYPE = "calendarType";
  public static final String CALENDAR_DATE = "calendarDate";

  public interface OnDatePickerListener {
    void dateSelected(String date, int calendarType);
    void clearDate();
  }

  OnDatePickerListener listener;
  DatePickerDialog dialog;

  private int calendarType;
  private long calendarDate;

  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);

    try {
      listener = (OnDatePickerListener) activity;
    } catch (ClassCastException e) {
      throw new ClassCastException(activity.toString()
          + " must implement OnDatePickerListener");
    }
  }

  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    calendarType = getArguments().getInt(CALENDAR_TYPE);
    calendarDate = getArguments().getLong(CALENDAR_DATE);

    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(calendarDate);

    dialog = new DatePickerDialog(getActivity(), this, calendar.get(Calendar.YEAR),
        calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
    dialog.setOnShowListener(new DialogInterface.OnShowListener() {
      @Override
      public void onShow(DialogInterface arg0) {
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(getActivity(), R.color.grey_vector));
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(getActivity(), R.color.orange_vector));
        if (calendarType == TextUtils.Gms.CALENDAR_EXPENSE_TO) {
          dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setText(R.string.calendar_eliminate);
          dialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.calendar_eliminate), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
              if (which == DialogInterface.BUTTON_NEGATIVE) {
                listener.clearDate();
              }
            }
          });
        }
      }
    });
    return dialog;
  }

  public void onDateSet(DatePicker view, int year, int month, int day) {
    String formattedDate = DateUtils.formatDateToLargeString(new Date(year - 1900, month, day));
    formattedDate = TextUtils.capitalizeFirstLetter(formattedDate);
    listener.dateSelected(formattedDate, calendarType);
  }
}