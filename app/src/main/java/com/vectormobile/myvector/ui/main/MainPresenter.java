package com.vectormobile.myvector.ui.main;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.base.BasePresenter;

import javax.inject.Inject;

/**
 * Created by Erik Medina on 27/10/2016.
 */
@ActivityScope
public class MainPresenter extends BasePresenter<MainView> {

  @Inject
  public MainPresenter(MainActivity activity) {
    super(activity);
  }
}
