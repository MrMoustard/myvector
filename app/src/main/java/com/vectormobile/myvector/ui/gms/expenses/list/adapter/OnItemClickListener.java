package com.vectormobile.myvector.ui.gms.expenses.list.adapter;

/**
 * Created by kurt on 05/12/16.
 */

public interface OnItemClickListener<T> {

  void onItemClick(T Object);
}
