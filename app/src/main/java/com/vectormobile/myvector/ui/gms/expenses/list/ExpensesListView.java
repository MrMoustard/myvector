package com.vectormobile.myvector.ui.gms.expenses.list;

import com.vectormobile.myvector.domain.entity.gms.response.GmsDayStateResult;
import com.vectormobile.myvector.model.gms.Expense;
import com.vectormobile.myvector.ui.base.PresenterView;

import java.util.List;

/**
 * Created by kurt on 05/12/16.
 */

public interface ExpensesListView extends PresenterView {

  void loadMiniCalendarEventsArray();

  void showMiniCalendarView();

  void hideMiniCalendarView();

  void loadExpenses();

  void showNoExpensesView(String dateFrom, String dateTo, int timeLapse);

  void setExpensesList(List<Expense> expenses);

  void showMessage(int messageId);

  void showMessage(String message);

  void showProgressBar();

  void hideProgressBar();

  void resetExpenses(int page);

  void goToAddExpense();

  void finishMultiSelectionMode();

  void refreshList();

  void removeAdapterItems();

  void setAdapterItemsSent();

  void showTutorial();

  void hideTutorial();

  void showMassiveLoading();

  void hideMassiveLoading();

  void decorateMiniCalendar(GmsDayStateResult dayStateResult);
}
