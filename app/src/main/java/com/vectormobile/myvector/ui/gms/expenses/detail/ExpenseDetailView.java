package com.vectormobile.myvector.ui.gms.expenses.detail;

import com.vectormobile.myvector.ui.base.PresenterView;

import java.io.File;

/**
 * Created by kurt on 12/12/16.
 */
public interface ExpenseDetailView extends PresenterView {

  void checkReadPermission(String id);

  void loadImage(File fileToWrite);

  void hideHeaderImage();

}
