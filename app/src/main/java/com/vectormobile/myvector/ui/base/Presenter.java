package com.vectormobile.myvector.ui.base;

import android.os.Bundle;
import timber.log.Timber;

import javax.annotation.Nullable;

/**
 * VectorDigital.
 *
 * @author -   AMarinaG
 * @since -   28/10/16
 */

public abstract class Presenter<View> {
  @Nullable
  private View view;

  protected void onCreate(@Nullable Bundle savedState) {
  }

  protected void onDestroy() {
  }

  protected void onSave(Bundle state) {
  }

  protected void onTakeView(View view) {
  }

  protected void onDropView() {
  }

  @Nullable
  public View getView() {
    return view;
  }

  public void create(Bundle bundle) {
    onCreate(bundle);
    Timber.d("%s::create", getClass().getSimpleName());
  }

  public void destroy() {
    Timber.d("%s::destroy", getClass().getSimpleName());
    onDestroy();
  }

  public void save(Bundle state) {
    onSave(state);
    Timber.d("%s::save", getClass().getSimpleName());

  }

  public void takeView(View view) {
    this.view = view;
    onTakeView(view);
    Timber.d("%s::takeView", getClass().getSimpleName());
  }

  public void dropView() {
    onDropView();
    Timber.d("%s::dropView", getClass().getSimpleName());
    this.view = null;
  }
}
