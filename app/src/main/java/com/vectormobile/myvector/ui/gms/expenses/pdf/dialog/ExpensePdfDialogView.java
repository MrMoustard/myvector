package com.vectormobile.myvector.ui.gms.expenses.pdf.dialog;

import android.content.Intent;
import com.vectormobile.myvector.model.gms.Expense;
import com.vectormobile.myvector.ui.base.PresenterView;

import java.util.List;

/**
 * Created by kurt on 15/03/17.
 */

public interface ExpensePdfDialogView extends PresenterView {

  void showProgressBar();

  void hideProgressBar();

  void resetExpenses(int lastPage);

  void loadExpenses(List<Expense> expenses);

  void showNoExpensesView(String lastDateFrom, String lastDateTo);

  void showMessage(int errorId);

  void sharedPdfExpenseIds(List<String> expenseIds);
}
