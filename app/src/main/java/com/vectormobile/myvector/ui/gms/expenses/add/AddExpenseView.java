package com.vectormobile.myvector.ui.gms.expenses.add;

import com.vectormobile.myvector.domain.entity.gms.response.Record;
import com.vectormobile.myvector.ui.base.PresenterView;

import java.io.File;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Erik Medina on 07/12/2016.
 */

public interface AddExpenseView extends PresenterView {

  void showHiddenButtons();

  void setDefaultDate(String date);

  void setFabBehaviour();

  void populateProjects(List<Record> list);

  void populateWorkOrders(List<Record> list);

  void populateExpenseCategories(List<Record> list);

  void populateCurrencies(List<Record> list);

  void showDatePickerDialog(int calendarType, Calendar date);

  String getExpenseDateFrom();

  String getExpenseDateTo();

  int getProjectPositionSelected();

  int getWorkOrderPositionSelected();

  int getExpenseCategoryPositionSelected();

  String getExpenseUnits();

  String getUnitPrice();

  int getCurrencyPositionSelected();

  String getComment();

  void showMessage(int messageId);

  void setDateFrom(String dateFrom);

  void setDateTo(String dateTo);

  void returnResult(String extra, String date, int itemCounter);

  void setProjectItem(String project);

  void setWorkOrderItem(String workOrder);

  void setCategoryItem(String category);

  void setCurrencyItem(String currency);

  void setUnits(int units);

  void setMinimumValueForUnits(int minimum_units);

  void setUnitPrice(double unitPrice);

  void setComment(String comment);

  void showProgressBar();

  void hideProgressBar();

  void makeCommentFieldScrollable();

  void blockCategoryFields(Record expenseCategory);

  void releaseCategoryFields();

  void showNetworkError();

  void loadImage(File fileToWrite);

  void checkReadPermission(String id);

  void setTitle(int titleId);

  void showDuplicateButton();
}
