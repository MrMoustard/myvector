package com.vectormobile.myvector.ui.base;

/**
 * VectorDigital.
 *
 * @author -   AMarinaG
 * @since -   28/10/16
 */

public interface PresenterView {
  void showError(int errorId);
}
