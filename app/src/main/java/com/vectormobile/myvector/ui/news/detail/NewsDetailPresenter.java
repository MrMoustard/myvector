package com.vectormobile.myvector.ui.news.detail;

import com.vectormobile.myvector.domain.interactor.intranet.NewsItemDetailInteractor;
import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.ui.base.BasePresenter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import javax.inject.Inject;

/**
 * Created by ajruiz on 11/11/2016.
 */

public class NewsDetailPresenter extends BasePresenter<NewsDetailView> {

  private static final int FIRST_ELEMENT = 0;
  private static final String INTRANET_LOGIN_PAGE_TITLE = "Login – Intranet Vector ITC Group";

  private NewsItemDetailInteractor interactor;

  @Inject
  public NewsDetailPresenter(NewsDetailView fragment, NewsItemDetailInteractor interactor) {
    super(fragment);
    this.interactor = interactor;
  }

  public void getNewsById(String id) {
    interactor.execute(id, new OnItemRetrievedListener() {
      @Override
      public void onSuccess(Object item) {
        if (getView() != null) {
          String newsDetailContent = parseNewsDetailContent(item.toString());
          if (!isLoginPage(newsDetailContent)) {
            getView().newsDetailImage(parseNewsImage(item.toString()));
            getView().newsDetailRetrieved(newsDetailContent);
          } else {
            getView().showProfileErrorDialog();
          }
        }
      }

      @Override
      public void onError(int errorId) {
        if (getView() != null) {
          getView().showError(errorId);
          getView().hideLoadingDialog();
          getView().closeNewsDetail();
        }
      }
    });
  }

  private boolean isLoginPage(String newsDetailContent) {
    return newsDetailContent.contains(INTRANET_LOGIN_PAGE_TITLE);
  }

  private String parseNewsImage(String content) {
    Document doc = Jsoup.parseBodyFragment(content);
    Elements element = doc.select("section[id=content]");
    if (!element.isEmpty())
      return element.get(FIRST_ELEMENT).select("img").attr("data-url-zoom");
    else
      return null;
  }

  private String parseNewsDetailContent(String content) {
    Document doc = Jsoup.parseBodyFragment(content);
    doc.select("script").remove();
    doc.select("img").remove();
    doc.select("footer").remove();
    doc.select("header").remove();
    doc.select("div.title").remove();
    doc.select("section[id=comments]").remove();
    return doc.toString().replace("&nbsp;", " ");
  }

}
