package com.vectormobile.myvector.ui.bot;

import com.vectormobile.myvector.R;
import com.vectormobile.myvector.domain.entity.bot.BFHistoryResponse;
import com.vectormobile.myvector.domain.entity.botframework.RsLoginResponse;
import com.vectormobile.myvector.domain.interactor.botframework.GetConversationHistoryInteractor;
import com.vectormobile.myvector.domain.interactor.botframework.SendBFTextInteractor;
import com.vectormobile.myvector.domain.interactor.botframework.StartConversationInteractor;
import com.vectormobile.myvector.domain.interactor.botframework.listener.GetConversationHistoryListener;
import com.vectormobile.myvector.domain.interactor.botframework.listener.StartConversationListener;
import com.vectormobile.myvector.ui.base.BasePresenter;

import javax.inject.Inject;

/**
 * Created by amarina on 14/03/2017.
 */
public class RoomPresenter extends BasePresenter<RoomView> {
  private StartConversationInteractor interactor;
  private SendBFTextInteractor sendBFTextInteractor;
  private GetConversationHistoryInteractor getConversationHistoryInteractor;

  @Inject
  public RoomPresenter(RoomActivity activity, StartConversationInteractor interactor, SendBFTextInteractor sendBFTextInteractor, GetConversationHistoryInteractor getConversationHistoryInteractor) {
    super(activity);
    this.interactor = interactor;
    this.sendBFTextInteractor = sendBFTextInteractor;
    this.getConversationHistoryInteractor = getConversationHistoryInteractor;
  }

  public void startConversation() {
    interactor.execute(new StartConversationListener() {
      @Override
      public void onError() {
        getView().showError(R.string.error_connection);
      }

      @Override
      public void onSuccess(RsLoginResponse body) {
        getView().openSocket(body.getConversationId(), body.getStreamUrl());
      }
    });
  }

  public void sendText(CharSequence input) {
    sendBFTextInteractor.execute(input.toString());
  }

  public void getConversationHistory() {
    getConversationHistoryInteractor.execute(new GetConversationHistoryListener() {
      @Override
      public void onSuccess(BFHistoryResponse bfHistoryResponse) {
        getView().setConversationHistory(bfHistoryResponse);
      }
    });
  }
}
