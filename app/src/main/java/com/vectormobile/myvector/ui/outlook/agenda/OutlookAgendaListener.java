package com.vectormobile.myvector.ui.outlook.agenda;

/**
 * Created by amarina on 13/03/2017.
 */

public interface OutlookAgendaListener {
  void onPageFinished();
}
