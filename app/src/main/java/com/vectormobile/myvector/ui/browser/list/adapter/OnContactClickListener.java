package com.vectormobile.myvector.ui.browser.list.adapter;

import com.vectormobile.myvector.model.intranet.Contact;

/**
 * Created by ajruiz on 03/02/2017.
 */

public interface OnContactClickListener {
  void onContactClick(Contact contact);
}
