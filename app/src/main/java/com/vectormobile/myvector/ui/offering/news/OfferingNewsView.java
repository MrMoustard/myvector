package com.vectormobile.myvector.ui.offering.news;

import com.vectormobile.myvector.model.Post;
import com.vectormobile.myvector.ui.base.PresenterView;

import java.util.List;

/**
 * Created by ajruiz on 10/11/2016.
 */

public interface OfferingNewsView extends PresenterView {

  void onListPostRetrieved(List<Post> list);

  void loadNewPage(int page);

  void hideLoadingDialog();

}
