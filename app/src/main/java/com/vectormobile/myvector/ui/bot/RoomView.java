package com.vectormobile.myvector.ui.bot;

import com.vectormobile.myvector.domain.entity.bot.BFHistoryResponse;
import com.vectormobile.myvector.ui.base.PresenterView;

/**
 * Created by amarina on 14/03/2017.
 */

public interface RoomView extends PresenterView {
  void openSocket(String conversationId, String streamUrl);

  void setConversationHistory(BFHistoryResponse bfHistoryResponse);
}
