package com.vectormobile.myvector.ui.gms.hour.add.jira.adapter;

import com.vectormobile.myvector.model.gms.Jira;

/**
 * Created by ajruiz on 02/03/2017.
 */

public interface OnJiraClickListener {
  void onJiraClick(Jira jira);
}
