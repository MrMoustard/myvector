package com.vectormobile.myvector.ui.gms.expenses.pdf;

import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.OnClick;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.DaggerExpensePdfComponent;
import com.vectormobile.myvector.di.component.ExpensePdfComponent;
import com.vectormobile.myvector.di.module.ExpensePdfModule;
import com.vectormobile.myvector.ui.base.BaseActivity;
import com.vectormobile.myvector.ui.calendar.DatePickerFragment;
import com.vectormobile.myvector.ui.gms.expenses.pdf.dialog.ExpensePdfDialogActivity;
import com.vectormobile.myvector.util.date.DateUtils;
import com.vectormobile.myvector.util.text.TextUtils;

import java.util.Calendar;

import javax.inject.Inject;

/**
 * Created by kurt on 15/03/17.
 */

public class ExpensePdfActivity extends BaseActivity<ExpensePdfPresenter>
    implements ExpensePdfView, View.OnTouchListener, DatePickerFragment.OnDatePickerListener {

  @Inject
  ExpensePdfPresenter presenter;

  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.et_expense_pdf_date_from)
  EditText etExpensePdfDateFrom;
  @BindView(R.id.et_expense_pdf_date_to)
  EditText etExpensePdfDateTo;

  private ExpensePdfComponent component;

  private String dateFrom;
  private String dateTo;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setUpActionBar();
    setUpView();
  }

  private void setUpActionBar() {
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);
  }

  private void setUpView() {
    Drawable mDrawable = getResources().getDrawable(R.drawable.ic_calendar);
    DrawableCompat.setTint(mDrawable, getResources().getColor(R.color.grey_vector));
    etExpensePdfDateFrom.setCompoundDrawablesWithIntrinsicBounds(null, null, mDrawable, null);
    etExpensePdfDateTo.setCompoundDrawablesWithIntrinsicBounds(null, null, mDrawable, null);

    etExpensePdfDateFrom.setOnTouchListener(this);
    etExpensePdfDateTo.setOnTouchListener(this);

    presenter.setDefaultDateFrom();
    presenter.setDefaultDateTo();
  }

  @Override
  protected void injectModule() {
    component = DaggerExpensePdfComponent.builder()
        .appComponent(((App)getApplication()).getComponent())
        .expensePdfModule(new ExpensePdfModule(this))
        .build();
    component.inject(this);
  }

  @Override
  protected ExpensePdfPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.activity_expense_pdf;
  }

  @Override
  public ExpensePdfComponent getComponent() {
    return component;
  }

  @Override
  public boolean onTouch(View view, MotionEvent event) {
    if (event.getAction() == MotionEvent.ACTION_UP) {
      switch (view.getId()) {
        case R.id.et_expense_pdf_date_from:
          presenter.onCalendarIconClicked(
              TextUtils.Gms.CALENDAR_EXPENSE_FROM,
              DateUtils.getTypedDate(etExpensePdfDateFrom.getText().toString(), null));
          return true;
        case R.id.et_expense_pdf_date_to:
          presenter.onCalendarIconClicked(
              TextUtils.Gms.CALENDAR_EXPENSE_TO,
              DateUtils.getTypedDate(etExpensePdfDateTo.getText().toString(), etExpensePdfDateFrom.getText().toString()));
          return true;
      }
    }
    return false;
  }

  @Override
  public void showDatePickerDialog(int calendarType, Calendar date) {
    Bundle bundle = new Bundle();
    bundle.putInt(DatePickerFragment.CALENDAR_TYPE, calendarType);
    bundle.putLong(DatePickerFragment.CALENDAR_DATE, date.getTimeInMillis());
    DialogFragment datePickerFragment = new DatePickerFragment();
    datePickerFragment.setArguments(bundle);
    datePickerFragment.show(getFragmentManager(), "datePicker");
  }

  @Override
  public void dateSelected(String date, int calendarType) {
    if (calendarType == TextUtils.Gms.CALENDAR_EXPENSE_FROM) {
      dateFrom = date;
      etExpensePdfDateFrom.setText(date);
    } else if (calendarType == TextUtils.Gms.CALENDAR_EXPENSE_TO) {
      dateTo = date;
      etExpensePdfDateTo.setText(date);
    }
    presenter.compareDates(calendarType, dateFrom, dateTo);
  }

  @Override
  public void clearDate() {
    etExpensePdfDateTo.setText("");
  }

  @Override
  public void setDateFrom(String date) {
    this.dateFrom = date;
    etExpensePdfDateFrom.setText(date);
  }

  @Override
  public void setDateTo(String date) {
    this.dateTo = date;
    etExpensePdfDateTo.setText(date);
  }

  @OnClick({R.id.iv_action_button, R.id.expense_pdf_build_and_send_btn})
  public void buildAndSendButtonHasBeenClicked() {
    presenter.buildAndSend();
  }

  @Override
  public void navigateToDialog() {
    Intent iDialog = new Intent(this, ExpensePdfDialogActivity.class);
    iDialog.putExtra(ExpensePdfDialogActivity.EXTRA_DATE_FROM, dateFrom);
    iDialog.putExtra(ExpensePdfDialogActivity.EXTRA_DATE_TO, dateTo);
    startActivity(iDialog);
  }
}
