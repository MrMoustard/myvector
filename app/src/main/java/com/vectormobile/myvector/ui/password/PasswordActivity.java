package com.vectormobile.myvector.ui.password;

import com.vectormobile.myvector.App;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.DaggerPasswordComponent;
import com.vectormobile.myvector.di.component.PasswordComponent;
import com.vectormobile.myvector.di.module.PasswordModule;
import com.vectormobile.myvector.ui.base.BaseActivity;

import javax.inject.Inject;

/**
 * Created by Erik Medina on 14/12/2016.
 */

public class PasswordActivity extends BaseActivity<PasswordPresenter> implements PasswordView {

  @Inject
  PasswordPresenter presenter;

  private PasswordComponent component;

  @Override
  protected void injectModule() {
    component = DaggerPasswordComponent.builder()
        .appComponent(((App) getApplication()).getComponent())
        .passwordModule(new PasswordModule(this))
        .build();
    component.inject(this);
  }

  @Override
  protected PasswordPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.activity_password;
  }

  @Override
  public PasswordComponent getComponent() {
    return component;
  }
}
