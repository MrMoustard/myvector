package com.vectormobile.myvector.ui.offering.menu.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.model.offering.Section;
import com.vectormobile.myvector.util.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ajruiz on 03/11/2016.
 */

public class OfferingMenuAdapter extends RecyclerView.Adapter<OfferingMenuAdapter.ItemViewHolder> {

  private List<Section> itemList = new ArrayList<>();
  private final OnItemClickListener listener;
  TypedArray imgOfferingList;
  private Context context;

  public OfferingMenuAdapter(OnItemClickListener listener, Context context) {
    this.listener = listener;
    this.context = context;
  }

  @Override
  public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_offering_comercial, parent, false);
    imgOfferingList = context.getResources().obtainTypedArray(R.array.img_offering_menu_list);
    return new ItemViewHolder(v);
  }

  @Override
  public void onBindViewHolder(ItemViewHolder holder, int position) {
    holder.itemTittle.setText(TextUtils.capitalizeFirstLetter(itemList.get(position).getTitle()));
    holder.separator.setColorFilter(itemList.get(position).getColor());
    holder.arrow.setColorFilter(itemList.get(position).getColor());
    holder.icon.setImageResource(imgOfferingList.getResourceId(position, -1));
    holder.bind(itemList.get(position), listener);
  }

  @Override
  public void onAttachedToRecyclerView(RecyclerView recyclerView) {
    super.onAttachedToRecyclerView(recyclerView);
  }

  @Override
  public int getItemCount() {
    return itemList.size();
  }


  public void setOfferingMenuList(List<Section> sections) {
    itemList.clear();
    itemList.addAll(sections);
    notifyDataSetChanged();
  }


  static class ItemViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.rl_comercial_item)
    RelativeLayout rlComercialItem;
    @BindView(R.id.tv_rv_offering_comercial)
    TextView itemTittle;
    @BindView(R.id.img_icon_comercial)
    ImageView icon;
    @BindView(R.id.img_arrow_comercial)
    ImageView arrow;
    @BindView(R.id.separator_offering_items)
    ImageView separator;

    public ItemViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }

    public void bind(final Section section, final OnItemClickListener listener) {
      rlComercialItem.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          listener.onItemClick(section);
        }
      });
    }
  }


}
