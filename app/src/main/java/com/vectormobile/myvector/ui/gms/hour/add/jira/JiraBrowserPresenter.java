package com.vectormobile.myvector.ui.gms.hour.add.jira;

import com.vectormobile.myvector.domain.entity.gms.request.GmsJiraBrowserRequest;
import com.vectormobile.myvector.domain.entity.gms.response.GmsResponse;
import com.vectormobile.myvector.domain.entity.gms.response.Record;
import com.vectormobile.myvector.domain.interactor.gms.BuildJiraBrowserBodyRequestInteractor;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.model.gms.Jira;
import com.vectormobile.myvector.ui.base.BasePresenter;
import com.vectormobile.myvector.util.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by ajruiz on 28/02/2017.
 */

public class JiraBrowserPresenter extends BasePresenter<JiraBrowserView> {

  private List<Record> jiraCodes;

  private BuildJiraBrowserBodyRequestInteractor interactor;

  @Inject
  public JiraBrowserPresenter(JiraBrowserActivity view, BuildJiraBrowserBodyRequestInteractor interactor) {
    super(view);
    this.interactor = interactor;
  }

  public void searchJiraCode(String projectId, String description, int page) {
    view.showProgressBar();
    GmsJiraBrowserRequest gmsJiraBrowserRequest = new GmsJiraBrowserRequest();
    gmsJiraBrowserRequest.setProjectId(projectId);
    gmsJiraBrowserRequest.setDescription(description);
    gmsJiraBrowserRequest.setKey(TextUtils.Gms.GMS_EMPTY_SEARCH);
    gmsJiraBrowserRequest.setPage(page);
    gmsJiraBrowserRequest.setLimit(page);

    interactor.execute(TextUtils.Gms.ACTION_IMPUTATION_HOUR_CONTROLLER,
        TextUtils.Gms.IMPUTABLE_SEARCH_JIRA_CODES, gmsJiraBrowserRequest,
        new OnListRetrievedListener<GmsResponse>() {
          @Override
          public void onSuccess(List<GmsResponse> list) {
            if (getView() != null) {
              getView().hideProgressBar();
              if (list.get(0).getResult().getRecords().size() > 0 && list.get(0).getResult() != null) {
                jiraCodes = list.get(0).getResult().getRecords();
                getView().jiraCodesRetrieved(getJiraNames(jiraCodes));
              } else {
                getView().messageNoResults();
              }
            }
          }

          @Override
          public void onError(int errorId) {
            if (getView() != null) {
              getView().hideProgressBar();
              getView().showMessage(errorId);
            }
          }
        });
  }

  private List<Jira> getJiraNames(List<Record> records) {
    List<Jira> namesList = new ArrayList<>(records.size());
    for (Record record : records) {
      Jira jira = new Jira();
      jira.setKey(record.getKey());
      jira.setDescription(record.getDescription());
      namesList.add(jira);
    }
    return namesList;
  }

}