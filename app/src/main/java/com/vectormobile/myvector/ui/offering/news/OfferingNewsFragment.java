package com.vectormobile.myvector.ui.offering.news;

import com.google.gson.Gson;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.OfferingComponent;
import com.vectormobile.myvector.model.Post;
import com.vectormobile.myvector.ui.base.BaseFragment;
import com.vectormobile.myvector.ui.offering.OfferingActivity;
import com.vectormobile.myvector.ui.offering.detail.OfferingDetailFragment;
import com.vectormobile.myvector.ui.offering.news.adapter.OfferingNewsAdapter;
import com.vectormobile.myvector.ui.offering.news.adapter.OnPostClickListener;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by ajruiz on 10/11/2016.
 */

public class OfferingNewsFragment extends BaseFragment<OfferingNewsPresenter> implements OfferingNewsView, View.OnTouchListener, OnPostClickListener {

  private static final int GRID_NUM_COLUMNS = 2;
  OnNewsItemSelectedListener mCallback;
  @Inject
  OfferingNewsPresenter presenter;
  @BindView(R.id.rv_offering_news)
  RecyclerView recycler;
  @BindView(R.id.mainGridLayout)
  RelativeLayout gridLayout;
  private OfferingNewsAdapter adapter;
  private ProgressDialog loadingDialog;
  private int totalWidthSpan;
  private int sectionId;
  private int sectionColor;
  private String sectionTitle;
  private OfferingDetailFragment offeringDetailFragment;

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);

    Activity activity = null;
    if (context instanceof Activity) {
      activity = (Activity) context;
    }

    try {
      mCallback = (OnNewsItemSelectedListener) activity;
    } catch (ClassCastException e) {
      throw new ClassCastException(activity.toString()
          + " must implement OnNewsItemSelectedListener");
    }
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    Bundle bundle = getArguments();
    sectionId = bundle.getInt(OfferingActivity.KEY_OFFERING_SECTION_ID);
    sectionTitle = bundle.getString(OfferingActivity.KEY_OFFERING_SECTION_TITLE);

    sectionColor = bundle.getInt(OfferingActivity.KEY_OFFERING_SECTION_COLOR);
    LinearLayout llBackgroundOffering = (LinearLayout) getActivity().findViewById(R.id.ly_news_toolbar);
    llBackgroundOffering.setBackgroundColor(sectionColor);

    adapter = new OfferingNewsAdapter(this, getContext(), this);
    adapter.setGroupPosition(0);
    recycler.setAdapter(adapter);

    recycler.setLayoutManager(new GridLayoutManager(getContext(), GRID_NUM_COLUMNS));

    GridLayoutManager manager = (GridLayoutManager) recycler.getLayoutManager();
    totalWidthSpan = manager.getSpanCount();
    manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
      @Override
      public int getSpanSize(int position) {
        int spanSize = 1;
        if (adapter.getItemViewType(position) == 0) {
          spanSize = totalWidthSpan;
        }
        return spanSize;
      }
    });

    recycler.setHasFixedSize(true);

    TextView tvTitle = (TextView) getActivity().findViewById(R.id.tv_section_title);
    tvTitle.setText(sectionTitle);
    createLoadingDialog();
    presenter.getOfferingNews(sectionId, OfferingActivity.FIRST_PAGE);
  }

  @Override
  public void onPause() {
    super.onPause();
    OfferingActivity.setNewsOpened(false);
    TextView tvTitle = (TextView) getActivity().findViewById(R.id.tv_section_title);
    LinearLayout llBackgroundOffering = (LinearLayout) getActivity().findViewById(R.id.ly_news_toolbar);
    tvTitle.setText(getResources().getString(R.string.offering_commercial_tittle));
    llBackgroundOffering.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.tab_blue_vector));
  }

  private void createLoadingDialog() {
    loadingDialog = new ProgressDialog(getActivity(), R.style.CustomLoadingDialogTheme);
    loadingDialog.setCancelable(true);
    loadingDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialog) {
        getActivity().getSupportFragmentManager().popBackStack();
      }
    });
    loadingDialog.setCanceledOnTouchOutside(false);
    loadingDialog.show();
  }

  @Override
  public void hideLoadingDialog() {
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        if ((loadingDialog != null) && loadingDialog.isShowing()) {
          loadingDialog.dismiss();
        }
        loadingDialog = null;
      }
    }, 1000);
  }

  @Override
  protected void inject() {
    getComponent(OfferingComponent.class).inject(this);
  }

  @Override
  protected OfferingNewsPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.fragment_offering_news;
  }

  @Override
  public boolean onTouch(View view, MotionEvent motionEvent) {
    return false;
  }

  @Override
  public void onListPostRetrieved(List<Post> list) {
    hideLoadingDialog();
    adapter.setPosts(list);
  }

  @Override
  public void loadNewPage(int page) {
    presenter.getOfferingNews(sectionId, page);
  }

  @Override
  public void onPostClick(Post post) {
    if (!OfferingActivity.getNewsDetailOpened()) {
      OfferingActivity.setNewsDetailOpened(true);
      Bundle args = new Bundle();
      if (post != null) {
        args.putString(OfferingActivity.KEY_OFFERING_POST_SELECTED, new Gson().toJson(post));
        args.putInt(OfferingActivity.KEY_OFFERING_SECTION_COLOR, sectionColor);
        args.putString(OfferingActivity.KEY_OFFERING_SECTION_TITLE, sectionTitle);
      }
      offeringDetailFragment = new OfferingDetailFragment();
      offeringDetailFragment.setArguments(args);
      getFragmentManager().beginTransaction().add(R.id.fl_offering_container, offeringDetailFragment).addToBackStack(null).commit();
      mCallback.onNewsItemSelected();
    }
  }

  public OfferingDetailFragment getOfferingDetailFragment() {
    return offeringDetailFragment;
  }

  public interface OnNewsItemSelectedListener {

    void onNewsItemSelected();
  }
}
