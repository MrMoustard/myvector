package com.vectormobile.myvector.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.ui.login.LoginActivity;

public class SplashActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(getLayoutResourceId());
    final Intent intent = new Intent(this, LoginActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        startActivity(intent);
        finish();
      }
    }, 2000);
  }

  protected int getLayoutResourceId() {
    return R.layout.activity_splash;
  }
}
