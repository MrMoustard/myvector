package com.vectormobile.myvector.ui.gms.expenses.pdf;

import com.vectormobile.myvector.ui.base.PresenterView;

import java.util.Calendar;

/**
 * Created by kurt on 15/03/17.
 */

public interface ExpensePdfView extends PresenterView {

  void showDatePickerDialog(int calendarType, Calendar date);

  void setDateTo(String stringFrom);

  void setDateFrom(String stringTo);

  void navigateToDialog();
}
