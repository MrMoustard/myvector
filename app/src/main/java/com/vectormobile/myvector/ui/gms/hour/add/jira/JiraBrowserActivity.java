package com.vectormobile.myvector.ui.gms.hour.add.jira;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.DaggerJiraBrowserComponent;
import com.vectormobile.myvector.di.component.JiraBrowserComponent;
import com.vectormobile.myvector.di.module.JiraBrowserModule;
import com.vectormobile.myvector.model.gms.Jira;
import com.vectormobile.myvector.ui.base.BaseActivity;
import com.vectormobile.myvector.ui.gms.hour.add.AddHourActivity;
import com.vectormobile.myvector.ui.gms.hour.add.jira.adapter.JiraAdapter;
import com.vectormobile.myvector.ui.gms.hour.add.jira.adapter.OnJiraClickListener;
import com.vectormobile.myvector.util.text.TextUtils;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by ajruiz on 28/02/2017.
 */

public class JiraBrowserActivity extends BaseActivity<JiraBrowserPresenter>
    implements JiraBrowserView, OnJiraClickListener {

  public static int FIRST_PAGE = 1;
  public static final String SEARCH_FINALIZED = "operationFinalized";
  public static final String EXTRA_JIRA_SELECTED = "jiraSelected";

  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.et_jira_search)
  EditText etJiraSeach;
  @BindView(R.id.rv_jira_browser)
  RecyclerView rvJiraBrowser;

  @Inject
  JiraBrowserPresenter presenter;

  private String projectId;
  private JiraAdapter adapter;
  private JiraBrowserComponent component;
  private ProgressDialog loadingDialog;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);

    Bundle bundle = getIntent().getExtras();
    if (bundle != null) {
      projectId = bundle.getString(AddHourActivity.EXTRA_PROJECT_ID_JIRA);
    }

    adapter = new JiraAdapter(this, this);
    rvJiraBrowser.setAdapter(adapter);
    rvJiraBrowser.setLayoutManager(new LinearLayoutManager(this));

    etJiraSeach.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
          searchContacts();
          return true;
        }
        return false;
      }
    });

    etJiraSeach.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
        final int DRAWABLE_LEFT = 0;
        final int DRAWABLE_TOP = 1;
        final int DRAWABLE_RIGHT = 2;
        final int DRAWABLE_BOTTOM = 3;

        if (event.getAction() == MotionEvent.ACTION_UP) {
          if (event.getRawX() >=
              (etJiraSeach.getRight() - etJiraSeach.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
            searchContacts();
            return true;
          }
        }
        return false;
      }
    });
  }

  private void searchContacts() {
    if (projectId != null) {
      adapter.clearItems();
      presenter.searchJiraCode(projectId, etJiraSeach.getText().toString(), FIRST_PAGE);
    }
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    adapter.clearItems();
  }

  @Override
  protected void injectModule() {
    component = DaggerJiraBrowserComponent.builder()
        .appComponent(((App) getApplication()).getComponent())
        .jiraBrowserModule(new JiraBrowserModule(this))
        .build();
    component.inject(this);

  }

  @Override
  protected JiraBrowserPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.activity_jira_browser;
  }

  @Override
  public JiraBrowserComponent getComponent() {
    return component;
  }

  @Override
  public void jiraCodesRetrieved(List<Jira> jiraCodes) {
    adapter.setItems(jiraCodes);
    TextUtils.hideKeyboard(this);
  }

  @Override
  public void messageNoResults() {
    View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
    Snackbar.make(rootView, R.string.jira_no_results_were_found, Snackbar.LENGTH_SHORT).show();
  }

  @Override
  public void returnResult(String extra, String jira) {
    Intent returnIntent = new Intent();
    returnIntent.putExtra(SEARCH_FINALIZED, extra);
    returnIntent.putExtra(EXTRA_JIRA_SELECTED, jira);
    setResult(Activity.RESULT_OK, returnIntent);
    finish();
  }

  @Override
  public void showMessage(int messageId) {
    View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
    Snackbar.make(rootView, messageId, Snackbar.LENGTH_SHORT).show();
  }

  @Override
  public void showProgressBar() {
    loadingDialog = new ProgressDialog(this, R.style.CustomLoadingDialogTheme);
    loadingDialog.setCancelable(true);
    loadingDialog.show();
  }

  @Override
  public void hideProgressBar() {
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        if ((loadingDialog != null) && loadingDialog.isShowing()) {
          loadingDialog.dismiss();
        }
        loadingDialog = null;
      }
    }, 1000);
  }

  @Override
  public void onJiraClick(Jira jira) {
    String jiraSelected = jira.getKey() +
        TextUtils.Gms.GMS_EMPTY_SEARCH +
        jira.getDescription();
    returnResult(EXTRA_JIRA_SELECTED, jiraSelected);
  }
}
