package com.vectormobile.myvector.ui.gms.hour.add;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.AddHourComponent;
import com.vectormobile.myvector.di.component.DaggerAddHourComponent;
import com.vectormobile.myvector.di.module.AddHourModule;
import com.vectormobile.myvector.domain.entity.gms.response.Record;
import com.vectormobile.myvector.model.gms.Hour;
import com.vectormobile.myvector.ui.base.BaseActivity;
import com.vectormobile.myvector.ui.calendar.DatePickerFragment;
import com.vectormobile.myvector.ui.calendar.DatePickerFragment.OnDatePickerListener;
import com.vectormobile.myvector.ui.gms.hour.add.jira.JiraBrowserActivity;
import com.vectormobile.myvector.util.date.DateUtils;
import com.vectormobile.myvector.util.number.MinValueInputFilter;
import com.vectormobile.myvector.util.text.TextUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Erik Medina on 13/01/2017.
 */

public class AddHourActivity extends BaseActivity<AddHourPresenter>
    implements AddHourView, OnTouchListener, OnDatePickerListener {

  public static final String EXTRA_ACTION = "action";
  public static final int CREATE_ACTION = 1;
  public static final int UPDATE_ACTION = 2;
  public static final int DUPLICATE_ACTION = 3;

  public static final String EXTRA_DETAILS_HOUR = "hour";
  public static final String EXTRA_DETAILS_HOUR_DATE = "date";
  public static final String EXTRA_PROJECT_ID_JIRA = "EXTRA_PROJECT_ID_JIRA";
  public static final String EXTRA_DETAILS_ITEM_COUNTER = "items_counter";
  public static final String EXTRA_DETAILS_ITEM_ID = "item_id";

  public static final String OPERATION_FINALIZED = "operationFinalized";
  public static final String DUPLICATE = "duplicate";

  private static final int JIRA_BROWSER_RESULT = 1;

  private static final int FIRST_ITEM = 0;

  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.tv_add_hour_title)
  TextView tvAddHourTitle;
  @BindView(R.id.appbar)
  AppBarLayout appbar;
  @BindView(R.id.iv_duplicate_button)
  ImageView ivDuplicateButton;
  @BindView(R.id.iv_action_button)
  ImageView ivActionButton;
  @BindView(R.id.et_add_hour_date_from)
  EditText etAddHourDateFrom;
  @BindView(R.id.et_add_hour_date_to)
  EditText etAddHourDateTo;
  @BindView(R.id.s_add_hour_projects)
  Spinner sAddHourProjects;
  @BindView(R.id.s_add_hour_work_orders)
  Spinner sAddHourWorkOrders;
  @BindView(R.id.s_add_hour_categories)
  Spinner sAddHourCategories;
  @BindView(R.id.s_add_hour_jira_code)
  Spinner sAddHourJiraCodes;
  @BindView(R.id.et_add_hour_hours)
  EditText etAddHourHours;
  @BindView(R.id.et_add_hour_external_code)
  EditText etAddHourExternalCode;
  @BindView(R.id.et_add_hour_comment)
  EditText etAddHourComment;
  @BindView(R.id.b_add_hour_remove)
  ImageView bAddHourRemove;
  @BindView(R.id.b_add_hour_save)
  Button bAddHourSave;
  @BindView(R.id.b_add_hour_send)
  Button bAddHourSend;
  @BindView(R.id.container)
  LinearLayout container;
  @BindView(R.id.pb_add_hour)
  ProgressBar pbAddHour;
  @BindView(R.id.tv_hint_project)
  TextView tvHintProject;
  @BindView(R.id.tv_hint_work_order)
  TextView tvHintWorkOrder;
  @BindView(R.id.tv_hint_category)
  TextView tvHintCategory;
  @BindView(R.id.tv_hint_jira_code)
  TextView tvHintJiraCode;
  @BindView(R.id.iv_add_hour_jira_browser)
  ImageView ivJiraBrowser;
  @BindView(R.id.ll_add_hour_jira_code)
  RelativeLayout layoutJiraCode;
  @Inject
  AddHourPresenter presenter;

  private AddHourComponent component;
  private int projectPositionSelected = -1, workOrderPositionSelected = -1,
      categoryPositionSelected = -1, secondIdPositionSelected = 0;

  private int sProjectPositionFixed;
  private int sJiraPositionFixed;
  private int sWorkOrderPositionFixed;
  private int sCategoryPositionFixed;

  private String dateFrom;
  private String dateTo;

  private ArrayAdapter<String> sProjectsAdapter;
  private ArrayAdapter<String> sWorkOrdersAdapter;
  private ArrayAdapter<String> sCategoryAdapter;
  private ArrayAdapter<String> sJiraAdapter;

  private int action;
  private List<String> hoursIds;
  private Hour hour;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    retrieveExtras();

    setUpActionBar();
    presenter.setUpActionBarTitle(action);
    presenter.shouldShowHiddenButtons(action);
    presenter.shouldShowDuplicateButton(action);

    presenter.fillForm(action, hour);
    presenter.initializeView(action);
    presenter.getProjects();
    initializeDataWatchers();

    Drawable drawable = getResources().getDrawable(R.drawable.ic_calendar);
    DrawableCompat.setTint(drawable, getResources().getColor(R.color.grey_vector));
    etAddHourDateFrom.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
    etAddHourDateTo.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);

    etAddHourDateFrom.setOnTouchListener(this);
    etAddHourDateTo.setOnTouchListener(this);
  }

  private void setUpActionBar() {
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);
  }

  private void retrieveExtras() {
    Bundle bundle = getIntent().getExtras();
    if (bundle != null) {
      action = bundle.getInt(EXTRA_ACTION);
      if (action != CREATE_ACTION) {
        hour = bundle.getParcelable(EXTRA_DETAILS_HOUR);
        hoursIds = new ArrayList<>();
        hoursIds.add(hour.getId());
      }
    }
  }

  private void initializeDataWatchers() {
    etAddHourDateFrom.addTextChangedListener(textWatcher);
    etAddHourDateTo.addTextChangedListener(textWatcher);
    etAddHourHours.addTextChangedListener(textWatcher);
    etAddHourComment.addTextChangedListener(textWatcher);
  }

  private TextWatcher textWatcher = new TextWatcher() {

    public void afterTextChanged(Editable s) {
      presenter.dataChanged();
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }
  };

  @Override
  protected void injectModule() {
    component = DaggerAddHourComponent.builder()
        .appComponent(((App) getApplication()).getComponent())
        .addHourModule(new AddHourModule(this))
        .build();
    component.inject(this);
  }

  @Override
  protected AddHourPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.activity_add_hour;
  }

  @Override
  public AddHourComponent getComponent() {
    return component;
  }

  @Override
  public boolean onTouch(View view, MotionEvent event) {
    int CALENDAR_EXPENSE_FROM = 0;
    int CALENDAR_EXPENSE_TO = 1;
    if (event.getAction() == MotionEvent.ACTION_UP) {
      switch (view.getId()) {
        case R.id.et_add_hour_date_from:
          presenter.onCalendarIconClicked(
              CALENDAR_EXPENSE_FROM,
              getTypedDate(etAddHourDateFrom.getText().toString(), null));
          return true;
        case R.id.et_add_hour_date_to:
          presenter.onCalendarIconClicked(
              CALENDAR_EXPENSE_TO,
              getTypedDate(etAddHourDateTo.getText().toString(), etAddHourDateFrom.getText().toString()));
          return true;
      }
    }
    return false;
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode == Activity.RESULT_OK) {
      if (requestCode == JIRA_BROWSER_RESULT) {
        String jiraSelected = data.getExtras().getString(JiraBrowserActivity.EXTRA_JIRA_SELECTED);
        if (sJiraAdapter != null) {
          sJiraAdapter.add(jiraSelected);
          sJiraAdapter.notifyDataSetChanged();
          sAddHourJiraCodes.setSelection(sJiraAdapter.getCount() - 1);
        }
      } else if (requestCode == DUPLICATE_ACTION) {
        returnResult(AddHourActivity.DUPLICATE, dateFrom, 1);
      }
    }
  }

  private Calendar getTypedDate(String dateOneString, String dateTwoString) {
    Calendar calendar;

    calendar = DateUtils.convertStringToCalendar(dateOneString, DateUtils.CALENDAR_DATE_FORMAT);
    if (calendar == null)
      calendar = DateUtils.convertStringToCalendar(dateTwoString, DateUtils.CALENDAR_DATE_FORMAT);

    return (calendar != null) ? calendar : Calendar.getInstance();
  }

  @Override
  public void onBackPressed() {
    if (presenter.isDataChanged() || !presenter.isDataSaved()) {
      showAlertDialog();
    } else {
      super.onBackPressed();
    }
  }

  private void showAlertDialog() {
    new AlertDialog.Builder(this)
        .setTitle(getResources().getString(R.string.gms_message_imputation_not_saved_title))
        .setMessage(getResources().getString(R.string.gms_message_imputation_not_saved))
        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            finish();
          }
        })
        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
          }
        })
        .show();
  }

  @Override
  public void showHiddenButtons() {
    bAddHourRemove.setVisibility(View.VISIBLE);
    ivActionButton.setVisibility(View.VISIBLE);
    bAddHourSend.setVisibility(View.VISIBLE);
  }

  @Override
  public void showDuplicateButton() {
    ivDuplicateButton.setVisibility(View.VISIBLE);
  }

  @Override
  public void setDefaultDate(String date) {
    dateFrom = date;
    etAddHourDateFrom.setText(date);
  }

  @Override
  public void populateProjects(List<Record> list) {
    List<String> projects = getNamesList(list);
    projects.add(0, getString(R.string.gms_hint_project));
    sProjectsAdapter = new ArrayAdapter<>(this, R.layout.item_spinner, projects);
    sProjectsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    sAddHourProjects.setAdapter(sProjectsAdapter);
  }

  @Override
  public void populateWorkOrders(List<Record> list) {
    List<String> workOrders = getNamesList(list);
    workOrders.add(0, getString(R.string.gms_hint_work_order));
    sWorkOrdersAdapter = new ArrayAdapter<>(this, R.layout.item_spinner, workOrders);
    sWorkOrdersAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    sAddHourWorkOrders.setAdapter(sWorkOrdersAdapter);
  }

  @Override
  public void populateImputationCategory(List<Record> list) {
    sAddHourCategories.setVisibility(View.VISIBLE);
    List<String> imputationCategories = getNamesList(list);
    imputationCategories.add(0, getString(R.string.hour_hint_add_hour_category));
    sCategoryAdapter = new ArrayAdapter<>(this, R.layout.item_spinner,
        imputationCategories);
    sCategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    sAddHourCategories.setAdapter(sCategoryAdapter);
  }

  @Override
  public void populateJiraCodes(List<Record> list) {
    layoutJiraCode.setVisibility(View.VISIBLE);
    List<String> imputationJiras = getNamesList(list);
    imputationJiras.add(0, getString(R.string.hour_hint_add_hour_jira_code));
    sJiraAdapter = new ArrayAdapter<>(this, R.layout.item_spinner, imputationJiras);
    sJiraAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    sAddHourJiraCodes.setAdapter(sJiraAdapter);
    sAddHourJiraCodes.setSelection(sJiraAdapter.getCount() - 1);
  }

  @Override
  public boolean isCategoriesSpinnerVisible() {
    return sAddHourCategories.getVisibility() == View.VISIBLE;
  }

  private List<String> getNamesList(List<Record> records) {
    List<String> namesList = new ArrayList<>();
    for (Record record : records) {
      namesList.add(record.getText());
    }
    return namesList;
  }

  @Override
  public void showDatePickerDialog(int calendarType, Calendar date) {
    Bundle bundle = new Bundle();
    bundle.putInt(DatePickerFragment.CALENDAR_TYPE, calendarType);
    bundle.putLong(DatePickerFragment.CALENDAR_DATE, date.getTimeInMillis());
    DialogFragment datePickerFragment = new DatePickerFragment();
    datePickerFragment.setArguments(bundle);
    datePickerFragment.show(getFragmentManager(), "datePicker");
  }

  @Override
  public void showImputationCategorySpinner(boolean show) {
    sAddHourCategories.setVisibility(show ? View.VISIBLE : View.GONE);
  }

  @Override
  public String getHours() {
    return etAddHourHours.getText().toString();
  }

  @Override
  public String getExternalCode() {
    return etAddHourExternalCode.getText().toString();
  }

  @Override
  public String getComment() {
    return etAddHourComment.getText().toString();
  }

  @Override
  public String getHourImputationDateFrom() {
    return etAddHourDateFrom.getText().toString();
  }

  @Override
  public String getHourImputationDateTo() {
    return etAddHourDateTo.getText().toString();
  }

  @Override
  public String getJiraCode() {
    if (sAddHourJiraCodes.getSelectedItem() != null) {
      return sAddHourJiraCodes.getSelectedItem().toString();
    } else {
      return null;
    }
  }

  @Override
  public int getCategorySecondIdPosition() {
    return secondIdPositionSelected;
  }

  @Override
  public void showMessage(int messageId) {
    View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
    Snackbar.make(rootView, messageId, Snackbar.LENGTH_SHORT).show();
  }

  @Override
  public void showDialogMessage(String message) {
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setMessage(message)
        .setCancelable(false)
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
          }
        });
    AlertDialog alert = builder.create();
    alert.show();
  }

  @Override
  public int getProjectPositionSelected() {
    return projectPositionSelected;
  }

  @Override
  public int getWorkOrderPositionSelected() {
    return workOrderPositionSelected;
  }

  @Override
  public int getCategoryPositionSelected() {
    return categoryPositionSelected;
  }

  @Override
  public void setDateFrom(String date) {
    this.dateFrom = date;
    etAddHourDateFrom.setText(date);
  }

  @Override
  public void setDateTo(String date) {
    dateTo = date;
    etAddHourDateTo.setText(date);
  }

  @Override
  public void setMinimumValueForHours(int minimumHours) {
    etAddHourHours.setFilters(new InputFilter[]{new MinValueInputFilter(minimumHours)});
  }

  @Override
  public void setHours(int hours) {
    etAddHourHours.setText(String.valueOf(hours));
  }

  @Override
  public void setComment(String comment) {
    etAddHourComment.setText(comment);
  }

  @Override
  public void setProjectItem(String projectName) {
    if (sProjectsAdapter.getCount() > 0) {
      if (!hour.equals(null)) {
        sProjectPositionFixed = sProjectsAdapter.getPosition(projectName);
        sAddHourProjects.setSelection(sProjectPositionFixed);
      }
    }
  }

  @Override
  public void setWorkOrderItem(String workOrderName) {
    if (sWorkOrdersAdapter.getCount() > 0) {
      if (!hour.equals(null)) {
        sWorkOrderPositionFixed = sWorkOrdersAdapter.getPosition(workOrderName);
        if (sWorkOrderPositionFixed != -1) {
          sAddHourWorkOrders.setSelection(sWorkOrderPositionFixed);
        } else {
          showMessage(R.string.gms_message_work_order_error);
        }
      }
    }
  }

  @Override
  public void setCategoryItem(String categoryName) {
    if (isCategoriesSpinnerVisible()) {
      if (sCategoryAdapter.getCount() > 0) {
        if (hour != null) {
          sCategoryPositionFixed = sCategoryAdapter.getPosition(categoryName);
          sAddHourCategories.setSelection(sCategoryPositionFixed);
        }
      }
    }
  }

  @Override
  public void dateSelected(String date, int calendarType) {
    presenter.setDataNotSaved();
    if (calendarType == TextUtils.Gms.CALENDAR_EXPENSE_FROM) {
      dateFrom = date;
      etAddHourDateFrom.setText(date);
      presenter.getProjects();
    } else if (calendarType == TextUtils.Gms.CALENDAR_EXPENSE_TO) {
      dateTo = date;
      etAddHourDateTo.setText(date);
    }
    presenter.compareDates(calendarType, dateFrom, dateTo);
  }

  @Override
  public void clearDate() {
    etAddHourDateTo.setText("");
  }

  @OnItemSelected(R.id.s_add_hour_projects)
  public void onProjectItemSelected(int position) {
    sAddHourCategories.setVisibility(View.GONE);
    projectPositionSelected = position - 1;
    presenter.getWorkOrders(projectPositionSelected);
    presenter.getJiraCodes(projectPositionSelected);
    if (position != FIRST_ITEM) {
      tvHintProject.setVisibility(View.VISIBLE);
      TextUtils.changeTextColorItemSpinner(this, sAddHourProjects);
    } else {
      tvHintProject.setVisibility(View.INVISIBLE);
    }
    if (sProjectPositionFixed != position) {
      presenter.setDataNotSaved();
    }
    if (hour != null && sProjectPositionFixed != position) {
      presenter.dataChanged();
    }
  }

  @OnItemSelected(R.id.s_add_hour_work_orders)
  public void onWorkOrderItemSelected(int position) {
    workOrderPositionSelected = position - 1;
    presenter.getImputationFirstCategory(workOrderPositionSelected);
    if (position != FIRST_ITEM) {
      tvHintWorkOrder.setVisibility(View.VISIBLE);
      TextUtils.changeTextColorItemSpinner(this, sAddHourWorkOrders);
    } else {
      tvHintWorkOrder.setVisibility(View.INVISIBLE);
    }
    if (hour != null && sWorkOrderPositionFixed != position) {
      presenter.dataChanged();
    }
  }

  @OnItemSelected(R.id.s_add_hour_categories)
  public void onCategoriesItemSelected(int position) {
    categoryPositionSelected = position - 1;
    if (position != FIRST_ITEM) {
      tvHintCategory.setVisibility(View.VISIBLE);
      secondIdPositionSelected = position - 1;
      presenter.updateCategorySecondId();
      TextUtils.changeTextColorItemSpinner(this, sAddHourCategories);
    } else {
      tvHintCategory.setVisibility(View.INVISIBLE);
    }
    if (hour != null && sCategoryPositionFixed != position) {
      presenter.dataChanged();
    }
  }

  @OnItemSelected(R.id.s_add_hour_jira_code)
  public void onJiraCodeItemSelected(int position) {
    if (position != FIRST_ITEM) {
      tvHintJiraCode.setVisibility(View.VISIBLE);
      TextUtils.changeTextColorItemSpinner(this, sAddHourJiraCodes);
    } else {
      tvHintJiraCode.setVisibility(View.GONE);
    }

  }

  @OnClick(R.id.iv_duplicate_button)
  public void onDuplicateButtonHasBeenClicked() {
    Intent iAddHour = new Intent(AddHourActivity.this, AddHourActivity.class);
    iAddHour.putExtra(AddHourActivity.EXTRA_ACTION, AddHourActivity.DUPLICATE_ACTION);
    iAddHour.putExtra(AddHourActivity.EXTRA_DETAILS_HOUR, hour);
    startActivityForResult(iAddHour, AddHourActivity.DUPLICATE_ACTION);
  }

  @OnClick(R.id.b_add_hour_remove)
  public void onRemoveButtonClicked() {
    presenter.removeImputation(hoursIds);
  }

  @OnClick(R.id.b_add_hour_save)
  public void onSaveButtonClicked() {
    presenter.saveImputation(AddHourPresenter.SAVE_ONLY);
  }

  @OnClick({R.id.b_add_hour_send, R.id.iv_action_button})
  public void onSendButtonClicked() {
    presenter.sendImputation(hoursIds);
  }

  @OnClick(R.id.iv_add_hour_jira_browser)
  public void onJiraBrowserClicked() {
    Intent intent = new Intent(this, JiraBrowserActivity.class);
    Bundle bundle = new Bundle();
    bundle.putString(EXTRA_PROJECT_ID_JIRA, presenter.getProjectSelectedId());
    intent.putExtras(bundle);
    startActivityForResult(intent, JIRA_BROWSER_RESULT);
  }

  @Override
  public void returnResult(String action, String date, int itemCounter) {
    Intent returnIntent = new Intent();
    returnIntent.putExtra(OPERATION_FINALIZED, action);
    returnIntent.putExtra(EXTRA_DETAILS_HOUR_DATE, date);
    returnIntent.putExtra(EXTRA_DETAILS_ITEM_COUNTER, itemCounter);
    setResult(Activity.RESULT_OK, returnIntent);
    finish();
  }

  @Override
  public void makeCommentFieldScrollable() {
    etAddHourComment.setOnTouchListener(new OnTouchListener() {
      public boolean onTouch(View view, MotionEvent event) {
        if (view.getId() == R.id.et_add_hour_comment) {
          view.getParent().requestDisallowInterceptTouchEvent(true);
          switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP:
              view.getParent().requestDisallowInterceptTouchEvent(false);
              break;
          }
        }
        return false;
      }
    });
  }

  @Override
  public void showProgressBar() {
    pbAddHour.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideProgressBar() {
    pbAddHour.setVisibility(View.GONE);
  }

  @Override
  public void setTitle(int titleId) {
    tvAddHourTitle.setText(titleId);
  }

  @Override
  public void hideJiraSpinner() {
    layoutJiraCode.setVisibility(View.GONE);
  }

  @Override
  public String getDateFrom() {
    return dateFrom;
  }
}
