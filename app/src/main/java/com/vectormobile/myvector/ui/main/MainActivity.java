package com.vectormobile.myvector.ui.main;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import butterknife.BindView;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.DaggerMainComponent;
import com.vectormobile.myvector.di.component.MainComponent;
import com.vectormobile.myvector.di.module.MainModule;
import com.vectormobile.myvector.ui.base.BaseActivity;
import com.vectormobile.myvector.ui.main.adapter.SectionsPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class MainActivity extends BaseActivity<MainPresenter> implements MainView {

  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.tabs)
  TabLayout tabs;
  @BindView(R.id.appbar)
  AppBarLayout appbar;
  @BindView(R.id.vp_container)
  ViewPager vpContainer;
  @BindView(R.id.fab)
  FloatingActionButton fab;
  @BindView(R.id.main_content)
  CoordinatorLayout mainContent;

  private static final int NUMBER_OF_FRAGMENTS = 3;

  @Inject
  MainPresenter presenter;

  private SectionsPagerAdapter sectionsPagerAdapter;
  private MainComponent component;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setSupportActionBar(toolbar);
    sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
    vpContainer.setAdapter(sectionsPagerAdapter);
    vpContainer.setOffscreenPageLimit(NUMBER_OF_FRAGMENTS);
    setupTabs();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
  }

  @Override
  protected void onResume() {
    super.onResume();
  }

  @Override
  protected void injectModule() {
    component = DaggerMainComponent.builder()
        .appComponent(((App) getApplication()).getComponent())
        .mainModule(new MainModule(this))
        .build();
    component.inject(this);
  }

  @Override
  protected MainPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.activity_main;
  }

  @Override
  public MainComponent getComponent() {
    if(component==null){
      injectModule();
    }
    return component;
  }

  private void setupTabs() {
    List<Integer> icons = new ArrayList<>();
    icons.add(R.drawable.tab_perfil);
    icons.add(R.drawable.tab_comunicacion);
    icons.add(R.drawable.tab_corporativo);
    tabs.setupWithViewPager(vpContainer);

    for (int i = 0; i < tabs.getTabCount(); i++) {
      if (icons.get(i) != null) {
        tabs.getTabAt(i).setIcon(icons.get(i));
      }
    }
  }
}
