package com.vectormobile.myvector.ui.news;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.BindView;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.DaggerNewsComponent;
import com.vectormobile.myvector.di.component.NewsComponent;
import com.vectormobile.myvector.di.module.NewsModule;
import com.vectormobile.myvector.ui.base.BaseActivity;
import com.vectormobile.myvector.ui.news.news.NewsFragment;

import javax.inject.Inject;

/**
 * Created by ajruiz on 11/11/2016.
 */

public class NewsActivity extends BaseActivity<NewsPresenter> implements NewsView, AppBarLayout.OnOffsetChangedListener {

  public static final String KEY_NEWS_URL_SELECTED = "NEWS_URL_SELECTED";
  public static final String KEY_NEWS_IMAGE_SELECTED = "NEWS_IMAGE_SELECTED";
  public static final String KEY_NEWS_TITLE_SELECTED = "NEWS_TITLE_SELECTED";
  public static String KEY_URL = "URL_TO_LOAD";
  private static boolean newsOpened;

  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.fl_news_container)
  FrameLayout fragmentContainer;
  @BindView(R.id.appbar)
  AppBarLayout barLayout;
  @BindView(R.id.tv_news_title)
  TextView tvNewsTitle;

  @Inject
  NewsPresenter presenter;

  private NewsComponent component;
  private State currentState = State.IDLE;

  NewsFragment newsFragment = new NewsFragment();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);

    barLayout.addOnOffsetChangedListener(this);
    newsOpened = false;
    setNewsFragment();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        super.onBackPressed();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  private void setNewsFragment() {
    getSupportFragmentManager().beginTransaction().replace(R.id.fl_news_container, newsFragment).commit();
  }

  public static boolean isNewsOpened() {
    return newsOpened;
  }

  public static void setNewsOpened(boolean newsOpened) {
    NewsActivity.newsOpened = newsOpened;
  }

  @Override
  protected void injectModule() {
    component = DaggerNewsComponent.builder()
        .appComponent(((App) getApplication()).getComponent())
        .newsModule(new NewsModule(this))
        .build();
    component.inject(this);
  }

  @Override
  protected NewsPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.activity_news;
  }

  @Override
  public NewsComponent getComponent() {
    return component;
  }

  @Override
  public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
    if (verticalOffset == 0) {
      if (currentState != State.EXPANDED) {
        tvNewsTitle.setSingleLine(false);
      }
      currentState = State.EXPANDED;
    } else if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
      if (currentState != State.COLLAPSED) {
        tvNewsTitle.setSingleLine(true);
        tvNewsTitle.setEllipsize(TextUtils.TruncateAt.END);
      }
      currentState = State.COLLAPSED;
    } else {
      currentState = State.IDLE;
    }
  }

  public enum State {
    EXPANDED,
    COLLAPSED,
    IDLE
  }
}
