package com.vectormobile.myvector.ui.browser.detail;

import com.vectormobile.myvector.model.intranet.Contact;
import com.vectormobile.myvector.ui.base.PresenterView;

/**
 * Created by ajruiz on 03/02/2017.
 */

public interface ContactsDetailView extends PresenterView {

  void setContactImage(String imageUrl);

  void setContactName(String name);

  void setContactDetails(Contact contact);

}
