package com.vectormobile.myvector.ui.offering.menu.adapter;

import com.vectormobile.myvector.model.offering.Section;

/**
 * Created by ajruiz on 10/11/2016.
 */

public interface OnItemClickListener {
  void onItemClick(Section item);
}
