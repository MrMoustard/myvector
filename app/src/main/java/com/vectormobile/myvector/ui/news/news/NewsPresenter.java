package com.vectormobile.myvector.ui.news.news;

import com.vectormobile.myvector.domain.interactor.intranet.NewsInteractor;
import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.model.news.NewsItem;
import com.vectormobile.myvector.ui.base.BasePresenter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by ajruiz on 11/11/2016.
 */

public class NewsPresenter extends BasePresenter<NewsView> {

  private NewsInteractor interactor;
  private List<NewsItem> newsList;

  @Inject
  public NewsPresenter(NewsView fragment, NewsInteractor interactor) {
    super(fragment);
    this.interactor = interactor;
    newsList = new ArrayList<>();
  }

  public void getIntranetNews(int page) {
    interactor.execute(page, new OnItemRetrievedListener() {
      @Override
      public void onSuccess(Object item) {
        if (getView() != null) {
          getView().fillNewsList(parseNewsContent(item.toString()));
        }
      }

      @Override
      public void onError(int errorId) {
        if (getView() != null) {
          getView().showError(errorId);
          getView().hideLoadingDialog();
        }
      }
    });
  }

  private List<NewsItem> parseNewsContent(String content) {
    newsList.clear();
    Document doc = Jsoup.parseBodyFragment(content);
    Elements element = doc.select("div[class=ivp-box-post]");
    doc.outputSettings().charset("UTF-8");
    for (int i = 0; i < element.size(); i++) {
      NewsItem newsItem = new NewsItem();
      newsItem.setNewsItemTitle(element.get(i).select("h3").html());
      newsItem.setNewsItemImage(element.get(i).select("img").attr("src"));
      newsItem.setNewsUrl(element.get(i).select("a").attr("href"));
      newsList.add(newsItem);
    }
    return newsList;
  }

}
