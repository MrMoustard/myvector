package com.vectormobile.myvector.ui.outlook.mail;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import butterknife.BindView;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.DaggerOutlookMailComponent;
import com.vectormobile.myvector.di.component.OutlookMailComponent;
import com.vectormobile.myvector.di.module.OutlookMailModule;
import com.vectormobile.myvector.ui.base.BaseActivity;

import javax.inject.Inject;

/**
 * Created by amarina on 13/03/2017.
 */

public class OutlookMailActivity extends BaseActivity<OutlookMailPresenter> implements OutlookMailView, OutlookMailListener {
  @Inject
  OutlookMailPresenter presenter;
  @Inject
  OutlookMailWebView webView;

  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.pb_login)
  ProgressBar pbLoading;
  @BindView(R.id.fm_outlook_section)
  FrameLayout contentOutlookSection;

  private OutlookMailComponent component;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);
    if (webView.getParent() != null) {
      ((ViewGroup) webView.getParent()).removeAllViews();
    }
    contentOutlookSection.addView(webView);
    webView.setListener(this);
  }

  @Override
  public void onDestroy() {
    webView.setListener(null);
    webView = null;
    super.onDestroy();
  }

  @Override
  protected void injectModule() {
    component = DaggerOutlookMailComponent.builder()
        .appComponent(((App) getApplication()).getComponent())
        .outlookMailModule(new OutlookMailModule(this))
        .build();
    component.inject(this);
  }

  @Override
  protected OutlookMailPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.activity_outlook_section;
  }

  @Override
  public OutlookMailComponent getComponent() {
    return component;
  }

  @Override
  public void onPageFinished() {
    if (pbLoading != null) {
      pbLoading.setVisibility(View.GONE);
    }
  }
}
