package com.vectormobile.myvector.ui.gms.hour.list.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bignerdranch.android.multiselector.MultiSelector;
import com.bignerdranch.android.multiselector.SwappingHolder;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.model.gms.Expense;
import com.vectormobile.myvector.model.gms.Hour;
import com.vectormobile.myvector.model.gms.HourItemRecycler;
import com.vectormobile.myvector.ui.gms.expenses.list.adapter.OnItemClickListener;
import com.vectormobile.myvector.ui.gms.hour.list.HoursListActivity;
import com.vectormobile.myvector.util.date.DateUtils;
import com.vectormobile.myvector.util.text.TextUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by ajruiz on 24/01/2017.
 */

public class HourAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private static final int ITEM_VIEW_TYPE_HEADER = 0;
  private static final int ITEM_VIEW_TYPE_ITEM = 1;
  private static final int FIRST_ITEM = 0;

  private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();

  private Context context;

  private List<Hour> tempImputationsList;
  private List<HourItemRecycler> itemsToShow;
  private OnItemClickListener listener;
  private Date weekStarts;
  private Date weekEnds;
  private MultiSelector multiSelector;

  public HourAdapter(Context context, OnItemClickListener listener) {
    itemsToShow = new ArrayList<>();
    this.context = context;
    this.listener = listener;
    setUpSwipe();
  }

  private void setUpSwipe() {
//    viewBinderHelper.setOpenOnlyOne(true);
  }

  public void setMultiSelector(MultiSelector multiSelector) {
    this.multiSelector = multiSelector;
  }

  public List<HourItemRecycler> getItemsToShow() {
    return itemsToShow;
  }

  private List<Hour> sortHoursListByHourDate(List<Hour> hours) {
    Collections.sort(hours, new Comparator<Hour>() {
      public int compare(Hour hour1, Hour hour2) {
        return Long.valueOf(hour1.getHourDate()).compareTo(hour2.getHourDate());
      }
    });
    return hours;
  }

  private void prepareArray(List<Hour> hours) {
    tempImputationsList = new ArrayList<>(hours);
    if (tempImputationsList.size() == 0)
      return;
    long weekStartLong = tempImputationsList.get(0).getWeekFrom();
    weekStarts = DateUtils.convertMillisecondsToDate(tempImputationsList.get(0).getWeekFrom());
    weekEnds = DateUtils.convertMillisecondsToDate(tempImputationsList.get(0).getWeekTo());

    insertHeader(itemsToShow);
    int i = 0;
    while (tempImputationsList.size() != 0 && i < tempImputationsList.size()) {
      if (tempImputationsList.get(i).getWeekFrom() == weekStartLong) {
        itemsToShow.add(new HourItemRecycler(tempImputationsList.get(i), false,
            Arrays.asList(weekStarts, weekEnds)));
        tempImputationsList.remove(i);
        i = 0;
      } else {
        i++;
      }
    }
    prepareArray(tempImputationsList);
  }

  private void insertHeader(List<HourItemRecycler> hours) {
    hours.add(new HourItemRecycler(true, Arrays.asList(weekStarts, weekEnds)));
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    RecyclerView.ViewHolder holder = null;
    View view;
    switch (viewType) {
      case ITEM_VIEW_TYPE_HEADER:
        view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.item_expense_date_row, parent, false);
        holder = new HourAdapter.HeaderViewHolder(view);
        break;
      case ITEM_VIEW_TYPE_ITEM:
        view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.view_swipe_row, parent, false);
        holder = new HourAdapter.ItemViewHolder(context, view);
        break;
    }
    return holder;
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int pos) {
    switch (getItemViewType(pos)) {
      case ITEM_VIEW_TYPE_HEADER:
        ((HourAdapter.HeaderViewHolder) holder).range.setText(
            String.format(
                Locale.getDefault(),
                context.getString(R.string.expense_header_list),
                getDateString(itemsToShow.get(pos).getWeekStarts()),
                getDateString(itemsToShow.get(pos).getWeekEnds())));
        break;
      case ITEM_VIEW_TYPE_ITEM:
        setItemImage((HourAdapter.ItemViewHolder) holder, itemsToShow.get(pos).getState());
        ((HourAdapter.ItemViewHolder) holder).date.setText(itemsToShow.get(pos).getDateString());
        ((HourAdapter.ItemViewHolder) holder).title.setText(itemsToShow.get(pos).getWorkOrderName());
        ((HourAdapter.ItemViewHolder) holder).amount.setText((String.format(Locale.getDefault(),
            context.getString(R.string.hour_list_), itemsToShow.get(pos).getUnits())));
        ((HourAdapter.ItemViewHolder) holder).bind(itemsToShow.get(pos), listener);
        ((HourAdapter.ItemViewHolder) holder).setSelectionModeBackgroundDrawable(
            ContextCompat.getDrawable(context, R.drawable.background_multiselected_item));


        if ((pos + 1 < getItemCount() && itemsToShow.get(pos + 1).isHeader()))
          ((HourAdapter.ItemViewHolder) holder).changeSeparatorType();

        setSwipeItemGesture(holder, pos);
        break;
    }
  }

  private void setSwipeItemGesture(RecyclerView.ViewHolder holder, int pos) {
    // Save/restore the open/close state.
    // You need to provide a String id which uniquely defines the data object.
    viewBinderHelper.bind(((ItemViewHolder) holder).swipeLayout, itemsToShow.get(pos).getId());

    if (!itemsToShow.get(pos).getState().equals(Hour.HOUR_SAVED) &&
        !itemsToShow.get(pos).getState().equals(Hour.HOUR_REJECTED)) {
      ((ItemViewHolder) holder).swipeLayout.setLockDrag(true);
    }
    ((ItemViewHolder) holder).swipeLayout.setMinFlingVelocity(30);
  }

  @Override
  public int getItemViewType(int position) {
    return itemsToShow.get(position).isHeader() ? ITEM_VIEW_TYPE_HEADER : ITEM_VIEW_TYPE_ITEM;
  }

  private String getDateString(Date date) {
    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
    return formatter.format(date);
  }

  private void setItemImage(HourAdapter.ItemViewHolder holder, String hourState) {
    switch (hourState) {
      case Hour.HOUR_SENT:
        holder.image.setImageResource(R.drawable.ic_send);
        holder.image.setBackgroundResource(R.drawable.circle_item_blue);
        break;
      case Hour.HOUR_APPROVED:
        holder.image.setImageResource(R.drawable.ic_accept);
        holder.image.setBackgroundResource(R.drawable.circle_item_green);
        break;
      case Hour.HOUR_REJECTED:
        holder.image.setImageResource(R.drawable.ic_reject);
        holder.image.setBackgroundResource(R.drawable.circle_item_red);
        break;
      case Hour.HOUR_SAVED:
        holder.image.setImageResource(R.drawable.ic_edit);
        holder.image.setBackgroundResource(R.drawable.circle_item_steel);
        break;
    }
  }

  @Override
  public int getItemCount() {
    return itemsToShow.size();
  }

  public void addItems(List<Hour> hours) {
    prepareArray(sortHoursListByHourDate(hours));
    this.notifyDataSetChanged();
  }

  public void removeItems(List<Integer> selectedPositions) {
    List<Integer> reverseList = selectedPositions.subList(FIRST_ITEM, selectedPositions.size());
    Collections.reverse(reverseList);
    for (int position : reverseList) {
      if (position < itemsToShow.size()) {
        itemsToShow.remove(position);
        this.notifyItemRemoved(position);
      }
    }
  }

  public void removeItem(String itemId) {
    for (HourItemRecycler item : itemsToShow) {
      if (itemId.equals(item.getId())) {
        itemsToShow.remove(item);
        notifyItemRemoved(itemsToShow.indexOf(item));
        break;
      }
    }
  }

  public void setItemsSent(List<Integer> selectedPositions) {
    List<Integer> reverseList = selectedPositions.subList(FIRST_ITEM, selectedPositions.size());
    Collections.reverse(reverseList);
    for (int position : reverseList) {
      if (position < itemsToShow.size()) {
        itemsToShow.get(position).setState(TextUtils.Gms.IMPUTATION_STATUS_SENT);
      }
    }
    this.notifyDataSetChanged();
  }

  public void reset() {
    itemsToShow.clear();
    notifyDataSetChanged();
  }

  public void closeSwipedItems() {
    for (HourItemRecycler item : itemsToShow) {
      if (!item.isHeader())
        viewBinderHelper.closeLayout(item.getId());
    }
  }

  public void enableSwipe() {
    for (HourItemRecycler hir : itemsToShow) {
      if (!hir.isHeader()) {
        if (hir.getState() != null && hir.getState().equals(Expense.EXPENSE_SAVED) ||
            hir.getState().equals(Expense.EXPENSE_REJECTED)) {
          viewBinderHelper.unlockSwipe(hir.getId());
        }
      }
    }
  }

  static class HeaderViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.date_range)
    TextView range;

    public HeaderViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }

  class ItemViewHolder extends SwappingHolder {

    @BindView(R.id.swipe_layout)
    SwipeRevealLayout swipeLayout;
    @BindView(R.id.rl)
    RelativeLayout rlRowContainer;
    @BindView(R.id.item_image)
    ImageView image;
    @BindView(R.id.mini_camera_img)
    ImageView imageCamera;
    @BindView(R.id.item_date)
    TextView date;
    @BindView(R.id.item_title)
    TextView title;
    @BindView(R.id.item_amount)
    TextView amount;
    @BindView(R.id.item_separator)
    View separator;
    @BindView(R.id.item_alt_separator)
    View separatorAlt;

    @BindView(R.id.sw_delete_button)
    TextView deleteBtn;
    @BindView(R.id.sw_send_button)
    TextView sendBtn;

    private Context context;
    private Boolean swipeLayoutOpened = false;

    public ItemViewHolder(Context context, View itemView) {
      super(itemView, multiSelector);
      this.context = context;
      ButterKnife.bind(this, itemView);
      imageCamera.setVisibility(View.GONE);
      swipeLayout.setLongClickable(true);
    }

    public Boolean isSwipeLayoutOpened() {
      return swipeLayoutOpened;
    }

    public void setSwipeLayoutOpened(Boolean open) {
      this.swipeLayoutOpened = open;
    }

    public void bind(final Hour hour, final OnItemClickListener listener) {
      deleteBtn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          multiSelector.setSelected(ItemViewHolder.this, true);
          ((HoursListActivity) context).removeImputation(hour.getId());
          viewBinderHelper.closeLayout(hour.getId());
        }
      });

      sendBtn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          multiSelector.setSelected(ItemViewHolder.this, true);
          ((HoursListActivity) context).sendImputation(hour.getId());
          viewBinderHelper.closeLayout(hour.getId());
        }
      });
      rlRowContainer.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          if (!isSwipeLayoutOpened()) {
            if (!multiSelector.tapSelection(ItemViewHolder.this)) {
              listener.onItemClick(hour);
            } else {
              if (canSelectedItem(hour)) {
                multiSelector.setSelected(ItemViewHolder.this, isActivated());
                setActionModeTitle();
                if (multiSelector.getSelectedPositions().size() == 0) {
                  multiSelector.setSelectable(false);
                  ((HoursListActivity) context).finishMultiSelectionMode();
                }
              } else {
                multiSelector.setSelected(ItemViewHolder.this, false);
                ((HoursListActivity) context).showMessage(R.string.gms_message_imputation_not_selectable);
              }
            }
          }
        }
      });
      rlRowContainer.setOnLongClickListener(new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
          if (!multiSelector.isSelectable() && !isSwipeLayoutOpened()) {
            if (canSelectedItem(hour)) {
              for (HourItemRecycler hir : itemsToShow) {
                viewBinderHelper.lockSwipe(hir.getId());
                viewBinderHelper.closeLayout(hir.getId());
              }
              ((HoursListActivity) context).initializeMultiSelectionMode();
              multiSelector.setSelectable(true);
              multiSelector.setSelected(ItemViewHolder.this, true);
              setActionModeTitle();
            } else {
              ((HoursListActivity) context).showMessage(R.string.gms_message_imputation_not_selectable);
            }
            return true;
          }
          return false;
        }
      });
      swipeLayout.setSwipeListener(new SwipeRevealLayout.SwipeListener() {
        @Override
        public void onClosed(SwipeRevealLayout view) {
          setSwipeLayoutOpened(false);
        }

        @Override
        public void onOpened(SwipeRevealLayout view) {
          setSwipeLayoutOpened(true);
        }

        @Override
        public void onSlide(SwipeRevealLayout view, float slideOffset) {

        }
      });
    }

    private boolean canSelectedItem(Hour hour) {
      return (TextUtils.Gms.IMPUTATION_STATUS_SAVED.equals(hour.getState()) ||
          TextUtils.Gms.IMPUTATION_STATUS_REJECTED.equals(hour.getState()));
    }

    private void setActionModeTitle() {
      int size = multiSelector.getSelectedPositions().size();
      String title = String.format(Locale.getDefault(), "%d %s", size,
          context.getResources().getQuantityString(R.plurals.gms_selected_item, size));
      TextView actionTitle = (TextView) ((HoursListActivity) context).findViewById(R.id.tv_action_mode_title);
      actionTitle.setText(title);
    }

    private void changeSeparatorType() {
      separatorAlt.setVisibility(View.VISIBLE);
      separator.setVisibility(View.GONE);
    }
  }
}