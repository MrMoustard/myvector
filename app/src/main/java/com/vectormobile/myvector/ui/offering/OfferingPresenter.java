package com.vectormobile.myvector.ui.offering;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.base.BasePresenter;

import javax.inject.Inject;

/**
 * Created by ajruiz on 03/11/2016.
 */
@ActivityScope
public class OfferingPresenter extends BasePresenter<OfferingView> {

  @Inject
  public OfferingPresenter(OfferingActivity activity) {
    super(activity);
  }
}
