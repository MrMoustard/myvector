package com.vectormobile.myvector.ui.gms.hour.add;

import com.vectormobile.myvector.domain.entity.gms.response.Record;
import com.vectormobile.myvector.ui.base.PresenterView;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Erik Medina on 13/01/2017.
 */

public interface AddHourView extends PresenterView {

  void showHiddenButtons();

  void setDefaultDate(String date);

  void populateProjects(List<Record> projects);

  void populateWorkOrders(List<Record> list);

  void populateImputationCategory(List<Record> list);

  void populateJiraCodes(List<Record> list);

  boolean isCategoriesSpinnerVisible();

  void showDatePickerDialog(int calendarType, Calendar date);

  void showImputationCategorySpinner(boolean show);

  String getHours();

  String getExternalCode();

  String getComment();

  String getHourImputationDateFrom();

  String getHourImputationDateTo();

  String getJiraCode();

  int getCategorySecondIdPosition();

  void returnResult(String extra, String date, int itemCounter);

  int getProjectPositionSelected();

  int getWorkOrderPositionSelected();

  int getCategoryPositionSelected();

  void setDateFrom(String date);

  void setDateTo(String date);

  void setMinimumValueForHours(int minimumHours);

  void setHours(int hours);

  void setComment(String comment);

  void setProjectItem(String projectName);

  void setWorkOrderItem(String workOrderName);

  void setCategoryItem(String categoryName);

  void makeCommentFieldScrollable();

  void showMessage(int messageId);

  void showDialogMessage(String message);

  void showProgressBar();

  void hideProgressBar();

  void setTitle(int titleId);

  void hideJiraSpinner();

  String getDateFrom();

  void showDuplicateButton();
}
