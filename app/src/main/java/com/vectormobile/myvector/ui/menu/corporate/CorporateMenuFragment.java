package com.vectormobile.myvector.ui.menu.corporate;

import android.support.v7.widget.RecyclerView;
import android.widget.RelativeLayout;
import butterknife.BindView;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.MainComponent;
import com.vectormobile.myvector.model.VectorItemMenu;
import com.vectormobile.myvector.model.menuaction.VectorAction;
import com.vectormobile.myvector.ui.base.BaseMenuFragment;

import java.util.ArrayList;

/**
 * Created by amarina on 08/11/2016.
 */

public class CorporateMenuFragment extends BaseMenuFragment<CorporateMenuPresenter> implements CorporateMenuView {
  @BindView(R.id.rv_corporate_menu)
  RecyclerView rvCorporateMenu;
  @BindView(R.id.fragment_container)
  RelativeLayout rlContainer;

  private CorporateMenuPresenter presenter;

  public CorporateMenuFragment() {
    presenter = new CorporateMenuPresenter(this);
  }

  @Override
  protected void loadItems() {
    items = new ArrayList<>();
    items.add(new VectorItemMenu(getString(R.string.menu_item_chat), R.drawable.menu_bg_chat, VectorAction.OPEN_VECTORTALK));
    items.add(new VectorItemMenu(getString(R.string.menu_item_vector_share), R.drawable.menu_bg_vector_share, VectorAction.OPEN_ROCKETCHAT));
    items.add(new VectorItemMenu(getString(R.string.menu_item_contacts), R.drawable.menu_bg_contacts, VectorAction.OPEN_CONTACTS));
    items.add(new VectorItemMenu(getString(R.string.menu_item_email), R.drawable.menu_bg_mail, VectorAction.OPEN_EMAIL));
    items.add(new VectorItemMenu(getString(R.string.menu_item_agenda), R.drawable.menu_bg_agenda, VectorAction.OPEN_AGENDA));
  }

  @Override
  protected RelativeLayout getRelativeLayoutContainer() {
    return rlContainer;
  }

  @Override
  public RecyclerView getRecyclerView() {
    return rvCorporateMenu;
  }


  @Override
  protected void inject() {
    getComponent(MainComponent.class).inject(this);
  }

  @Override
  protected CorporateMenuPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.fragment_corporate_menu;
  }
}
