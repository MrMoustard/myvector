package com.vectormobile.myvector.ui.menu;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.storage.ImageManager;
import com.vectormobile.myvector.ui.base.BaseActivity;
import timber.log.Timber;

import java.io.File;
import java.io.IOException;

/**
 * Created by ajruiz on 30/11/2016.
 */

public class ImagePickerDialogFragment extends BottomSheetDialogFragment {

  static final int REQUEST_IMAGE_CAPTURE = 1;
  static final int REQUEST_IMAGE_GALLERY = 2;

  @BindView(R.id.img_profile_bottom_gallery)
  ImageView imgBottomGallery;
  @BindView(R.id.img_profile_bottom_camera)
  ImageView imgBottomCamera;

  private PermissionListener permissionGalleryListener;
  private PermissionListener permissionCameraListener;

  @Override
  public void setupDialog(final Dialog dialog, int style) {
    super.setupDialog(dialog, style);
    View contentView = View.inflate(getContext(), R.layout.activity_profile_bottom_sheet, null);
    ButterKnife.bind(this, contentView);
    dialog.setContentView(contentView);
  }

  @OnClick(R.id.img_profile_bottom_gallery)
  public void galleryClicked() {
    final Activity activity = getActivity();
    permissionGalleryListener = new PermissionListener() {
      @Override
      public void onPermissionGranted(PermissionGrantedResponse response) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        activity.startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_IMAGE_GALLERY);
        permissionGalleryListener = null;
      }

      @Override
      public void onPermissionDenied(PermissionDeniedResponse response) {
        ((BaseActivity) activity).showError(R.string.permissions_denied_error_show_image);
      }

      @Override
      public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
        token.continuePermissionRequest();
      }
    };

    Dexter.withActivity(activity)
        .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        .withListener(permissionGalleryListener)
        .onSameThread()
        .check();
  }

  @OnClick(R.id.img_profile_bottom_camera)
  public void cameraClicked() {
    final Activity activity = getActivity();
    Dexter.withActivity(activity)
        .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        .withListener(new PermissionListener() {
          @Override
          public void onPermissionGranted(PermissionGrantedResponse response) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
              try {
                File tmpImage;
                tmpImage = ImageManager.getInstance().createTmpImage(activity);
                if (tmpImage != null) {
                  if (Build.VERSION.SDK_INT <= 23) {
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tmpImage));
                  } else {
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(
                        getContext(), getContext().getApplicationContext().getPackageName() + ".provider", tmpImage));
                  }
                  activity.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
              } catch (IOException e) {
                Timber.e(e, "Error %s", e.getMessage());
                ((BaseActivity) activity).showError(R.string.error_image_no_saved);
              }
            }
          }

          @Override
          public void onPermissionDenied(PermissionDeniedResponse response) {
            ((BaseActivity) activity).showError(R.string.permissions_denied_error_save_image);
          }

          @Override
          public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
            token.continuePermissionRequest();
          }
        })
        .onSameThread()
        .check();
  }

}