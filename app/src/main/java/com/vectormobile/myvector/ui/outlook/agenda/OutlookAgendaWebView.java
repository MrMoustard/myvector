package com.vectormobile.myvector.ui.outlook.agenda;

import android.content.Context;
import android.os.Build;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import javax.annotation.Nonnull;

/**
 * Created by amarina on 13/03/2017.
 */

public class OutlookAgendaWebView extends WebView {
  private final static String CALENDAR_URL = "https://outlook.office365.com/owa/?realm=vectoritcgroup.com&path=/calendar/view/Monthgi";
  private StringBuilder javascriptFunction;
  private OutlookAgendaListener listener;
  private boolean firstLoadingDone = false;

  public OutlookAgendaWebView(Context context) {
    super(context);
    defaultSettings();
    loadUrl(CALENDAR_URL);
  }

  private void defaultSettings() {
    WebSettings settings = getSettings();
    settings.setJavaScriptEnabled(true);
    settings.setDomStorageEnabled(true);
    setWebViewClient(new WebViewClient() {

      @Override
      public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        if (url.contains("correo.vectoritcgroup.com") && javascriptFunction != null) {
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            evaluateJavascript(getLoginJavascript(), null);
          } else {
            loadUrl(getLoginJavascript());
          }
        }
        if (listener != null) {
          listener.onPageFinished();
        } else {
          firstLoadingDone = true;
        }
      }

      private String getLoginJavascript() {
        return javascriptFunction.toString();
      }

    });
  }

  public void loginOnOutlook(@Nonnull String username, @Nonnull String password) {
    javascriptFunction = new StringBuilder()
        .append("javascript:{")
        .append("document.getElementById('userNameInput').value='" + username + "@vectoritcgroup.com';")
        .append("document.getElementById('passwordInput').value='" + password + "';")
        .append("document.getElementById('submitButton').click();")
        .append("}");
    loadUrl(CALENDAR_URL);
  }

  public void setListener(OutlookAgendaListener listener) {
    this.listener = listener;
    if (firstLoadingDone && listener != null) {
      listener.onPageFinished();
    }
  }
}
