package com.vectormobile.myvector.ui.offering.menu;

import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.domain.interactor.offering.OfferingMenuInteractor;
import com.vectormobile.myvector.model.offering.SectionMenu;
import com.vectormobile.myvector.ui.base.BasePresenter;

import javax.inject.Inject;

/**
 * Created by ajruiz on 03/11/2016.
 */


public class OfferingMenuPresenter extends BasePresenter<OfferingMenuView> {

  private OfferingMenuInteractor interactor;

  @Inject
  public OfferingMenuPresenter(OfferingMenuView fragment, OfferingMenuInteractor interactor) {
    super(fragment);
    this.interactor = interactor;
  }

  public void getOfferingMenuList() {
    interactor.execute(new OnItemRetrievedListener() {

      @Override
      public void onSuccess(Object item) {
        if (getView() != null) {
          getView().onOfferingMenuRetrieved((SectionMenu) item);
        }
      }

      @Override
      public void onError(int errorId) {
        if (getView() != null) {
          getView().showError(errorId);
          getView().hideLoadingDialog();
        }
      }
    });
  }
}
