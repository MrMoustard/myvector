package com.vectormobile.myvector.ui.base;

import javax.inject.Inject;

/**
 * Created by amarina on 07/11/2016.
 */

public class BaseMenuPresenter<F extends BaseMenuFragment> extends BasePresenter<F> {
  @Inject
  public BaseMenuPresenter(F fragment) {
    super(fragment);
  }
}
