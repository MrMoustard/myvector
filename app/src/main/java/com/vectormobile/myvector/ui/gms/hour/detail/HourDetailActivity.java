package com.vectormobile.myvector.ui.gms.hour.detail;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.DaggerHourDetailComponent;
import com.vectormobile.myvector.di.component.HourDetailComponent;
import com.vectormobile.myvector.di.module.HourDetailModule;
import com.vectormobile.myvector.model.gms.Hour;
import com.vectormobile.myvector.ui.base.BaseActivity;
import com.vectormobile.myvector.ui.gms.expenses.list.ExpensesListActivity;
import com.vectormobile.myvector.ui.gms.hour.add.AddHourActivity;
import com.vectormobile.myvector.ui.gms.hour.list.HoursListActivity;
import com.vectormobile.myvector.util.date.DateUtils;
import com.vectormobile.myvector.util.text.TextUtils;

import javax.inject.Inject;

/**
 * Created by ajruiz on 26/01/2017.
 */

public class HourDetailActivity extends BaseActivity<HourDetailPresenter> implements HourDetailView {

  public static final String EXTRA_DETAILS_HOUR = "hour";

  @Inject
  HourDetailPresenter presenter;

  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.tv_section_title)
  TextView sectionTitle;
  @BindView(R.id.iv_action_button)
  ImageView actionButton;

  @BindView(R.id.status_image)
  ImageView statusImage;
  @BindView(R.id.status_message_layout)
  RelativeLayout statusLayout;
  @BindView(R.id.status_title)
  TextView statusTitle;
  @BindView(R.id.status_message)
  TextView statusMessage;
  @BindView(R.id.status_separator)
  View statusSeparator;
  @BindView(R.id.status_delete_button)
  Button statusDeleteExpenseBtn;
  @BindView(R.id.status_edit_button)
  Button statusEditExpenseBtn;

  @BindView(R.id.detail_date_from_value)
  TextView dateFrom;
  @BindView(R.id.detail_date_to_value)
  TextView dateTo;
  @BindView(R.id.detail_date_to_header)
  TextView dateToHeader;
  @BindView(R.id.detail_date_to_separator)
  View dateToSeparator;
  @BindView(R.id.detail_project_value)
  TextView detailProjectValue;
  @BindView(R.id.detail_work_order_value)
  TextView detailWorkOrderValue;
  @BindView(R.id.detail_units_value)
  TextView detailUnitsValue;
  @BindView(R.id.detail_comment_value)
  TextView detailCommentValue;

  private HourDetailComponent component;
  private Hour hour;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);

    hour = getIntent().getExtras().getParcelable(EXTRA_DETAILS_HOUR);
    configUi();
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode == Activity.RESULT_OK) {
      String action;
      if (requestCode == HoursListActivity.DUPLICATE_HOUR_REQUEST) {
        action = AddHourActivity.DUPLICATE;
      } else {
        action = data.getExtras().getString(AddHourActivity.OPERATION_FINALIZED);
      }
      String date = data.getExtras().getString(AddHourActivity.EXTRA_DETAILS_HOUR_DATE);
      int itemCounter = data.getExtras().getInt(AddHourActivity.EXTRA_DETAILS_ITEM_COUNTER);
      returnResult(action, date, itemCounter);
      finish();
    }
  }

  private void returnResult(String action, String date, int itemCounter) {
    Intent returnIntent = new Intent();
    returnIntent.putExtra(AddHourActivity.OPERATION_FINALIZED, action);
    returnIntent.putExtra(AddHourActivity.EXTRA_DETAILS_HOUR_DATE, date);
    returnIntent.putExtra(AddHourActivity.EXTRA_DETAILS_ITEM_COUNTER, itemCounter);
    setResult(Activity.RESULT_OK, returnIntent);
  }

  private void returnResult(String action, String date, String itemId) {
    Intent returnIntent = new Intent();
    returnIntent.putExtra(AddHourActivity.OPERATION_FINALIZED, action);
    returnIntent.putExtra(AddHourActivity.EXTRA_DETAILS_HOUR_DATE, date);
    returnIntent.putExtra(AddHourActivity.EXTRA_DETAILS_ITEM_ID, itemId);
    setResult(Activity.RESULT_OK, returnIntent);
  }

  private void configUi() {
    setToolbarStatusTitle();
    configStatusMessage();
    hideActionButton();
    hideDateTo();
    setHourObject();
  }

  private void hideDateTo() {
    if (!hour.isMassiveReport()) {
      dateTo.setVisibility(View.GONE);
      dateToHeader.setVisibility(View.GONE);
      dateToSeparator.setVisibility(View.GONE);
    }
  }

  private void setHourObject() {
    statusMessage.setText(hour.getApproverComment());
    dateFrom.setText(TextUtils.capitalizeFirstLetter(
        DateUtils.formatDateToLargeString(hour.getDateFrom())));
    detailProjectValue.setText(hour.getProjectName());
    detailWorkOrderValue.setText(hour.getWorkOrderName());
    detailUnitsValue.setText(String.valueOf(hour.getUnits()));
    detailCommentValue.setText(hour.getComment());
  }

  private void setToolbarStatusTitle() {
    sectionTitle.setText(hour.getStateString(this));
  }

  private void configStatusMessage() {
    switch (hour.getState()) {
      case Hour.HOUR_SENT:
        setSentStatusMessage();
        break;
      case Hour.HOUR_APPROVED:
        setApprovedStatusMessage();
        break;
      case Hour.HOUR_REJECTED:
        setRejectedStatusMessage();
        break;
    }
  }

  private void setSentStatusMessage() {
    statusImage.setImageResource(R.drawable.ic_send);
    statusImage.setBackgroundResource(R.drawable.circle_item_blue);
    statusTitle.setText(getString(R.string.hour_sent_status_message_title));
    statusMessage.setVisibility(View.GONE);
    statusSeparator.setVisibility(View.GONE);
    statusDeleteExpenseBtn.setVisibility(View.GONE);
    statusEditExpenseBtn.setVisibility(View.GONE);
    statusLayout.setVisibility(View.VISIBLE);

  }

  private void setApprovedStatusMessage() {
    statusImage.setImageResource(R.drawable.ic_accept);
    statusImage.setBackgroundResource(R.drawable.circle_item_green);
    if (hour.getApproverComment().isEmpty()) {
      statusTitle.setText(String.format("%s%s", getString(R.string.hour_approved_status_message_title), "."));
    } else {
      statusTitle.setText(String.format("%s%s", getString(R.string.hour_approved_status_message_title), ":"));
    }
    statusSeparator.setVisibility(View.GONE);
    statusDeleteExpenseBtn.setVisibility(View.GONE);
    statusEditExpenseBtn.setVisibility(View.GONE);
    statusMessage.setVisibility(View.GONE);
    statusLayout.setVisibility(View.VISIBLE);
  }

  private void setRejectedStatusMessage() {
    statusImage.setImageResource(R.drawable.ic_reject);
    statusImage.setBackgroundResource(R.drawable.circle_item_red);
    if (hour.getApproverComment().isEmpty()) {
      statusTitle.setText(String.format("%s%s", getString(R.string.hour_rejected_status_message_title), "."));
    } else {
      statusTitle.setText(String.format("%s%s", getString(R.string.hour_rejected_status_message_title), ":"));
    }
    statusLayout.setVisibility(View.VISIBLE);
    statusDeleteExpenseBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        returnResult(
            ExpensesListActivity.REMOVED_EXPENSE,
            DateUtils.formatDateToCompactString(hour.getDateFrom(), DateUtils.GMS_SHORT_DATE_FORMAT),
            hour.getId());
        finish();
      }
    });
    statusEditExpenseBtn.setText(R.string.hour_rejected_status_edit_button);
    statusEditExpenseBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent iAddHour = new Intent(HourDetailActivity.this, AddHourActivity.class);
        iAddHour.putExtra(AddHourActivity.EXTRA_ACTION, AddHourActivity.UPDATE_ACTION);
        iAddHour.putExtra(AddHourActivity.EXTRA_DETAILS_HOUR, hour);
        startActivityForResult(iAddHour, HoursListActivity.UPDATE_HOUR_REQUEST);
      }
    });

  }

  private void hideActionButton() {
    actionButton.setVisibility(View.GONE);
  }

  @OnClick(R.id.iv_duplicate_button)
  public void onDuplicateButtonHasBeenClicked() {
    Intent iAddHour = new Intent(HourDetailActivity.this, AddHourActivity.class);
    iAddHour.putExtra(AddHourActivity.EXTRA_ACTION, AddHourActivity.DUPLICATE_ACTION);
    iAddHour.putExtra(AddHourActivity.EXTRA_DETAILS_HOUR, hour);
    startActivityForResult(iAddHour, HoursListActivity.DUPLICATE_HOUR_REQUEST);
  }

  @Override
  protected void injectModule() {
    component = DaggerHourDetailComponent.builder()
        .appComponent(((App) getApplication()).getComponent())
        .hourDetailModule(new HourDetailModule(this))
        .build();
    component.inject(this);
  }

  @Override
  protected HourDetailPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.activity_hour_detail;
  }

  @Override
  public HourDetailComponent getComponent() {
    return component;
  }
}
