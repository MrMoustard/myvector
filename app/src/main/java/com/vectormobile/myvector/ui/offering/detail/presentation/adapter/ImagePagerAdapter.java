package com.vectormobile.myvector.ui.offering.detail.presentation.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.squareup.picasso.Picasso;
import com.vectormobile.myvector.R;

/**
 * Created by ajruiz on 22/11/2016.
 */

public class ImagePagerAdapter extends PagerAdapter {

  private Context context;
  private String[] urls;
  private LayoutInflater mLayoutInflater;
  private View.OnClickListener onClickListener;

  public ImagePagerAdapter(Context context, String[] urls, View.OnClickListener onClickListener) {
    this.context = context;
    this.urls = urls.clone();
    this.onClickListener = onClickListener;
    mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  }

  @Override
  public int getCount() {
    return urls.length;
  }

  @Override
  public boolean isViewFromObject(View view, Object object) {
    return view == object;
  }

  @Override
  public Object instantiateItem(ViewGroup container, final int position) {
    View itemView = mLayoutInflater.inflate(R.layout.item_presentation, container, false);
    ImageView imageView = (ImageView) itemView.findViewById(R.id.imgViewSlide);
    imageView.setOnClickListener(onClickListener);
    Picasso.with(context)
        .load(urls[position])
        .into(imageView);
    container.addView(itemView);

    return itemView;
  }

  @Override
  public void destroyItem(ViewGroup container, int position, Object object) {
    container.removeView((LinearLayout) object);
  }
}
