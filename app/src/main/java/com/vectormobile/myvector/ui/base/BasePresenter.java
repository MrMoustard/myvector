package com.vectormobile.myvector.ui.base;

/**
 * VectorDigital.
 *
 * @author -   AMarinaG
 * @since -   28/10/16
 */

public class BasePresenter<V extends PresenterView> extends Presenter<V> {
  protected V view;

  public BasePresenter(V view) {
    this.view = view;
  }
}
