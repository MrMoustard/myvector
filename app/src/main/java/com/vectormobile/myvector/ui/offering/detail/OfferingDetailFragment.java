package com.vectormobile.myvector.ui.offering.detail;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.gson.Gson;

import android.Manifest;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.PermissionChecker;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.squareup.picasso.Picasso;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.OfferingComponent;
import com.vectormobile.myvector.model.PostAbout;
import com.vectormobile.myvector.storage.PdfDownloadAsyncTask;
import com.vectormobile.myvector.ui.base.BaseFragment;
import com.vectormobile.myvector.ui.offering.OfferingActivity;
import com.vectormobile.myvector.ui.offering.detail.presentation.PresentationActivity;
import timber.log.Timber;

import java.io.File;
import java.lang.annotation.Annotation;

import javax.inject.Inject;

/**
 * Created by ajruiz on 10/11/2016.
 */

public class OfferingDetailFragment extends BaseFragment<OfferingDetailPresenter> implements OfferingDetailView,
    YouTubePlayer.OnInitializedListener, PermissionChecker.PermissionResult {

  private static final int REQUEST_EXTERNAL_STORAGE = 1;
  private static final String API_KEY = "AIzaSyByMx-c4Mca3jtE34Qo4Fg2fC59ZGxXc34";
  private static String[] PERMISSIONS_STORAGE = {
      Manifest.permission.READ_EXTERNAL_STORAGE,
      Manifest.permission.WRITE_EXTERNAL_STORAGE
  };
  @Inject
  OfferingDetailPresenter presenter;
  @BindView(R.id.categories_lay)
  LinearLayout categoriesLayout;
  @BindView(R.id.tags_lay)
  LinearLayout tagsLayout;
  @BindView(R.id.btnGallery)
  Button btnGallery;
  @BindView(R.id.btn_gallery_share)
  Button btnGalleryShare;
  @BindView(R.id.about_post_tv)
  TextView aboutTexView;
  @BindView(R.id.ly_gallery)
  RelativeLayout lyGallery;
  @BindView(R.id.imageView_gallery)
  ImageView imageView_gallery;
  @BindView(R.id.view_youtube_fragment)
  FrameLayout viewYoutubeFragment;
  @BindView(R.id.post_image_header)
  ImageView headerImage;
  @BindString(R.string.offering_pdf_saved)
  String pdfSavedMessage;
  private LinearLayout postToolbar;
  private TextView tvOfferingPostTitle;
  private String videoId = "";
  private PostAbout post;
  private MyFullScreenListener myFullscreenChangeListener;
  private YouTubePlayer youTubePlayer;
  private boolean isFullScreen = false;
  private ProgressDialog loadingDialog;
  private int sectionColor;
  private String sectionTitle;

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    initView();
    createLoadingDialog();
    loadDataPost();
  }

  public boolean isFullScreen() {
    return isFullScreen;
  }

  public YouTubePlayer getYouTubePlayer() {
    return youTubePlayer;
  }

  private void initView() {
    tvOfferingPostTitle = (TextView) getActivity().findViewById(R.id.tv_section_title);
    postToolbar = (LinearLayout) getActivity().findViewById(R.id.ly_news_toolbar);
  }

  private void loadDataPost() {
    Bundle bundle = getArguments();
    post = new Gson().fromJson(bundle.getString(OfferingActivity.KEY_OFFERING_POST_SELECTED), PostAbout.class);
    presenter.setPost(post);
    sectionColor = bundle.getInt(OfferingActivity.KEY_OFFERING_SECTION_COLOR);
    sectionTitle = bundle.getString(OfferingActivity.KEY_OFFERING_SECTION_TITLE);
    postToolbar.setBackgroundColor(sectionColor);

    if (post != null) {
      Answers.getInstance().logContentView(
          new ContentViewEvent()
              .putContentId(post.getLink())
              .putContentName(post.getTitle().getRendered())
              .putContentType("offering news")
      );
      tvOfferingPostTitle.setText(presenter.getTitlePost());

      videoId = post.getAcf().getVideoUrl().trim();
      if (videoId != null && !videoId.equals("")) {
        initYoutubeFragment();
      }

      aboutTexView.setText(presenter.getAboutContent());
      aboutTexView.setVisibility(View.VISIBLE);

      if (post.getBetterFeaturedImage() != null) {
        Picasso.with(getContext())
            .load(post.getBetterFeaturedImage().getMediaDetails().getSizes().getSingle360().getSourceUrl())
            .placeholder(R.drawable.offering_placeholder)
            .into(headerImage);
      }

      if (post.getGalleryImages() != null) {
        if (post.getGalleryImages().size() == 0) {
          imageView_gallery.setVisibility(View.GONE);
        } else {
          lyGallery.setVisibility(View.VISIBLE);
          imageView_gallery.setVisibility(View.VISIBLE);
          Picasso.with(getContext())
              .load(post.getGalleryImages().get(0))
              .into(imageView_gallery);
        }
      }
    }

    hideLoadingDialog();
  }

  private void initYoutubeFragment() {
    viewYoutubeFragment.setVisibility(View.VISIBLE);
    myFullscreenChangeListener = new MyFullScreenListener();

    YouTubePlayerSupportFragment youTubePlayerSupportFragment = new YouTubePlayerSupportFragment();
    youTubePlayerSupportFragment.initialize(API_KEY, this);

    FragmentManager fragmentManager = getFragmentManager();
    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
    fragmentTransaction.replace(R.id.view_youtube_fragment, youTubePlayerSupportFragment);
    fragmentTransaction.commit();
  }

  @Override
  public void onPause() {
    super.onPause();
    OfferingActivity.setNewsDetailOpened(false);
    TextView tvTitle = (TextView) getActivity().findViewById(R.id.tv_section_title);
    tvTitle.setText(sectionTitle);
  }

  @OnClick({R.id.btnGallery})
  public void onClick() {
    Intent intent = new Intent(getActivity(), PresentationActivity.class);
    intent.putExtra(PresentationActivity.GALLERY_IMAGES, post.getGalleryImages().toArray(new String[post.getGalleryImages().size()]));
    intent.putExtra(PresentationActivity.PRESENTATION_PDF_URL, post.getAcf().getPdf());
    startActivity(intent);
  }

  @OnClick({R.id.btn_gallery_share})
  public void verifyStoragePermissions() {
    // Check if we have write permission
    int permission = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

    if (permission != PackageManager.PERMISSION_GRANTED) {
      // We don't have permission so prompt the user
      ActivityCompat.requestPermissions(
          getActivity(),
          PERMISSIONS_STORAGE,
          REQUEST_EXTERNAL_STORAGE
      );
    } else {
      downloadPdfOnBackground();
    }
  }

  private void downloadPdfOnBackground() {
    final NotificationManager notificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
    final NotificationCompat.Builder builder = new NotificationCompat.Builder(getActivity());
    builder.setContentTitle(post.getTitle().getRendered())
        .setContentText(getString(R.string.user_pdf_download_in_progress))
        .setSmallIcon(R.drawable.ic_launcher);
    builder.setProgress(0, 0, true);

    new PdfDownloadAsyncTask() {
      @Override
      protected void onPreExecute() {
        super.onPreExecute();
        notificationManager.notify(post.getId(), builder.build());
      }

      @Override
      protected void onPostExecute(String filePath) {
        super.onPostExecute(filePath);
        if (filePath != null) {
          showSuccesfulTask();
          builder.setContentText(getString(R.string.user_pdf_download_success));
          File file = new File(filePath);
          Intent target = new Intent(Intent.ACTION_VIEW);
          target.setDataAndType(Uri.fromFile(file), "application/pdf");
          target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

          Intent intent = Intent.createChooser(target, getString(R.string.user_pdf_open_file));
          PendingIntent pendingIntent = PendingIntent.getActivity(getActivity(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
          builder.setContentIntent(pendingIntent);
        } else {
          builder.setContentText(getString(R.string.user_pdf_download_failure));
          showError(R.string.error_not_available);
        }
        builder.setProgress(0, 0, false);
        builder.setAutoCancel(true);
        notificationManager.notify(post.getId(), builder.build());

      }
    }.execute(post.getAcf().getPdf());
  }

  private void hideLoadingDialogNow() {
    if ((loadingDialog != null) && loadingDialog.isShowing()) {
      loadingDialog.dismiss();
    }
    loadingDialog = null;
  }

  public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
    switch (requestCode) {
      case REQUEST_EXTERNAL_STORAGE: {
        if (grantResults.length > 0
            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          // permission was granted
          downloadPdfOnBackground();
        }
      }
    }
  }

  private void showSuccesfulTask() {
    View rootView = getActivity().getWindow().getDecorView().findViewById(android.R.id.content);
    Snackbar.make(rootView, pdfSavedMessage, Snackbar.LENGTH_LONG).show();
  }

  @Override
  protected void inject() {
    getComponent(OfferingComponent.class).inject(this);
  }


  @Override
  protected OfferingDetailPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.fragment_offering_post;
  }

  @Override
  public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer tubePlayer, boolean wasRestored) {
    youTubePlayer = tubePlayer;
    youTubePlayer.setOnFullscreenListener(myFullscreenChangeListener);

    if (!wasRestored) {
      try {
        youTubePlayer.cueVideo(videoId);
      } catch (IllegalStateException e) {
        Timber.wtf(e.getMessage());
      }
    }

  }

  @Override
  public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

  }

  private void createLoadingDialog() {
    loadingDialog = new ProgressDialog(getActivity(), R.style.CustomLoadingDialogTheme);
    loadingDialog.setCancelable(true);
    loadingDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialog) {
        getActivity().getSupportFragmentManager().popBackStack();
      }
    });
    loadingDialog.setCanceledOnTouchOutside(false);
    loadingDialog.show();
  }

  @Override
  public void hideLoadingDialog() {
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        if ((loadingDialog != null) && loadingDialog.isShowing()) {
          loadingDialog.dismiss();
        }
        loadingDialog = null;
      }
    }, 1000);
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return null;
  }

  private final class MyFullScreenListener implements YouTubePlayer.OnFullscreenListener {

    @Override
    public void onFullscreen(boolean b) {
      isFullScreen = b;
      youTubePlayer.play();
    }
  }

}
