package com.vectormobile.myvector.ui.rocketchat;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import butterknife.BindView;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.DaggerRocketchatComponent;
import com.vectormobile.myvector.di.component.RocketchatComponent;
import com.vectormobile.myvector.di.module.RocketchatModule;
import com.vectormobile.myvector.ui.base.BaseActivity;

import javax.inject.Inject;

/**
 * Created by amarina on 26/01/2017.
 */

public class RocketchatActivity extends BaseActivity<RocketchatPresenter> implements RocketchatView, RocketchatListener {

  private RocketchatComponent component;

  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.pb_login)
  ProgressBar pbLoading;
  @BindView(R.id.fm_rocketchat)
  FrameLayout contentRocketchat;
  @Inject
  RocketchatPresenter presenter;
  @Inject
  RocketWebView webView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);
    if (webView.getParent() != null) {
      ((ViewGroup) webView.getParent()).removeAllViews();
    }
    contentRocketchat.addView(webView);
    webView.setRocketchatListener(this);
  }

  @Override
  public void onDestroy() {
    webView.setRocketchatListener(null);
    webView = null;
    super.onDestroy();
  }

  @Override
  protected void injectModule() {
    component = DaggerRocketchatComponent.builder()
        .appComponent(((App) getApplication()).getComponent())
        .rocketchatModule(new RocketchatModule(this))
        .build();
    component.inject(this);
  }

  @Override
  protected RocketchatPresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.activity_rocketchat;
  }

  @Override
  public RocketchatComponent getComponent() {
    return component;
  }

  @Override
  public void onPageFinished() {
    if (pbLoading != null) {
      pbLoading.setVisibility(View.GONE);
    }
  }
}
