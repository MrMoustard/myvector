package com.vectormobile.myvector.ui.login;


import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.LoginEvent;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.domain.entity.offering.AuthenticationResponse;
import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.domain.interactor.login.GmsLoginInteractor;
import com.vectormobile.myvector.domain.interactor.login.IntranetLoginInteractor;
import com.vectormobile.myvector.domain.interactor.login.OfferingLoginInteractor;
import com.vectormobile.myvector.storage.SessionManager;
import com.vectormobile.myvector.ui.base.BasePresenter;
import okhttp3.ResponseBody;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Erik Medina on 27/10/2016.
 */
@ActivityScope
public class LoginPresenter extends BasePresenter<LoginView> {

  private static int TOTAL_LOGINS = 3;
  @Inject
  SessionManager sessionManager;
  private OfferingLoginInteractor offeringLoginInteractor;
  private GmsLoginInteractor gmsLoginInteractor;
  private IntranetLoginInteractor intranetLoginInteractor;
  private String username;
  private String usernameToStore;
  private List<Boolean> completeLogins;

  @Inject
  public LoginPresenter(LoginActivity activity, OfferingLoginInteractor offeringLoginInteractor,
                        GmsLoginInteractor gmsLoginInteractor, IntranetLoginInteractor intranetLoginInteractor) {
    super(activity);
    this.offeringLoginInteractor = offeringLoginInteractor;
    this.gmsLoginInteractor = gmsLoginInteractor;
    this.intranetLoginInteractor = intranetLoginInteractor;
    completeLogins = new ArrayList<>();
  }

  public void sendLogin(String username, String password) {
    getView().hideKeyboard();
    this.username = username;
    if (checkInputs(this.username, password)) {
      completeLogins.clear();
      getView().showProgressBar(true);
      getView().hideBtnLogin();
      gmsLogin(this.username, password);
      intranetLogin(this.username, password);
      offeringLogin(this.username, password);
      getView().loginOnEmbeddedWebViews(this.username, password);
      usernameToStore = this.username;
    }
  }

  public void offeringLogin(String username, String password) {
    offeringLoginInteractor.execute(username, password, new OnItemRetrievedListener() {
      @Override
      public void onSuccess(Object item) {
        if (item != null) {
          sessionManager.setOfferingToken(((AuthenticationResponse) item).getCookie());
          sessionManager.setOfferingTokenActive(true);
        }
        if (getView() != null) {
          completeLogins.add(true);
        }
        checkLoginCompletes();
      }


      @Override
      public void onError(int errorId) {
        if (getView() != null) {
          getView().showProgressBar(false);
          getView().enableLoginButton();
          getView().showError(errorId);
          getView().enableLoginButton();
        }
        completeLogins.add(false);
        checkLoginCompletes();
      }
    });
  }

  private void checkLoginCompletes() {
    if (completeLogins.size() == TOTAL_LOGINS) {
      if (!completeLogins.contains(false)) {
        saveUsername();
        successLogin();
        Answers.getInstance().logLogin(new LoginEvent()
            .putMethod("Total logins: " + TOTAL_LOGINS)
            .putSuccess(true)

        );
      } else {
        getView().showProgressBar(false);
        getView().enableLoginButton();
        getView().showLoginError();
      }
    }
  }

  private void successLogin() {
    getView().showProgressBar(false);
    getView().enableLoginButton();
    getView().goToMainActivity();
  }

  private void saveUsername() {
    sessionManager.saveUsername(usernameToStore);
  }

  public String getSavedUsername() {
    return sessionManager.getUsername();
  }


  public void gmsLogin(String username, String password) {
    gmsLoginInteractor.execute(username, password, new OnItemRetrievedListener<ResponseBody>() {
      @Override
      public void onSuccess(ResponseBody item) {
        if (getView() != null) {
          completeLogins.add(true);
        }
        checkLoginCompletes();
      }

      @Override
      public void onError(int errorId) {
        completeLogins.add(false);
        checkLoginCompletes();
        getView().showError(errorId);
      }
    });
  }

  public void intranetLogin(String username, String password) {
    intranetLoginInteractor.execute(username, password, new OnItemRetrievedListener<ResponseBody>() {
      @Override
      public void onSuccess(ResponseBody item) {
        if (getView() != null) {
          completeLogins.add(true);
        }
        checkLoginCompletes();
      }

      @Override
      public void onError(int errorId) {
        completeLogins.add(false);
        checkLoginCompletes();
        getView().showError(errorId);
      }
    });
  }

  private boolean checkInputs(String user, String pwd) {
    if (user.isEmpty()) {
      getView().showMessage(R.string.login_username_empty);
      return false;
    } else if (pwd.isEmpty()) {
      getView().showMessage(R.string.login_password_empty);
      return false;
    } else {
      this.username = trimAtWithinUsername(user);
      return true;
    }
  }

  private String trimAtWithinUsername(String user) {
    return user.contains("@") ? user.substring(0, user.indexOf("@")) : user;
  }

  public boolean needLogin() {
    return !sessionManager.checkLogins();
  }
}
