package com.vectormobile.myvector.ui.gms.expenses.add;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.DecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.vectormobile.myvector.App;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.di.component.AddExpenseComponent;
import com.vectormobile.myvector.di.component.DaggerAddExpenseComponent;
import com.vectormobile.myvector.di.module.AddExpenseModule;
import com.vectormobile.myvector.domain.entity.gms.response.Record;
import com.vectormobile.myvector.model.gms.Expense;
import com.vectormobile.myvector.storage.ImageManager;
import com.vectormobile.myvector.ui.base.BaseActivity;
import com.vectormobile.myvector.ui.calendar.DatePickerFragment;
import com.vectormobile.myvector.ui.menu.ImagePickerDialogFragment;
import com.vectormobile.myvector.util.date.DateUtils;
import com.vectormobile.myvector.util.number.MinValueInputFilter;
import com.vectormobile.myvector.util.text.TextUtils;
import timber.log.Timber;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Erik Medina on 07/12/2016.
 */
public class AddExpenseActivity extends BaseActivity<AddExpensePresenter>
    implements AddExpenseView, OnTouchListener, DatePickerFragment.OnDatePickerListener {

  public static final String EXTRA_ACTION = "action";
  public static final int CREATE_ACTION = 1;
  public static final int UPDATE_ACTION = 2;
  public static final int DUPLICATE_ACTION = 3;

  public static final String EXTRA_DETAILS_EXPENSE = "expense";
  public static final String EXTRA_DETAILS_EXPENSE_DATE = "date";
  public static final String EXTRA_DETAILS_ITEM_COUNTER = "items_counter";
  public static final String EXTRA_DETAILS_ITEM_ID = "item_id";
  public static final String OPERATION_FINALIZED = "operationFinalized";
  public static final String DUPLICATE = "duplicate";

  public static final int FIRST_ITEM = 0;

  @Inject
  AddExpensePresenter presenter;
  @Inject
  ImageManager imageManager;

  @BindView(R.id.appbar)
  AppBarLayout appbar;
  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.iv_duplicate_button)
  ImageView ivDuplicateButton;
  @BindView(R.id.scrollView)
  NestedScrollView scrollView;
  @BindView(R.id.iv_action_button)
  ImageView ivActionButton;
  @BindView(R.id.s_add_expense_work_orders)
  Spinner sAddExpenseWorkOrders;
  @BindView(R.id.s_add_expense_projects)
  Spinner sAddExpenseProjects;
  @BindView(R.id.s_add_expense_categories)
  Spinner sAddExpenseCategories;
  @BindView(R.id.s_add_expense_currencies)
  Spinner sAddExpenseCurrencies;
  @BindView(R.id.iv_add_expense_header)
  ImageView ivAddExpenseHeader;
  @BindView(R.id.et_add_expense_date_from)
  EditText etAddExpenseDateFrom;
  @BindView(R.id.et_add_expense_date_to)
  EditText etAddExpenseDateTo;
  @BindView(R.id.tv_section_title)
  TextView tvSectionTitle;
  @BindView(R.id.fab_pick_image_expense)
  FloatingActionButton fabPickImageExpense;
  @BindView(R.id.b_add_expense_remove)
  ImageView bAddExpenseRemove;
  @BindView(R.id.b_add_expense_save)
  Button bAddSaveExpense;
  @BindView(R.id.b_add_expense_send)
  Button bAddSendExpense;
  @BindView(R.id.et_add_expense_units)
  EditText etAddExpenseUnits;
  @BindView(R.id.et_add_expense_unit_price)
  EditText etAddExpenseUnitPrice;
  @BindView(R.id.et_add_expense_comment)
  EditText etAddExpenseComment;
  @BindView(R.id.pb_add_expense)
  ProgressBar pbAddExpense;
  @BindView(R.id.tv_hint_project)
  TextView tvHintProject;
  @BindView(R.id.tv_hint_work_order)
  TextView tvHintWorkOrder;
  @BindView(R.id.tv_hint_category)
  TextView tvHintCategory;
  @BindView(R.id.expanded_image)
  ImageView ivExpandedView;

  private int sProjectPositionFixed;
  private int sWorkOrderPositionFixed;
  private int sCategoryPositionFixed;
  private int sCurrencyPositionFixed;

  private int action;
  private Expense expense;
  private ArrayAdapter<String> sProjectsAdapter;
  private ArrayAdapter<String> sWorkOrdersAdapter;
  private ArrayAdapter<String> sCategoriesAdapter;
  private ArrayAdapter<String> sCurrencyAdapter;

  private AddExpenseComponent component;
  private BottomSheetDialogFragment imagePickerDialog;
  private int projectPositionSelected = -1, workOrderPositionSelected = -1,
      expenseCategoryPositionSelected = -1, currencyPositionSelected = -1;
  private String dateFrom;
  private String dateTo;
  private List<String> expensesIds;
  private TextWatcher textWatcher = new TextWatcher() {

    public void afterTextChanged(Editable s) {
      if (!s.toString().matches(TextUtils.Gms.GMS_PRICE_PER_KILOMETER) &&
          sAddExpenseCategories.getSelectedItemPosition() != 0) {
        presenter.dataChanged();
      }
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }
  };
  private Animator animator;
  private int shortAnimationDuration;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setUpActionBar();

    retrieveExtras();
    shortAnimationDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);

    presenter.setUpActionBarTitle(action);
    presenter.shouldShowHiddenButtons(action);
    presenter.shouldShowDuplicateButton(action);

    presenter.fillForm(action, expense);
    presenter.initializeView(action);
    presenter.getProjects();
    presenter.getCurrencies();
    initializeDataWatchers();

    imagePickerDialog = new ImagePickerDialogFragment();

    Drawable mDrawable = getResources().getDrawable(R.drawable.ic_calendar);
    DrawableCompat.setTint(mDrawable, getResources().getColor(R.color.grey_vector));
    etAddExpenseDateFrom.setCompoundDrawablesWithIntrinsicBounds(null, null, mDrawable, null);
    etAddExpenseDateTo.setCompoundDrawablesWithIntrinsicBounds(null, null, mDrawable, null);

    etAddExpenseDateFrom.setOnTouchListener(this);
    etAddExpenseDateTo.setOnTouchListener(this);
  }

  private void setUpActionBar() {
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    getSupportActionBar().setDisplayShowTitleEnabled(false);
  }

  private void retrieveExtras() {
    Bundle bundle = getIntent().getExtras();
    if (bundle != null) {
      action = bundle.getInt(EXTRA_ACTION);
      if (action != CREATE_ACTION) {
        expense = bundle.getParcelable(EXTRA_DETAILS_EXPENSE);
        expensesIds = new ArrayList<>();
        expensesIds.add(expense.getId());
      }
    }
  }

  private void initializeDataWatchers() {
    etAddExpenseDateFrom.addTextChangedListener(textWatcher);
    etAddExpenseDateTo.addTextChangedListener(textWatcher);
    etAddExpenseUnits.addTextChangedListener(textWatcher);
    etAddExpenseUnitPrice.addTextChangedListener(textWatcher);
    etAddExpenseComment.addTextChangedListener(textWatcher);
  }

  @Override
  protected void injectModule() {
    component = DaggerAddExpenseComponent.builder()
        .appComponent(((App) getApplication()).getComponent())
        .addExpenseModule(new AddExpenseModule(this))
        .build();
    component.inject(this);
  }

  @Override
  protected AddExpensePresenter getPresenter() {
    return presenter;
  }

  @Override
  protected int getLayoutResourceId() {
    return R.layout.activity_add_expense;
  }

  @Override
  public AddExpenseComponent getComponent() {
    return component;
  }

  @Override
  protected void onPause() {
    super.onPause();
    if (!imagePickerDialog.isHidden() && imagePickerDialog.getFragmentManager() != null) {
      imagePickerDialog.dismiss();
    }
  }

  @Override
  public void onBackPressed() {
    if (!presenter.isFirsTime() || !presenter.isDataSaved()) {
      showAlertDialog();
    } else {
      super.onBackPressed();
    }
  }

  private void showAlertDialog() {
    new AlertDialog.Builder(this)
        .setTitle(getString(R.string.gms_message_imputation_not_saved_title))
        .setMessage(getString(R.string.gms_message_imputation_not_saved))
        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            finish();
          }
        })
        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
          }
        })
        .show();
  }

  @Override
  public void showHiddenButtons() {
    bAddExpenseRemove.setVisibility(View.VISIBLE);
    ivActionButton.setVisibility(View.VISIBLE);
    bAddSendExpense.setVisibility(View.VISIBLE);
  }

  @Override
  public void showDuplicateButton() {
    ivDuplicateButton.setVisibility(View.VISIBLE);
  }

  @Override
  public void setDefaultDate(String date) {
    dateFrom = date;
    etAddExpenseDateFrom.setText(date);
  }

  @Override
  public void setFabBehaviour() {
    appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
      @Override
      public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (-verticalOffset == toolbar.getHeight()) {
          fabPickImageExpense.hide();
        }
      }
    });
    scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
      @Override
      public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        if (scrollY == 0) {
          fabPickImageExpense.show();
        }
      }
    });
  }

  @Override
  public void populateProjects(List<Record> list) {
    List<String> projects = getNamesList(list);
    projects.add(FIRST_ITEM, getString(R.string.gms_hint_project));
    sProjectsAdapter = new ArrayAdapter<>(this, R.layout.item_spinner, projects);
    sProjectsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    sAddExpenseProjects.setAdapter(sProjectsAdapter);
  }

  @Override
  public void populateWorkOrders(List<Record> list) {
    List<String> workOrders = getNamesList(list);
    workOrders.add(FIRST_ITEM, getString(R.string.gms_hint_work_order));
    sWorkOrdersAdapter = new ArrayAdapter<>(this, R.layout.item_spinner, workOrders);
    sWorkOrdersAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    sAddExpenseWorkOrders.setAdapter(sWorkOrdersAdapter);
  }

  @Override
  public void populateExpenseCategories(List<Record> list) {
    List<String> expenseCategories = getNamesList(list);
    expenseCategories.add(FIRST_ITEM, getString(R.string.expense_hint_add_expense_category));
    sCategoriesAdapter = new ArrayAdapter<>(this, R.layout.item_spinner, expenseCategories);
    sCategoriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    sAddExpenseCategories.setAdapter(sCategoriesAdapter);
  }

  @Override
  public void populateCurrencies(List<Record> list) {
    sCurrencyAdapter = new SpinnerAdapter(this, R.layout.item_spinner, R.layout.item_spinner_currency,
        getIdsList(list), getNamesList(list));
    sAddExpenseCurrencies.setAdapter(sCurrencyAdapter);
  }

  private List<String> getNamesList(List<Record> records) {
    List<String> namesList = new ArrayList<>(records.size());
    for (Record record : records) {
      namesList.add(record.getText());
    }
    return namesList;
  }

  private List<String> getIdsList(List<Record> records) {
    List<String> idsList = new ArrayList<>(records.size());
    for (Record record : records) {
      idsList.add(record.getId());
    }
    return idsList;
  }

  @OnClick(R.id.fab_pick_image_expense)
  public void onCameraFabClicked() {
    showImagePickerDialog();
  }

  private void showImagePickerDialog() {
    imagePickerDialog.show(getSupportFragmentManager(), imagePickerDialog.getTag());
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    int REQUEST_IMAGE_CAPTURE = 101;
    int REQUEST_IMAGE_GALLERY = 102;

    if (resultCode == RESULT_OK) {
      File sourceFile = null;
      if (requestCode == DUPLICATE_ACTION) {
        returnResult(AddExpenseActivity.DUPLICATE, dateFrom, 1);
      } else if (requestCode == REQUEST_IMAGE_CAPTURE) {
        sourceFile = new File(ImageManager.getInstance().getLastTempFile());
      } else if (requestCode == REQUEST_IMAGE_GALLERY) {
        String url = data.getData().toString();
        if (url.startsWith(TextUtils.Intranet.GOOGLE_PHOTOS_URL)) {
          InputStream is = null;
          try {
            is = getContentResolver().openInputStream(Uri.parse(data.getData().toString()));
            Bitmap bitmap = BitmapFactory.decodeStream(is);
            sourceFile = (imageManager.convertBitmapToFile(this, bitmap));
          } catch (IOException e) {
            e.printStackTrace();
          } finally {
            try {
              assert is != null;
              is.close();
            } catch (IOException e) {
              e.printStackTrace();
            }
          }
        } else {
          sourceFile = new File(imageManager.getImagePathFromGallery(this, data.getData()));
          try {
            ImageManager.getInstance().copyImageToGalleryFromTmpExpense(
                sourceFile, ImageManager.getInstance().createTmpImage(this)
            );
          } catch (IOException e) {
            Timber.e(e, "No se ha podido crear un archivo temporal! %s", e.getMessage());
          }
        }
      }

      if (sourceFile != null) {
        Picasso.with(this).load(sourceFile).into(ivAddExpenseHeader);
        ivAddExpenseHeader.setVisibility(View.VISIBLE);
      }
      presenter.setDataNotSaved();
      presenter.setDataChanged(true);
    }
  }

  @OnClick(R.id.iv_add_expense_header)
  public void expandImage() {
    zoomImageFromThumb(ivAddExpenseHeader, ivAddExpenseHeader.getDrawable());
  }

  private void zoomImageFromThumb(final ImageView ivHeaderAddExpense, Drawable imageDrawable) {
    // If there's an animation in progress, cancel it
    // immediately and proceed with this one.
    if (animator != null) {
      animator.cancel();
    }
    if (imageDrawable != null) {
      ivExpandedView.setImageDrawable(imageDrawable);
    } else {
      ivExpandedView.setImageResource(R.drawable.placeholder_add_expense_header);
    }
    // Calculate the starting and ending bounds for the zoomed-in image.
    // This step involves lots of math. Yay, math.
    final Rect startBounds = new Rect();
    final Rect finalBounds = new Rect();
    final Point globalOffset = new Point();

    // The start bounds are the global visible rectangle of the thumbnail,
    // and the final bounds are the global visible rectangle of the container
    // view. Also set the container view's offset as the origin for the
    // bounds, since that's the origin for the positioning animation
    // properties (X, Y).
    ivHeaderAddExpense.getGlobalVisibleRect(startBounds);
    findViewById(R.id.container)
        .getGlobalVisibleRect(finalBounds, globalOffset);
    startBounds.offset(-globalOffset.x, -globalOffset.y);
    finalBounds.offset(-globalOffset.x, -globalOffset.y);

    // Adjust the start bounds to be the same aspect ratio as the final
    // bounds using the "center crop" technique. This prevents undesirable
    // stretching during the animation. Also calculate the start scaling
    // factor (the end scaling factor is always 1.0).
    float startScale;
    if ((float) finalBounds.width() / finalBounds.height()
        > (float) startBounds.width() / startBounds.height()) {
      // Extend start bounds horizontally
      startScale = (float) startBounds.height() / finalBounds.height();
      float startWidth = startScale * finalBounds.width();
      float deltaWidth = (startWidth - startBounds.width()) / 2;
      startBounds.left -= deltaWidth;
      startBounds.right += deltaWidth;
    } else {
      // Extend start bounds vertically
      startScale = (float) startBounds.width() / finalBounds.width();
      float startHeight = startScale * finalBounds.height();
      float deltaHeight = (startHeight - startBounds.height()) / 2;
      startBounds.top -= deltaHeight;
      startBounds.bottom += deltaHeight;
    }

    // Hide the thumbnail and show the zoomed-in view. When the animation
    // begins, it will position the zoomed-in view in the place of the
    // thumbnail.
    ivHeaderAddExpense.setAlpha(0f);
    ivExpandedView.setVisibility(View.VISIBLE);

    // Set the pivot point for SCALE_X and SCALE_Y transformations
    // to the top-left corner of the zoomed-in view (the default
    // is the center of the view).
    ivExpandedView.setPivotX(0f);
    ivExpandedView.setPivotY(0f);

    // Construct and run the parallel animation of the four translation and
    // scale properties (X, Y, SCALE_X, and SCALE_Y).
    AnimatorSet set = new AnimatorSet();
    set
        .play(ObjectAnimator.ofFloat(ivExpandedView, View.X,
            startBounds.left, finalBounds.left))
        .with(ObjectAnimator.ofFloat(ivExpandedView, View.Y,
            startBounds.top, finalBounds.top))
        .with(ObjectAnimator.ofFloat(ivExpandedView, View.SCALE_X,
            startScale, 1f)).with(ObjectAnimator.ofFloat(ivExpandedView,
        View.SCALE_Y, startScale, 1f));
    set.setDuration(shortAnimationDuration);
    set.setInterpolator(new DecelerateInterpolator());
    set.addListener(new AnimatorListenerAdapter() {
      @Override
      public void onAnimationEnd(Animator animation) {
        fabPickImageExpense.hide();
        animator = null;
      }

      @Override
      public void onAnimationCancel(Animator animation) {
        fabPickImageExpense.hide();
        animator = null;
      }
    });
    set.start();
    animator = set;
    // Upon clicking the zoomed-in image, it should zoom back down
    // to the original bounds and show the thumbnail instead of
    // the expanded image.
    final float startScaleFinal = startScale;

    ivExpandedView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (animator != null) {
          animator.cancel();
        }

        // Animate the four positioning/sizing properties in parallel,
        // back to their original values.
        AnimatorSet set = new AnimatorSet();
        set.play(ObjectAnimator
            .ofFloat(ivExpandedView, View.X, startBounds.left))
            .with(ObjectAnimator
                .ofFloat(ivExpandedView,
                    View.Y, startBounds.top))
            .with(ObjectAnimator
                .ofFloat(ivExpandedView,
                    View.SCALE_X, startScaleFinal))
            .with(ObjectAnimator
                .ofFloat(ivExpandedView,
                    View.SCALE_Y, startScaleFinal));
        set.setDuration(shortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
          @Override
          public void onAnimationEnd(Animator animation) {
            ivHeaderAddExpense.setAlpha(1f);
            ivExpandedView.setVisibility(View.GONE);
            fabPickImageExpense.show();
            animator = null;
          }

          @Override
          public void onAnimationCancel(Animator animation) {
            ivHeaderAddExpense.setAlpha(1f);
            ivExpandedView.setVisibility(View.GONE);
            fabPickImageExpense.show();
            animator = null;
          }
        });
        set.start();
        animator = set;
      }
    });
  }

  @Override
  public boolean onTouch(View view, MotionEvent event) {
    if (event.getAction() == MotionEvent.ACTION_UP) {
      switch (view.getId()) {
        case R.id.et_add_expense_date_from:
          presenter.onCalendarIconClicked(
              TextUtils.Gms.CALENDAR_EXPENSE_FROM,
              DateUtils.getTypedDate(etAddExpenseDateFrom.getText().toString(), null));
          return true;
        case R.id.et_add_expense_date_to:
          presenter.onCalendarIconClicked(
              TextUtils.Gms.CALENDAR_EXPENSE_TO,
              DateUtils.getTypedDate(etAddExpenseDateTo.getText().toString(), etAddExpenseDateFrom.getText().toString()));
          return true;
      }
    }
    return false;
  }

  public void showDatePickerDialog(int calendarType, Calendar date) {
    Bundle bundle = new Bundle();
    bundle.putInt(DatePickerFragment.CALENDAR_TYPE, calendarType);
    bundle.putLong(DatePickerFragment.CALENDAR_DATE, date.getTimeInMillis());
    DialogFragment datePickerFragment = new DatePickerFragment();
    datePickerFragment.setArguments(bundle);
    datePickerFragment.show(getFragmentManager(), "datePicker");
  }

  @Override
  public String getExpenseDateFrom() {
    return etAddExpenseDateFrom.getText().toString();
  }

  @Override
  public String getExpenseDateTo() {
    return etAddExpenseDateTo.getText().toString();
  }

  @Override
  public int getProjectPositionSelected() {
    return projectPositionSelected;
  }

  @Override
  public int getWorkOrderPositionSelected() {
    return workOrderPositionSelected;
  }

  @Override
  public int getExpenseCategoryPositionSelected() {
    return expenseCategoryPositionSelected;
  }

  @Override
  public String getExpenseUnits() {
    return etAddExpenseUnits.getText().toString();
  }

  @Override
  public String getUnitPrice() {
    return etAddExpenseUnitPrice.getText().toString();
  }

  @Override
  public void setUnitPrice(double unitPrice) {
    etAddExpenseUnitPrice.setText(String.valueOf(unitPrice));
  }

  @Override
  public int getCurrencyPositionSelected() {
    return currencyPositionSelected;
  }

  @Override
  public String getComment() {
    return etAddExpenseComment.getText().toString();
  }

  @Override
  public void setComment(String comment) {
    etAddExpenseComment.setText(comment);
  }

  @OnItemSelected(R.id.s_add_expense_projects)
  public void onProjectItemSelected(int position) {
    projectPositionSelected = position - 1;
    presenter.getWorkOrders(projectPositionSelected);
    if (position != FIRST_ITEM) {
      tvHintProject.setVisibility(View.VISIBLE);
      TextUtils.changeTextColorItemSpinner(this, sAddExpenseProjects);
    } else {
      tvHintProject.setVisibility(View.INVISIBLE);
    }
    if (sProjectPositionFixed != position) {
      presenter.setDataNotSaved();
    }
    if (expense != null && sProjectPositionFixed != position) {
      presenter.dataChanged();
    }
  }

  @OnItemSelected(R.id.s_add_expense_work_orders)
  public void onWorkOrderItemSelected(int position) {
    workOrderPositionSelected = position - 1;
    presenter.getExpenseCategories(workOrderPositionSelected);
    if (position != FIRST_ITEM) {
      tvHintWorkOrder.setVisibility(View.VISIBLE);
      TextUtils.changeTextColorItemSpinner(this, sAddExpenseWorkOrders);
    } else {
      tvHintWorkOrder.setVisibility(View.INVISIBLE);
    }
    if (expense != null && sWorkOrderPositionFixed != position) {
      presenter.dataChanged();
    }
  }

  @OnItemSelected(R.id.s_add_expense_categories)
  public void onExpenseCategoryItemSelected(int position) {
    expenseCategoryPositionSelected = position - 1;
    presenter.getReportCategory(expenseCategoryPositionSelected);
    if (position != FIRST_ITEM) {
      tvHintCategory.setVisibility(View.VISIBLE);
      TextUtils.changeTextColorItemSpinner(this, sAddExpenseCategories);
    } else {
      tvHintCategory.setVisibility(View.INVISIBLE);
    }
    if (expense != null && sCategoryPositionFixed != position) {
      presenter.dataChanged();
    }
  }

  @OnItemSelected(R.id.s_add_expense_currencies)
  public void onCurrencyItemSelected(int position) {
    currencyPositionSelected = position;
    TextUtils.changeTextColorItemSpinner(this, sAddExpenseCurrencies);
    if (expense != null && sCurrencyPositionFixed != position) {
      presenter.setDataChanged(true);
      presenter.setDataNotSaved();
    }
  }

  @OnClick(R.id.iv_duplicate_button)
  public void onDuplicateButtonHasBeenClicked() {
    Intent iAddExpense = new Intent(AddExpenseActivity.this, AddExpenseActivity.class);
    iAddExpense.putExtra(AddExpenseActivity.EXTRA_ACTION, AddExpenseActivity.DUPLICATE_ACTION);
    iAddExpense.putExtra(AddExpenseActivity.EXTRA_DETAILS_EXPENSE, expense);
    startActivityForResult(iAddExpense, AddExpenseActivity.DUPLICATE_ACTION);
  }

  @OnClick(R.id.b_add_expense_remove)
  public void onRemoveButtonClicked() {
    presenter.removeImputation(expensesIds);
  }

  @OnClick(R.id.b_add_expense_save)
  public void onSaveButtonClicked() {
    presenter.saveExpense(AddExpensePresenter.SAVE_ONLY);
  }

  @OnClick({R.id.b_add_expense_send, R.id.iv_action_button})
  public void onSendButtonClicked() {
    presenter.sendExpense(expensesIds);
  }

  @Override
  public void showMessage(int messageId) {
    View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
    Snackbar.make(rootView, messageId, Snackbar.LENGTH_SHORT).show();
  }

  @Override
  public void setDateFrom(String date) {
    this.dateFrom = date;
    etAddExpenseDateFrom.setText(date);
  }

  @Override
  public void setDateTo(String dateTo) {
    this.dateTo = dateTo;
    etAddExpenseDateTo.setText(dateTo);
  }

  @Override
  public void returnResult(String extra, String date, int itemCounter) {
    Intent returnIntent = new Intent();
    returnIntent.putExtra(OPERATION_FINALIZED, extra);
    returnIntent.putExtra(EXTRA_DETAILS_EXPENSE_DATE, date);
    returnIntent.putExtra(EXTRA_DETAILS_ITEM_COUNTER, itemCounter);
    setResult(Activity.RESULT_OK, returnIntent);
    finish();
  }

  @Override
  public void setProjectItem(String project) {
    if (sProjectsAdapter.getCount() > 0) {
      if (!project.equals(null)) {
        sProjectPositionFixed = sProjectsAdapter.getPosition(project);
        sAddExpenseProjects.setSelection(sProjectPositionFixed);
      }
    }
  }

  @Override
  public void setWorkOrderItem(String workOrder) {
    if (sWorkOrdersAdapter.getCount() > 0) {
      if (!workOrder.equals(null)) {
        sWorkOrderPositionFixed = sWorkOrdersAdapter.getPosition(workOrder);
        if (sWorkOrderPositionFixed != -1) {
          sAddExpenseWorkOrders.setSelection(sWorkOrderPositionFixed);
        } else {
          showMessage(R.string.gms_message_work_order_error);
        }
      }
    }
  }

  @Override
  public void setCategoryItem(String category) {
    if (sCategoriesAdapter.getCount() > 0) {
      if (!category.equals(null)) {
        sCategoryPositionFixed = sCategoriesAdapter.getPosition(category);
        sAddExpenseCategories.setSelection(sCategoryPositionFixed);
      }
    }
  }

  @Override
  public void setCurrencyItem(String currency) {
    if (sCurrencyAdapter.getCount() > 0) {
      if (!currency.equals(null)) {
        sCurrencyPositionFixed = sCurrencyAdapter.getPosition(currency);
        sAddExpenseCurrencies.setSelection(sCurrencyPositionFixed);
      }
    }
  }

  @Override
  public void setMinimumValueForUnits(int minimumUnits) {
    etAddExpenseUnits.setFilters(new InputFilter[]{new MinValueInputFilter(minimumUnits)});
  }

  @Override
  public void setUnits(int units) {
    etAddExpenseUnits.setText(String.valueOf(units));
  }

  @Override
  public void showProgressBar() {
    pbAddExpense.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideProgressBar() {
    pbAddExpense.setVisibility(View.GONE);
  }

  @Override
  public void showNetworkError() {
    showError(R.string.error_gms_send_failure);
  }

  @Override
  public void loadImage(File fileToWrite) {
    Picasso.with(this)
        .load(fileToWrite)
        .memoryPolicy(MemoryPolicy.NO_CACHE)
        .networkPolicy(NetworkPolicy.NO_CACHE)
        .fit()
        .into(ivAddExpenseHeader);
    ivAddExpenseHeader.setVisibility(View.VISIBLE);
  }

  @Override
  public void checkReadPermission(final String expenseId) {
    Dexter.withActivity(this)
        .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
        .withListener(new PermissionListener() {
          @Override
          public void onPermissionGranted(PermissionGrantedResponse response) {
            presenter.loadImage(expenseId);
          }

          @Override
          public void onPermissionDenied(PermissionDeniedResponse response) {
            Snackbar
                .make(toolbar, getResources().getString(R.string.permissions_denied_error_show_image),
                    Snackbar.LENGTH_INDEFINITE)
                .show();
          }

          @Override
          public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
            token.continuePermissionRequest();
          }
        })
        .onSameThread()
        .check();
  }

  @Override
  public void setTitle(int titleId) {
    tvSectionTitle.setText(titleId);
  }

  @Override
  public void makeCommentFieldScrollable() {
    etAddExpenseComment.setOnTouchListener(new OnTouchListener() {
      public boolean onTouch(View view, MotionEvent event) {
        if (view.getId() == R.id.et_add_expense_comment) {
          view.getParent().requestDisallowInterceptTouchEvent(true);
          switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP:
              view.getParent().requestDisallowInterceptTouchEvent(false);
              break;
          }
        }
        return false;
      }
    });
  }

  @Override
  public void blockCategoryFields(Record expenseCategory) {
    etAddExpenseUnitPrice.setText(String.valueOf(expenseCategory.getPrice()));
    etAddExpenseUnitPrice.setFocusable(false);
    etAddExpenseUnitPrice.setClickable(false);
    sAddExpenseCurrencies.setSelection(sCurrencyAdapter.getPosition(expenseCategory.getCurrency()));
    sAddExpenseCurrencies.setEnabled(false);
  }

  @Override
  public void releaseCategoryFields() {
    etAddExpenseUnitPrice.setFocusableInTouchMode(true);
    etAddExpenseUnitPrice.setClickable(true);
    sAddExpenseCurrencies.setEnabled(true);
  }

  @Override
  public void dateSelected(String date, int calendarType) {
    presenter.setDataNotSaved();
    if (calendarType == TextUtils.Gms.CALENDAR_EXPENSE_FROM) {
      dateFrom = date;
      etAddExpenseDateFrom.setText(date);
      presenter.getProjects();
    } else if (calendarType == TextUtils.Gms.CALENDAR_EXPENSE_TO) {
      dateTo = date;
      etAddExpenseDateTo.setText(date);
    }
    presenter.compareDates(calendarType, dateFrom, dateTo);
  }

  @Override
  public void clearDate() {
    etAddExpenseDateTo.setText("");
  }
}
