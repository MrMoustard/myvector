package com.vectormobile.myvector.ui.menu.personal;

import com.vectormobile.myvector.ui.base.PresenterView;

/**
 * VectorDigital.
 *
 * @author -   AMarinaG
 * @since -   28/10/16
 */
public interface PersonalMenuView extends PresenterView {
}
