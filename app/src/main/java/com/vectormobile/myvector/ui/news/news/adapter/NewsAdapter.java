package com.vectormobile.myvector.ui.news.news.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.domain.repository.IntranetRepositoryImpl;
import com.vectormobile.myvector.model.news.NewsItem;
import com.vectormobile.myvector.ui.news.news.NewsView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ajruiz on 11/11/2016.
 */

public class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private static final int ITEM_VIEW_TYPE_HEADER = 0;
  private static final int ITEM_VIEW_TYPE_ITEM = 1;

  private OnNewsItemClickListener listener;
  private int page = 1;
  private NewsView view;
  private int groupPosition;

  private List<NewsItem> newsList;

  private Context context;

  public NewsAdapter(NewsView view, Context context, OnNewsItemClickListener onNewsItemClickListener) {
    this.context = context;
    this.view = view;
    this.listener = onNewsItemClickListener;
    newsList = new ArrayList<>();
  }

  public int getGroupPosition() {
    return groupPosition;
  }

  public void setGroupPosition(int pos) {
    groupPosition = pos;
  }

  public boolean isHeader(int position) {
    return position == 0;
  }

  @Override
  public int getItemViewType(int position) {
    if (groupPosition == ITEM_VIEW_TYPE_ITEM) {
      return isHeader(position) ? ITEM_VIEW_TYPE_HEADER : ITEM_VIEW_TYPE_ITEM;
    } else {
      return ITEM_VIEW_TYPE_ITEM;
    }
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    RecyclerView.ViewHolder viewHolder = null;
    if (viewType == ITEM_VIEW_TYPE_HEADER) {
      View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news_header, parent, false);
      viewHolder = new NewsAdapter.HeaderViewHolder(v);
    } else if (viewType == ITEM_VIEW_TYPE_ITEM) {
      View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news, parent, false);
      viewHolder = new NewsAdapter.ItemViewHolder(v);
    }
    return viewHolder;
  }


  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    switch (getItemViewType(position)) {
      case ITEM_VIEW_TYPE_HEADER:
        loadItemImageAndTitle(((NewsAdapter.HeaderViewHolder) holder).headerNewsItemImage,
            ((NewsAdapter.HeaderViewHolder) holder).headerNewsItemTitle, position);
        ((NewsAdapter.HeaderViewHolder) holder).bind(newsList.get(position), listener);
        break;

      case ITEM_VIEW_TYPE_ITEM:
        loadItemImageAndTitle(((NewsAdapter.ItemViewHolder) holder).newsItemImg,
            ((NewsAdapter.ItemViewHolder) holder).newsItemTitle, position);
        ((NewsAdapter.ItemViewHolder) holder).bind(newsList.get(position), listener);
        break;
    }
    if (position > ((page * IntranetRepositoryImpl.NEWS_PER_PAGE) - 2)) {
      view.loadNewPage(++page);
    }

  }


  private void loadItemImageAndTitle(ImageView image, TextView title, int pos) {
    NewsItem newsItem = this.newsList.get(pos);
    image.setColorFilter(ContextCompat.getColor(context, R.color.black_54));
    if (newsItem.getNewsItemTitle() != null) {
      title.setText(newsItem.getNewsItemTitle());
    }

    if (newsItem.getNewsItemImage() != null) {
      PicassoTrustAll.getInstance(context)
          .load(newsItem.getNewsItemImage())
          .resize(200, 200)
          .into(image);
    }
  }

  @Override
  public int getItemCount() {
    return newsList.size();
  }

  public void resetPage() {
    page = 1;
  }

  public void setItems(List<NewsItem> newsItem) {
    this.newsList.addAll(newsItem);
    notifyDataSetChanged();
  }

  public static class ItemViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.row_title)
    TextView newsItemTitle;
    @BindView(R.id.row_image)
    ImageView newsItemImg;
    @BindView(R.id.row_category_icon)
    ImageView newsItemCategoryImg;
    @BindView(R.id.row_category_icon_back)
    ImageView newsItemCategoryImgBack;

    private ItemViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }

    public void bind(final NewsItem newsItem, final OnNewsItemClickListener listener) {
      newsItemImg.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          listener.onNewsItemClick(newsItem);
        }
      });
    }
  }

  public static class HeaderViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvItemTitle)
    TextView headerNewsItemTitle;
    @BindView(R.id.ivItemImage)
    ImageView headerNewsItemImage;
    @BindView(R.id.ivItemCategoryImage)
    ImageView headerNewsItemCategoryImg;
    @BindView(R.id.ivItemCategoryBackground)
    ImageView headerNewsItemCategoryBack;

    private HeaderViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }

    public void bind(final NewsItem newsItem, final OnNewsItemClickListener listener) {
      headerNewsItemImage.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          listener.onNewsItemClick(newsItem);
        }
      });
    }
  }
}
