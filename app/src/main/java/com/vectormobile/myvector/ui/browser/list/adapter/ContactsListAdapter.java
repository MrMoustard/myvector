package com.vectormobile.myvector.ui.browser.list.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.domain.repository.IntranetRepositoryImpl;
import com.vectormobile.myvector.model.intranet.Contact;
import com.vectormobile.myvector.ui.browser.list.ContactsListView;
import com.vectormobile.myvector.ui.news.news.adapter.PicassoTrustAll;
import com.vectormobile.myvector.util.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ajruiz on 01/02/2017.
 */

public class ContactsListAdapter extends RecyclerView.Adapter<ContactsListAdapter.ContactsViewHolder> {
  public static int FIRST_PAGE = 1;

  private static List<Contact> contactList;
  private int page = 1;
  private ContactsListView contactsListView;
  private OnContactClickListener listener;
  private Context context;

  public ContactsListAdapter(ContactsListView contactsListView, OnContactClickListener listener, Context context) {
    this.contactsListView = contactsListView;
    this.listener = listener;
    this.context = context;
    contactList = new ArrayList<>();
  }

  @Override
  public ContactsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact_list_row, parent, false);
    return new ContactsViewHolder(view);
  }

  @Override
  public void onBindViewHolder(ContactsViewHolder holder, int position) {
    holder.tvContactName.setText(contactList.get(position).getName());
    holder.tvContactCity.setText(contactList.get(position).getCity());
    if (!contactList.get(position).getImageUrl().contains(TextUtils.Intranet.DEFAULT_AVATAR_CONTACTS_FILE_NAME)) {
      PicassoTrustAll.getInstance(context)
          .load(contactList.get(position).getImageUrl())
          .placeholder(ContextCompat.getDrawable(context, R.drawable.placeholder_profile_normal))
          .error(ContextCompat.getDrawable(context, R.drawable.placeholder_profile_normal))
          .into(holder.imgContactsImage);
    } else {
      holder.imgContactsImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.placeholder_profile_normal));
    }

    holder.bind(contactList.get(position), listener);
    if (position > ((page * IntranetRepositoryImpl.CONTACTS_PER_PAGE) - IntranetRepositoryImpl.REMAINING_CONTACTS)) {
      contactsListView.loadNewPage(++page);
    }
  }

  public void setItems(List<Contact> newsItem) {
    contactList.addAll(newsItem);
    notifyDataSetChanged();
  }

  public void clearItems() {
    contactList.clear();
    page = IntranetRepositoryImpl.FIRST_PAGE;
    notifyDataSetChanged();
  }

  public int getPage() {
    return page;
  }

  @Override
  public void onAttachedToRecyclerView(RecyclerView recyclerView) {
    super.onAttachedToRecyclerView(recyclerView);
  }

  @Override
  public int getItemCount() {
    return contactList.size();
  }

  static class ContactsViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.rl_contacts)
    RelativeLayout rlContacts;
    @BindView(R.id.tv_contact_name)
    TextView tvContactName;
    @BindView(R.id.img_contacts_image)
    ImageView imgContactsImage;
    @BindView(R.id.tv_contact_city)
    TextView tvContactCity;


    public ContactsViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }

    public void bind(final Contact contact, final OnContactClickListener listener) {
      rlContacts.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          listener.onContactClick(contact);
        }
      });
    }

  }


}
