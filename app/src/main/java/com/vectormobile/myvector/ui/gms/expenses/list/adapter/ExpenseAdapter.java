package com.vectormobile.myvector.ui.gms.expenses.list.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bignerdranch.android.multiselector.MultiSelector;
import com.bignerdranch.android.multiselector.SwappingHolder;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.model.gms.Expense;
import com.vectormobile.myvector.model.gms.ExpenseItemRecycler;
import com.vectormobile.myvector.ui.gms.expenses.list.ExpensesListActivity;
import com.vectormobile.myvector.util.date.DateUtils;
import com.vectormobile.myvector.util.text.TextUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by kurt on 05/12/16.
 */
public class ExpenseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private static final int ITEM_VIEW_TYPE_HEADER = 0;
  private static final int ITEM_VIEW_TYPE_ITEM = 1;
  private static final int FIRST_ITEM = 0;
  private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();
  private List<Expense> expenses, tempExpensesList;
  private List<ExpenseItemRecycler> itemsToShow;
  private Context context;
  private OnItemClickListener listener;
  private Date weekStarts;
  private Date weekEnds;
  private MultiSelector multiSelector;

  public ExpenseAdapter(Context context, OnItemClickListener listener) {
    this.expenses = new ArrayList<>();
    itemsToShow = new ArrayList<>();
    this.context = context;
    this.listener = listener;
    setUpSwipe();
  }

  private void setUpSwipe() {
//    viewBinderHelper.setOpenOnlyOne(true);
  }

  public void setMultiSelector(MultiSelector multiSelector) {
    this.multiSelector = multiSelector;
  }

  public List<ExpenseItemRecycler> getItemsToShow() {
    return itemsToShow;
  }

  private List<Expense> sortExpensesListByExpenseDate(List<Expense> expenses) {
    Collections.sort(expenses, new Comparator<Expense>() {
      public int compare(Expense expense1, Expense expense2) {
        return Long.valueOf(expense1.getExpenseDate()).compareTo(expense2.getExpenseDate());
      }
    });
    return expenses;
  }

  private void prepareArray(List<Expense> expenses) {
    tempExpensesList = new ArrayList<>(expenses);
    if (tempExpensesList.size() == 0)
      return;

    long weekStartLong = tempExpensesList.get(0).getWeekFrom();
    weekStarts = DateUtils.convertMillisecondsToDate(tempExpensesList.get(0).getWeekFrom());
    weekEnds = DateUtils.convertMillisecondsToDate(tempExpensesList.get(0).getWeekTo());

    insertHeader(itemsToShow);
    int i = 0;
    while (tempExpensesList.size() != 0 && i < tempExpensesList.size()) {
      if (tempExpensesList.get(i).getWeekFrom() == weekStartLong) {
        itemsToShow.add(new ExpenseItemRecycler(tempExpensesList.get(i), false,
            Arrays.asList(weekStarts, weekEnds)));
        tempExpensesList.remove(i);
        i = 0;
      } else {
        i++;
      }
    }
    prepareArray(tempExpensesList);
  }

  private void insertHeader(List<ExpenseItemRecycler> expenses) {
    expenses.add(new ExpenseItemRecycler(true, Arrays.asList(weekStarts, weekEnds)));
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    RecyclerView.ViewHolder holder = null;
    View view;
    switch (viewType) {
      case ITEM_VIEW_TYPE_HEADER:
        view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.item_expense_date_row, parent, false);
        holder = new HeaderViewHolder(view);
        break;
      case ITEM_VIEW_TYPE_ITEM:
        view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.view_swipe_row, parent, false);
        holder = new ItemViewHolder(view);
        break;
    }
    return holder;
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int pos) {
    switch (getItemViewType(pos)) {
      case ITEM_VIEW_TYPE_HEADER:
        ((HeaderViewHolder) holder).range.setText(
            String.format(
                Locale.getDefault(),
                context.getString(R.string.expense_header_list),
                getDateString(itemsToShow.get(pos).getWeekStarts()),
                getDateString(itemsToShow.get(pos).getWeekEnds())));
        break;
      case ITEM_VIEW_TYPE_ITEM:
        setItemImage((ItemViewHolder) holder, itemsToShow.get(pos).getState());
        ((ItemViewHolder) holder).date.setText(itemsToShow.get(pos).getDateString());
        ((ItemViewHolder) holder).title.setText(itemsToShow.get(pos).getTitle());
        ((ItemViewHolder) holder).amount.setText(itemsToShow.get(pos).getAmount() + " " +
            itemsToShow.get(pos).getCurrencyId());
        ((ItemViewHolder) holder).cameraIcon.setVisibility(
            itemsToShow.get(pos).isThereAnAttachedImage() ? View.VISIBLE : View.INVISIBLE);
        ((ItemViewHolder) holder).bind(itemsToShow.get(pos), listener);
        ((ItemViewHolder) holder).setSelectionModeBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.background_multiselected_item));

        if ((pos + 1 < getItemCount() && itemsToShow.get(pos + 1).isHeader()))
          ((ItemViewHolder) holder).changeSeparatorType();

        setSwipeItemGesture(holder, pos);
        break;
    }
  }

  private void setSwipeItemGesture(RecyclerView.ViewHolder holder, int pos) {
    // Save/restore the open/close state.
    // You need to provide a String id which uniquely defines the data object.
    viewBinderHelper.bind(((ItemViewHolder) holder).swipeLayout, itemsToShow.get(pos).getId());

    if (!itemsToShow.get(pos).getState().equals(Expense.EXPENSE_SAVED) &&
        !itemsToShow.get(pos).getState().equals(Expense.EXPENSE_REJECTED)) {
      ((ItemViewHolder) holder).swipeLayout.setLockDrag(true);
    }
    ((ItemViewHolder) holder).swipeLayout.setMinFlingVelocity(30);
  }

  @Override
  public int getItemViewType(int position) {
    return itemsToShow.get(position).isHeader() ? ITEM_VIEW_TYPE_HEADER : ITEM_VIEW_TYPE_ITEM;
  }

  private String getDateString(Date date) {
    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
    return formatter.format(date);
  }

  private void setItemImage(ItemViewHolder holder, String expenseState) {
    switch (expenseState) {
      case Expense.EXPENSE_SENT:
        holder.image.setImageResource(R.drawable.ic_send);
        holder.image.setBackgroundResource(R.drawable.circle_item_blue);
        break;
      case Expense.EXPENSE_APPROVED:
        holder.image.setImageResource(R.drawable.ic_accept);
        holder.image.setBackgroundResource(R.drawable.circle_item_green);
        break;
      case Expense.EXPENSE_REJECTED:
        holder.image.setImageResource(R.drawable.ic_reject);
        holder.image.setBackgroundResource(R.drawable.circle_item_red);
        break;
      case Expense.EXPENSE_SAVED:
        holder.image.setImageResource(R.drawable.ic_edit);
        holder.image.setBackgroundResource(R.drawable.circle_item_steel);
        break;
    }
  }

  @Override
  public int getItemCount() {
    return itemsToShow.size();
  }

  public void addItems(List<Expense> expenses) {
    this.expenses = expenses;
    prepareArray(sortExpensesListByExpenseDate(this.expenses));
    multiSelector.refreshAllHolders();
    this.notifyDataSetChanged();
  }

  public void reset() {
    expenses.clear();
    itemsToShow.clear();
    this.notifyDataSetChanged();
  }

  public void closeSwipedItems() {
    for (Expense item : expenses) {
      viewBinderHelper.closeLayout(item.getId());
    }
  }

  public void enableSwipe() {
    for (ExpenseItemRecycler eir : itemsToShow) {
      if (!eir.isHeader()) {
        if (eir.getState() != null && eir.getState().equals(Expense.EXPENSE_SAVED) ||
            eir.getState().equals(Expense.EXPENSE_REJECTED)) {
          viewBinderHelper.unlockSwipe(eir.getId());
        }
      }
    }
  }

  public void removeItems(List<Integer> selectedPositions) {
    List<Integer> reverseList = selectedPositions.subList(FIRST_ITEM, selectedPositions.size());
    Collections.reverse(reverseList);
    for (int position : reverseList) {
      if (position < itemsToShow.size()) {
        itemsToShow.remove(position);
        this.notifyItemRemoved(position);
      }
    }
  }

  public void setItemsSent(List<Integer> selectedPositions) {
    List<Integer> reverseList = selectedPositions.subList(FIRST_ITEM, selectedPositions.size());
    Collections.reverse(reverseList);
    for (int position : reverseList) {
      if (position < itemsToShow.size()) {
        itemsToShow.get(position).setState(TextUtils.Gms.IMPUTATION_STATUS_SENT);
      }
    }
    this.notifyDataSetChanged();
  }

  static class HeaderViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.date_range)
    TextView range;

    public HeaderViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }

  public class ItemViewHolder extends SwappingHolder {

    @BindView(R.id.swipe_layout)
    SwipeRevealLayout swipeLayout;
    @BindView(R.id.rl)
    RelativeLayout rlRowContainer;
    @BindView(R.id.item_image)
    ImageView image;
    @BindView(R.id.item_date)
    TextView date;
    @BindView(R.id.item_title)
    TextView title;
    @BindView(R.id.mini_camera_img)
    ImageView cameraIcon;
    @BindView(R.id.item_amount)
    TextView amount;
    @BindView(R.id.item_separator)
    View separator;
    @BindView(R.id.item_alt_separator)
    View separatorAlt;

    @BindView(R.id.sw_delete_button)
    TextView deleteBtn;
    @BindView(R.id.sw_send_button)
    TextView sendBtn;

    private Boolean swipeLayoutOpened = false;

    public ItemViewHolder(View itemView) {
      super(itemView, multiSelector);
      ButterKnife.bind(this, itemView);
      swipeLayout.setLongClickable(true);
    }

    public Boolean isSwipeLayoutOpened() {
      return swipeLayoutOpened;
    }

    public void setSwipeLayoutOpened(Boolean open) {
      this.swipeLayoutOpened = open;
    }

    public void bind(final Expense expense, final OnItemClickListener listener) {
      deleteBtn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          multiSelector.setSelected(ItemViewHolder.this, true);
          ((ExpensesListActivity) context).removeExpense(expense.getId());
          viewBinderHelper.closeLayout(expense.getId());
        }
      });

      sendBtn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          multiSelector.setSelected(ItemViewHolder.this, true);
          ((ExpensesListActivity) context).sendExpense(expense.getId());
          viewBinderHelper.closeLayout(expense.getId());
        }
      });

      rlRowContainer.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          if (!isSwipeLayoutOpened()) {
            if (!multiSelector.tapSelection(ItemViewHolder.this)) {
              listener.onItemClick(expense);
            } else {
              if (canSelectedItem(expense)) {
                multiSelector.setSelected(ItemViewHolder.this, isActivated());
                setActionModeTitle();
                if (multiSelector.getSelectedPositions().size() == 0) {
                  multiSelector.setSelectable(false);
                  ((ExpensesListActivity) context).finishMultiSelectionMode();
                }
              } else {
                multiSelector.setSelected(ItemViewHolder.this, false);
                ((ExpensesListActivity) context).showMessage(R.string.gms_message_imputation_not_selectable);
              }
            }
          }
        }
      });

      rlRowContainer.setOnLongClickListener(new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
          if (!multiSelector.isSelectable() && !isSwipeLayoutOpened()) {
            if (canSelectedItem(expense)) {
              for (ExpenseItemRecycler eir : itemsToShow) {
                viewBinderHelper.lockSwipe(eir.getId());
                viewBinderHelper.closeLayout(eir.getId());
              }
              ((ExpensesListActivity) context).initializeMultiSelectionMode();
              multiSelector.setSelectable(true);
              multiSelector.setSelected(ItemViewHolder.this, true);
              setActionModeTitle();
            } else {
              ((ExpensesListActivity) context).showMessage(R.string.gms_message_imputation_not_selectable);
            }
            return true;
          }
          return false;
        }
      });

      swipeLayout.setSwipeListener(new SwipeRevealLayout.SwipeListener() {
        @Override
        public void onClosed(SwipeRevealLayout view) {
          setSwipeLayoutOpened(false);
        }

        @Override
        public void onOpened(SwipeRevealLayout view) {
          setSwipeLayoutOpened(true);
        }

        @Override
        public void onSlide(SwipeRevealLayout view, float slideOffset) {

        }
      });


    }

    private boolean canSelectedItem(Expense expense) {
      return (TextUtils.Gms.IMPUTATION_STATUS_SAVED.equals(expense.getState()) ||
          TextUtils.Gms.IMPUTATION_STATUS_REJECTED.equals(expense.getState()));
    }

    private void setActionModeTitle() {
      int size = multiSelector.getSelectedPositions().size();
      String title = String.format(Locale.getDefault(), "%d %s", size,
          context.getResources().getQuantityString(R.plurals.gms_selected_item, size));
      TextView actionTitle = (TextView) ((ExpensesListActivity) context).findViewById(R.id.tv_action_mode_title);
      actionTitle.setText(title);
    }

    private void changeSeparatorType() {
      separatorAlt.setVisibility(View.VISIBLE);
      separator.setVisibility(View.GONE);
    }
  }
}
