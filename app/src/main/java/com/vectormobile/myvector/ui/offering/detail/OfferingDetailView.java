package com.vectormobile.myvector.ui.offering.detail;

import com.vectormobile.myvector.ui.base.PresenterView;

/**
 * Created by ajruiz on 10/11/2016.
 */

public interface OfferingDetailView extends PresenterView {

  void hideLoadingDialog();

}
