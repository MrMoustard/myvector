package com.vectormobile.myvector.ui.offering.news.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.squareup.picasso.Picasso;
import com.vectormobile.myvector.R;
import com.vectormobile.myvector.domain.repository.TrendsRepositoryImpl;
import com.vectormobile.myvector.model.Post;
import com.vectormobile.myvector.ui.offering.news.OfferingNewsView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ajruiz on 10/11/2016.
 */

public class OfferingNewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private static final int ITEM_VIEW_TYPE_HEADER = 0;
  private static final int ITEM_VIEW_TYPE_ITEM = 1;

  private OnPostClickListener listener;
  private int page = 1;
  private OfferingNewsView view;
  private int groupPosition;

  private List<Post> posts;

  private Context context;

  public OfferingNewsAdapter(OfferingNewsView view, Context context, OnPostClickListener onClickListener) {
    this.context = context;
    this.view = view;
    this.listener = onClickListener;
    posts = new ArrayList<>();
  }

  public void setPosts(List<Post> posts) {
    this.posts.clear();
    this.posts.addAll(posts);
    notifyDataSetChanged();
  }

  public int getGroupPosition() {
    return groupPosition;
  }

  public void setGroupPosition(int pos) {
    groupPosition = pos;
  }

  public boolean isHeader(int position) {
    return position == 0;
  }

  @Override
  public int getItemViewType(int position) {
    if (groupPosition == ITEM_VIEW_TYPE_ITEM) {
      return isHeader(position) ? ITEM_VIEW_TYPE_HEADER : ITEM_VIEW_TYPE_ITEM;
    } else {
      return ITEM_VIEW_TYPE_ITEM;
    }
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    RecyclerView.ViewHolder viewHolder = null;
    if (viewType == ITEM_VIEW_TYPE_HEADER) {
      View v = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.item_news_header, parent, false);
      viewHolder = new HeaderViewHolder(v);
    } else if (viewType == ITEM_VIEW_TYPE_ITEM) {
      View v = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.item_news, parent, false);
      viewHolder = new ItemViewHolder(v);
    }
    return viewHolder;
  }


  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    switch (getItemViewType(position)) {
      case ITEM_VIEW_TYPE_HEADER:
        loadItemImageTitle(((HeaderViewHolder) holder).headerPostImage,
            ((HeaderViewHolder) holder).headerPostTitle, position);
        ((HeaderViewHolder) holder).bind(posts.get(position), listener);
        break;

      case ITEM_VIEW_TYPE_ITEM:
        loadItemImageTitle(((ItemViewHolder) holder).postImg,
            ((ItemViewHolder) holder).postTitle, position);
        ((ItemViewHolder) holder).bind(posts.get(position), listener);
        break;
    }
    if (position > ((page * TrendsRepositoryImpl.POSTS_PER_PAGE) - 3)) {
      view.loadNewPage(++page);
    }
  }


  private void loadItemImageTitle(ImageView image, TextView title, int pos) {
    Post post = posts.get(pos);
    if (post.getBetterFeaturedImage() == null ||
        post.getBetterFeaturedImage().getMediaDetails().getSizes().getSingle360() == null ||
        post.getBetterFeaturedImage().getMediaDetails().getSizes().getSingle360().getSourceUrl() == null) {
      image.setImageDrawable(context.getResources().getDrawable(R.drawable.banner_grande));
    } else {
      Picasso.with(context)
          .load(post.getBetterFeaturedImage().getMediaDetails().getSizes().getSingle360().getSourceUrl())
          .placeholder(R.drawable.menu_bg)
          .error(R.drawable.menu_bg)
          .into(image);
    }
    image.setColorFilter(ContextCompat.getColor(context, R.color.black_54));
    if (post.getTitle() != null) {
      title.setText(Html.fromHtml(post.getTitle().getRendered()));
    }

  }

  @Override
  public int getItemCount() {
    return posts.size();
  }

  public void resetPage() {
    page = 1;
  }

  public class ItemViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.row_title)
    TextView postTitle;
    @BindView(R.id.row_image)
    ImageView postImg;
    @BindView(R.id.row_category_icon)
    ImageView postCategoryImg;
    @BindView(R.id.row_category_icon_back)
    ImageView postCategoryImgBack;

    private ItemViewHolder(View v) {
      super(v);
      ButterKnife.bind(this, v);
    }

    public void bind(final Post post, final OnPostClickListener listener) {
      postImg.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          listener.onPostClick(post);
        }
      });
    }
  }

  public class HeaderViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvItemTitle)
    TextView headerPostTitle;
    @BindView(R.id.ivItemImage)
    ImageView headerPostImage;
    @BindView(R.id.ivItemCategoryImage)
    ImageView headerPostCategoryImg;
    @BindView(R.id.ivItemCategoryBackground)
    ImageView headerPostCategoryBack;

    private HeaderViewHolder(View v) {
      super(v);
      ButterKnife.bind(this, v);
    }

    public void bind(final Post post, final OnPostClickListener listener) {
      headerPostImage.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          listener.onPostClick(post);
        }
      });
    }
  }
}
