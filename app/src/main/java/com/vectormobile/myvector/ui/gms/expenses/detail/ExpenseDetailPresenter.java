package com.vectormobile.myvector.ui.gms.expenses.detail;

import android.content.Context;
import android.graphics.Bitmap;
import com.vectormobile.myvector.model.gms.Expense;
import com.vectormobile.myvector.storage.ImageManager;
import com.vectormobile.myvector.ui.base.BasePresenter;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

/**
 * Created by kurt on 12/12/16.
 */
public class ExpenseDetailPresenter extends BasePresenter<ExpenseDetailView> {

  private ImageManager imageManager;

  @Inject
  public ExpenseDetailPresenter(ExpenseDetailActivity view) {
    super(view);
    this.imageManager = ImageManager.getInstance();
  }

  public void checkImageHeader(Expense expense) {
    if (imageManager.haveImage(expense.getId())) {
      view.checkReadPermission(expense.getId());
    } else {
      view.hideHeaderImage();
    }
  }

  public void loadImage(String expenseId) {
    view.loadImage(imageManager.getFileToWrite(expenseId));

  }

  public File getImageToShare(Context context, Bitmap bitmap) {
    try {
      return imageManager.convertBitmapToFile(context, bitmap);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

}
