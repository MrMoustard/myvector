package com.vectormobile.myvector.ui.offering.news;

import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.domain.interactor.offering.OfferingNewsInteractor;
import com.vectormobile.myvector.domain.repository.TrendsRepositoryImpl;
import com.vectormobile.myvector.model.Post;
import com.vectormobile.myvector.ui.base.BasePresenter;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by ajruiz on 10/11/2016.
 */

public class OfferingNewsPresenter extends BasePresenter<OfferingNewsView> {

  private OfferingNewsInteractor interactor;

  @Inject
  public OfferingNewsPresenter(OfferingNewsView fragment, OfferingNewsInteractor interactor) {
    super(fragment);
    this.interactor = interactor;
  }

  public void getOfferingNews(int id, int page) {
    interactor.execute(id, page, TrendsRepositoryImpl.POSTS_PER_PAGE, new OnListRetrievedListener<Post>() {
      @Override
      public void onSuccess(List<Post> list) {
        if (getView() != null) {
          getView().onListPostRetrieved(list);
        }
      }

      @Override
      public void onError(int errorId) {
        if (getView() != null) {
          getView().showError(errorId);
          getView().hideLoadingDialog();
        }
      }
    });
  }
}
