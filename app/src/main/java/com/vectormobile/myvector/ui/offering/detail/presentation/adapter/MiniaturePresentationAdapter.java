package com.vectormobile.myvector.ui.offering.detail.presentation.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.vectormobile.myvector.R;

/**
 * Created by ajruiz on 22/11/2016.
 */

public class MiniaturePresentationAdapter extends BaseAdapter {

  private Context context;
  private String[] urls;
  private View.OnClickListener onClickListener;

  public MiniaturePresentationAdapter(Context context, String[] urls, View.OnClickListener onClickListener) {
    this.context = context;
    this.urls = urls;
    this.onClickListener = onClickListener;
  }

  public int getCount() {
    return urls.length;
  }

  public Object getItem(int position) {
    return position;
  }

  public long getItemId(int position) {
    return position;
  }

  public View getView(int position, View convertView, ViewGroup parent) {
    if (convertView == null) {
      LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      convertView = inflater.inflate(R.layout.gallery_miniatures, parent, false);
      ImageView imageView = (ImageView) convertView.findViewById(R.id.iv_gallery_miniature);
      ((TextView) convertView.findViewById(R.id.tv_gallery_number)).setText((position + 1) + "");

      urls[position] = urls[position].replaceAll(".jpg", "-130x75.jpg");

      Picasso.with(context)
          .load(urls[position])
          .placeholder(ContextCompat.getDrawable(context, R.drawable.ic_logo_vector))
          .into(imageView);
      imageView.setAdjustViewBounds(true);
      imageView.setTag(1);

      convertView.setOnClickListener(onClickListener);
      convertView.setTag(position);
    }
    return convertView;
  }
}