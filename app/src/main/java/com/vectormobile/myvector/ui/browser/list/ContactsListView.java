package com.vectormobile.myvector.ui.browser.list;

import com.vectormobile.myvector.model.intranet.Contact;
import com.vectormobile.myvector.ui.base.PresenterView;

import java.util.List;

/**
 * Created by ajruiz on 01/02/2017.
 */

public interface ContactsListView extends PresenterView {

  void loadNewPage(int page);

  void showProgressBar();

  void hideProgressBar();

  void fillContactsList(List<Contact> contact);

  void search(String search);

}
