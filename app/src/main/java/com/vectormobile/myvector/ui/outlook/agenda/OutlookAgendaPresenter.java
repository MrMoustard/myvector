package com.vectormobile.myvector.ui.outlook.agenda;

import com.vectormobile.myvector.ui.base.BasePresenter;

import javax.inject.Inject;

/**
 * Created by amarina on 13/03/2017.
 */
public class OutlookAgendaPresenter extends BasePresenter<OutlookAgendaView> {
  @Inject
  public OutlookAgendaPresenter(OutlookAgendaActivity activity) {
    super(activity);
  }
}
