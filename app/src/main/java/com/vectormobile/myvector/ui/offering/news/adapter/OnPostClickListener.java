package com.vectormobile.myvector.ui.offering.news.adapter;

import com.vectormobile.myvector.model.Post;

/**
 * Created by ajruiz on 11/11/2016.
 */

public interface OnPostClickListener {
  void onPostClick(Post post);
}
