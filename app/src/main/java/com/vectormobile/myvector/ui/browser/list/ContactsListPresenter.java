package com.vectormobile.myvector.ui.browser.list;

import com.vectormobile.myvector.domain.interactor.intranet.ContactsListInteractor;
import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.model.intranet.Contact;
import com.vectormobile.myvector.ui.base.BasePresenter;
import com.vectormobile.myvector.util.text.TextUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by ajruiz on 01/02/2017.
 */

public class ContactsListPresenter extends BasePresenter<ContactsListView> {

  private ContactsListInteractor contactsListInteractor;
  private List<Contact> contactList;

  @Inject
  public ContactsListPresenter(ContactsListActivity activity, ContactsListInteractor contactsListInteractor) {
    super(activity);
    this.contactsListInteractor = contactsListInteractor;
    contactList = new ArrayList<>();
  }

  public void getContactsList(int page, String search) {
    contactsListInteractor.execute(page, search, new OnItemRetrievedListener() {
      @Override
      public void onSuccess(Object item) {
        if (getView() != null) {
          getView().fillContactsList(parseContactsListContent(item.toString()));
          getView().hideProgressBar();
        }
      }

      @Override
      public void onError(int errorId) {
        if (getView() != null) {
          getView().showError(errorId);
          getView().hideProgressBar();
        }
      }
    });
  }

  public String formatSearchQuery(String name) {
    String searchQuery = name.trim().replaceAll(TextUtils.Intranet.CONTACTS_MULTIPLE_WHITE_SPACE,
        TextUtils.Intranet.CONTACTS_WHITE_SPACE);
    searchQuery = TextUtils.Intranet.SEARCH_FORMAT + searchQuery.replaceAll(
        TextUtils.Intranet.CONTACTS_WHITE_SPACE, TextUtils.Intranet.SEARCH_FORMAT)
        + TextUtils.Intranet.SEARCH_FORMAT;
    return searchQuery;
  }

  private List<Contact> parseContactsListContent(String content) {
    contactList.clear();
    Document doc = Jsoup.parse(content);
    doc.outputSettings(new Document.OutputSettings().prettyPrint(false));
    doc.select("br").append(TextUtils.Intranet.ELEMENT_SEPARATOR);
    Elements elements = doc.select("div[class=box-employee]");
    doc.outputSettings().charset("UTF-8");
    for (Element scrapEle : elements) {
      contactList.add(new Contact(scrapEle));
    }
    return contactList;
  }

}
