package com.vectormobile.myvector.ui.offering.detail.presentation;

import com.vectormobile.myvector.di.ActivityScope;
import com.vectormobile.myvector.ui.base.BasePresenter;

import javax.inject.Inject;

/**
 * Created by ajruiz on 23/11/2016.
 */
@ActivityScope
public class PresentationPresenter extends BasePresenter<PresentationView> {

  @Inject
  public PresentationPresenter(PresentationActivity activity) {
    super(activity);
  }

}
