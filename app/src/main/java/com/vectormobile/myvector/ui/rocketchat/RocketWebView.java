package com.vectormobile.myvector.ui.rocketchat;

import android.content.Context;
import android.net.http.SslError;
import android.os.Build;
import android.util.AttributeSet;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import javax.annotation.Nonnull;

/**
 * Created by amarina on 26/01/2017.
 */
public class RocketWebView extends WebView {

  private final static String ROCKETCHAT_URL = "http://vectorshare.thinknowa.com";

  private StringBuilder javascriptFunction;
  private RocketchatListener rocketchatListener;
  private boolean firstLoadingDone = false;

  public RocketWebView(Context context) {
    super(context);
    defaultSettings();
    loadUrl(ROCKETCHAT_URL);
  }

  private void defaultSettings() {
    WebSettings settings = getSettings();
    settings.setJavaScriptEnabled(true);
    settings.setDomStorageEnabled(true);
    setWebViewClient(new WebViewClient() {

      @Override
      public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        if (url.contains("home") && !url.contains("name=") && javascriptFunction != null) {
          if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            evaluateJavascript(getLoginJavascript(), null);
          } else {
            loadUrl(getLoginJavascript());
          }
        }
        if (rocketchatListener != null) {
          rocketchatListener.onPageFinished();
        } else {
          firstLoadingDone = true;
        }
      }

      private String getLoginJavascript() {
        return javascriptFunction.toString();
      }

    });
  }

  public void loginOnRocketchat(@Nonnull String username, @Nonnull String password) {
    javascriptFunction = new StringBuilder()
        .append("javascript:{")
        .append("document.getElementsByName('emailOrUsername')[0].value='" + username + "@vectoritcgroup.com';")
        .append("document.getElementsByName('pass')[0].value='" + password + "';")
        .append("document.getElementsByClassName('button primary login')[0].click();")
        .append("}");
    loadUrl(ROCKETCHAT_URL);
  }

  public void setRocketchatListener(RocketchatListener rocketchatListener) {
    this.rocketchatListener = rocketchatListener;
    if (firstLoadingDone && rocketchatListener != null) {
      rocketchatListener.onPageFinished();
    }
  }
}