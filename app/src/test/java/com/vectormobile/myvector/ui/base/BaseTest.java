package com.vectormobile.myvector.ui.base;

import org.junit.Before;
import org.mockito.MockitoAnnotations;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.shadows.ShadowApplication;

/**
 * Created by kurt on 13/01/17.
 */

public abstract class BaseTest {
  public static final String USERNAME_OK = "ok";
  public static final String PASSWORD_OK = "ok";

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    if (RuntimeEnvironment.application != null) {
      ShadowApplication shadowApplication = ShadowApplication.getInstance();
      shadowApplication.grantPermissions("android.permission.INTERNET");
    }
  }
}
