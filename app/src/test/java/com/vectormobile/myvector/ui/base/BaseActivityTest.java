package com.vectormobile.myvector.ui.base;

import android.os.Bundle;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

/**
 * Created by kurt on 13/01/17.
 */

public abstract class BaseActivityTest<A extends BaseActivity> extends BaseTest {

  private A activity;
  @Mock Bundle bundle;

  @Override
  @Before
  public void setUp() throws Exception {
    super.setUp();
    activity = createActivity();
  }

  @Test
  public void testOnCreate_callDestroyView() throws Exception {}

  protected abstract A createActivity();

}
