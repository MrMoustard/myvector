package com.vectormobile.myvector.util.date;

import static junit.framework.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by kurt on 16/03/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class DateUtilsTest {

  @Before
  public void setUp() throws Exception {
  }

  @Test
  public void dateFromShouldBeSecondFortnightOfPreviousMonth() {
    // Given
    Calendar mockCalendar = Calendar.getInstance();
    mockCalendar.set(Calendar.DAY_OF_MONTH, 1);

    Calendar expected = Calendar.getInstance();
    expected.set(Calendar.DAY_OF_MONTH, 15);
    expected.set(Calendar.MONTH, expected.get(Calendar.MONTH) - 1);

    // Then
    String startDate = DateUtils.getExpensePdfDefaultStartDate(mockCalendar);
    Calendar actual = DateUtils.convertStringToCalendar(startDate, DateUtils.CALENDAR_DATE_FORMAT);

    // When
    String dayErrorMsg = String.format(
        Locale.getDefault(),
        "Expected day %d but was %d",
        expected.get(Calendar.DAY_OF_MONTH),
        actual.get(Calendar.DAY_OF_MONTH));
    assertEquals(dayErrorMsg, expected.get(Calendar.DAY_OF_MONTH), actual.get(Calendar.DAY_OF_MONTH));

    String monthErrorMsg = String.format(
        Locale.getDefault(),
        "Expected month %d but was %d",
        expected.get(Calendar.MONTH),
        actual.get(Calendar.MONTH));
    assertEquals(monthErrorMsg, expected.get(Calendar.MONTH), actual.get(Calendar.MONTH));
  }

  @Test
  public void dateFromShouldBeFirstDayOfCurrentMonth() {
    // Given
    Calendar mockCalendar = Calendar.getInstance();
    mockCalendar.set(Calendar.DAY_OF_MONTH, 2);

    Calendar expected = Calendar.getInstance();
    expected.set(Calendar.DAY_OF_MONTH, 1);

    // Then
    String startDate = DateUtils.getExpensePdfDefaultStartDate(mockCalendar);
    Calendar actual = DateUtils.convertStringToCalendar(startDate, DateUtils.CALENDAR_DATE_FORMAT);

    // When
    String dayErrorMsg = String.format(
        Locale.getDefault(),
        "Expected day %d but was %d",
        expected.get(Calendar.DAY_OF_MONTH),
        actual.get(Calendar.DAY_OF_MONTH));
    assertEquals(dayErrorMsg, expected.get(Calendar.DAY_OF_MONTH), actual.get(Calendar.DAY_OF_MONTH));
  }

  @Test
  public void dateFromShouldBeFirstDayOfCurrentMonthToo() {
    // Given
    Calendar mockCalendar = Calendar.getInstance();
    mockCalendar.set(Calendar.DAY_OF_MONTH, 15);

    Calendar expected = Calendar.getInstance();
    expected.set(Calendar.DAY_OF_MONTH, 1);

    // Then
    String startDate = DateUtils.getExpensePdfDefaultStartDate(mockCalendar);
    Calendar actual = DateUtils.convertStringToCalendar(startDate, DateUtils.CALENDAR_DATE_FORMAT);

    // When
    String dayErrorMsg = String.format(
        Locale.getDefault(),
        "Expected day %d but was %d",
        expected.get(Calendar.DAY_OF_MONTH),
        actual.get(Calendar.DAY_OF_MONTH));
    assertEquals(dayErrorMsg, expected.get(Calendar.DAY_OF_MONTH), actual.get(Calendar.DAY_OF_MONTH));
  }

  @Test
  public void dateFromShouldBeSecondFortnightOfCurrentMonth() {
    // Given
    Calendar mockCalendar = Calendar.getInstance();
    mockCalendar.set(Calendar.DAY_OF_MONTH, 16);

    Calendar expected = Calendar.getInstance();
    expected.set(Calendar.DAY_OF_MONTH, 15);

    // Then
    String startDate = DateUtils.getExpensePdfDefaultStartDate(mockCalendar);
    Calendar actual = DateUtils.convertStringToCalendar(startDate, DateUtils.CALENDAR_DATE_FORMAT);

    // When
    String dayErrorMsg = String.format(
        Locale.getDefault(),
        "Expected day %d but was %d",
        expected.get(Calendar.DAY_OF_MONTH),
        actual.get(Calendar.DAY_OF_MONTH));
    assertEquals(dayErrorMsg, expected.get(Calendar.DAY_OF_MONTH), actual.get(Calendar.DAY_OF_MONTH));

    String monthErrorMsg = String.format(
        Locale.getDefault(),
        "Expected month %d but was %d",
        expected.get(Calendar.MONTH),
        actual.get(Calendar.MONTH));
    assertEquals(monthErrorMsg, expected.get(Calendar.MONTH), actual.get(Calendar.MONTH));
  }
}