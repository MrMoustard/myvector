package com.vectormobile.myvector.domain.interactor.gms;

import static org.mockito.Mockito.verify;

import com.vectormobile.myvector.domain.entity.gms.request.ExpenseRequest;
import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.domain.repository.GmsRepository;
import com.vectormobile.myvector.ui.base.BaseTest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

/**
 * Created by amarina on 16/01/2017.
 */
public class SaveExpenseInteractorImplTest extends BaseTest {
  @Mock
  private GmsRepository gmsRepository;
  @Mock
  private ExpenseRequest expenseRequest;
  @Mock
  private OnListRetrievedListener listRetrievedListener;

  private SaveExpenseInteractor interactor;

  @Before
  public void setUp() throws Exception {
    super.setUp();
    interactor = new SaveExpenseInteractorImpl(gmsRepository);
  }

  @Test
  public void execute() throws Exception {
    String action = "";
    String method = "";
    interactor.execute(expenseRequest, action, method ,listRetrievedListener);
    verify(gmsRepository).saveExpense(expenseRequest, action, method, listRetrievedListener);
  }

}