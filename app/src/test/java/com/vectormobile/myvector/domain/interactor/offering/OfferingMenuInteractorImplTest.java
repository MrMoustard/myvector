package com.vectormobile.myvector.domain.interactor.offering;

import static org.mockito.Mockito.verify;

import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.domain.repository.TrendsRepository;
import com.vectormobile.myvector.ui.base.BaseTest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

/**
 * Created by amarina on 16/01/2017.
 */
public class OfferingMenuInteractorImplTest extends BaseTest {
  @Mock
  private TrendsRepository repository;
  @Mock
  private OnItemRetrievedListener listener;

  private OfferingMenuInteractor interactor;

  @Before
  @Override
  public void setUp() throws Exception {
    super.setUp();
    interactor = new OfferingMenuInteractorImpl(repository);
  }

  @Test
  public void execute() throws Exception {
    interactor.execute(listener);
    verify(repository).buildOfferingMenu(listener);
  }

}