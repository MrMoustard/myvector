package com.vectormobile.myvector.domain.interactor.login;

import static org.mockito.Mockito.verify;

import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.domain.repository.GmsRepository;
import com.vectormobile.myvector.ui.base.BaseTest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

/**
 * Created by amarina on 16/01/2017.
 */
public class GmsLoginInteractorImplTest extends BaseTest {
  @Mock
  private GmsRepository gmsRepository;
  @Mock
  private OnItemRetrievedListener listener;

  private GmsLoginInteractor interactor;

  @Override
  @Before
  public void setUp() throws Exception {
    super.setUp();
    interactor = new GmsLoginInteractorImpl(gmsRepository);
  }

  @Test
  public void execute_callValidateLogin() throws Exception {
    interactor.execute(BaseTest.USERNAME_OK, BaseTest.PASSWORD_OK, listener);
    verify(gmsRepository).validateLogin(BaseTest.USERNAME_OK, BaseTest.PASSWORD_OK, listener);
  }

}