package com.vectormobile.myvector.domain.interactor.login;

import static org.mockito.Mockito.verify;

import com.vectormobile.myvector.domain.interactor.listener.OnItemRetrievedListener;
import com.vectormobile.myvector.domain.repository.IntranetRepository;
import com.vectormobile.myvector.ui.base.BaseTest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

/**
 * Created by amarina on 16/01/2017.
 */
public class IntranetLoginInteractorImplTest extends BaseTest {
  @Mock
  private IntranetRepository intranetRepository;
  @Mock
  private OnItemRetrievedListener listener;

  private IntranetLoginInteractor interactor;

  @Override
  @Before
  public void setUp() throws Exception {
    super.setUp();
    interactor = new IntranetLoginInteractorImpl(intranetRepository);
  }

  @Test
  public void execute() throws Exception {
    interactor.execute(BaseTest.USERNAME_OK, BaseTest.PASSWORD_OK, listener);
    verify(intranetRepository).validateLogin(USERNAME_OK, PASSWORD_OK, listener);
  }

}