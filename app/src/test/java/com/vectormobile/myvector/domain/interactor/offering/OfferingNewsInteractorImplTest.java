package com.vectormobile.myvector.domain.interactor.offering;

import static org.mockito.Mockito.verify;

import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.domain.repository.TrendsRepository;
import com.vectormobile.myvector.ui.base.BaseTest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

/**
 * Created by amarina on 16/01/2017.
 */
public class OfferingNewsInteractorImplTest extends BaseTest {
  @Mock
  private TrendsRepository repository;
  @Mock
  private OnListRetrievedListener listener;

  private OfferingNewsInteractor interactor;

  @Before
  public void setUp() throws Exception {
    super.setUp();
    interactor = new OfferingNewsInteractorImpl(repository);
  }

  @Test
  public void execute() throws Exception {
    int id = 1;
    int page = 1;
    int postsPerPage = 1;

    interactor.execute(id, page, postsPerPage, listener);
    verify(repository).getOfferingNews(id, page, postsPerPage, listener);
  }

}