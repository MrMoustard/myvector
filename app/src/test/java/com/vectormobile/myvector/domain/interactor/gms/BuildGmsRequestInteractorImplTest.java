package com.vectormobile.myvector.domain.interactor.gms;

import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.domain.repository.GmsRepository;
import com.vectormobile.myvector.ui.base.BaseTest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

/**
 * Created by amarina on 16/01/2017.
 */
public class BuildGmsRequestInteractorImplTest extends BaseTest {
  @Mock
  private GmsRepository gmsRepository;
  @Mock
  private OnListRetrievedListener listRetrievedListener;

  private BuildGmsBodyRequestInteractor interactor;

  @Before
  public void setUp() throws Exception {
    super.setUp();
    interactor = new BuildGmsBodyRequestInteractorImpl(gmsRepository);
  }

  @Test
  public void execute() throws Exception {
//    interactor.execute(TextUtils.Gms.ACTION_IMPUTATION_HOUR_CONTROLLER,
//        TextUtils.Gms.IMPUTABLE_PROJECTS, listRetrievedListener);
//    verify(gmsRepository).getImputableProjects(TextUtils.Gms.ACTION_IMPUTATION_HOUR_CONTROLLER,
//        TextUtils.Gms.IMPUTABLE_PROJECTS, listRetrievedListener);
  }

}