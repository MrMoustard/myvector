package com.vectormobile.myvector.domain.interactor.gms;

import static org.mockito.Mockito.verify;

import com.vectormobile.myvector.domain.interactor.listener.OnListRetrievedListener;
import com.vectormobile.myvector.domain.repository.GmsRepository;
import com.vectormobile.myvector.ui.base.BaseTest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

/**
 * Created by amarina on 16/01/2017.
 */
public class GetWorkOrderInteractorImplTest extends BaseTest {
  @Mock
  private GmsRepository gmsRepository;
  @Mock
  private OnListRetrievedListener listRetrievedListener;

  private GetWorkOrderInteractor interactor;

  @Before
  public void setUp() throws Exception {
    super.setUp();
    interactor = new GetWorkOrderInteractorImpl(gmsRepository);
  }

  @Test
  public void execute() throws Exception {
    interactor.execute("aaa", listRetrievedListener);
    verify(gmsRepository).getWorkOrders("aaa", listRetrievedListener);
  }

}