**Vector digital**
==============

Aplicación interna para Vector ITC Group
----------------------------------------
Intenta facilitar las tareas habituales que tenemos los empleados de *Vector* tienen que hacer y dando la posibilidad de realizarlas de una manera fácil desde el teléfono.

De obligado cumplimiento descargarse y aplicar este formateo de código -> [Android Studio format style](https://gitlab.vectoritcgroup.com/android-internalprojects/documentation/uploads/d510b4313b6075974a9dc10252921c3c/google-style-xml-amg.xml)

---------

[Link to the general doc](https://gitlab.vectoritcgroup.com/docs/internal-projects/blob/master/VectorDigital.md)
----------