#Vector Digital

App tope chula para vectores digitales.

##Servicios GMS

Detalle de las llamadas/respuestas que hemos ido probando.

###Login

######Llamada:

Es una llamada *rest* de tipo **post**,

	https://gms.vectoritcgroup.com/ngms-web/j_spring_security_check?simple=false
	
Dentro del *body* se le pasan los siguientes valores,

	{
		"j_username" : "mushgo",
		"j_password" : "Vector2016"
	}
	
Por último, añadimos un header indicando el **Content-Type** de tipo *application/json*.

######Respuesta:
La  respuesta nos va a llegar en forma de *cookie*, por lo que tenemos que tener habilitado, en caso de estar usando postman, o tener implementado en el código un interceptor de cookies.

Recibiremos dos cookies, una nos proporciona el **JSESSIONID** y, la otra, el **hash**, aunque el hash creo que siempre será el mismo.

![login_cookie](https://gitlab.vectoritcgroup.com/android-internalprojects/VectorDigital/blob/develop/docs/img/login_cookie.png)


###Logout

######Llamada:

https://gms.vectoritcgroup.com/ngms-web/j_spring_security_logout ¿?